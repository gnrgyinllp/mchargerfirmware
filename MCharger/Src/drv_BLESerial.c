#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal_uart.h"
#include <stdbool.h>
#include <string.h>
#include "drv_BLESerial.h"

#define REV_TIMEOUT  50
#define READ_TIMEOUT 50
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart1;
extern char FirstEE[2];
static char cmdSetMachineCommandSend[] = "set sy c m machine\r\n";
static char cmdSetMachineCommandResp[] = "R000009\r\nSuccess\r\n";
static char cmdOffResponseHeaderSend[] = "set sy c h 0\r\n";
static char cmdOffResponseHeaderResp[] = "Success\r\n";
static char cmdSetDeviceNameSend[] = "set sy d n GNRGYBT\r\n";
static char cmdSetDeviceNameResp[] = "Success\r\n";
static char cmdSetLedSend[] = "gfu 10 status_led\r\n";
static char cmdSetLedResp[] = "Success\r\n";
static char cmdSetLedRetrySend[] = "gfu 10 status_led\r\n";
static char cmdSetLedRetryResp[] = "Command failed\r\n";
static char cmdSetLedOperationSend[] = "set sy i s 007f0505\r\n";
static char cmdSetLedOperationResp[] = "Success\r\n";
static char cmdSetAdversingModeSend[] = "set bl v h d 0\r\n";
static char cmdSetAdversingModeResp[] = "Success\r\n";
static char cmdSetIncreasePowerSend[] = "set bl t a 3\r\n";
static char cmdSetIncreasePowerResp[] = "Success\r\n";
static char cmdSetStreamSend[] = "set bu i stream\r\n";
static char cmdSetStreamResp[] = "Success\r\n";
static char cmdSaveSend[] = "save\r\n";
static char cmdSaveResp[] = "Success\r\n";
static char cmdGetCurrentModeSend[] = "get bu i\r\n";
static char cmdGetCurrentModeResp[] = "stream\r\n";
#if DEVELOPMENT
static char cmdGetGPIOUsage[] = "get gp u\r\n";
static char cmdGetTransmitPower[] = "get bl t a\r\n";
static char cmdGetDeviceName[] = "get sy d n\r\n";
static char cmdGetStatusLedConfig[] = "get sy i s\r\n";
static char cmdGetAdvertisingDuration[] = "get bl v h d\r\n";
static char msgReportGetBTFail[] = "ERROR: Cannot get bluetooth configuration\r\n";
#endif

static char msgReportStatusFail[] = "ERROR: Cannot change BT name\r\n";
static char msgReportStatusPass[] = "MSG: Changed BT name successful\r\n";
static char msgReportModeFail[] = "ERROR: Cannot set stream mode\r\n";
static char msgReportModePass[] = "MSG: Switch to stream mode successful\r\n";

static bool drv_BLESendRevCommand(char *pSendCmdBuf, char* pRevCmdBuf, int ulTimeout);
#if DEVELOPMENT
static bool drv_BLEGetRevCommand(char *pSendCmdBuf, char* pRevCmdBuf, int line, const int Timeout);
#endif
static bool drv_BLESendCommand(void);
static bool drv_BLESetDeviceName(void);
static bool drv_BLESetInitOperation(void);


static volatile int ulTimeout = 0;
BLEConfig_t g_bleconfig;

void drv_BLEGetConfig(void)
{
#if DEVELOPMENT
  char buf[128];
  bool LenStatus = false;
  LenStatus = drv_BLESendCommand();
  if (false == LenStatus) goto final;
  LenStatus = drv_BLEGetRevCommand(cmdGetDeviceName, g_bleconfig.strDeviceName, 1, REV_TIMEOUT);
  if (false == LenStatus) goto final;
  LenStatus = drv_BLEGetRevCommand(cmdGetTransmitPower, g_bleconfig.strTransmitPower, 1, REV_TIMEOUT);
  if (false == LenStatus) goto final;
  LenStatus = drv_BLEGetRevCommand(cmdGetStatusLedConfig, g_bleconfig.strStatusLedConfig, 1, REV_TIMEOUT);
  if (false == LenStatus) goto final;
  LenStatus = drv_BLEGetRevCommand(cmdGetAdvertisingDuration, g_bleconfig.strAdvertisingDuration, 1, REV_TIMEOUT);
  if (false == LenStatus) goto final;

final:
  if (true == LenStatus)
  {
    sprintf(buf, "Bluetooth Module Initial Configuration Report\r\n");
    HAL_UART_Transmit(&huart1, (uint8_t*)buf, strlen(buf), 1000);
    sprintf(buf, "\t 1.Set BT module default name %s\r\n",g_bleconfig.strDeviceName);
    HAL_UART_Transmit(&huart1, (uint8_t*)buf, strlen(buf), 1000);
    sprintf(buf, "\t 2.Set system indicator status LED %s\r\n",g_bleconfig.strStatusLedConfig);
    HAL_UART_Transmit(&huart1, (uint8_t*)buf, strlen(buf), 1000);
    sprintf(buf, "\t 3.Set bluetooth with advertising duration %s second(s)\r\n",g_bleconfig.strAdvertisingDuration);
    HAL_UART_Transmit(&huart1, (uint8_t*)buf, strlen(buf), 1000);
    sprintf(buf, "\t 4.Set transmit power advertising with %sdBm\r\n",g_bleconfig.strTransmitPower);
    HAL_UART_Transmit(&huart1, (uint8_t*)buf, strlen(buf), 1000);
  }
  else
  {
    HAL_UART_Transmit(&huart1, (uint8_t*)msgReportGetBTFail, strlen(msgReportGetBTFail), 1000);
  }
#endif
}

void drv_BLESetStreamMode(void)
{
  bool LenStatus = true;

  if (false == drv_BLESendRevCommand(cmdSetStreamSend, cmdSetStreamResp, REV_TIMEOUT)) LenStatus = false;
  if (false == drv_BLESendRevCommand(cmdGetCurrentModeSend, cmdGetCurrentModeResp, REV_TIMEOUT)) LenStatus = false;
  if (true == LenStatus)
  {
    HAL_UART_Transmit(&huart1, (uint8_t*)msgReportModePass, strlen(msgReportModePass), 1000);
  }
  else
  {
    HAL_UART_Transmit(&huart1, (uint8_t*)msgReportModeFail, strlen(msgReportModeFail), 1000);
  }
}

void drv_BLECheckFirstBoot(void)
{
  bool LenReturn = true;

  //   This is first time of boot up device. Let update name of device
  if (( FirstEE[1] != '1' ) || ( FirstEE[0] != '0'))
  {
    if (false == drv_BLESendCommand())
    {
      LenReturn = false;
    }
    if (false == drv_BLESetDeviceName())
    {
      LenReturn = false;
    }
    if (false == drv_BLESetInitOperation())
    {
      LenReturn = false;
    }
    // Not first eeprom flashed
    if (true == LenReturn)
    {
      HAL_UART_Transmit(&huart1, (uint8_t*)msgReportStatusPass, strlen(msgReportStatusPass), 1000);
    }
    else
    {
      HAL_UART_Transmit(&huart1, (uint8_t*)msgReportStatusFail, strlen(msgReportStatusFail), 1000);
    }
  }

}

static bool drv_BLESendCommand(void)
{
  bool LenStatus = false;
  LenStatus = drv_BLESendRevCommand(cmdSetMachineCommandSend, cmdSetMachineCommandResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  LenStatus = drv_BLESendRevCommand(cmdOffResponseHeaderSend, cmdOffResponseHeaderResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  LenStatus = drv_BLESendRevCommand(cmdSaveSend, cmdSaveResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  return true;
}

static bool drv_BLESetDeviceName(void)
{
  bool LenStatus = false;
  LenStatus = drv_BLESendRevCommand(cmdSetDeviceNameSend, cmdSetDeviceNameResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  LenStatus = drv_BLESendRevCommand(cmdSaveSend, cmdSaveResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  return true;
}

static bool drv_BLESetInitOperation(void)
{
  bool LenStatus = false;
  LenStatus = drv_BLESendRevCommand(cmdSetLedSend, cmdSetLedResp, REV_TIMEOUT);
  if (false == LenStatus)
  {
    /* If response command failed -> pin 10 already setted status_led*/
    LenStatus = drv_BLESendRevCommand(cmdSetLedRetrySend, cmdSetLedRetryResp, REV_TIMEOUT);
    if (false == LenStatus) return false;
  }
  LenStatus = drv_BLESendRevCommand(cmdSetLedOperationSend, cmdSetLedOperationResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  LenStatus = drv_BLESendRevCommand(cmdSetAdversingModeSend, cmdSetAdversingModeResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  LenStatus = drv_BLESendRevCommand(cmdSetIncreasePowerSend, cmdSetIncreasePowerResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  LenStatus = drv_BLESendRevCommand(cmdSaveSend, cmdSaveResp, REV_TIMEOUT);
  if (false == LenStatus) return false;
  return true;
}

static bool drv_BLESendRevCommand(char *pSendCmdBuf, char* pRevCmdBuf, const int Timeout)
{
  bool blStatus = false;
  uint8_t revChar;
  char *ptr = pRevCmdBuf;
  int lenRevCmd = strlen(pRevCmdBuf);
  int counter = 0;
  if (HAL_UART_Transmit(&huart3, (uint8_t *)pSendCmdBuf, strlen(pSendCmdBuf), 1000) != HAL_OK)
  {
    return false;
  }

  ulTimeout =  Timeout;

  do
  {
    do
    {
      if (HAL_UART_Receive(&huart3, &revChar, 1 , READ_TIMEOUT) == HAL_OK)
      {
        if (revChar == *ptr)
        {
          ptr++;
          counter++;
          if (counter >= lenRevCmd)
          {
            blStatus = true;
            break;
          }
        }
        else
        {
          counter = 0;
          ptr = pRevCmdBuf;
        }
      }
      else
      {
        break;
      }
    }while (1);
    if (ulTimeout > 0) ulTimeout--;
  } while((blStatus == false) && (ulTimeout != 0));

  return blStatus;
}

#if DEVELOPMENT
static bool drv_BLEGetRevCommand(char *pSendCmdBuf, char* pRevCmdBuf, int line, const int Timeout)
{
  bool blStatus = false;
  uint8_t counterline = 0;
  uint8_t revChar;
  if (HAL_UART_Transmit(&huart3, (uint8_t *)pSendCmdBuf, strlen(pSendCmdBuf), 1000) != HAL_OK)
  {
    return false;
  }

  ulTimeout =  Timeout;

  do
  {
    do
    {
      if (HAL_UART_Receive(&huart3, &revChar, 1 , READ_TIMEOUT) == HAL_OK)
      {
        if (revChar == '\n')
        {
          counterline++;
          if (counterline == line)
          {
            blStatus = true;
            break;
          }
        }
        else if (revChar == '\r')
        {
          /* Nothing to do */
        }
        else
        {
          *pRevCmdBuf = revChar;
          pRevCmdBuf++;
        }
      }
      else
      {
        break;
      }
    }while (1);
    if (ulTimeout > 0) ulTimeout--;
  } while((blStatus == false) && (ulTimeout != 0));

  return blStatus;
}
#endif