/*
FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
All rights reserved

VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

This file is part of the FreeRTOS distribution.

FreeRTOS is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License (version 2) as published by the
Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

***************************************************************************
>>!   NOTE: The modification to the GPL is included to allow you to     !<<
>>!   distribute a combined work that includes FreeRTOS without being   !<<
>>!   obliged to provide the source code for proprietary components     !<<
>>!   outside of the FreeRTOS kernel.                                   !<<
***************************************************************************

FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  Full license text is available on the following
link: http://www.freertos.org/a00114.html

***************************************************************************
*                                                                       *
*    FreeRTOS provides completely free yet professionally developed,    *
*    robust, strictly quality controlled, supported, and cross          *
*    platform software that is more than just the market leader, it     *
*    is the industry's de facto standard.                               *
*                                                                       *
*    Help yourself get started quickly while simultaneously helping     *
*    to support the FreeRTOS project by purchasing a FreeRTOS           *
*    tutorial book, reference manual, or both:                          *
*    http://www.FreeRTOS.org/Documentation                              *
*                                                                       *
***************************************************************************

http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
the FAQ page "My application does not run, what could be wrong?".  Have you
defined configASSERT()?

http://www.FreeRTOS.org/support - In return for receiving this top quality
embedded software for free we request you assist our global community by
participating in the support forum.

http://www.FreeRTOS.org/training - Investing in training allows your team to
be as productive as possible as early as possible.  Now you can receive
FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
Ltd, and the world's leading authority on the world's leading RTOS.

http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
including FreeRTOS+Trace - an indispensable productivity tool, a DOS
compatible FAT file system, and our tiny thread aware UDP/IP stack.

http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
licenses offer ticketed support, indemnification and commercial middleware.

http://www.SafeRTOS.com - High Integrity Systems also provide a safety
engineered and independently SIL3 certified version for use in safety and
mission critical applications that require provable dependability.

1 tab == 4 spaces!
*/


/******************************************************************************
*
* http://www.FreeRTOS.org/cli
*
******************************************************************************/


/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "EVSEtest1.h"
#include "StateMachine.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_rtc_ex.h"
#include "stm32f4xx_hal_rtc.h"

/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ff.h"
#include "os_layer/os_dir.h"
#include "sdcard/sd_card.h"
/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"

//extern void vTaskList( char * pcWriteBuffer );

// TEST MODE By defaul is 0 to not let technitians affect the normal EVSE behaviour
int TestMode = 1; //0
int CPDC;
extern uint8_t          buf[ADC_ARRAY_LENGHT],IdAllowed,AAlarmDaily=0,BAlarmDaily=0;
extern uint16_t         high_c,low_c;
extern uint16_t         Count12V,Count9V,Count6V,Count3V,Count0V,Count_12V;
extern uint8_t          CP_FinalStatus, state_charger,state_vehicle,Contactor_State;
extern uint16_t         Current_Charge,Init_Current_Charge,Max_Current_Charge;
extern char             FirstEE[2],BackBoneSNEE[12],PasswordEE[12],LoginEE[2],MaxCurrentEE[2],CurrentEE[2],CSTypeEE[2],MeterEE[2],BleEE[2],RS485EE[2],RS485AddEE[2],RfidEE[2],
AuthEE[2],OutputTypeEE[2],FirmwareEE[6],DateFlashEE[4],AudiEE[2];

extern bool             error_status[MAX_NUM_ERROR];
extern GPIO_PinState    Contactor_Mon;
char                    WriteAddress[2],WriteData[12],PassLog[12],SetTime[8],CurrentSet[2];
char                    adc[40],schdf[2],scheflagEN = '0',scheflagDIS = '0';
uint8_t                 buf2[ADC_ARRAY_LENGHT],hexbyte,hexbyte0,LoggedFlag;
extern                  RTC_HandleTypeDef hrtc;
TickType_t              Log_ulSystemTime,Log_ulSystemPreviousTimestamp;
RTC_AlarmTypeDef        AAlarmTime,BAlarmTime;
extern bool isCommandSerial;

/*
* The function that registers the commands that are defined within this file.
*/
void vRegisterSampleCLICommands( void );

/*
* Implements the task-stats command.
*/
static BaseType_t prvTaskStatsCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the echo-three-parameters command.
*/
static BaseType_t prvThreeParameterEchoCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the echo-parameters command.
*/
static BaseType_t prvParameterEchoCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the White Led turn on off Command.
*/
static BaseType_t prvTestCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the White Led turn on off Command.
*/
static BaseType_t prvWhiteLEDCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Blue Led turn on off Command.
*/
static BaseType_t prvBlueLEDCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Contactor open close Command.
*/
static BaseType_t prvContactorCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the CP PWM ON and OFF.
*/
static BaseType_t prvCPCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the CP Duty cycle.
*/
static BaseType_t prvCPDutyCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Read ADC of CP
*/
static BaseType_t prvReadADC_CPCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Read EEPROM
*/
static BaseType_t prvReadEEPROMCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Write EEPROM
*/
static BaseType_t prvWriteEEPROMCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Get Status Command
*/
static BaseType_t prvGSCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Get Status Simplified Command
*/
static BaseType_t prvGSSCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the Get Configuration Parameters Command
*/
static BaseType_t prvGCPCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );



/*
* Implements the Get Time Command
*/
static BaseType_t prvGetTimeCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the Set Time Command
*/
static BaseType_t prvSetTimeCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the Set Date Command
*/
static BaseType_t prvSetDateCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the Set Current Command
*/
static BaseType_t prvSetCurrentCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Get Serial Number
*/
static BaseType_t prvGetSNCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Super User Login
*/
static BaseType_t prvSULoginCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the Software Reset
*/
static BaseType_t prvResetCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );


/*
* Implements the Charging enable
*/
static BaseType_t prvChEnCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Charging disable
*/
static BaseType_t prvChDisCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Set Scheduling Enable or Start command
*/
static BaseType_t prvSetSchEnableCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the Set Scheduling Disable or Stop command
*/
static BaseType_t prvSetSchDisableCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

#if DEVELOPMENT
/*
* Implements the read EEPROM with JSON format
*/
static BaseType_t prvJSONReadEEPROM( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the get bluetooth configuration in first boot up
*/
static BaseType_t prvGetBTConfiguration( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
#endif

/*
* Implements the ls command for file system
*/
static BaseType_t prvCommandLS(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the cd command for file system
*/
static BaseType_t prvCommandCD(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
* Implements the cat command for file system
*/
static BaseType_t prvCommandCAT(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
/*******************************************************************************************/

/* Structure that defines the "task-stats" command line command.  This generates
a table that gives information on each task in the system. */
static const CLI_Command_Definition_t xTaskStats =
{
  "task-stats", /* The command string to type. */
  "\r\ntask-stats:\r\n Displays a table showing the state of each FreeRTOS task\r\n",
  prvTaskStatsCommand, /* The function to run. */
  0 /* No parameters are expected. */
};

/* Structure that defines the "echo_3_parameters" command line command.  This
takes exactly three parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xThreeParameterEcho =
{
  "echo-3-parameters",
  "\r\necho-3-parameters <param1> <param2> <param3>:\r\n Expects three parameters, echos each in turn\r\n",
  prvThreeParameterEchoCommand, /* The function to run. */
  3 /* Three parameters are expected, which can take any value. */
};

/* Structure that defines the "echo_parameters" command line command.  This
takes a variable number of parameters that the command simply echos back one at
a time. */
static const CLI_Command_Definition_t xParameterEcho =
{
  "echo-parameters",
  "\r\necho-parameters <...>:\r\n Take variable number of parameters, echos each in turn\r\n",
  prvParameterEchoCommand, /* The function to run. */
  -1 /* The user can enter any number of commands. */
};

static const CLI_Command_Definition_t xTest =
{
  "test",
  "\r\nTest <...>:\r\n Test Mode config  1--> test mode 0-->regular mode\r\n",
  prvTestCommand, /* The function to run. */
  -1 /* The user can enter any number of commands. */
};

static const CLI_Command_Definition_t xWhiteLED =
{
  "WL",
  "\r\nWL <...>:\r\n Take the parameter to turn on or off the WHITE led\r\n",
  prvWhiteLEDCommand, /* The function to run. */
  -1 /* The user can enter any number of commands. */
};

static const CLI_Command_Definition_t xBlueLED =
{
  "BL",
  "\r\nBL <...>:\r\n Take the parameter to turn on or off the BLUE led\r\n",
  prvBlueLEDCommand, /* The function to run. */
  -1 /* The user can enter any number of commands. */
};

static const CLI_Command_Definition_t xContactor =
{
  "CONT",
  "\r\nCONT <...>:\r\n Open(0) or Close(1) parameter for the contactor driving\r\n",
  prvContactorCommand, /* The function to run. */
  -1 /* The user can enter any number of commands. */
};

static const CLI_Command_Definition_t xCP =
{
  "CP",
  "\r\nCP <...>:\r\n Contro Pilot Turn On(1) and Off(0)\r\n",
  prvCPCommand, /* The function to run. */
  -1 /* The user can enter any number of commands. */
};

static const CLI_Command_Definition_t xCPDuty =
{
  "CPDuty",
  "\r\nCPDuty <...>:\r\n Contro Pilot Duty Cycle value\r\n",
  prvCPDutyCommand, /* The function to run. */
  -1 /* The user can enter any number of commands. */
};


static const CLI_Command_Definition_t xReadADC_CP =
{
  "ReadADC_CP", /* The command string to type. */
  "\r\nReadADC_CP:\r\n Displays a CP adc reading\r\n",
  prvReadADC_CPCommand, /* The function to run. */
  0 /* No parameters are expected. */
};


static const CLI_Command_Definition_t xReadEEPROM =
{
  "reeprom", /* The command string to type. */
  "\r\nreeprom:\r\n Displays EEPROM Configuration\r\n",
  prvReadEEPROMCommand, /* The function to run. */
  0 /* No parameters are expected. */
};


/* Structure that defines the "Write EEprom with 2 paramters" command line command.  This
takes exactly 2 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xWriteEEPROM =
{
  "weeprom",
  "\r\nweeprom <param1> <param2>:\r\n Expects <param1> as address and <param2> as value to save in EEPROM emulated memory\r\n",
  prvWriteEEPROMCommand, /* The function to run. */
  2 /* 2 parameters are expected, which can take any value. */
};


/* Structure that defines the "Get Status" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xGS =
{
  "gs",
  "\r\ngs:\r\n Get Status of the charger. Print in screen the EVSE Status\r\n",
  prvGSCommand, /* The function to run. */
  0 /* 2 parameters are expected, which can take any value. */
};

/* Structure that defines the "Get Status" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xGSS =
{
  "gss",
  "\r\ngss:\r\n Get Status Simplified of the charger.\r\n",
  prvGSSCommand, /* The function to run. */
  0 /* 2 parameters are expected, which can take any value. */
};


/* Structure that defines the "Get Config Parameters" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xGCP =
{
  "gcp",
  "\r\ngcp:\r\n Get Configuration Parameters of the charger.\r\n",
  prvGCPCommand, /* The function to run. */
  0 /* 0 parameters are expected, which can take any value. */
};


/* Structure that defines the "Get time" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xGT =
{
  "gt",
  "\r\ngt:\r\n Get Time of the charger. Print in screen the Time\r\n",
  prvGetTimeCommand, /* The function to run. */
  0 /* 2 parameters are expected, which can take any value. */
};

/* Structure that defines the "Set Time with 1 paramters" command line command.  This
takes exactly 1 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xST =
{
  "st",
  "\r\nst <hh:dd:ss>:\r\n Expects <hh:dd:ss> to set time. \r\n",
  prvSetTimeCommand, /* The function to run. */
  1 /* 2 parameters are expected, which can take any value. */
};

/* Structure that defines the "Set Date with 1 paramters" command line command.  This
takes exactly 1 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xSD =
{
  "sd",
  "\r\nsd <dd/mm/yy>:\r\n Expects <dd/mm/yy> to set date. \r\n",
  prvSetDateCommand, /* The function to run. */
  1 /* 2 parameters are expected, which can take any value. */
};

/* Structure that defines the "Set Time with 1 paramters" command line command.  This
takes exactly 1 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xSC =
{
  "sc",
  "\r\nsc <dd>:\r\n Expects <dd> to set charging current. \r\n",
  prvSetCurrentCommand, /* The function to run. */
  1 /* 1 parameters are expected, which can take any value. */
};


/* Structure that defines the "Get Serial Number" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xGSN =
{
  "gsn",
  "\r\ngsn:\r\n Get Serial Number of the charger. Print in screen the Serial Number\r\n",
  prvGetSNCommand, /* The function to run. */
  0 /* 2 parameters are expected, which can take any value. */
};

/* Structure that defines the "Set Time with 1 paramters" command line command.  This
takes exactly 1 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xSUL =
{
  "su",
  "\r\nsu :\r\n Login Error \r\n",
  prvSULoginCommand, /* The function to run. */
  1 /* 1 parameters are expected, which can take any value. */
};


/* Structure that defines the "Reset" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xRST =
{
  "reboot",
  "\r\nreboot\r\n Software Reset instruction\r\n",
  prvResetCommand, /* The function to run. */
  0 /* 2 parameters are expected, which can take any value. */
};

/* Structure that defines the "chen" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xCHEN =
{
  "chen",
  "\r\nchen\r\n Charging enable in authentification mode\r\n",
  prvChEnCommand, /* The function to run. */
  0 /* 2 parameters are expected, which can take any value. */
};

/* Structure that defines the "chen" command line command.  This
takes exactly 0 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xCHDIS =
{
  "chdis",
  "\r\nchdis\r\n Charging disable in authentification mode\r\n",
  prvChDisCommand, /* The function to run. */
  0 /* 0 parameters are expected, which can take any value. */
};


/* Structure that defines the "Set Time for Start Scheduling with 1 paramters" command line command.  This
takes exactly 1 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xSSEN =
{
  "ssen",
  "\r\nssen <dd>:\r\n Expects <hh:mm> to set start scheduling time. \r\n",
  prvSetSchEnableCommand, /* The function to run. */
  2 /* 2 parameters are expected, which can take any value. */
};


/* Structure that defines the "Set Time for Start Scheduling with 1 paramters" command line command.  This
takes exactly 1 parameters that the command simply echos back one at a
time. */
static const CLI_Command_Definition_t xSSDIS =
{
  "ssdis",
  "\r\nssdis <dd>:\r\n Expects <hh:mm> to set stop scheduling time. \r\n",
  prvSetSchDisableCommand, /* The function to run. */
  2 /* 2 parameters are expected, which can take any value. */
};

#if DEVELOPMENT
static const CLI_Command_Definition_t xJsonReadEEPROM =
{
  "jsonreeprom",
  "\r\njsonreeprom: \r\n Displays EEPROM Configuration in JSON format \r\n",
  prvJSONReadEEPROM, /* The function to run. */
  0 /* 0 parameters are expected, which can take any value. */
};

static const CLI_Command_Definition_t xGetBTConfig =
{
  "getbtconfig",
  "\r\ngetbtconfig: \r\n Read initial bluetooth configuration. \r\n",
  prvGetBTConfiguration, /* The function to run. */
  0 /* 0 parameters are expected, which can take any value. */
};

static const CLI_Command_Definition_t xCommandLS =
{
  "ls",
  "\r\nls: \r\n List all files and folders in current directory \r\n",
  prvCommandLS, /* The function to run. */
  0 /* 0 parameters are expected, which can take any value. */
};

static const CLI_Command_Definition_t xCommandCD =
{
  "cd",
  "\r\ncd: \r\n Set new directory [Note: Please use '.' for current directory]. \r\n",
  prvCommandCD, /* The function to run. */
  1 /* 0 parameters are expected, which can take any value. */
};

static const CLI_Command_Definition_t xCommandCAT =
{
  "cat",
  "\r\ncat: \r\n Read content of file as input parameter. \r\n",
  prvCommandCAT, /* The function to run. */
  1 /* 0 parameters are expected, which can take any value. */
};
/*-----------------------------------------------------------*/
#endif


void vRegisterSampleCLICommands( void )
{
  /* Register all the command line commands defined immediately above. */
  FreeRTOS_CLIRegisterCommand( &xTaskStats );
  FreeRTOS_CLIRegisterCommand( &xThreeParameterEcho );
  FreeRTOS_CLIRegisterCommand( &xParameterEcho );
  FreeRTOS_CLIRegisterCommand( &xTest );
  FreeRTOS_CLIRegisterCommand( &xWhiteLED );
  FreeRTOS_CLIRegisterCommand( &xBlueLED );
  FreeRTOS_CLIRegisterCommand( &xContactor );
  FreeRTOS_CLIRegisterCommand( &xCP );
  FreeRTOS_CLIRegisterCommand( &xCPDuty );
  FreeRTOS_CLIRegisterCommand( &xReadADC_CP );
  FreeRTOS_CLIRegisterCommand( &xReadEEPROM );
  FreeRTOS_CLIRegisterCommand( &xWriteEEPROM );
  FreeRTOS_CLIRegisterCommand( &xGS );
  FreeRTOS_CLIRegisterCommand( &xGSS );
  FreeRTOS_CLIRegisterCommand( &xGCP );
  FreeRTOS_CLIRegisterCommand( &xGT );
  FreeRTOS_CLIRegisterCommand( &xST );
  FreeRTOS_CLIRegisterCommand( &xSD );
  FreeRTOS_CLIRegisterCommand( &xSC );
  FreeRTOS_CLIRegisterCommand( &xGSN );
  FreeRTOS_CLIRegisterCommand( &xSUL );
  FreeRTOS_CLIRegisterCommand( &xRST );
  FreeRTOS_CLIRegisterCommand( &xCHEN );
  FreeRTOS_CLIRegisterCommand( &xCHDIS );
  FreeRTOS_CLIRegisterCommand( &xSSEN );
  FreeRTOS_CLIRegisterCommand( &xSSDIS );
#if DEVELOPMENT
  FreeRTOS_CLIRegisterCommand( &xJsonReadEEPROM );
#endif
  FreeRTOS_CLIRegisterCommand( &xCommandLS );
  FreeRTOS_CLIRegisterCommand( &xCommandCD );
  FreeRTOS_CLIRegisterCommand( &xCommandCAT );
  
}
/*-----------------------------------------------------------*/


static BaseType_t prvTaskStatsCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *const pcHeader = "     State   Priority  Stack    #\r\n************************************************\r\n";
  BaseType_t xSpacePadding;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){

    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "Task" );
    pcWriteBuffer += strlen( pcWriteBuffer );

    /* Minus three for the null terminator and half the number of characters in
    "Task" so the column lines up with the centre of the heading. */
    configASSERT( configMAX_TASK_NAME_LEN > 3 );
    for( xSpacePadding = strlen( "Task" ); xSpacePadding < ( configMAX_TASK_NAME_LEN - 3 ); xSpacePadding++ )
    {
      /* Add a space to align columns after the task's name. */
      *pcWriteBuffer = ' ';
      pcWriteBuffer++;

      /* Ensure always terminated. */
      *pcWriteBuffer = 0x00;
    }
    strcpy( pcWriteBuffer, pcHeader );
    //vTaskList( pcWriteBuffer + strlen( pcHeader ) );
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );

  }
  /* There is no more data to return after this single string, so return
  pdFALSE. */
  return pdFALSE;
}
/*-----------------------------------------------------------*/

static BaseType_t prvThreeParameterEchoCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){


    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The three parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      /* Sanity check something was returned. */
      configASSERT( pcParameter );

      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
      strncat( pcWriteBuffer, pcParameter, ( size_t ) xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

      /* If this is the last of the three parameters then there are no more
      strings to return after this one. */
      if( uxParameterNumber == 3U )
      {
        /* If this is the last of the three parameters then there are no more
        strings to return after this one. */
        xReturn = pdFALSE;
        uxParameterNumber = 0;
      }
      else
      {
        /* There are more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}
/*-----------------------------------------------------------*/



static BaseType_t prvParameterEchoCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();

    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}




/*-----------------------------------------------------------*/
static BaseType_t prvTestCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );



        if ((strcmp (pcParameter,"1"))==0){
          TestMode=1;
        } else if ((strcmp (pcParameter,"0"))==0){
          TestMode=0;
        }else{}



        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}

/*-----------------------------------------------------------*/
static BaseType_t prvWhiteLEDCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


        if (TestMode == 1){
          if ((strcmp (pcParameter,"1"))==0){
            WhiteLedTurnON();
          } else if ((strcmp (pcParameter,"0"))==0){
            WhiteLedTurnOFF();
          }else{}
        }


        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}

/*-----------------------------------------------------------*/
static BaseType_t prvBlueLEDCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


        if (TestMode == 1){
          if ((strcmp (pcParameter,"1"))==0){
            BlueLedTurnON();
          } else if ((strcmp (pcParameter,"0"))==0){
            BlueLedTurnOFF();
          }else{}
        }


        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}



/*-----------------------------------------------------------*/
static BaseType_t prvContactorCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


        if (TestMode == 1){
          if ((strcmp (pcParameter,"1"))==0){
            CloseContactor();
          } else if ((strcmp (pcParameter,"0"))==0){
            OpenContactor();
          }else{}
        }


        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}

static BaseType_t prvCPCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


        if (TestMode == 1){
          if ((strcmp (pcParameter,"1"))==0){
            CPON();
          } else if ((strcmp (pcParameter,"0"))==0){
            CPOFF();
          }else{}
        }


        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}



static BaseType_t prvCPDutyCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;
  int CPDC;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


        if (TestMode == 1){
          CPDC = atoi(pcParameter);
          if ((CPDC>=0)&&(CPDC<=100)){
            if (CPDC!= 0) CPDC = CPDC++;
            SetCPDuty(CPDC);
          }
        }


        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}



static BaseType_t prvReadADC_CPCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "CP_ADC: " );
    pcWriteBuffer += strlen( pcWriteBuffer );

    //strcpy( pcWriteBuffer, "CP_high: " );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    sprintf(adc,"CP 12V:  %d",Count12V); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

    // strncat( pcWriteBuffer, "CP_low: ", strlen( "CP_low: ") );
    sprintf(adc,"CP 9V:  %d",Count9V); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

    sprintf(adc,"CP 6V:  %d",Count6V); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

    sprintf(adc,"CP 3V:  %d",Count3V); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

    sprintf(adc,"CP 0V:  %d",Count0V); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

    sprintf(adc,"CP -12V:  %d",Count_12V); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

    strncat( pcWriteBuffer, "CP Status -->  ", strlen( "CP Status -->  " ) );
    if (CP_FinalStatus == STATE_A) strncat( pcWriteBuffer, "A", strlen( "A" ) );
    else if (CP_FinalStatus == STATE_B1) strncat( pcWriteBuffer, "B1", strlen( "B1" ) );
    else if (CP_FinalStatus == STATE_B2) strncat( pcWriteBuffer, "B2", strlen( "B2" ) );
    else if (CP_FinalStatus == STATE_C) strncat( pcWriteBuffer, "C", strlen( "C" ) );
    else if (CP_FinalStatus == STATE_D) strncat( pcWriteBuffer, "D", strlen( "D" ) );
    else if (CP_FinalStatus == STATE_E) strncat( pcWriteBuffer, "E", strlen( "E" ) );
    else if (CP_FinalStatus == STATE_F) strncat( pcWriteBuffer, "F", strlen( "F" ) );
    else if (CP_FinalStatus == STATE_G) strncat( pcWriteBuffer, "Diode Missing", strlen( "Diode Missing" ) );
    else if (CP_FinalStatus == STATE_H) strncat( pcWriteBuffer, "Other Failure State", strlen( "Other Failure State" ) );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }

  return pdFALSE;
}

static BaseType_t prvReadEEPROMCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    ReadEepromEVSEValues();

    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "EEPROM CONFIGURATION: " );
    pcWriteBuffer += strlen( pcWriteBuffer );

    /*
    //Password Serial number
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Password                0006:12         :", strlen( "Password                0006,12         :" ) );
    strncat( pcWriteBuffer, PasswordEE, 12 );
    */

    //Backbone Serial number
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "BackBone S/N            0007,12         :", strlen( "BackBone S/N            0000,12         :" ) );
    strncat( pcWriteBuffer, BackBoneSNEE, 12 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );


    //Loign Enabling
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Force Login            0013,2           :", strlen( "Force Login             0012,2          :" ) );
    strncat( pcWriteBuffer, LoginEE, 2 );
    strncat( pcWriteBuffer, " (00:No Login and Password, 01:Required Login and Password);", strlen( " (00:No Login and Password, 01:Required Login and Password);" ) );

    //MAX Current setting
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Installation Breaker Current    0014,2  :", strlen( "Installation Breaker Current    0014,2  :" ) );
    strncat( pcWriteBuffer, MaxCurrentEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Current setting
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Charging Current           0015,2       :", strlen( "Charging Current           0015,2       :" ) );
    strncat( pcWriteBuffer, CurrentEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Charger Type
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "CS Type                 0016,2          :", strlen( "Current Rated           0014,2          :" ) );
    strncat( pcWriteBuffer, CSTypeEE, 2 );
    strncat( pcWriteBuffer, " (01:Type 1 charger, 02:Type 2 Charger);", strlen( " (01:Type 1 charger, 02:Type 2 Charger);" ) );

    //Output type
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Output type             0017,2          :", strlen( "Current Rated           0019,2          :" ) );
    strncat( pcWriteBuffer, OutputTypeEE, 2 );
    strncat( pcWriteBuffer, " (00:Cable, 01:Socket);", strlen( " (00:Cable, 01:Socket);" ) );


    //Meter Enabling
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Meter Type              0018,2          :", strlen( "Current Rated           0015,2          :" ) );
    strncat( pcWriteBuffer, MeterEE, 2 );
    strncat( pcWriteBuffer, " (00:Meter Not Enabled, 01:Internal Meter Enabled);", strlen( " (00:Meter not enabled, 01:Internal Meter Enabled);" ) );

    //Bluetooth Enabling
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Bluetooth Enable        0019,2          :", strlen( "Current Rated           0016,2          :" ) );
    strncat( pcWriteBuffer, BleEE, 2 );
    strncat( pcWriteBuffer, " (00:Bluetooth Not Enabled, 01:Bluetooth Enabled);", strlen( " (00:Bluetooth Not Enabled, 01:Bluetooth Enabled);" ) );

    //RS485 Enabling
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "RS485 Enable            0020,2          :", strlen( "Current Rated           0017,2          :" ) );
    strncat( pcWriteBuffer, RS485EE, 2 );
    strncat( pcWriteBuffer, " (00:RS485 Not Enabled, 01:RS485 Enabled);", strlen( " (00:RS485 Not Enabled, 01:RS485 Enabled);" ) );

    //RS485 address
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "RS485 Address           0021,2          :", strlen( "Rs485 Address           0018,2          :" ) );
    strncat( pcWriteBuffer, RS485AddEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //RFID Enabling
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "RFID Enable             0022,2          :", strlen( "Current Rated           0019,2          :" ) );
    strncat( pcWriteBuffer, RfidEE, 2 );
    strncat( pcWriteBuffer, " (00:RFID Not Enabled, 01:RFID Enabled);", strlen( " (00:RFID Not Enabled, 01:RFID Enabled);" ) );

    //Authentification Enabling
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Authentification Enable 0023,2          :", strlen( "Current Rated           0019,2          :" ) );
    strncat( pcWriteBuffer, AuthEE, 2 );
    strncat( pcWriteBuffer, " (00:Authentification Not Enabled, 01:Authentification Enabled);", strlen( " (00:Authentification Not Enabled, 01:Authentification Enabled);" ) );

    //Firmware Version
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Firmware Version        0024,6          :", strlen( "BackBone S/N            0000,12         :" ) );
    strncat( pcWriteBuffer, FirmwareEE, 6 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );


    //Firmware Version
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Programming date        0027,8          :", strlen( "BackBone S/N            0000,12         :" ) );
    strncat( pcWriteBuffer, DateFlashEE, 8 );
    strncat( pcWriteBuffer, " (dd-mm-yy);", strlen( " (dd-mm-yy);" ) );

  //Audi Enabling
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, "Audi Protocol Enable    0031,2          :", strlen( "Current Rated           0019,2          :" ) );
    strncat( pcWriteBuffer, AudiEE, 2 );
    strncat( pcWriteBuffer, " (00:Regular Charging Protocol, 01:Audi Charging Protocol);", strlen( " (00:Regular Charging Protocol, 01:Audi Charging Protocol);" ) );

    
    

  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }
  return pdFALSE;
}



static BaseType_t prvWriteEEPROMCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "Success:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      /* Sanity check something was returned. */
      configASSERT( pcParameter );

      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
      strncat( pcWriteBuffer, pcParameter, ( size_t ) xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


      if( uxParameterNumber == 1U ) {
        strncpy(WriteAddress, pcParameter, sizeof(WriteAddress));
      }

      /* If this is the last of the three parameters then there are no more
      strings to return after this one. */
      if( uxParameterNumber == 2U ) {
        strncpy(WriteData, pcParameter, ( size_t )xParameterStringLength);

        WriteEepromEVSEValues(WriteAddress,WriteData,( size_t )xParameterStringLength);


        xReturn = pdFALSE;
        uxParameterNumber = 0;
      }
      else
      {
        /* There are more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}



static BaseType_t prvGSCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  char StateEV[25],PilotVoltage[25],CarConnected[15],ContactorState[5],ContactorMon[5];
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;
  uint8_t error_flag;

  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){

    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "Outlet :1;" );
    pcWriteBuffer += strlen( pcWriteBuffer );


    if (state_vehicle == STATE_A) {
      sprintf(PilotVoltage,"State A --> 12V"); // put the int into a string
      sprintf(StateEV,"Car Disconnected");
      sprintf(CarConnected,"Disconnected");
    }else if (state_vehicle == STATE_A2){
      sprintf(PilotVoltage,"State A2 --> 12V PWM");
      sprintf(StateEV,"Car Disconnected");
      sprintf(CarConnected,"Disconnected");
    }else if (state_vehicle == STATE_B1){
      sprintf(PilotVoltage,"State B1 --> 9V");
      sprintf(StateEV,"Car Connected");
      sprintf(CarConnected,"Connected");
    }else if (state_vehicle == STATE_B2){
      sprintf(PilotVoltage,"State B2 --> 9V PWM");
      sprintf(StateEV,"Car Connected");
      sprintf(CarConnected,"Connected");
    }else if (state_vehicle == STATE_C0){
      sprintf(PilotVoltage,"State C0 --> 6V");
      sprintf(StateEV,"Car Connected");
      sprintf(CarConnected,"Connected");
    }else if (state_vehicle == STATE_C){
      sprintf(PilotVoltage,"State C --> 6V PWM");
      sprintf(StateEV,"Charging");
      sprintf(CarConnected,"Connected");
    }else if (state_vehicle == STATE_D){
      sprintf(PilotVoltage,"State D --> 3V PWM");
      sprintf(StateEV,"Charging (Ventilation)");
      sprintf(CarConnected,"Connected");
    }else if (state_vehicle == STATE_E){
      sprintf(PilotVoltage,"State E --> 0");
      sprintf(StateEV,"CP to Groung");
      sprintf(CarConnected,"--------");
    }else if (state_vehicle == STATE_F){
      sprintf(PilotVoltage,"State F --> -12V");
      sprintf(StateEV,"Error");
      sprintf(CarConnected,"--------");
    }else if (state_vehicle == STATE_G){
      sprintf(PilotVoltage,"State A2 --> -12V PWM");
      sprintf(StateEV,"No diode precense");
      sprintf(CarConnected,"--------");
    }else if (state_vehicle == STATE_H){
      sprintf(PilotVoltage,"State H --> No identified");
      sprintf(StateEV,"No identified");
      sprintf(CarConnected,"--------");
    }

    // check contactor state
    if (Contactor_State == CLOSE_CONTACTOR_STATE)
      sprintf(ContactorState,"On");
    else sprintf(ContactorState,"Off");

    // check MOnitor contacto state
    if (Contactor_Mon == GPIO_PIN_SET)
      sprintf(ContactorMon,"On");
    else sprintf(ContactorMon,"Off");





    //Activity State
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Activity state.......................:", 39 );
    strncat( pcWriteBuffer, "Active", strlen( "Active" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //RFID Management
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Rfid mgmt............................:", 39 );
    strncat( pcWriteBuffer, "On", strlen( "On" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //Bluetooth Management
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Bluetooth mgmt.......................:", 39 );
    strncat( pcWriteBuffer, "On", strlen( "On" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //Current Charge configuration
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Current Charge configuration.........:", 39 );
    sprintf(adc,"%d",Init_Current_Charge); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ";", strlen( "," ) );
    //strncat( pcWriteBuffer, "16", strlen( "16" ) );

    //Peak hour plan
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Peak hour plan.......................:", 39 );
    strncat( pcWriteBuffer, "00:00, 0 Min, 0 Amps", strlen( "00:00, 0 Min, 0 Amps" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //Immediate charge time
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Immediate charge time................:", 39 );
    strncat( pcWriteBuffer, "33", strlen( "33" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //Continue pwm time
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Continue pwm time....................:", 39 );
    strncat( pcWriteBuffer, "120", strlen( "120" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //Pre step 0v time
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Pre step 0v time.....................:", 39 );
    strncat( pcWriteBuffer, "3", strlen( "3" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //Pwm auto detect time
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Pwm auto detect time.................:", 39 );
    strncat( pcWriteBuffer, "30", strlen( "30" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //State
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " State................................:", 39 );
    strncat( pcWriteBuffer, ( char * ) StateEV, ( size_t )StateEV );
    strncat( pcWriteBuffer, ";", strlen( "," ) );
    //strncat( pcWriteBuffer, "Charge started", strlen( "Charge started" ) );

    //Door
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Door is..............................:", 39 );
    strncat( pcWriteBuffer, "Opened", strlen( "Opened" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //Cable cord is
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Cable cord is........................:", 39 );
    strncat( pcWriteBuffer, "Locked", strlen( "Locked" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    // Car connected
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Car connected........................:", 39 );
    strncat( pcWriteBuffer, ( char * ) CarConnected, ( size_t )CarConnected );
    strncat( pcWriteBuffer, ";", strlen( "," ) );
    //strncat( pcWriteBuffer, "Connected", strlen( "Connected" ) );

    // Contactor
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Contactor............................:", 39 );
    strncat( pcWriteBuffer, ( char * ) ContactorState, ( size_t )ContactorState );
    strncat( pcWriteBuffer, ";", strlen( "," ) );
    //strncat( pcWriteBuffer, "On", strlen( "On" ) );

    // Contactor monitor
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Contactor monitor....................:", 39 );
    strncat( pcWriteBuffer, ( char * ) ContactorMon, ( size_t )ContactorMon );
    strncat( pcWriteBuffer, ";", strlen( "," ) );
    //strncat( pcWriteBuffer, "Off", strlen( "Off" ) );

    // Proximity level
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Proximity level......................:", 39 );
    strncat( pcWriteBuffer, "3", strlen( "3" ) );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    // Pilot voltage
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Pilot voltage........................:", 39 );
    strncat( pcWriteBuffer, ( char * ) PilotVoltage, ( size_t )PilotVoltage );
    strncat( pcWriteBuffer, ";", strlen( "," ) );
    //strncat( pcWriteBuffer, "6", strlen( "6" ) );

    // Pilot current
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Pilot current........................:", 39 );
    sprintf(adc,"%d",Current_Charge); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ";", strlen( "," ) );
    //strncat( pcWriteBuffer, "20 (Cable limit)", strlen( "20 (Cable limit)" ) );

    // Power meter current L1
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Power meter current L1[A]............:", 39 );
    strncat( pcWriteBuffer, "0.000;", strlen( "0.000," ) );

    // Power meter current L2
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Power meter current L2[A]............:", 39 );
    strncat( pcWriteBuffer, "0.000;", strlen( "0.000," ) );

    // Power meter current L3
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Power meter current L3[A]............:", 39 );
    strncat( pcWriteBuffer, "0.000;", strlen( "0.000," ) );

    // Power meter
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Power meter [Wh,kWh].................:", 39 );
    strncat( pcWriteBuffer, "0,      0.000;", strlen( "0,      0.000," ) );

    // Current rfid number
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Current rfid number..................:", 39 );
    strncat( pcWriteBuffer, "None;", strlen( "None," ) );

    // Malfunctions
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Malfunctions.........................:", 39 );
    //strncat( pcWriteBuffer, "None,", strlen( "None," ) );
    error_flag = 0;
    if (error_status[ERROR_IN_VEHICLE] == true) {
      strncat( pcWriteBuffer, "Vehicle problem, ", strlen( " Vehicle problem," ) );
      error_flag++;
    }
    if (error_status[ERROR_EVSE_NOT_AVAILABLE] == true) {
      strncat( pcWriteBuffer, "EVSE not available, ", strlen( " EVSE not available," ) );
      error_flag++;
    }
    if (error_status[ERROR_WHILE_CHARGING] == true)  {
      strncat( pcWriteBuffer, "Error While charging, ", strlen( " Error While charging," ) );
      error_flag++;
    }

    if (error_status[ERROR_VENTILATION_REQUIREMENT] == true)  {
      strncat( pcWriteBuffer, "Ventilation Required, ", strlen( " Ventilation Required," ) );
      error_flag++;
    }
    if (error_status[ERROR_TIMER_CONTACTOR_ON] == true)    {
      strncat( pcWriteBuffer, "Timer contactor On, ", strlen( " Timer contactor On," ) );
      error_flag++;
    }
    if (error_status[ERROR_MELT_CONTACTOR] == true)      {
      strncat( pcWriteBuffer, "Contactor Malfunction, ", strlen( " Contactor Malfunction," ) );
      error_flag++;
    }
    if (error_status[ERROR_POWERFAILURE] == true)  {
      strncat( pcWriteBuffer, "Power Failure, ", strlen( " Power Failure," ) );
      error_flag++;
    }
    if (error_status[ERROR_CPTOGND] == true)      {
      strncat( pcWriteBuffer, "CP line crossed to GND, ", strlen( " CP line crossed to GND," ) );
      error_flag++;
    }
    if (error_status[ERROR_NOAC] == true)  {
      strncat( pcWriteBuffer, "No AC, ", strlen( " No AC," ) );
      error_flag++;
    }
    if (error_status[ERROR_NODIODE] == true)  {
      strncat( pcWriteBuffer, "No Diode, ", strlen( " No Diode," ) );
      error_flag++;
    }
    if (error_status[ERROR_UNKNOWN] == true)  {
      strncat( pcWriteBuffer, "Unknown Malfunction, ", strlen( " Unknown Malfunction," ) );
      error_flag++;
    }
    if ( error_flag == 0){
      strncat( pcWriteBuffer, "None,", strlen( "None," ) );
    }


    // Electric feeding
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Electric feeding.....................:", 39 );
    strncat( pcWriteBuffer, "Single phase;", strlen( "Single phase," ) );

    // Meter Malfunctions
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Meter malfunctions...................:", 39 );
    strncat( pcWriteBuffer, "None;", strlen( "None," ) );

    // Meter alerts
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
    strncat( pcWriteBuffer, " Meter alerts.........................:", 39 );
    strncat( pcWriteBuffer, "None;", strlen( "None," ) );


  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }


  return pdFALSE;
}
/*-----------------------------------------------------------*/



static BaseType_t prvGSSCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  char StateEV[25],PilotVoltage[25],CarConnected[15],ContactorState[5],ContactorMon[5];
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;
  uint16_t error_flag;

  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){

    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    /* Generate a table of task stats. */
    ReadEepromEVSEValues();

    strcpy( pcWriteBuffer, "gss:" );
    pcWriteBuffer += strlen( pcWriteBuffer );


    //State
    sprintf(adc,"%d",state_vehicle);
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ";", strlen( "," ) );

    //MAX Current setting
    //strncat( pcWriteBuffer, MaxCurrentEE, 2 );
    //strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Current setting
    //strncat( pcWriteBuffer, CurrentEE, 2 );
    //strncat( pcWriteBuffer, ";", strlen( ";" ) );
    // Pilot current
    sprintf(adc,"%d",Current_Charge); // put the int into a string
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ";", strlen( "," ) );


    // Power meter  CarConnected,16,08,type2,20,gnsXXXXXx,schedule
    strncat( pcWriteBuffer, "0.000;", strlen( "0,      0.000," ) );


    //scheduling start time

    sprintf(adc,"%02x:%02x",AAlarmTime.AlarmTime.Hours,AAlarmTime.AlarmTime.Minutes);
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ":", strlen( "," ) );
    sprintf(adc,"%c",scheflagEN);
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ":", strlen( "," ) );



    //scheduling stop time

    sprintf(adc,"%02x:%02x",BAlarmTime.AlarmTime.Hours,BAlarmTime.AlarmTime.Minutes);
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ":", strlen( "," ) );
    sprintf(adc,"%c",scheflagDIS);
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ":", strlen( "," ) );


    // Malfunctions
    //strncat( pcWriteBuffer, "None,", strlen( "None," ) );
    error_flag = 0;
    if (error_status[ERROR_IN_VEHICLE] == true) {
      // strncat( pcWriteBuffer, "Vehicle problem, ", strlen( " Vehicle problem," ) );
      error_flag |= 1UL << ERROR_IN_VEHICLE ;
    }
    if (error_status[ERROR_EVSE_NOT_AVAILABLE] == true) {
      //strncat( pcWriteBuffer, "EVSE not available, ", strlen( " EVSE not available," ) );
      error_flag |= 1UL << ERROR_EVSE_NOT_AVAILABLE ;
    }
    if (error_status[ERROR_WHILE_CHARGING] == true)  {
      //strncat( pcWriteBuffer, "Error While charging, ", strlen( " Error While charging," ) );
      error_flag |= 1UL << ERROR_WHILE_CHARGING ;
    }

    if (error_status[ERROR_VENTILATION_REQUIREMENT] == true)  {
      //strncat( pcWriteBuffer, "Ventilation Required, ", strlen( " Ventilation Required," ) );
      error_flag |= 1UL << ERROR_VENTILATION_REQUIREMENT ;
    }
    if (error_status[ERROR_TIMER_CONTACTOR_ON] == true)    {
      //strncat( pcWriteBuffer, "Timer contactor On, ", strlen( " Timer contactor On," ) );
      error_flag |= 1UL << ERROR_TIMER_CONTACTOR_ON ;
    }
    if (error_status[ERROR_MELT_CONTACTOR] == true)      {
      //strncat( pcWriteBuffer, "Contactor Malfunction, ", strlen( " Contactor Malfunction," ) );
      error_flag |= 1UL << ERROR_MELT_CONTACTOR ;
    }
    if (error_status[ERROR_POWERFAILURE] == true)  {
      //strncat( pcWriteBuffer, "Power Failure, ", strlen( " Power Failure," ) );
      error_flag |= 1UL << ERROR_POWERFAILURE ;
    }
    if (error_status[ERROR_CPTOGND] == true)      {
      //strncat( pcWriteBuffer, "CP line crossed to GND, ", strlen( " CP line crossed to GND," ) );
      error_flag |= 1UL << ERROR_CPTOGND ;
    }
    if (error_status[ERROR_NOAC] == true)  {
      //strncat( pcWriteBuffer, "No AC, ", strlen( " No AC," ) );
      error_flag |= 1UL << ERROR_NOAC ;
    }
    if (error_status[ERROR_NODIODE] == true)  {
      //strncat( pcWriteBuffer, "No Diode, ", strlen( " No Diode," ) );
      error_flag |= 1UL << ERROR_NODIODE ;
    }
    if (error_status[ERROR_UNKNOWN] == true)  {
      //strncat( pcWriteBuffer, "Unknown Malfunction, ", strlen( " Unknown Malfunction," ) );
      error_flag |= 1UL << ERROR_UNKNOWN ;
    }
    /*if ( error_flag == 0){
    strncat( pcWriteBuffer, "None;", strlen( "None;" ) );
  }  else {
    strncat( pcWriteBuffer, "Error;", strlen( "Error;" ) );
  }
    */
    sprintf(adc,"%d",error_flag);
    strncat( pcWriteBuffer, ( char * ) adc, ( size_t )adc );
    strncat( pcWriteBuffer, ";", strlen( "," ) );







  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }


  return pdFALSE;
}
/*-----------------------------------------------------------*/



static BaseType_t prvGCPCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  char StateEV[25],PilotVoltage[25],CarConnected[15],ContactorState[5],ContactorMon[5];
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;
  uint16_t error_flag;

  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){

    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    /* Generate a table of task stats. */
    ReadEepromEVSEValues();

    strcpy( pcWriteBuffer, "gcp:" );
    pcWriteBuffer += strlen( pcWriteBuffer );


    //Backbone Serial number
    strncat( pcWriteBuffer, BackBoneSNEE, 12 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );


    //Loign Enabling
    strncat( pcWriteBuffer, LoginEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //MAX Current setting
    strncat( pcWriteBuffer, MaxCurrentEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Current setting
    strncat( pcWriteBuffer, CurrentEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Charger Type
    strncat( pcWriteBuffer, CSTypeEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );
    //Output type
    strncat( pcWriteBuffer, OutputTypeEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Meter Enabling
    strncat( pcWriteBuffer, MeterEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Bluetooth Enabling
    strncat( pcWriteBuffer, BleEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //RS485 Enabling
    strncat( pcWriteBuffer, RS485EE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //RS485 address
    strncat( pcWriteBuffer, RS485AddEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //RFID Enabling
    strncat( pcWriteBuffer, RfidEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Authentification Enabling
    strncat( pcWriteBuffer, AuthEE, 2 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );

    //Firmware Version
    strncat( pcWriteBuffer, FirmwareEE, 6 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );


    //date flashed Version
    strncat( pcWriteBuffer, DateFlashEE, 8 );
    strncat( pcWriteBuffer, ";", strlen( ";" ) );


    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }


  return pdFALSE;
}


static BaseType_t prvGetTimeCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  char time[16],date[16],Hours[6],Minutes[6],Seconds[6];
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "Time: " );
    pcWriteBuffer += strlen( pcWriteBuffer );

    // IMPORTANT!!! To read just the Time is also a must to read also DATE, if not it freezes RTC
    HAL_RTC_GetTime(&hrtc, &gTime, RTC_FORMAT_BCD);
    HAL_RTC_GetDate(&hrtc, &gDate, RTC_FORMAT_BCD);


    // Time
    sprintf(time, "%02x:%02x:%02x\n", gTime.Hours, gTime.Minutes, gTime.Seconds);
    strncat( pcWriteBuffer, ( char * ) time, ( size_t )time );
    // Date
    sprintf(date, "Date: %02x/%02x/%02x\n", gDate.Date, gDate.Month, gDate.Year);
    strncat( pcWriteBuffer, ( char * ) date, ( size_t )time );


    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }
  return pdFALSE;
}



static BaseType_t prvSetTimeCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;
  char            tim[2];
  char            hh,mm,ss;


  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The Time to set is:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      /* Sanity check something was returned. */
      configASSERT( pcParameter );

      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
      strncat( pcWriteBuffer, pcParameter, ( size_t ) xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


      if( uxParameterNumber == 1U ) {

        strncpy(SetTime, pcParameter, sizeof(SetTime));

        tim[0] = SetTime[0];
        tim[1] = SetTime[1];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);
        gTime.Hours = hexbyte;
        tim[0] = SetTime[3];
        tim[1] = SetTime[4];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);
        gTime.Minutes = hexbyte;
        tim[0] = SetTime[6];
        tim[1] = SetTime[7];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);
        gTime.Seconds = hexbyte;
        gTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
        gTime.StoreOperation = RTC_STOREOPERATION_RESET;
        if (HAL_RTC_SetTime(&hrtc, &gTime, RTC_FORMAT_BCD) != HAL_OK)                  {
          _Error_Handler(__FILE__, __LINE__);
        }
        /*
        gDate.WeekDay = RTC_WEEKDAY_WEDNESDAY;
        gDate.Month = RTC_MONTH_OCTOBER;
        gDate.Date = 0x25;
        gDate.Year = 0x17;

        if (HAL_RTC_SetDate(&hrtc, &gDate, RTC_FORMAT_BCD) != HAL_OK)
        {
        _Error_Handler(__FILE__, __LINE__);
      }*/


        xReturn = pdFALSE;
        uxParameterNumber = 0;
      }
      else
      {
        /* There are more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}


static BaseType_t prvSetDateCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;
  char            tim[2];
  char            hh,mm,ss;


  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The Date to set is:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      /* Sanity check something was returned. */
      configASSERT( pcParameter );

      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
      strncat( pcWriteBuffer, pcParameter, ( size_t ) xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


      if( uxParameterNumber == 1U ) {

        strncpy(SetTime, pcParameter, sizeof(SetTime));

        tim[0] = SetTime[0];
        tim[1] = SetTime[1];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);
        gDate.Date = hexbyte;
        tim[0] = SetTime[3];
        tim[1] = SetTime[4];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);
        gDate.Month = hexbyte;
        tim[0] = SetTime[6];
        tim[1] = SetTime[7];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);
        gDate.Year = hexbyte;
        gDate.WeekDay = RTC_WEEKDAY_MONDAY; //It is a must to add this parameter. If we dont do year is not working correctly

        if (HAL_RTC_SetDate(&hrtc, &gDate, RTC_FORMAT_BCD) != HAL_OK)
        {
          _Error_Handler(__FILE__, __LINE__);
        }



        xReturn = pdFALSE;
        uxParameterNumber = 0;
      }
      else
      {
        /* There are more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}


static BaseType_t prvSetCurrentCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;
  int CurrentSet;

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );

  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){


    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The parameters were:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      if( pcParameter != NULL )
      {
        /* Return the parameter string. */
        memset( pcWriteBuffer, 0x00, xWriteBufferLen );
        sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
        strncat( pcWriteBuffer, ( char * ) pcParameter, ( size_t ) xParameterStringLength );
        strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


        if (TestMode == 1){
          CurrentSet = atoi(pcParameter);
          if (CurrentSet <= Max_Current_Charge){
            Init_Current_Charge = CurrentSet;
            if ( (state_charger == STATESCHARGER_VEHICLECONNECTED ) ||
                (state_charger == STATESCHARGER_CHARGING ) ){
                  CheckCableStatus();
                  EnablePWM();


                }

          }
        }


        /* There might be more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
      else
      {
        /* No more parameters were found.  Make sure the write buffer does
        not contain a valid string. */
        pcWriteBuffer[ 0 ] = 0x00;

        /* No more data to return. */
        xReturn = pdFALSE;

        /* Start over the next time this command is executed. */
        uxParameterNumber = 0;
      }
    }
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}


///Get Serial Number command
static BaseType_t prvGetSNCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );

  ReadEepromEVSEValues();

  /* Generate a table of task stats. */
  strcpy( pcWriteBuffer, "SN: " );
  pcWriteBuffer += strlen( pcWriteBuffer );
  strncat( pcWriteBuffer, BackBoneSNEE, 12 );

  strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );

  return pdFALSE;
}



// super user login to the system
static BaseType_t prvSULoginCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;

  char            Pass[12];



  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );



  if( uxParameterNumber == 0 )
  {
    /* The first time the function is called after the command has been
    entered just a header string is returned. */
    sprintf( pcWriteBuffer, "Login:\r\n" );

    /* Next time the function is called the first parameter will be echoed
    back. */
    uxParameterNumber = 1U;

    /* There is more data to be returned as no parameters have been echoed
    back yet. */
    xReturn = pdPASS;
  }
  else
  {
    /* Obtain the parameter string. */
    pcParameter = FreeRTOS_CLIGetParameter
      (
       pcCommandString,		/* The command string itself. */
       uxParameterNumber,		/* Return the next parameter. */
       &xParameterStringLength	/* Store the parameter string length. */
         );

    /* Sanity check something was returned. */
    configASSERT( pcParameter );

    /* Return the parameter string. */
    memset( pcWriteBuffer, 0x00, xWriteBufferLen );
    sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
    strncat( pcWriteBuffer, pcParameter, ( size_t ) xParameterStringLength );
    strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


    if( uxParameterNumber == 1U ) {

      ReadEepromEVSEValues();

      strncpy(PassLog, pcParameter, sizeof(PassLog));

      if (( PasswordEE[0] == PassLog[0]) &&
          ( PasswordEE[1] == PassLog[1]) &&
            ( PasswordEE[2] == PassLog[2]) &&
              ( PasswordEE[3] == PassLog[3]) &&
                ( PasswordEE[4] == PassLog[4]) &&
                  ( PasswordEE[5] == PassLog[5]) &&
                    ( PasswordEE[6] == PassLog[6]) &&
                      ( PasswordEE[7] == PassLog[7]) &&
                        ( PasswordEE[8] == PassLog[8]) &&
                          ( PasswordEE[9] == PassLog[9]) &&
                            ( PasswordEE[10] == PassLog[10]) &&
                              ( PasswordEE[11] == PassLog[11])) {

                                sprintf( pcWriteBuffer, "Correct\r\n" );
                                LoggedFlag = 1;
                                Log_ulSystemPreviousTimestamp = xTaskGetTickCount();

                              } else{

                                sprintf( pcWriteBuffer, "Password not correct\r\n" );
                                LoggedFlag = 0;

                              }
      xReturn = pdFALSE;
      uxParameterNumber = 0;
    }
    else
    {
      /* There are more parameters to return after this one. */
      xReturn = pdTRUE;
      uxParameterNumber++;
    }
  }



  return xReturn;
}




// Reset/Reboot Command Definition
static BaseType_t prvResetCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );


  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "Rebooting..." );
    pcWriteBuffer += strlen( pcWriteBuffer );
    HAL_NVIC_SystemReset();
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );


  }

  return pdFALSE;
}


// Charging enable Command Definition
static BaseType_t prvChEnCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );


  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "Charge Enabled" );
    pcWriteBuffer += strlen( pcWriteBuffer );
    IdAllowed = 1;
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );


  }

  return pdFALSE;
}


// Charging disable Command Definition
static BaseType_t prvChDisCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );


  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "Charge Disabled" );
    pcWriteBuffer += strlen( pcWriteBuffer );
    IdAllowed = 0;
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );


  }

  return pdFALSE;
}


// set start scheduling time
static BaseType_t prvSetSchEnableCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;


  char            tim[2];
  char            hh,mm,ss;


  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The Time to Start is:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      /* Sanity check something was returned. */
      configASSERT( pcParameter );

      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
      strncat( pcWriteBuffer, pcParameter, ( size_t ) xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


      if( uxParameterNumber == 1U ) {

        strncpy(SetTime, pcParameter, sizeof(SetTime));

        tim[0] = SetTime[0];
        tim[1] = SetTime[1];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);

        tim[0] = SetTime[3];
        tim[1] = SetTime[4];
        hexbyte0 = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);

        if (!((SetTime[0] == 'x') && (SetTime[1] == 'x') && (SetTime[2] == 'x') && (SetTime[3] == 'x'))){

          AAlarmTime.AlarmTime.Hours = hexbyte;
          AAlarmTime.AlarmTime.Minutes = hexbyte0;
          AAlarmTime.AlarmTime.Seconds = 0;
          AAlarmTime.AlarmTime.SubSeconds = 0;
          AAlarmTime.AlarmTime.TimeFormat = RTC_HOURFORMAT_24;
          AAlarmTime.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
          AAlarmTime.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;

        }
        //xReturn = pdPASS;
        //uxParameterNumber = 2;
      }

      if( uxParameterNumber == 2U ) {

        strncpy(schdf, pcParameter, sizeof(schdf));
        scheflagEN = schdf[0];

        switch (scheflagEN)
        {
        case '0':

          //HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_A);
          DisableAlarmA();

          break;

        case '1':

          AAlarmTime.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
          AAlarmTime.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
          AAlarmTime.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
          AAlarmTime.AlarmDateWeekDay = 1;
          AAlarmTime.Alarm = RTC_ALARM_A;
          HAL_RTC_SetAlarm_IT(&hrtc, &AAlarmTime, RTC_FORMAT_BCD);
          AAlarmDaily = 0;

          break;

        case '2':

          AAlarmTime.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
          AAlarmTime.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
          AAlarmTime.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
          AAlarmTime.AlarmDateWeekDay = 1;
          AAlarmTime.Alarm = RTC_ALARM_A;
          AAlarmDaily = 1;
          HAL_RTC_SetAlarm_IT(&hrtc, &AAlarmTime, RTC_FORMAT_BCD);

          break;
        }

        xReturn = pdFALSE;
        uxParameterNumber = 0;
      }

      else
      {
        /* There are more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}


// set stop scheduling time
static BaseType_t prvSetSchDisableCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;
  RTC_TimeTypeDef gTime;
  RTC_DateTypeDef gDate;


  char            tim[2];
  char            hh,mm,ss;


  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;
  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    if( uxParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "The Time to Stop is:\r\n" );

      /* Next time the function is called the first parameter will be echoed
      back. */
      uxParameterNumber = 1U;

      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         uxParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );

      /* Sanity check something was returned. */
      configASSERT( pcParameter );

      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      sprintf( pcWriteBuffer, "%d: ", ( int ) uxParameterNumber );
      strncat( pcWriteBuffer, pcParameter, ( size_t ) xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );


      if( uxParameterNumber == 1U ) {

        strncpy(SetTime, pcParameter, sizeof(SetTime));

        tim[0] = SetTime[0];
        tim[1] = SetTime[1];
        hexbyte = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);

        tim[0] = SetTime[3];
        tim[1] = SetTime[4];
        hexbyte0 = ((((int)tim[0])-0x30)*16) + (((int)tim[1])-0x30);


        if (!((SetTime[0] == 'x') && (SetTime[1] == 'x') && (SetTime[2] == 'x') && (SetTime[3] == 'x'))){

          BAlarmTime.AlarmTime.Hours = hexbyte;
          BAlarmTime.AlarmTime.Minutes = hexbyte0;
          BAlarmTime.AlarmTime.Seconds = 0;
          BAlarmTime.AlarmTime.SubSeconds = 0;
          BAlarmTime.AlarmTime.TimeFormat = RTC_HOURFORMAT_24;
          BAlarmTime.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
          BAlarmTime.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;

        }
        //xReturn = pdPASS;
        //uxParameterNumber = 2;
      }

      if( uxParameterNumber == 2U ) {

        strncpy(schdf, pcParameter, sizeof(schdf));
        scheflagDIS = schdf[0];

        switch (scheflagDIS)
        {
        case '0':

          DisableAlarmB();
          //HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_B);

          break;

        case '1':

          BAlarmTime.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
          BAlarmTime.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
          BAlarmTime.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
          BAlarmTime.AlarmDateWeekDay = 1;
          BAlarmTime.Alarm = RTC_ALARM_B;
          HAL_RTC_SetAlarm_IT(&hrtc, &BAlarmTime, RTC_FORMAT_BCD);
          BAlarmDaily = 0;

          break;

        case '2':

          BAlarmTime.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
          BAlarmTime.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
          BAlarmTime.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
          BAlarmTime.AlarmDateWeekDay = 1;
          BAlarmTime.Alarm = RTC_ALARM_B;
          BAlarmDaily = 1;
          HAL_RTC_SetAlarm_IT(&hrtc, &BAlarmTime, RTC_FORMAT_BCD);

          break;
        }

        xReturn = pdFALSE;
        uxParameterNumber = 0;
      }

      else
      {
        /* There are more parameters to return after this one. */
        xReturn = pdTRUE;
        uxParameterNumber++;
      }
    }
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
    /*No more parameters were found.  Make sure the write buffer does
    not contain a valid string. */
    pcWriteBuffer[ 0 ] = 0x00;

    /* No more data to return. */
    xReturn = pdFALSE;

    /* Start over the next time this command is executed. */
    uxParameterNumber = 0;
  }
  return xReturn;
}

///Get default configuration for BT (First initial)
#if USE_NOT_TEMPLATE
void str_replace(const char* str, char find, char replace, char* newstr){
  char res[128];
  memset(res,'\0',sizeof(res));
  strncpy(res, str, strlen(str));
  char *current_pos = strchr(res,find);
  while (current_pos){
    *current_pos = replace;
    current_pos = strchr(current_pos,find);
  }
  strncpy(newstr, res, strlen(res));
}

static void pCreateElementJson(const char* description, const char* value, int lenvalue, bool lastelement, char* output, int* len)
{
  char elementbuf[256];
  char buf[128];
  char *element = malloc(lenvalue + 1);
  snprintf(element,lenvalue + 1, "%s",value);
  memset(elementbuf,'\0',sizeof(elementbuf));
  memset(buf,'\0',sizeof(buf));
  if (false == lastelement)
  {
    str_replace(description,' '  , '_', buf);
    sprintf((char*) elementbuf, "      \"%s\":\"%s\",\r\n",
            buf,element);
  }
  else
  {
    str_replace(description,' '  , '_', buf);
    sprintf((char*) elementbuf, "      \"%s\":\"%s\"\r\n",
            buf,element);
  }
  strncpy(output, elementbuf, strlen(elementbuf));
  *len = strlen(elementbuf);
}
#else
static void pCreateElementJson(const char* description, const char* value, int lenvalue, bool lastelement, char* output, int* len)
{
  char elementbuf[256];
  char element[24];
  strncat( element,value,lenvalue);
  if (false == lastelement)
  {
    sprintf((char*) elementbuf, "    {\r\n      \"description\":\"%s\",\r\n      \"value\":\"%s\"\r\n    },\r\n",
            description,element);
  }
  else
  {
    sprintf((char*) elementbuf, "    {\r\n      \"description\":\"%s\",\r\n      \"value\":\"%s\"\r\n    }",
            description,element);
  }

  strncpy(output, elementbuf, strlen(elementbuf));
  *len = strlen(elementbuf);
}
#endif
static BaseType_t prvJSONReadEEPROM( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;

  configASSERT( pcWriteBuffer );
  char buf[256];
  int len;
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    ReadEepromEVSEValues();

    /* Generate a table of task stats. */
    strcpy( pcWriteBuffer, "{\r\n  \"description\":\"EEPROM Configuration\",\r\n");
    pcWriteBuffer += strlen( pcWriteBuffer );
    strncat( pcWriteBuffer, "  \"data\":{\r\n     ", strlen("  \"data\":{\r\n"));
    pCreateElementJson("Serial Number", BackBoneSNEE, 12, false,buf,&len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Force Login", LoginEE , 2, false, buf, &len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Installation Breaker Current", MaxCurrentEE , 2, false, buf, &len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Charging Current", CurrentEE, 2, false, buf, &len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("CS Type", CSTypeEE, 2, false, buf, &len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Output type", OutputTypeEE, 2, false, buf, &len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Meter Type", MeterEE, 2, false, buf, &len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Bluetooth Enable", BleEE, 2, false,buf,&len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("RS485 Enable", RS485EE, 2, false,buf,&len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("RS485 Address", RS485AddEE, 2, false,buf,&len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("RFID Enable", RfidEE, 2, false,buf,&len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Authentification Enable", AuthEE, 2, false,buf,&len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Firmware Version", FirmwareEE, 6, false, buf, &len);
    strncat( pcWriteBuffer,buf,len);
    pCreateElementJson("Programming date", DateFlashEE, 8, true,buf,&len);
    strncat( pcWriteBuffer,buf,len);
    strncat( pcWriteBuffer, "  }\r\n}", strlen( "  }\r\n}" ) );
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }
  return pdFALSE;
}

/* Callback function to get bluetooth configuration. This Command only support in CLI Serial */
static BaseType_t prvGetBTConfiguration( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{

  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;

  configASSERT( pcWriteBuffer );
  // LOGGED IN OR NOT LOGGIN ENNABLED
  if ((!(( LoginEE[1] == '1' ) && ( LoginEE[0] == '0')))||(LoggedFlag == 1)){
    if (LoggedFlag == 1) Log_ulSystemPreviousTimestamp = xTaskGetTickCount();
    strcpy( pcWriteBuffer, "Bluetooth Module Initial Configuration For First Boot\r\n");
    pcWriteBuffer += strlen( pcWriteBuffer );
    strncat( pcWriteBuffer,"  1. Set BT module to machine mode \"set sy c m machine\"\r\n",
            strlen("  1. Set BT module to machine mode \"set sy c m machine\"\r\n"));
    strncat( pcWriteBuffer,"  1. Set BT module to machine mode \"set sy c m machine\"\r\n",
            strlen("  1. Set BT module to machine mode \"set sy c m machine\"\r\n"));
    strncat( pcWriteBuffer,"  2. Disable header response \"set sy c h 0\"\r\n",
            strlen("  2. Disable header response \"set sy c h 0\"\r\n"));
    strncat( pcWriteBuffer,"  3. Set BT module default name to GNRGYBT \"set sy d n GNRGYBT\"\r\n",
            strlen("  3. Set BT module default name to GNRGYBT \"set sy d n GNRGYBT\"\r\n"));
    strncat( pcWriteBuffer,"  4. Set GPIO 10 as status_led \"gfu 10 status_led\"\r\n",
            strlen("  4. Set GPIO 10 as status_led \"gfu 10 status_led\"\r\n"));
    strncat( pcWriteBuffer,"  5. Set status_led with {no connect: 50%, connect: 100%}  \"set sy i s 0101007f\"\r\n",
            strlen("  5. Set status_led with {no connect: 50%, connect: 100%}  \"set sy i s 0101007f\"\r\n"));
    strncat( pcWriteBuffer,"  6. Set bluetooth with advertising duration forever \"set bl v h d 0\"\r\n",
            strlen("  6. Set bluetooth with advertising duration forever \"set bl v h d 0\"\r\n"));
    strncat( pcWriteBuffer,"  7. Set transmit power advertising with 3dBm \"set bl t a 3\"\r\n",
            strlen("  7. Set transmit power advertising with 3dBm \"set bl t a 3\"\r\n"));
  } else {
    strcpy( pcWriteBuffer, NO_LOGGED_MESSAGE );
    pcWriteBuffer += strlen( pcWriteBuffer );
  }
  return pdFALSE;
}

static BaseType_t prvCommandLS( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static UBaseType_t uxParameterNumber = 0;
  char buffer[128];
  FRESULT res;
  FILINFO fno;
  DIR dir;
  char *fn;   /* This function is assuming non-Unicode cfg. */
  /* Remove compile time warnings about unused parameters, and check the
  write buffer is not NULL.  NOTE - for simplicity, this example assumes the
  write buffer length is adequate, so does not check for buffer overflows. */
  ( void ) pcCommandString;
  ( void ) xWriteBufferLen;

  configASSERT( pcWriteBuffer );
  if (isCommandSerial && sdCard_isReady())
  { 
    memset( pcWriteBuffer, 0x00, xWriteBufferLen );
    char path[128];
    if (!osDir_CurrentPWD(path))
    {
      sprintf(buffer,"Current path is invalid directory. Please change \r\n");
      strncat( pcWriteBuffer, buffer, strlen(buffer));
      xReturn = pdFALSE;
    }
    else
    {
      res = f_opendir(&dir, path);                       /* Open the directory */
      if (res == FR_OK) {
        for (;;) {
          res = f_readdir(&dir, &fno);                   /* Read a directory item */
          if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
          if (fno.fname[0] == '.') continue;             /* Ignore dot entry */
#if _USE_LFN
          fn = *fno.fname ? fno.fname : fno.fname;
#else
          fn = fno.fname;
#endif
          if (fno.fattrib & AM_DIR) {                    /* It is a directory */
            sprintf(buffer,"[folder] %s\r\n",fn);
            strncat( pcWriteBuffer, buffer, strlen(buffer));
          } else {                                       /* It is a file. */
            sprintf(buffer,"[file] %s\r\n",fn);
            strncat( pcWriteBuffer, buffer, strlen(buffer));
          }
        }
        xReturn = pdFALSE;
      }
      else
      {
        sprintf(buffer,"Current path is invalid directory. Please change\r\n");
        strncat( pcWriteBuffer, buffer, strlen(buffer));
        xReturn = pdFALSE;
      }
    }
  }
  else if (!sdCard_isReady())
  {
    strcpy( pcWriteBuffer, "Sorry, your current SDCard is not ready. Please check \r\n");
    pcWriteBuffer += strlen( pcWriteBuffer );
    xReturn = pdFALSE;
  }
  else
  {
    strcpy( pcWriteBuffer, "Sorry, this command only supports in serial communication \r\n");
    pcWriteBuffer += strlen( pcWriteBuffer );
    xReturn = pdFALSE;
  }
  return xReturn;
}

static BaseType_t prvCommandCD( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static BaseType_t lParameterNumber = 0;
  char buffer[128];
	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
  
  if (isCommandSerial && sdCard_isReady())
  {
    if( lParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "Location path :" );
      
      /* Next time the function is called the first parameter will be echoed
      back. */
      lParameterNumber = 1L;
      
      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         lParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );
      
      /* Sanity check something was returned. */
      configASSERT( pcParameter );
      
      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      strncat( pcWriteBuffer, pcParameter, xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
      
      /* If this is the last of the three parameters then there are no more
      strings to return after this one. */
      if( lParameterNumber == 1L )
      {
        
        if (!(osDir_SetCD(pcParameter)))
        {
          sprintf(buffer,"Can't change your directory path. Please change \r\n");
          strncat( pcWriteBuffer, buffer, strlen(buffer));
        }
        {
          char path[128];
          osDir_CurrentPWD(path);
          sprintf(buffer,"Current directory %s\r\n", path);
          strncat( pcWriteBuffer, buffer, strlen(buffer));
        }
        /* If this is the last of the three parameters then there are no more
        strings to return after this one. */
        xReturn = pdFALSE;
        lParameterNumber = 0L;
      }
    }
  }
  else if (!sdCard_isReady())
  {
    strcpy( pcWriteBuffer, "Sorry, your current SDCard is not ready. Please check\r\n ");
    pcWriteBuffer += strlen( pcWriteBuffer );
    xReturn = pdFALSE;
  }
  else
  {
    strcpy( pcWriteBuffer, "Sorry, this command only supports in serial communication\r\n");
    pcWriteBuffer += strlen( pcWriteBuffer );
    xReturn = pdFALSE;
  }
  return xReturn;
}

static BaseType_t prvCommandCAT( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter;
  BaseType_t xParameterStringLength, xReturn;
  static BaseType_t lParameterNumber = 0;
  FIL fil;
  uint8_t g_whitelist[128];
  FRESULT res;
  UINT read;
  char buffer[128];
	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
  
  if (isCommandSerial && sdCard_isReady())
  {
    if( lParameterNumber == 0 )
    {
      /* The first time the function is called after the command has been
      entered just a header string is returned. */
      sprintf( pcWriteBuffer, "Location path :" );
      
      /* Next time the function is called the first parameter will be echoed
      back. */
      lParameterNumber = 1L;
      
      /* There is more data to be returned as no parameters have been echoed
      back yet. */
      xReturn = pdPASS;
    }
    else
    {
      /* Obtain the parameter string. */
      pcParameter = FreeRTOS_CLIGetParameter
        (
         pcCommandString,		/* The command string itself. */
         lParameterNumber,		/* Return the next parameter. */
         &xParameterStringLength	/* Store the parameter string length. */
           );
      
      /* Sanity check something was returned. */
      configASSERT( pcParameter );
      
      /* Return the parameter string. */
      memset( pcWriteBuffer, 0x00, xWriteBufferLen );
      strncat( pcWriteBuffer, pcParameter, xParameterStringLength );
      strncat( pcWriteBuffer, "\r\n", strlen( "\r\n" ) );
      
      /* If this is the last of the three parameters then there are no more
      strings to return after this one. */
      if( lParameterNumber == 1L )
      {
        res = f_open(&fil, pcParameter, FA_READ);
        
        if (res!=FR_OK)
        {
          sprintf(buffer,"Failed in read your file. Please check\r\n");
          strncat( pcWriteBuffer, buffer, strlen(buffer));
        }
        else
        {
          while (!f_eof(&fil))
          {
            if (NULL == f_gets(g_whitelist, 128, &fil))
            {
              sprintf(buffer,"Failed in read your file. Please check\r\n");
              strncat( pcWriteBuffer, buffer, strlen(buffer));
              break;
            }
            else
            {
              strncat( pcWriteBuffer, g_whitelist, strlen(g_whitelist));
            }
          }
        }
        /* If this is the last of the three parameters then there are no more
        strings to return after this one. */
        xReturn = pdFALSE;
        lParameterNumber = 0L;
      }
    }
  }
  else if (!sdCard_isReady())
  {
    strcpy( pcWriteBuffer, "Sorry, your current SDCard is not ready. Please check \r\n ");
    pcWriteBuffer += strlen( pcWriteBuffer );
    xReturn = pdFALSE;
  }
  else
  {
    strcpy( pcWriteBuffer, "Sorry, this command only supports in serial communication \r\n");
    pcWriteBuffer += strlen( pcWriteBuffer );
    xReturn = pdFALSE;
  }
  return xReturn;
}

//checking the logging time out
/* Once we are logged in the CLI we have LOGIN_TIMEOUT_PERIOD period from last instructions sent
before we are log out again.
*/
void CheckLoginTime(void){

  if ( LoggedFlag == 1){
    Log_ulSystemTime = xTaskGetTickCount();
    if ((Log_ulSystemTime - Log_ulSystemPreviousTimestamp) >= LOGIN_TIMEOUT_PERIOD){
      LoggedFlag = 0;
      LoggedFlag = 0;
    }
  }


}


/**
* @brief  Alarm A callback.
* @param  hrtc: pointer to a RTC_HandleTypeDef structure that contains
*                the configuration information for RTC.
* @retval None
*/
void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
  IdAllowed = 1;

  //if is it not daily Schedule we disable the schedulint
  if (AAlarmDaily == 0)  {
    scheflagEN = '0';
    DisableAlarmA();
    // HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_A);

  }

}


/**
* @brief  Alarm B callback.
* @param  hrtc: pointer to a RTC_HandleTypeDef structure that contains
*                the configuration information for RTC.
* @retval None
*/

void HAL_RTCEx_AlarmBEventCallback(RTC_HandleTypeDef *hrtc)
{
  IdAllowed = 0;

  //if is it not daily Schedule we disable the schedulint
  if (BAlarmDaily == 0)   {
    scheflagDIS = '0';
    DisableAlarmB();
    //HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_B);

  }
}


void DisableAlarmA(void){

  HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_A);

}

void DisableAlarmB(void){

  HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_B);

}


/*
Outlet 1

Activity state.......................:  Active

Active events........................:  0x0

Rfid mgmt............................:  On

Evse mode............................:  Off

Continues charge current.............:  7

Peak hour plan.......................:  00:00, 0 Min, 0 Amps

Immediate charge time................:  33

Continue pwm time....................:  120

Pre step 0v time.....................:  3

Pwm auto detect time.................:  30

State................................:  Charge started

Door is..............................:  Opened

Cable cord is........................:  Locked

Car connected........................:  Connected

Contactor............................:  On

Contactor monitor....................:  Off

Proximity level......................:  3

Pilot voltage........................:  6

Pilot current........................:  20 (Cable limit)

Power meter current L1[A]............:  0.000

Power meter current L2[A]............:  0.000

Power meter current L3[A]............:  0.000

Power meter [Wh,kWh].................:  0,      0.000

Current rfid number..................:  048DE933

Malfunctions.........................:  BadCable

Electric feeding.....................:  Single phase

Meter malfunctions...................:  None

Meter alerts.........................:  None

*/
