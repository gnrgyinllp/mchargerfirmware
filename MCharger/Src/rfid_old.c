
/* rfid.c 
//////////////////////RFID TASK //////////////////////////////////////////////////////

Task implementation to read a module via UART with CR95hf ic
example modules:
- mikroe rfid click
- BM019: SERIAL TO NEAR FIELD COMMUNICATION (NFC)

read about uart code:
http://blog.solutions-cubed.com/near-field-communication-nfc-with-st-micro-cr95hf-part-2/

*/

/* RFID includes. */
#include "rfid.h"
#include "EVSEtest1.h"
#include "lib_CR95HF.h"
#include "StateMachine.h"
#include "stm32f4xx_hal.h"
#include "lib_95HFConfigManager.h"
#include "lib_pcd.h"



/* Standard includes. */
#include "string.h"
#include "stdio.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Example includes. */
#include "FreeRTOS_CLI.h"

/* Demo application includes. */
#include "serial.h"

/* Dimensions the buffer into which input characters are placed. */
#define cmdMAX_INPUT_SIZE		50

/* Dimentions a buffer to be used by the UART driver, if the UART driver uses a
buffer at all. */
#define cmdQUEUE_LENGTH			40

/* DEL acts as a backspace. */
#define cmdASCII_DEL		( 0x7F )

/* The maximum time to wait for the mutex that guards the UART to become
available. */
#define cmdMAX_MUTEX_WAIT		pdMS_TO_TICKS( 300 )

#ifndef configRFID_BAUD_RATE
	#define configRFID_BAUD_RATE	57600
#endif

RxDataPacketTypeDef tRFIDPacket,tRFIDPacket0;
static xQueueHandle xRFIDPackedRX;
extern ISO14443A_CARD ISO14443A_Card;
extern ISO14443B_CARD ISO14443B_Card;
extern uint8_t testResponse[10];
uint8_t rfResponse[20],rfidelay = 0;
uint8_t TagUIDx[16],ptest[10],protocolreading = 0; 

ReaderConfigStruct					ReaderConfig;
int8_t TagType = TRACK_NOTHING;
bool TagDetected = false;
bool terminal_msg_flag = false ;
uint8_t status = ERRORCODE_GENERIC;
uint8_t	TagReply0	[ISO15693_NBBYTE_UID+7];
uint8_t resptest        [10];
//static char dataOut[COM_XFER_SIZE]; 
/*-----------------------------------------------------------*/

/*
 * The task that implements the command console processing.
 */
static void prvRFIDOperationTask( void *pvParameters );
void vRFIDOperationTaskStart( uint16_t usStackSize, UBaseType_t uxPriority );

/*-----------------------------------------------------------*/
/* Const messages output by the command console. */
static const char * const pcWelcomeMessage = "\r\n\r\n-------------------------------------\r\n          GNRGY RTOS cli\r\n-------------------------------------\r\n \r\n          M EVSE code ver1.0\r\n\r\n\r\nType Help to view a list of registered commands.\r\n\r\n>";
static const char * const pcEndOfOutputMessage = "\r\n[Press ENTER to execute the previous command again]\r\n>";
static const char * const pcNewLine = "\r\n";
static char rfidOutputBuffer[ 40 ];

/* Used to guard access to the UART in case messages are sent to the UART from
more than one task. */
static SemaphoreHandle_t xRFIDTxMutex = NULL;

/* The handle to the UART port, which is not used by all ports. */
static xComPortHandle xPort = 0;

/*-----------------------------------------------------------*/

void vRFIDOperationTaskStart( uint16_t usStackSize, UBaseType_t uxPriority )
{
	/* Create the semaphore used to access the UART Tx. */
	xRFIDTxMutex = xSemaphoreCreateMutex();
	configASSERT( xRFIDTxMutex );
        
        //xRFIDPackedRX = xQueueCreate( 5, 22 );
        //xRFIDPackedRX = xQueueCreate( 5, sizeof ( struct RxDataPacketTypeDef *) );
        xRFIDPackedRX = xQueueCreate( 5, TYPE_RFID_PACKET_SIZE );
        
        vQueueAddToRegistry(xRFIDPackedRX, "RXPACKET_RFID_QUEUE");
        configASSERT( xRFIDPackedRX );
        
	/* Create that task that handles the console itself. */
	xTaskCreate( 	prvRFIDOperationTask,	/* The task that implements the command console. */
					"RFID_Reading",						/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
					usStackSize,				/* The size of the stack allocated to the task. */
					NULL,						/* The parameter is not used, so NULL is passed. */
					uxPriority,					/* The priority allocated to the task. */
					NULL );						/* A handle is not required, so just pass NULL. */
}
/*-----------------------------------------------------------*/

static void prvRFIDOperationTask( void *pvParameters )
{
signed char cRxedChar;
char     chartx;
uint8_t ucInputIndex = 0;
char *pcOutputString;
static char cInputString[ cmdMAX_INPUT_SIZE ], cLastInputString[ cmdMAX_INPUT_SIZE ];
BaseType_t xReturned;
xComPortHandle xPort;
RxDataPacketTypeDef RxRFIDPacket;

	( void ) pvParameters;

	/* Obtain the address of the output buffer.  Note there is no mutual
	exclusion on this buffer as it is assumed only one command console interface
	will be used at any one time. */
	pcOutputString = rfidOutputBuffer;//FreeRTOS_CLIGetOutputBuffer();       
        
	/* Initialise the UART. */
	xPort = xRFIDSerialPortInitMinimal( configRFID_BAUD_RATE, cmdQUEUE_LENGTH );


        
	/* Send the POR instruction. */
        chartx = 0x00;
        sprintf( pcOutputString, &chartx, 1 ); 
        chartx = ECHO;
        strncat( pcOutputString, &chartx,1);
        vRFIDSerialPutString( xPort, ( signed char * ) pcOutputString, ( unsigned short ) strlen( pcOutputString ) );
        
        
	for( ;; )
	{         
                  
          
		/* Wait for the next character.  The while loop is used in case
		INCLUDE_vTaskSuspend is not set to 1 - in which case portMAX_DELAY will
		be a genuine block time rather than an infinite block time. */
		while( xRFIDSerialGetChar( xPort, &cRxedChar, portMAX_DELAY ) != pdPASS );


			
                  if ( ucInputIndex == 0) {
                    RxRFIDPacket.Command = cRxedChar;                      
                    if (cRxedChar == ECHO){            // check if it is a single command back of echo test          
                      ucInputIndex=0;
                      tRFIDPacket = RxRFIDPacket;
                      xQueueSend( xRFIDPackedRX, &tRFIDPacket, ( TickType_t ) 0 );
                      ////paket finished
                    } else {
                      ucInputIndex++;
                    }
                  } else if ( ucInputIndex == 1){       //check if it is a simple command without lenght
                    RxRFIDPacket.Length = cRxedChar;                     
                    if (cRxedChar == 0x00) {
                      ucInputIndex=0;
                      tRFIDPacket = RxRFIDPacket;
                      xQueueSend( xRFIDPackedRX, &tRFIDPacket, ( TickType_t ) 0 );
 
                      ////paket finished
                    } else {
                      ucInputIndex++;
                    }
                  } else {                  
                    RxRFIDPacket.Data[ucInputIndex-2] = cRxedChar;  
                    if (RxRFIDPacket.Length == (ucInputIndex-1)) {
                      ucInputIndex=0;
                      tRFIDPacket = RxRFIDPacket;
                      xQueueSend( xRFIDPackedRX, &tRFIDPacket, ( TickType_t ) 0 );
 
                      ////paket finished
                      
                    } else {
                      ucInputIndex++;
                    }
                  }
                  
                  tRFIDPacket = RxRFIDPacket;
                  
                  
                  
                  

			
					/* Get the next output string from the command interpreter. */
					//xReturned = FreeRTOS_CLIProcessCommand( cInputString, pcOutputString, configCOMMAND_INT_MAX_OUTPUT_SIZE );

					/* Write the generated string to the UART. */
					//vRFIDSerialPutString( xPort, ( signed char * ) pcOutputString, ( unsigned short ) strlen( pcOutputString ) );
		/* Ensure exclusive access to the UART Tx. */
		if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
		{
				


			/* Must ensure to give the mutex back. */
			xSemaphoreGive( xRFIDTxMutex );
		}
	}
}
/*-----------------------------------------------------------*/

void RFID_TXOperation(void){
  
  uint8_t 	        UIDsizex,i;
  uint8_t 	        UIDx[ISO14443A_MAX_UID_SIZE];
  uint8_t               pResponse[10];
  RxDataPacketTypeDef   RxRFIDPacket0;
  uint8_t  CR95HFhandled;
  
  rfidelay++; 
  
  /*if ( uxQueueMessagesWaiting( xRFIDPackedRX ) != 0) {
    if( xQueueReceive( xRFIDPackedRX, &( RxRFIDPacket0 ), ( TickType_t ) 10 ) )
        {
            // pcRxedMessage now points to the struct AMessage variable posted
            // by vATask.
          tRFIDPacket0 = RxRFIDPacket0;
          
        }
  }*/
  
  if (rfidelay == 100){    
    for (i=0;i<16;i++) TagUIDx[i] = 0;
    
    
 
    switch (protocolreading){
      
    case 0:
      CR95HFhandled = gCR95HF_PORsequence( );
      gCR95HF_IDN(rfResponse);
      protocolreading = 1;
      break;
    case 1:
      //////ISO15693 *//*
      gPCD_FieldOff();
      HAL_Delay(5);
      gISO15693_GetUID (TagUIDx); 
     /* 
      ptest[0] = 0x02;
      ptest[1]	= 0x02;
      ptest[2]	= 0x00;
      ptest[3]	= 0x00;
      ptest[4]  = 0x00;
      gRFIDSendReceive( ptest ,rfResponse);
      */
      protocolreading = 2;
      break;
    case 2:
      
      protocolreading = 1;
      break;      
    
    }
    
    rfidelay = 0;
    
  }  
  
  
  //gCR95HF_Receive_UART_Response(pResponse);
  
  
}


void InitRFIDModule(void){
  
  uint8_t  CR95HFhandled;
  RxDataPacketTypeDef   RxRFIDPacket0;
  
  CR95HFhandled = 0;  
  CR95HFhandled = gCR95HF_PORsequence( );
  
  
  /*if ( uxQueueMessagesWaiting( xRFIDPackedRX ) != 0) {
    if( xQueueReceive( xRFIDPackedRX, &( RxRFIDPacket0 ), ( TickType_t ) 10 ) )
        {
            // pcRxedMessage now points to the struct AMessage variable posted
            // by vATask.
          tRFIDPacket0 = RxRFIDPacket0;
          
        }
  }
  
  
  if (CR95HFhandled ==CR95HF_SUCCESS_CODE)  {
    
      osDelay(1);  
      CR95HF_IDN(rfResponse);
      testResponse[0] = rfResponse[0];
  
  }*/
}





/**
 *	@brief  Thi function send POR sequnce. It might be use to initialize CR95HF after a POR.
 *  @param  none
 *  @return CR95HF_ERRORCODE_PORERROR : the POR sequence doesn't succeded
 *  @return CR95HF_SUCCESS_CODE : CR95HF is ready
 */
uint8_t gCR95HF_PORsequence( void ){
  
uint8_t pResponse[10],
		NthAttempt=1;

		gCR95HF_Send_IRQIN_NegativePulse();
		osDelay(10);

do{
		// send an ECHO command and checks CR95HF response 		
		gCR95HF_Echo(pResponse);
                testResponse[0] = pResponse[0];
		if (pResponse[0]==ECHORESPONSE)
			return CR95HF_SUCCESS_CODE;

		/*else if(ReaderConfig.Interface == CR95HF_INTERFACE_SPI)
		{	// send an pulse on IRQ in case of the chip is on sleep state
			if (NthAttempt ==2)
				CR95HF_Send_IRQIN_NegativePulse();
			//else if(ReaderConfig.Interface == CR95HF_INTERFACE_SPI)
				//CR95HF_Send_SPI_ResetSequence();				
		}*/
		else NthAttempt = 5;
		osDelay(50);
	} while (pResponse[0]!=ECHORESPONSE && NthAttempt++ <5);

	return CR95HF_ERRORCODE_PORERROR;
}


/**
 *	@brief  Send a negative pulse on IRQin pin
 *  @param  none
 *  @return None
 */
void gCR95HF_Send_IRQIN_NegativePulse(void)
{
  uint8_t cChar; 

  cChar = 0x00;
  if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
  {
    xRFIDSerialPutChar( xPort, cChar, ( ( portTickType ) 0 ) );
    xSemaphoreGive( xRFIDTxMutex );
  }	
}



/**
 *	@brief  this function send Echo command to CR95HF 
 *  @param  pResponse : pointer on CR95HF reply
 *  @return CR95HF_SUCCESS_CODE 
 */
uint8_t gCR95HF_Echo(uint8_t *pResponse)
{
uint8_t command[]= {ECHO};

	gRFIDSendReceive(command, pResponse);

return CR95HF_SUCCESS_CODE;

}

/**
 *	@brief  this function send an IDN command to CR95HF device
 *  @param  pResponse : pointer on CR95HF reply
 *  @return CR95HF_SUCCESS_CODE
 */
uint8_t gCR95HF_IDN(uint8_t *pResponse)
{
uint8_t DataToSend[] = {IDN	,0x02};
	
	gRFIDSendReceive(DataToSend, pResponse);

return CR95HF_SUCCESS_CODE;
}

/**
 *	@brief  this functions turns the field off
 *  @param  none
 *  @retval PCD_SUCCESSCODE : the function is succesful 
 */
void gPCD_FieldOff( void )
{
	uint8_t ParametersByte=0x00;
	uint8_t pResponse[10];

	gPCD_ProtocolSelect(0x02, PCD_PROTOCOL_FIELDOFF, &ParametersByte,pResponse);
}

/**  
* @brief  this function return a tag UID 
* @param  	UIDout: 	UID of a tag in the field
* @retval status function
*/
int8_t gISO15693_GetUID (uint8_t *UIDout) 
{ 
int8_t 	FlagsByteData, status;
uint8_t	TagReply	[ISO15693_NBBYTE_UID+7];
uint8_t Tag_error_check;
	

	/* select 15693 protocol */
	errchk(gISO15693_Init	(	));

	FlagsByteData = gISO15693_CreateRequestFlag 	(	ISO15693_REQFLAG_SINGLESUBCARRIER,
													ISO15693_REQFLAG_HIGHDATARATE,
													ISO15693_REQFLAG_INVENTORYFLAGSET,
													ISO15693_REQFLAG_NOPROTOCOLEXTENSION,
													ISO15693_REQFLAG_NOTAFI,
													ISO15693_REQFLAG_1SLOT,
													ISO15693_REQFLAG_OPTIONFLAGNOTSET,
													ISO15693_REQFLAG_RFUNOTSET);
	
        status = gISO15693_Inventory (	FlagsByteData, 0x00, 0x00, 0x00, TagReply0 );
	errchk( status );

	Tag_error_check = TagReply0[ISO15693_OFFSET_LENGTH]+1;
	if((TagReply0[Tag_error_check] & ISO15693_CRC_MASK) != 0x00 )
		return ISO15693_ERRORCODE_DEFAULT;
	

	if (status == ISO15693_SUCCESSCODE)
		memcpy(UIDout,&(TagReply0[ISO15693_OFFSET_UID]),ISO15693_NBBYTE_UID);

	//devicemode = PCD;
	//nfc_tagtype = TT5;
	return ISO15693_SUCCESSCODE; 
Error:
	return ISO15693_ERRORCODE_DEFAULT;	
}

/**  
* @brief  this function return a Byte, which is concatenation of iventory flags 
* @param  SubCarrierFlag	:  
* @param	DataRateFlag
* @param	InventoryFlag
* @param	ProtExtFlag
* @param	SelectOrAFIFlag
* @param	AddrOrNbSlotFlag
* @param	OptionFlag
* @param	RFUFlag
* @retval 	Flags byte
*/
static int8_t gISO15693_CreateRequestFlag(const uint8_t SubCarrierFlag,const uint8_t DataRateFlag,const uint8_t InventoryFlag,const uint8_t ProtExtFlag,const uint8_t SelectOrAFIFlag,const uint8_t AddrOrNbSlotFlag,const uint8_t OptionFlag,const uint8_t RFUFlag)
{
int32_t FlagsByteBuf=0;

		FlagsByteBuf = 	(SubCarrierFlag 	& 0x01)					|
										((DataRateFlag  	& 0x01)	<< 1)		|
										((InventoryFlag 	& 0x01) << 2)		|
										((ProtExtFlag		& 0x01)	<< 3)			|
										((SelectOrAFIFlag   & 0x01)	<< 4)	|
										((AddrOrNbSlotFlag  & 0x01)	<< 5)	|
										((OptionFlag  		& 0x01) << 6)		|
										((RFUFlag  			& 0x01) << 7);

	return (int8_t) FlagsByteBuf; 
}


/**  
* @brief  this function selects 15693 protocol accoording to input parameters
* @retval ISO15693_SUCCESSCODE : the function is successful
* @retval ISO15693_ERRORCODE_DEFAULT : an error occured
*/
int8_t gISO15693_Init	( void )
{
	uint8_t 	ParametersByte=0,
					  pResponse[PROTOCOLSELECT_LENGTH];
	int8_t		status;
	u8 DemoGainParameters []  = {PCD_TYPEV_ARConfigA, PCD_TYPEV_ARConfigB}; 

	ParametersByte =  	((ISO15693_APPENDCRC << ISO15693_OFFSET_APPENDCRC ) 	&  ISO15693_MASK_APPENDCRC) |
											((ISO15693_SINGLE_SUBCARRIER << ISO15693_OFFSET_SUBCARRIER)	& ISO15693_MASK_SUBCARRIER)	|
											((ISO15693_MODULATION_100 << ISO15693_OFFSET_MODULATION) & ISO15693_MASK_MODULATION) |
											((ISO15693_WAIT_FOR_SOF <<  ISO15693_OFFSET_WAITORSOF ) & ISO15693_MASK_WAITORSOF) 	|
											((ISO15693_TRANSMISSION_26 <<   ISO15693_OFFSET_DATARATE  )	& ISO15693_MASK_DATARATE);
	
	errchk(gPCD_ProtocolSelect(ISO15693_SELECTLENGTH,ISO15693_PROTOCOL,&(ParametersByte),pResponse));
	//TechnoSelected = PCDPROTOCOL_15693;

	/* in order to adjust the demoduation gain of the PCD*/
        errchk(gPCD_WriteRegister    ( 0x04,AFE_ANALOG_CONF_REG_SELECTION,0x01,DemoGainParameters,pResponse)); 

//	if (PCD_IsReaderResultCodeOk (PROTOCOL_SELECT,pResponse) == ISO15693_ERRORCODE_DEFAULT)
//		return ISO15693_ERRORCODE_DEFAULT;
	

	return ISO15693_SUCCESSCODE;
Error:
	return ISO15693_ERRORCODE_DEFAULT;
}


/**
 *	@brief  this function send a WriteRegister command to the PCD device
 *  @param  Length 		: Number of bytes of WrReg parameters
 *  @param	Address		: address of first register to write
 *  @param	pData 		: pointer data to be write
 *  @param	Flags		: whether to increment address after register read
 *  @param  pResponse : pointer on the PCD device response
 *  @return PCD_SUCCESSCODE : the command was succedfully send
 *  @return PCD_ERRORCODE_PARAMETERLENGTH : Length parameter is erroneous
 */
int8_t gPCD_WriteRegister(const uint8_t Length,const uint8_t Address,const uint8_t Flags,const uint8_t *pData,uint8_t *pResponse)
{
	uint8_t DataToSend[WRREG_BUFFER_SIZE];

	DataToSend[PCD_COMMAND_OFFSET ] = WRITE_REGISTER;
	DataToSend[PCD_LENGTH_OFFSET  ]	= Length;
	DataToSend[PCD_DATA_OFFSET  ]	= Address;
	DataToSend[PCD_DATA_OFFSET +1 ]	= Flags;

	/* Parameters[0] first byte to emmit	*/
	memcpy(&(DataToSend[PCD_DATA_OFFSET + 2]),pData,Length - 2 );
	
	/* Send the command the Rf transceiver	*/
	gRFIDSendReceive(DataToSend, pResponse);
	return PCD_SUCCESSCODE;
}


/**  
* @brief  this function send an inventory command to a contacless tag.  
* @param  Flags		:  	Request flags
* @param	AFI			:	AFI byte (optional)
* @param	MaskLength 	: 	Number of bits of mask value
* @param	MaskValue	:  	mask value which is compare to Contacless tag UID 
* @param	pResponse	: 	pointer on PCD  response
* @retval 	ISO15693_SUCCESSCODE	: 	PCD  returns a succesful code
* @retval 	ISO15693_ERRORCODE_PARAMETERLENGTH	: 	MaskLength value is erroneous
* @retval 	ISO15693_ERRORCODE_DEFAULT	: 	 PCD  returns an error code
*/
static int8_t gISO15693_Inventory(const uint8_t Flags , const uint8_t AFI, const uint8_t MaskLength, const uint8_t *MaskValue, uint8_t *pResponse)
{
uint8_t 	NthByte = 0,
					InventoryBuf [ISO15693_MAXLENGTH_INVENTORY],
					NbMaskBytes = 0,
					NbSignificantBits=0;
int8_t 		FirstByteMask,
					NthMaskByte = 0,
					status;
	
	// initialize the result code to 0xFF and length to 0  in case of error 
	*pResponse = SENDRECV_ERRORCODE_SOFT;
	*(pResponse+1) = 0x00;

	if (MaskLength>ISO15693_NBBITS_MASKPARAMETER)
		return ISO15693_ERRORCODE_PARAMETERLENGTH;

	errchk(gISO15693_IsInventoryFlag (Flags));
		
	
	InventoryBuf[NthByte++] = Flags;
	InventoryBuf[NthByte++] = ISO15693_CMDCODE_INVENTORY;
	
	if (gISO15693_GetSelectOrAFIFlag (Flags) == true)
		InventoryBuf[NthByte++] = AFI;
	
	InventoryBuf[NthByte++] = MaskLength;

	if (MaskLength !=0)
	{
		// compute the number of bytes of mask value(2 border exeptions)
	   	if (MaskLength == 64)
			NbMaskBytes = 8;
		else
	   		NbMaskBytes = MaskLength / 8 + 1;
	
		NbSignificantBits = MaskLength - (NbMaskBytes-1) * 8;
		if (NbSignificantBits !=0)
	   		FirstByteMask = (0x01 <<NbSignificantBits)-1;
		else 
			FirstByteMask = 0xFF;
	
	   	// copy the mask value 
		if (NbMaskBytes >1)
		{
			for (NthMaskByte = 0; NthMaskByte < NbMaskBytes - 1; NthMaskByte ++ )
				InventoryBuf[NthByte++] = MaskValue[NthMaskByte];
		}
	
		if (NbSignificantBits !=0)
			InventoryBuf[NthByte++] = MaskValue[NthMaskByte] & FirstByteMask;
	}

 	errchk(gPCD_SendRecv(NthByte,InventoryBuf,pResponse));

	if (gPCD_IsReaderResultCodeOk (SEND_RECEIVE,pResponse) != ISO15693_SUCCESSCODE)
		return ISO15693_ERRORCODE_DEFAULT;

	return ISO15693_SUCCESSCODE;
Error:
	return ISO15693_ERRORCODE_DEFAULT;
}


/**  
* @brief  	this function returns ISO15693_SUCCESSCODE is Inventorye flag is set
* @param  	FlagsByte	: the byts cantaining the eight flags  	
* @retval 	Inventory flag
*/
static int8_t gISO15693_IsInventoryFlag(const uint8_t FlagsByte)
{
	if ((FlagsByte & ISO15693_MASK_INVENTORYFLAG) != 0x00)
		return ISO15693_SUCCESSCODE ;
	else
		return ISO15693_ERRORCODE_DEFAULT ;
}

/**  
* @brief  	this function returns Select Or AFI flag  (depending on inventory flag)
* @param  	FlagsByte	: Request flags on one byte	
* @retval 	Select Or AFI
*/
static int8_t gISO15693_GetSelectOrAFIFlag(const uint8_t FlagsByte)
{

	if ((FlagsByte & ISO15693_MASK_SELECTORAFIFLAG) != 0x00)
		return true ;
	else
		return false ;
}


/**
 *	@brief  this function send a SendRecv command to the PCD device. the command to contacless device is embeded in Parameters.
 *  @param  Length 		: Number of bytes
 *  @param	Parameters 	: data depenps on protocl selected
 *  @param  pResponse : pointer on the PCD device response
 *  @return PCD_SUCCESSCODE : the command was succedfully send
 *  @return PCD_ERRORCODE_DEFAULT : the PCD device returned an error code
 *  @return PCD_ERRORCODE_PARAMETERLENGTH : Length parameter is erroneous
 */
int8_t gPCD_SendRecv(const uint8_t Length,const uint8_t *Parameters,uint8_t *pResponse)
{
	uint8_t DataToSend[SENDRECV_BUFFER_SIZE];

	/* initialize the result code to 0xFF and length to 0 */
	*pResponse = PCD_ERRORCODE_DEFAULT;
	*(pResponse+1) = 0x00;
	
//	/* check the function parameters	*/
//	if (CHECKVAL (Length,1,255)==false)
//		return PCD_ERRORCODE_PARAMETERLENGTH; 

	DataToSend[PCD_COMMAND_OFFSET ] = SEND_RECEIVE;
	DataToSend[PCD_LENGTH_OFFSET  ]	= Length;

	/* DataToSend CodeCmd Length Data*/
	/* Parameters[0] first byte to emmit	*/
	memcpy(&(DataToSend[PCD_DATA_OFFSET]),Parameters,Length);

	/* Send the command the Rf transceiver	*/
	//drv95HF_SendReceive(DataToSend, pResponse);
        gRFIDSendReceive(DataToSend, pResponse);
        
	if (gPCD_IsReaderResultCodeOk (SEND_RECEIVE,pResponse) != PCD_SUCCESSCODE)
	{
			if(*pResponse == PCD_ERRORCODE_NOTAGFOUND)
				return PCD_ERRORCODE_TIMEOUT;
			else
				return PCD_ERRORCODE_DEFAULT;
	}
	return PCD_SUCCESSCODE;
}


/**  
* @brief  	this function returns PCD_SUCCESSCODE is the reader reply is a succesful code.
* @param  	CmdCode		:  	code command send to the reader
* @param  	ReaderReply	:  	pointer on the PCD device response	
* @retval  	PCD_SUCCESSCODE :  the PCD device returned a succesful code
* @retval  	PCD_ERRORCODE_DEFAULT  :  the PCD device didn't return a succesful code
* @retval  	PCD_NOREPLY_CODE : no the PCD device response
*/
int8_t gPCD_IsReaderResultCodeOk(uint8_t CmdCode, const uint8_t *ReaderReply)
{

	if (ReaderReply[READERREPLY_STATUSOFFSET] == PCD_ERRORCODE_DEFAULT)
		return PCD_NOREPLY_CODE;

  	switch (CmdCode)
	{
		case ECHO: 
			if (ReaderReply[PSEUDOREPLY_OFFSET] == ECHO)
				return PCD_SUCCESSCODE;
			else 
				return PCD_ERRORCODE_DEFAULT;
			
		case IDN: 
			if (ReaderReply[READERREPLY_STATUSOFFSET] == IDN_RESULTSCODE_OK)
				return PCD_SUCCESSCODE;
			else 
				return PCD_ERRORCODE_DEFAULT;
			
		case PROTOCOL_SELECT: 
			switch (ReaderReply[READERREPLY_STATUSOFFSET])
			{
				case IDN_RESULTSCODE_OK :
					return PCD_SUCCESSCODE;
					
				case PROTOCOLSELECT_ERRORCODE_CMDLENGTH :
					return PCD_ERRORCODE_DEFAULT;
					
				case PROTOCOLSELECT_ERRORCODE_INVALID :
					return PCD_ERRORCODE_DEFAULT;
					
				default : return PCD_ERRORCODE_DEFAULT;
					
			}

		case SEND_RECEIVE: 
			switch (ReaderReply[READERREPLY_STATUSOFFSET])
			{
				case SENDRECV_RESULTSCODE_OK :
					if (ReaderReply[READERREPLY_STATUSOFFSET+1] != 0)
						return PCD_SUCCESSCODE;
					else
						return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_RESULTSRESIDUAL :
					return PCD_SUCCESSCODE;
					
				case SENDRECV_ERRORCODE_COMERROR :
					return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_ERRORCODE_FRAMEWAIT :
					return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_ERRORCODE_SOF :
					return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_ERRORCODE_OVERFLOW :
					return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_ERRORCODE_FRAMING :
					return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_ERRORCODE_EGT :
					return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_ERRORCODE_LENGTH :
					return PCD_ERRORCODE_DEFAULT;
					
				case SENDRECV_ERRORCODE_CRC :
					return PCD_ERRORCODE_DEFAULT;
				case SENDRECV_ERRORCODE_RECEPTIONLOST :
					return PCD_ERRORCODE_DEFAULT;
					
				default :
					return PCD_ERRORCODE_DEFAULT;
					
			}
			
		case IDLE: 
			switch (ReaderReply[READERREPLY_STATUSOFFSET])
			{
				case IDLE_RESULTSCODE_OK :
					return PCD_SUCCESSCODE;
					
				case IDLE_ERRORCODE_LENGTH :
					return PCD_ERRORCODE_DEFAULT;
					
				default : return PCD_ERRORCODE_DEFAULT;
				
			}
			
		case READ_REGISTER: 
			switch (ReaderReply[READERREPLY_STATUSOFFSET])
			{
				case READREG_RESULTSCODE_OK :
					return PCD_SUCCESSCODE;
					
				case READREG_ERRORCODE_LENGTH :
					return PCD_ERRORCODE_DEFAULT;
					
				default : return PCD_ERRORCODE_DEFAULT;
				
			}
			
		case WRITE_REGISTER: 
			switch (ReaderReply[READERREPLY_STATUSOFFSET])
			{
				case WRITEREG_RESULTSCODE_OK :
					return PCD_SUCCESSCODE;
					
				default : return PCD_ERRORCODE_DEFAULT;
				
			}
			
		case BAUD_RATE: 
			return PCD_ERRORCODE_DEFAULT;
			
		default: 
			return ERRORCODE_GENERIC;
			
	}
}

/**
 *	@brief  this function send a ProtocolSeclect command to the PCD device
 *  @param  Length  : number of byte of protocol select command parameters
 *  @param  Protocol : RF protocol (ISO 14443 A or B or 15 693 or Fellica)
 *  @param  Parameters: prococol parameters (see reader datasheet)
 *  @param  pResponse : pointer on the PCD device response
 *  @return PCD_SUCCESSCODE : the command was succedfully send
 *  @return PCD_ERRORCODE_PARAMETERLENGTH : the Length parameter is erroneous
 *  @return PCD_ERRORCODE_PARAMETER : a parameter is erroneous
 */
int8_t gPCD_ProtocolSelect(const uint8_t Length,const uint8_t Protocol,const uint8_t *Parameters,uint8_t *pResponse)
{
	uint8_t DataToSend[SELECT_BUFFER_SIZE];
	int8_t	status; 
	uint8_t u95HFBuffer[10];
        
	/* check ready to receive Protocol select command */
	gCR95HF_Echo(u95HFBuffer);
	
        ptest[0] = u95HFBuffer[0];
        
	if (u95HFBuffer[0] != ECHORESPONSE)
	{
		/* reset the device */
		gCR95HF_PORsequence( );
	}

		/* initialize the result code to 0xFF and length to in case of error 	*/
	*pResponse = PCD_ERRORCODE_DEFAULT;
	*(pResponse+1) = 0x00;
	
	/* check the function parameters */
	errchk(gIsAnAvailableProtocol(Protocol));

	DataToSend[PCD_COMMAND_OFFSET ] = PROTOCOL_SELECT;
	DataToSend[PCD_LENGTH_OFFSET  ]	= Length;
	DataToSend[PCD_DATA_OFFSET    ]	= Protocol;

        ptest[PCD_COMMAND_OFFSET] = DataToSend[PCD_COMMAND_OFFSET ];
        ptest[PCD_LENGTH_OFFSET] = DataToSend[PCD_LENGTH_OFFSET ];
        ptest[PCD_DATA_OFFSET] = DataToSend[PCD_DATA_OFFSET ];
        
	/* DataToSend CodeCmd Length Data	*/
	/* Parameters[0] first byte to emmit	*/
	memcpy(&(DataToSend[PCD_DATA_OFFSET +1]),Parameters,Length-1);
	
	/* Send the command the Rf transceiver	*/
 	gRFIDSendReceive(DataToSend, pResponse);

	return PCD_SUCCESSCODE;

Error:
	return PCD_ERRORCODE_PARAMETER;	
}


/**
 *	@brief  this function send a ProtocolSeclect command to CR95HF
 *  @param  Length  : number of byte of protocol select command parameters
 *  @param  Protocol : RF protocol (ISO 14443 A or B or 15 693 or Fellica)
 *  @param  Parameters: prococol parameters (see reader datasheet)
 *  @param  pResponse : pointer on CR95HF response
 *  @return CR95HF_SUCCESS_CODE : the command was succedfully send
 *  @return CR95HF_ERRORCODE_PARAMETERLENGTH : the Length parameter is erroneous
 *  @return CR95HF_ERRORCODE_PARAMETER : a parameter is erroneous
 *//*
uint8_t gCR95HF_ProtocolSelect(uint8_t Length,uint8_t Protocol,uint8_t *Parameters,uint8_t *pResponse)
{
uint8_t DataToSend[SELECT_BUFFER_SIZE],
		SelectParameters[SELECT_BUFFER_SIZE];
uint8_t	status,
		i=0; 

	if (CHECKVAL(Length,1,SELECT_BUFFER_SIZE) == false)
		return CR95HF_ERRORCODE_PARAMETERLENGTH;

	// check the function parameters
	errchk(IsAnAvailableProtocol(Protocol));
	errchk(IsAnAvailableSelectLength(Protocol,Length));
	errchk(IsAnAvailableSelectParameters(Protocol,Length,Parameters));

	memcpy(SelectParameters,Parameters,Length);

	// force the RFU bits to 0
	errchk(ForceSelectRFUBitsToNull	(Protocol,Length,SelectParameters));

	DataToSend[CR95HF_COMMAND_OFFSET ] = PROTOCOL_SELECT;
	DataToSend[CR95HF_LENGTH_OFFSET  ]	= Length;
	DataToSend[CR95HF_DATA_OFFSET    ]	= Protocol;

	// DataToSend CodeCmd Length Data
	// Parameters[0] first byte to emmit
	for (i = 0; i< Length -1  ;i++)
		DataToSend[CR95HF_DATA_OFFSET +1 + i ] = SelectParameters[i];

  	SPIUART_SendReceive(DataToSend, pResponse);

	return CR95HF_SUCCESS_CODE;

Error:
	// initialize the result code to 0xFF and length to in case of error 
	*pResponse = CR95HF_ERRORCODE_DEFAULT;
	*(pResponse+1) = 0x00;
	return CR95HF_ERRORCODE_PARAMETER;	
}
*/

/**
 *	@brief  this functions returns CR95HF_SUCCESS_CODE if the protocol is available, otherwise CR95HF_ERRORCODE_PARAMETER
 *  @param  Protocol : RF protocol (ISO 14443 A or 14443 B or 15 693 or Fellica)
 *  @return CR95HF_SUCCESS_CODE	: the protocol is available
 *  @return CR95HF_ERRORCODE_PARAMETER : the protocol isn't available
 */
static uint8_t gIsAnAvailableProtocol (uint8_t Protocol) 
{
	switch(Protocol)
	{
		case PROTOCOL_TAG_FIELDOFF:
			return CR95HF_SUCCESS_CODE;
		case PROTOCOL_TAG_ISO15693:
			return CR95HF_SUCCESS_CODE;
		case PROTOCOL_TAG_ISO14443A:
			return CR95HF_SUCCESS_CODE;
		case PROTOCOL_TAG_ISO14443B:
			return CR95HF_SUCCESS_CODE;
		case PROTOCOL_TAG_FELICA:
			return CR95HF_SUCCESS_CODE;
		default: return CR95HF_ERRORCODE_PARAMETER;
	}	
}

/**
 *	@brief  this function send a command to CR95HF device over SPI or UART bus and reveive its response
 *  @param  *pCommand  : pointer on the buffer to send to the CR95HF ( Command | Length | Data)
 *  @param  *pResponse : pointer on the CR95HF response ( Command | Length | Data)
 *  @retval 
 */
uint8_t gRFIDSendReceive(uint8_t *pCommand, uint8_t *pResponse)
{

	*pResponse =CR95HF_ERRORCODE_DEFAULT;
	*(pResponse+1) = 0x00;


		// First step  - Sending command
		gCR95HF_Send_UART_Command(pCommand);
                resptest[0]=pCommand[0];
                resptest[1]=pCommand[1];
                osDelay(5);
		// Second step - Receiving bytes 
		gCR95HF_Receive_UART_Response(pResponse);
                resptest[0]=pResponse[0];
                resptest[1]=pResponse[1];
	// update global variable case of shadow mode
	//SpyTransaction(pCommand,pResponse);
	return CR95HF_SUCCESS_CODE; 
}

/**												   
 *	@brief  this function send a command to CR95HF device over SPI bus
 *  @param  *pData : pointer on data to send to the CR95HF
 *  @return None
 */
static void gCR95HF_Send_UART_Command(uint8_t *pData)
{
  uint8_t cChar;
  
  if(pData[0] == ECHO){
		// send Echo 
		//UART_SendByte(CR95HF_UART, ECHO);
                if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
                  {
                    cChar = pData[0];      
                    xRFIDSerialPutChar( xPort, cChar, ( ( portTickType ) 0 ) );
                    xSemaphoreGive( xRFIDTxMutex );
                  }
  }else
		// send the buffer over UART 
		//UART_SendBuffer(CR95HF_UART, pData, pData[CR95HF_LENGTH_OFFSET] + CR95HF_DATA_OFFSET);
                if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
                    {
                      
                            vRFIDSerialPutString2( xPort, ( signed char * ) pData, ( unsigned short ) (pData[1]+2) );
                            xSemaphoreGive( xRFIDTxMutex );
                            
                    }
                         //   HAL_UART_Transmit(&huart6, pData, (pData[CR95HF_LENGTH_OFFSET] + CR95HF_DATA_OFFSET),100);
        
}

/**
 *	@brief  this functions recovers a response from CR95HF device over SPI bus
 *  @param  *pData : pointer on data received from CR95HF device
 *  @return None
 */
static void gCR95HF_Receive_UART_Response(uint8_t *pData)
{
  
  RxDataPacketTypeDef   RxRFIDPacket0;
  uint8_t               ir=0;
  
        
        if ( uxQueueMessagesWaiting( xRFIDPackedRX ) != 0) {
          if( xQueueReceive( xRFIDPackedRX, &( RxRFIDPacket0 ), ( TickType_t ) 10 ) )
              {
                  // pcRxedMessage now points to the struct AMessage variable posted
                  // by vATask.
                tRFIDPacket0 = RxRFIDPacket0;
                if (RxRFIDPacket0.Command == ECHO) {
                  pData[0] = RxRFIDPacket0.Command;
                } else {
                  pData[0] = RxRFIDPacket0.Command;
                  pData[1] = RxRFIDPacket0.Length;                      
                  for (ir=0;ir<RxRFIDPacket0.Length;ir++){
                    pData[ir+2] = RxRFIDPacket0.Data[ir];
                  }   
                } 
              }
        }               

}



void vRFIDOutputString( const char * const pcMessage )
{
	if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
	{
		vRFIDSerialPutString( xPort, ( signed char * ) pcMessage, ( unsigned short ) strlen( pcMessage ) );
		xSemaphoreGive( xRFIDTxMutex );
	}
}
/*  
void RFID_Operation(void){
  

  	/* UID : unique Identification*//*
  uint8_t 	UIDsizex,i;
  uint8_t 	UIDx[ISO14443A_MAX_UID_SIZE];
  rfidelay++; 
  if (rfidelay == 100){
    
    for (i=0;i<16;i++){
      TagUIDx[i] = 0;
    }
    
    PCD_FieldOff();
    HAL_Delay(5);
    
   //////ISO15693 *//*
    
   //ReadISO15693();
   ISO15693_GetUID (TagUIDx); 
    
  }
   
  if (rfidelay == 200){
  
   //////ISO14443A */
   /* PCD_FieldOff();
    HAL_Delay(5); 
    
    ISO14443A_Init( );
    if(ISO14443A_IsPresent() == RESULTOK)
    {			
      if(ISO14443A_Anticollision() == RESULTOK)
      {	
        
        UIDsizex = ISO14443A_Card.UIDsize;
        
        UIDx[0] = ISO14443A_Card.UID[0];
      }
    }
  }
    //////ISO14443B */
    /******* NFC type 4B ********/

     /*
    if(ISO14443B_IsPresent() == RESULTOK )
    {
      if(ISO14443B_Anticollision() == RESULTOK)
      {
        ISO14443B_Card.
      }// */
 //   }

    
      
  
    //*/
//}
      
      



/*
void RFID_Operation(void){
  
uint8_t pResponse[10];
 

    
    
    testResponse[0] = rfResponse[0];

  rfidelay++; 
  if (rfidelay == 100){
    rfidelay = 0;
        // Scan to find if there is a tag 
    //TagType = ConfigManager_TagHunting(TRACK_ALL);

    TagType = ConfigManager_TagHunting(TRACK_NFCTYPE2);

  
  
   
    switch(TagType)
    {
    case TRACK_NFCTYPE1:
      {
        //LED_Off(GREEN_LED1);
        //LED_On(BLUE_LED2);
        //LED_On(GREEN_LED3);
        //LED_On(BLUE_LED4);
        
        TagDetected = true;
        
        if(terminal_msg_flag == true )
        {
          terminal_msg_flag = false ;
          //---HT UI msg----------//
          //sprintf( dataOut, "\r\n\r\nTRACK_NFCTYPE1 NFC tag detected nearby");
          //HAL_UART_Transmit( &UartHandle, ( uint8_t * )dataOut, strlen( dataOut ), 500 );
        }
        
      }
      break;
    case TRACK_NFCTYPE2:
      {	
        //LED_Off(GREEN_LED1);
        //LED_On(BLUE_LED2);
        TagDetected = true;
        
        if(terminal_msg_flag == true )
        {
          terminal_msg_flag = false ;
          
          //---HT UI msg----------///
          //sprintf( dataOut, "\r\n\r\nTRACK_NFCTYPE2 NFC tag detected nearby");
          //HAL_UART_Transmit( &UartHandle, ( uint8_t * )dataOut, strlen( dataOut ), 500 );
        }
      }
      break;
      
    case TRACK_NFCTYPE3:
      {
       // LED_Off(GREEN_LED1);
        //LED_On(GREEN_LED3);
        TagDetected = true;
        
        if(terminal_msg_flag == true )
        {
          terminal_msg_flag = false ;
          
          //---HT UI msg----------
         // sprintf( dataOut, "\r\n\r\nTRACK_NFCTYPE3 NFC tag detected nearby");
         // HAL_UART_Transmit( &UartHandle, ( uint8_t * )dataOut, strlen( dataOut ), 500 );
        }
      }
      break;
      
    case TRACK_NFCTYPE4A:
      {
        //LED_Off(GREEN_LED1);
        //LED_On(BLUE_LED4);
        TagDetected = true;
        
        if(terminal_msg_flag == true )
        {
          terminal_msg_flag = false ;
          
          //---HT UI msg----------
        //  sprintf( dataOut, "\r\n\r\nTRACK_NFCTYPE4A NFC tag detected nearby");
         // HAL_UART_Transmit( &UartHandle, ( uint8_t * )dataOut, strlen( dataOut ), 500 );
        }
      }
      break;
      
    case TRACK_NFCTYPE4B:
      {
        /// LED_Off(GREEN_LED1);
        //LED_On(GREEN_LED3); 
        //LED_On(BLUE_LED4);
        TagDetected = true;
        
        if(terminal_msg_flag == true )
        {
          terminal_msg_flag = false ;
          
          //---HT UI msg----------
          //sprintf( dataOut, "\r\n\r\nTRACK_NFCTYPE4B NFC tag detected nearby");
          //HAL_UART_Transmit( &UartHandle, ( uint8_t * )dataOut, strlen( dataOut ), 500 );
        }
      }
      break;
    case TRACK_NFCTYPE5:
      {
       // LED_Off(GREEN_LED1);
        //LED_On(BLUE_LED2);
        //LED_On(GREEN_LED3);
        TagDetected = true;
        
        if(terminal_msg_flag == true )
        {
          terminal_msg_flag = false ;
          
          //---HT UI msg----------
          //sprintf( dataOut, "\r\n\r\nTRACK_NFCTYPE5 NFC tag detected nearby");
          //HAL_UART_Transmit( &UartHandle, ( uint8_t * )dataOut, strlen( dataOut ), 500 );
        }
      }
      break;
      
    default:
      //LED_Off(BLUE_LED2);
      //LED_Off(GREEN_LED3);
      //LED_Off(BLUE_LED4);
      //LED_Toggle(GREEN_LED1);/
      //terminal_display_enabled = 0;
      TagDetected = false;
      
      if(terminal_msg_flag == false)
      {
        terminal_msg_flag = true ;
        //---HT UI msg----------/
        //sprintf( dataOut, "\r\n\r\nCurrently there is no NFC tag in the vicinity");
        //HAL_UART_Transmit( &UartHandle, ( uint8_t * )dataOut, strlen( dataOut ), 500 );
      }
      break;
    }
  
  }

}   */