/*
FreeRTOS V6.1.1 - Copyright (C) 2011 Real Time Engineers Ltd.

***************************************************************************
*                                                                         *
* If you are:                                                             *
*                                                                         *
*    + New to FreeRTOS,                                                   *
*    + Wanting to learn FreeRTOS or multitasking in general quickly       *
*    + Looking for basic training,                                        *
*    + Wanting to improve your FreeRTOS skills and productivity           *
*                                                                         *
* then take a look at the FreeRTOS books - available as PDF or paperback  *
*                                                                         *
*        "Using the FreeRTOS Real Time Kernel - a Practical Guide"        *
*                  http://www.FreeRTOS.org/Documentation                  *
*                                                                         *
* A pdf reference manual is also available.  Both are usually delivered   *
* to your inbox within 20 minutes to two hours when purchased between 8am *
* and 8pm GMT (although please allow up to 24 hours in case of            *
* exceptional circumstances).  Thank you for your support!                *
*                                                                         *
***************************************************************************

This file is part of the FreeRTOS distribution.

FreeRTOS is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License (version 2) as published by the
Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
***NOTE*** The exception to the GPL is included to allow you to distribute
a combined work that includes FreeRTOS without being obliged to provide the
source code for proprietary components outside of the FreeRTOS kernel.
FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details. You should have received a copy of the GNU General Public
License and the FreeRTOS license exception along with FreeRTOS; if not it
can be viewed here: http://www.freertos.org/a00114.html and also obtained
by writing to Richard Barry, contact details for whom are available on the
FreeRTOS WEB site.

1 tab == 4 spaces!

http://www.FreeRTOS.org - Documentation, latest information, license and
contact details.

http://www.SafeRTOS.com - A version that is certified for use in safety
critical systems.

http://www.OpenRTOS.com - Commercial support, development, porting,
licensing and training services.
*/

/*
BASIC INTERRUPT DRIVEN SERIAL PORT DRIVER FOR UART0.

***Note*** This example uses queues to send each character into an interrupt
service routine and out of an interrupt service routine individually.  This
is done to demonstrate queues being used in an interrupt, and to deliberately
load the system to test the FreeRTOS port.  It is *NOT* meant to be an 
example of an efficient implementation.  An efficient implementation should
use FIFO's or DMA if available, and only use FreeRTOS API functions when 
enough has been received to warrant a task being unblocked to process the
data.
*/

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "rfid.h"

/* Library includes. */
//#include "stm32l152_eval.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx.h"
#include "stm32f4xx_it.h"
#include "cmsis_os.h"


/* Demo application includes. */
#include "serial.h"
/*-----------------------------------------------------------*/

/* Misc defines. */
#define serINVALID_QUEUE				( ( xQueueHandle ) 0 )
#define serNO_BLOCK						( ( portTickType ) 0 )

/*-----------------------------------------------------------*/

/* The queue used to hold received characters. */
static xQueueHandle xRxedChars;
static xQueueHandle xCharsForTx;
static xQueueHandle xBLERxedChars;
static xQueueHandle xBLECharsForTx;
static xQueueHandle xRFIDRxedChars;
static xQueueHandle xRFIDCharsForTx;



extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart6;

char            rxchar[100];
uint16_t        x = 0;
char            Rx_indx, Transfer_cplt;
uint8_t         uart_buf[UART_DMA_BUFFER_SIZE]; 
uint8_t         rfid_buf[UART_DMA_BUFFER_SIZE];
uint16_t        uart_buffer_count;
/*-----------------------------------------------------------*/

/*
xSerialPortInitMinimal
Initialitzation of the uart1 port to get the serial input signal.
*/
xComPortHandle xSerialPortInitMinimal( unsigned portLONG ulWantedBaud, unsigned portBASE_TYPE uxQueueLength )
{
  //USART_InitTypeDef USART_InitStructure;
  xComPortHandle xReturn;
  //NVIC_InitTypeDef NVIC_InitStructure;
  //char string[200];
  
  //string[0]='h';
  
  /* Create the queues used to hold Rx/Tx characters. */
  xRxedChars = xQueueCreate( uxQueueLength, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );
  xCharsForTx = xQueueCreate( uxQueueLength + 1, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );
  
  vQueueAddToRegistry(xRxedChars, "RX_UART_QUEUE");
  vQueueAddToRegistry(xCharsForTx, "TX_UART_QUEUE");
  //HAL_UART_Transmit(&huart1, (uint8_t*)string, strlen(string), 0xffffff);
  
  /* If the queues were created correctly then setup the serial port
  hardware. */
  if( ( xRxedChars != serINVALID_QUEUE ) && ( xCharsForTx != serINVALID_QUEUE ) )
  {
    HAL_UART_Receive_DMA(&huart1,uart_buf,UART_DMA_BUFFER_SIZE);
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
  }
  else
  {
    xReturn = ( xComPortHandle ) 0;
  }
  
  /* This demo file only supports a single port but we have to return
  something to comply with the standard demo header file. */
  return xReturn;
}
/*-----------------------------------------------------------*/

/*
xBLESerialPortInitMinimal
Initialitzation of the uart3 port to get the Bluetooth input signal.
*/
xComPortHandle xBLESerialPortInitMinimal( unsigned portLONG ulWantedBaud, unsigned portBASE_TYPE uxQueueLength )
{
  //USART_InitTypeDef USART_InitStructure;
  xComPortHandle xReturn;
  //NVIC_InitTypeDef NVIC_InitStructure;
  //char string[200];
  
  //string[0]='h';
  
  /* Create the queues used to hold Rx/Tx characters. */
  xBLERxedChars = xQueueCreate( uxQueueLength, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );
  xBLECharsForTx = xQueueCreate( uxQueueLength + 1, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );
  
  vQueueAddToRegistry(xBLERxedChars, "RX_BLE_QUEUE");
  vQueueAddToRegistry(xBLECharsForTx, "TX_BLE_QUEUE");
  //HAL_UART_Transmit(&huart1, (uint8_t*)string, strlen(string), 0xffffff);
  
  /* If the queues were created correctly then setup the serial port
  hardware. */
  if( ( xBLERxedChars != serINVALID_QUEUE ) && ( xBLECharsForTx != serINVALID_QUEUE ) )
  {
    HAL_UART_Receive_DMA(&huart3,uart_buf,UART_DMA_BUFFER_SIZE);
    __HAL_UART_ENABLE_IT(&huart3, UART_IT_IDLE);
  }
  else
  {
    xReturn = ( xComPortHandle ) 0;
  }
  
  /* This demo file only supports a single port but we have to return
  something to comply with the standard demo header file. */
  return xReturn;
}

/*-----------------------------------------------------------*/

/*
xRFIDSerialPortInitMinimal
Initialitzation of the uart6 port to get the RFID management.
*/
xComPortHandle xRFIDSerialPortInitMinimal( unsigned portLONG ulWantedBaud, unsigned portBASE_TYPE uxQueueLength )
{
  //USART_InitTypeDef USART_InitStructure;
  xComPortHandle xReturn;
  //NVIC_InitTypeDef NVIC_InitStructure;
  //char string[200];
  
  //string[0]='h';
  
  /* Create the queues used to hold Rx/Tx characters. */
  xRFIDRxedChars = xQueueCreate( uxQueueLength, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );
  xRFIDCharsForTx = xQueueCreate( uxQueueLength + 1, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );
  
  //xRFIDPackedRX = xQueueGenericCreate( 5, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );
  
  vQueueAddToRegistry(xRFIDRxedChars, "RX_RFID_QUEUE");
  vQueueAddToRegistry(xRFIDCharsForTx, "TX_RFID_QUEUE");
  
  //HAL_UART_Transmit(&huart1, (uint8_t*)string, strlen(string), 0xffffff);
  
  /* If the queues were created correctly then setup the serial port
  hardware. */
  if( ( xRFIDRxedChars != serINVALID_QUEUE ) && ( xRFIDCharsForTx != serINVALID_QUEUE ) )
  {
    HAL_UART_Receive_DMA(&huart6,rfid_buf,UART_DMA_BUFFER_SIZE);
    __HAL_UART_ENABLE_IT(&huart6, UART_IT_IDLE);
  }
  else
  {
    xReturn = ( xComPortHandle ) 0;
  }
  
  /* This demo file only supports a single port but we have to return
  something to comply with the standard demo header file. */
  return xReturn;
}


signed portBASE_TYPE xSerialGetChar( xComPortHandle pxPort, signed portCHAR *pcRxedChar, portTickType xBlockTime )
{
  /* The port handle is not required as this driver only supports one port. */
  ( void ) pxPort;
  
  /* Get the next character from the buffer.  Return false if no characters
  are available, or arrive before xBlockTime expires. */
  if( xQueueReceive( xRxedChars, pcRxedChar, xBlockTime ) )
  {
    return pdTRUE;
  }
  else
  {
    return pdFALSE;
  }
}
/*-----------------------------------------------------------*/


signed portBASE_TYPE xBLESerialGetChar( xComPortHandle pxPort, signed portCHAR *pcRxedChar, portTickType xBlockTime )
{
  /* The port handle is not required as this driver only supports one port. */
  ( void ) pxPort;
  
  /* Get the next character from the buffer.  Return false if no characters
  are available, or arrive before xBlockTime expires. */
  if( xQueueReceive( xBLERxedChars, pcRxedChar, xBlockTime ) )
  {
    return pdTRUE;
  }
  else
  {
    return pdFALSE;
  }
}
/*-----------------------------------------------------------*/

signed portBASE_TYPE xRFIDSerialGetChar( xComPortHandle pxPort, signed portCHAR *pcRxedChar, portTickType xBlockTime )
{
  /* The port handle is not required as this driver only supports one port. */
  ( void ) pxPort;
  
  /* Get the next character from the buffer.  Return false if no characters
  are available, or arrive before xBlockTime expires. */
  if( xQueueReceive( xRFIDRxedChars, pcRxedChar, xBlockTime ) )
  {
    return pdTRUE;
  }
  else
  {
    return pdFALSE;
  }
}
/*-----------------------------------------------------------*/


void vSerialPutString( xComPortHandle pxPort, const signed portCHAR * const pcString, unsigned portSHORT usStringLength )
{
  signed portCHAR *pxNext;
  
  /* A couple of parameters that this port does not use. */
  ( void ) usStringLength;
  ( void ) pxPort;
  
  /* NOTE: This implementation does not handle the queue being full as no
  block time is used! */
  
  /* The port handle is not required as this driver only supports UART1. */
  ( void ) pxPort;
  
  /* Send each character in the string, one at a time. */
  pxNext = ( signed portCHAR * ) pcString;
  while( *pxNext )
  {
    xSerialPutChar( pxPort, *pxNext, serNO_BLOCK );
    pxNext++;
  }
}
/*-----------------------------------------------------------*/

signed portBASE_TYPE xSerialPutChar( xComPortHandle pxPort, signed portCHAR cOutChar, portTickType xBlockTime )
{
  signed portBASE_TYPE xReturn;
  
  if( xQueueSend( xCharsForTx, &cOutChar, xBlockTime ) == pdPASS )
  {
    xReturn = pdPASS;
    
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_TXE);
    
    
  }
  else
  {
    xReturn = pdFAIL;
  }
  
  return xReturn;
}
/*-----------------------------------------------------------*/

void vBLESerialPutString( xComPortHandle pxPort, const signed portCHAR * const pcString, unsigned portSHORT usStringLength )
{
  signed portCHAR *pxNext;
  
  /* A couple of parameters that this port does not use. */
  ( void ) usStringLength;
  ( void ) pxPort;
  
  /* NOTE: This implementation does not handle the queue being full as no
  block time is used! */
  
  /* The port handle is not required as this driver only supports UART1. */
  ( void ) pxPort;
  
  /* Send each character in the string, one at a time. */
  pxNext = ( signed portCHAR * ) pcString;
  while( *pxNext )
  {
    xBLESerialPutChar( pxPort, *pxNext, serNO_BLOCK );
    pxNext++;
  }
}
/*-----------------------------------------------------------*/

signed portBASE_TYPE xBLESerialPutChar( xComPortHandle pxPort, signed portCHAR cOutChar, portTickType xBlockTime )
{
  signed portBASE_TYPE xReturn;
  
  if( xQueueSend( xBLECharsForTx, &cOutChar, xBlockTime ) == pdPASS )
  {
    xReturn = pdPASS;
    
    __HAL_UART_ENABLE_IT(&huart3, UART_IT_TXE);
    
    
  }
  else
  {
    xReturn = pdFAIL;
  }
  
  return xReturn;
}
/*-----------------------------------------------------------*/



void vRFIDSerialPutString( xComPortHandle pxPort, const signed portCHAR * const pcString, unsigned portSHORT usStringLength )
{
  signed portCHAR *pxNext;
  
  /* A couple of parameters that this port does not use. */
  ( void ) usStringLength;
  ( void ) pxPort;
  
  /* NOTE: This implementation does not handle the queue being full as no
  block time is used! */
  
  /* The port handle is not required as this driver only supports UART1. */
  ( void ) pxPort;
  
  /* Send each character in the string, one at a time. */
  pxNext = ( signed portCHAR * ) pcString;
  while( *pxNext )
  {
    xRFIDSerialPutChar( pxPort, *pxNext, serNO_BLOCK );
    pxNext++;
  }
}

void vRFIDSerialPutString2( xComPortHandle pxPort, const signed portCHAR * const pcString, unsigned portSHORT usStringLength )
{
  signed portCHAR *pxNext;
  
  /* A couple of parameters that this port does not use. */
  ( void ) usStringLength;
  ( void ) pxPort;
  
  /* NOTE: This implementation does not handle the queue being full as no
  block time is used! */
  
  /* The port handle is not required as this driver only supports UART1. */
  ( void ) pxPort;
  
  /* Send each character in the string, one at a time. */
  pxNext = ( signed portCHAR * ) pcString;
  //while( *pxNext )
  while(usStringLength > 0)
  {
    xRFIDSerialPutChar( pxPort, *pxNext, serNO_BLOCK );
    pxNext++;
    usStringLength--;
  }
}
/*-----------------------------------------------------------*/

signed portBASE_TYPE xRFIDSerialPutChar( xComPortHandle pxPort, signed portCHAR cOutChar, portTickType xBlockTime )
{
  signed portBASE_TYPE xReturn;
  
  if( xQueueSend( xRFIDCharsForTx, &cOutChar, xBlockTime ) == pdPASS )
  {
    xReturn = pdPASS;
    
    __HAL_UART_ENABLE_IT(&huart6, UART_IT_TXE);
    
    
  }
  else
  {
    xReturn = pdFAIL;
  }
  
  return xReturn;
}
/*-----------------------------------------------------------*/


void vSerialClose( xComPortHandle xPort )
{
  /* Not supported as not required by the demo application. */
}
/*-----------------------------------------------------------*/


//// DEBUG UART 1 SERIAL Interrupt handler
void USART1_CLI_IRQHandler( void )
{
  portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
  portCHAR cChar;
  uint16_t y;
  
  //if( USART_GetITStatus( USART1, USART_IT_TXE ) == SET )
  if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_TXE))
  {
    /* The interrupt was caused by the TX register becoming empty.  Are 
    there any more characters to transmit? */
    if( xQueueReceiveFromISR( xCharsForTx, &cChar, &xHigherPriorityTaskWoken ) == pdTRUE )
    {
      /* A character was retrieved from the queue so can be sent to the
      USART now. */
      HAL_UART_Transmit(&huart1, &cChar, 1,100);
    }
    else
    {
      __HAL_UART_DISABLE_IT(&huart1, UART_IT_TXE);
      __HAL_UART_CLEAR_FLAG(&huart1, UART_FLAG_TC);
      __HAL_UART_CLEAR_FLAG(&huart1, UART_FLAG_TXE);		
    }		
  }
  
  if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_RXNE))
  {
    /* A character has been received on the USART, send it to the Rx
    handler task. */
    
  }	
  
  if (__HAL_UART_GET_FLAG(&huart1, UART_FLAG_IDLE)){
    
    uart_buffer_count = UART_DMA_BUFFER_SIZE - (huart1.hdmarx->Instance->NDTR);
    
    for (y=0;y<uart_buffer_count;y++){
      
      cChar =  (char) uart_buf[y];
      xQueueSendFromISR( xRxedChars, &cChar, &xHigherPriorityTaskWoken );                      
      
    }  
    
    HAL_UART_DMAStop(&huart1);
    HAL_UART_Receive_DMA(&huart1,uart_buf,UART_DMA_BUFFER_SIZE);
    
  }
  
  /* If sending or receiving from a queue has caused a task to unblock, and
  the unblocked task has a priority equal to or higher than the currently 
  running task (the task this ISR interrupted), then xHigherPriorityTaskWoken 
  will have automatically been set to pdTRUE within the queue send or receive 
  function.  portEND_SWITCHING_ISR() will then ensure that this ISR returns 
  directly to the higher priority unblocked task. */
  portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}


//// BLE UART 1 SERIAL Interrupt handler
void USART3_CLI_IRQHandler( void )
{
  portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
  portCHAR cChar;
  uint16_t y;
  
  if (__HAL_UART_GET_FLAG(&huart3, UART_FLAG_TXE))
  {
    /* The interrupt was caused by the TX register becoming empty.  Are 
    there any more characters to transmit? */
    if( xQueueReceiveFromISR( xBLECharsForTx, &cChar, &xHigherPriorityTaskWoken ) == pdTRUE )
    {
      /* A character was retrieved from the queue so can be sent to the
      USART now. */
      HAL_UART_Transmit(&huart3, &cChar, 1,100);
    }
    else
    {
      __HAL_UART_DISABLE_IT(&huart3, UART_IT_TXE);
      __HAL_UART_CLEAR_FLAG(&huart3, UART_FLAG_TC);
      __HAL_UART_CLEAR_FLAG(&huart3, UART_FLAG_TXE);		
    }		
  }
  
  if (__HAL_UART_GET_FLAG(&huart3, UART_FLAG_RXNE))
  {
    /* A character has been received on the USART, send it to the Rx
    handler task. */
    
  }	
  
  if (__HAL_UART_GET_FLAG(&huart3, UART_FLAG_IDLE)){
    
    uart_buffer_count = UART_DMA_BUFFER_SIZE - (huart3.hdmarx->Instance->NDTR);
    
    for (y=0;y<uart_buffer_count;y++){
      cChar =  (char) uart_buf[y];
      xQueueSendFromISR( xBLERxedChars, &cChar, &xHigherPriorityTaskWoken );                      
    }  
    
    HAL_UART_DMAStop(&huart3);
    HAL_UART_Receive_DMA(&huart3,uart_buf,UART_DMA_BUFFER_SIZE);
  }
  
  /* If sending or receiving from a queue has caused a task to unblock, and
  the unblocked task has a priority equal to or higher than the currently 
  running task (the task this ISR interrupted), then xHigherPriorityTaskWoken 
  will have automatically been set to pdTRUE within the queue send or receive 
  function.  portEND_SWITCHING_ISR() will then ensure that this ISR returns 
  directly to the higher priority unblocked task. */
  portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}


//// RFID UART 1 SERIAL Interrupt handler
void USART6_RFID_IRQHandler( void )
{
  portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
  portCHAR cChar;
  uint16_t y;
  
  if (__HAL_UART_GET_FLAG(&huart6, UART_FLAG_TXE))
  {
    /* The interrupt was caused by the TX register becoming empty.  Are 
    there any more characters to transmit? */
    if( xQueueReceiveFromISR( xRFIDCharsForTx, &cChar, &xHigherPriorityTaskWoken ) == pdTRUE )
    {
      /* A character was retrieved from the queue so can be sent to the
      USART now. */
      HAL_UART_Transmit(&huart6, &cChar, 1,100);
    }
    else
    {
      __HAL_UART_DISABLE_IT(&huart6, UART_IT_TXE);
      __HAL_UART_CLEAR_FLAG(&huart6, UART_FLAG_TC);
      __HAL_UART_CLEAR_FLAG(&huart6, UART_FLAG_TXE);		
    }		
  }
  
  if (__HAL_UART_GET_FLAG(&huart6, UART_FLAG_RXNE))
  {
    /* A character has been received on the USART, send it to the Rx
    handler task. */
    
  }	
  
  if (__HAL_UART_GET_FLAG(&huart6, UART_FLAG_IDLE)){
    
    uart_buffer_count = UART_DMA_BUFFER_SIZE - (huart6.hdmarx->Instance->NDTR);
    
    for (y=0;y<uart_buffer_count;y++){
      
      cChar =  (char) rfid_buf[y];
      xQueueSendFromISR( xRFIDRxedChars, &cChar, &xHigherPriorityTaskWoken );                      
      
    }  
    
    HAL_UART_DMAStop(&huart6);
    HAL_UART_Receive_DMA(&huart6,rfid_buf,UART_DMA_BUFFER_SIZE);
    
  }
  
  /* If sending or receiving from a queue has caused a task to unblock, and
  the unblocked task has a priority equal to or higher than the currently 
  running task (the task this ISR interrupted), then xHigherPriorityTaskWoken 
  will have automatically been set to pdTRUE within the queue send or receive 
  function.  portEND_SWITCHING_ISR() will then ensure that this ISR returns 
  directly to the higher priority unblocked task. */
  portEND_SWITCHING_ISR( xHigherPriorityTaskWoken );
}
