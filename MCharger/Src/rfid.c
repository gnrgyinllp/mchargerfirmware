
/* rfid.c 
//////////////////////RFID TASK //////////////////////////////////////////////////////

Task implementation to read a module via UART with CR95hf ic
example modules:
- mikroe rfid click
- BM019: SERIAL TO NEAR FIELD COMMUNICATION (NFC)

read about uart code:
http://blog.solutions-cubed.com/near-field-communication-nfc-with-st-micro-cr95hf-part-2/

*/

/* RFID includes. */
#include "rfid.h"
#include "EVSEtest1.h"
#include "lib_CR95HF.h"
#include "StateMachine.h"
#include "stm32f4xx_hal.h"
#include "lib_95HFConfigManager.h"
#include "lib_pcd.h"



/* Standard includes. */
#include "string.h"
#include "stdio.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Example includes. */
#include "FreeRTOS_CLI.h"

/* Demo application includes. */
#include "serial.h"

/* Dimensions the buffer into which input characters are placed. */
#define cmdMAX_INPUT_SIZE		50

/* Dimentions a buffer to be used by the UART driver, if the UART driver uses a
buffer at all. */
#define cmdQUEUE_LENGTH			40

/* DEL acts as a backspace. */
#define cmdASCII_DEL		( 0x7F )

/* The maximum time to wait for the mutex that guards the UART to become
available. */
#define cmdMAX_MUTEX_WAIT		pdMS_TO_TICKS( 300 )

#ifndef configRFID_BAUD_RATE
#define configRFID_BAUD_RATE	57600
#endif

RxDataPacketTypeDef tRFIDPacket,tRFIDPacket0;
xQueueHandle xRFIDPackedRX;

//static char dataOut[COM_XFER_SIZE]; 
/*-----------------------------------------------------------*/

/*
* The task that implements the command console processing.
*/
static void prvRFIDOperationTask( void *pvParameters );
void vRFIDOperationTaskStart( uint16_t usStackSize, UBaseType_t uxPriority );

/*-----------------------------------------------------------*/
/* Const messages output by the command console. */
static const char * const pcWelcomeMessage = "\r\n\r\n-------------------------------------\r\n          GNRGY RTOS cli\r\n-------------------------------------\r\n \r\n          M EVSE code ver1.0\r\n\r\n\r\nType Help to view a list of registered commands.\r\n\r\n>";
static const char * const pcEndOfOutputMessage = "\r\n[Press ENTER to execute the previous command again]\r\n>";
static const char * const pcNewLine = "\r\n";
static char rfidOutputBuffer[ 40 ];

/* Used to guard access to the UART in case messages are sent to the UART from
more than one task. */
static SemaphoreHandle_t xRFIDTxMutex = NULL;

/* The handle to the UART port, which is not used by all ports. */
static xComPortHandle xPort = 0;

/*-----------------------------------------------------------*/

void vRFIDOperationTaskStart( uint16_t usStackSize, UBaseType_t uxPriority )
{
  /* Create the semaphore used to access the UART Tx. */
  xRFIDTxMutex = xSemaphoreCreateMutex();
  configASSERT( xRFIDTxMutex );
  
  xRFIDPackedRX = xQueueCreate( 1, TYPE_RFID_PACKET_SIZE );
  
  vQueueAddToRegistry(xRFIDPackedRX, "RXPACKET_RFID_QUEUE");
  configASSERT( xRFIDPackedRX );
  
  /* Create that task that handles the console itself. */
  xTaskCreate( 	prvRFIDOperationTask,	/* The task that implements the command console. */
              "RFID_Reading",						/* Text name assigned to the task.  This is just to assist debugging.  The kernel does not use this name itself. */
              usStackSize,				/* The size of the stack allocated to the task. */
              NULL,						/* The parameter is not used, so NULL is passed. */
              uxPriority,					/* The priority allocated to the task. */
              NULL );						/* A handle is not required, so just pass NULL. */
}
/*-----------------------------------------------------------*/

static void prvRFIDOperationTask( void *pvParameters )
{
  signed char cRxedChar;
  char     chartx;
  uint8_t ucInputIndex = 0;
  char *pcOutputString;
  static char cInputString[ cmdMAX_INPUT_SIZE ], cLastInputString[ cmdMAX_INPUT_SIZE ];
  BaseType_t xReturned;
  xComPortHandle xPort;
  RxDataPacketTypeDef RxRFIDPacket;
  
  ( void ) pvParameters;
  
  /* Obtain the address of the output buffer.  Note there is no mutual
  exclusion on this buffer as it is assumed only one command console interface
  will be used at any one time. */
  pcOutputString = rfidOutputBuffer;//FreeRTOS_CLIGetOutputBuffer();       
  
  /* Initialise the UART. */
  xPort = xRFIDSerialPortInitMinimal( configRFID_BAUD_RATE, cmdQUEUE_LENGTH );

  for( ;; )
  {         
    /* Wait for the next character.  The while loop is used in case
    INCLUDE_vTaskSuspend is not set to 1 - in which case portMAX_DELAY will
    be a genuine block time rather than an infinite block time. */
    while( xRFIDSerialGetChar( xPort, &cRxedChar, portMAX_DELAY ) != pdPASS );
 
    if ( ucInputIndex == 0) {
      RxRFIDPacket.Command = cRxedChar;                      
      if (cRxedChar == ECHO){            // check if it is a single command back of echo test          
        ucInputIndex=0;
        tRFIDPacket = RxRFIDPacket;
        xQueueSend( xRFIDPackedRX, &tRFIDPacket, ( TickType_t ) 0 );
      } else {
        ucInputIndex++;
      }
    } else if ( ucInputIndex == 1){       //check if it is a simple command without lenght
      RxRFIDPacket.Length = cRxedChar;                     
      if (cRxedChar == 0x00) {
        ucInputIndex=0;
        tRFIDPacket = RxRFIDPacket;
        xQueueSend( xRFIDPackedRX, &tRFIDPacket, ( TickType_t ) 0 );
        
        ////paket finished
      } else {
        ucInputIndex++;
      }
    } else {                  
      RxRFIDPacket.Data[ucInputIndex-2] = cRxedChar;  
      if (RxRFIDPacket.Length == (ucInputIndex-1)) {
        ucInputIndex=0;
        tRFIDPacket = RxRFIDPacket;
        xQueueSend( xRFIDPackedRX, &tRFIDPacket, ( TickType_t ) 0 );
        
        ////paket finished
        
      } else {
        ucInputIndex++;
      }
    }
    
    tRFIDPacket = RxRFIDPacket;
    if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
    {    
      /* Must ensure to give the mutex back. */
      xSemaphoreGive( xRFIDTxMutex );
    }
  }
}

extern UART_HandleTypeDef huart6;

void UART_SendByte(uint8_t data)
{
  if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
  {    
    xRFIDSerialPutChar( xPort, data, ( ( portTickType ) 0 ) );
    xSemaphoreGive( xRFIDTxMutex );
  }
}

void UART_SendBuffer(const uint8_t *pData)
{
  uint8_t cChar;
  
  if(pData[0] == ECHO)
  {
    
    if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
    {
      cChar = pData[0];      
      xRFIDSerialPutChar( xPort, cChar, ( ( portTickType ) 0 ) );
      xSemaphoreGive( xRFIDTxMutex );
    }
  }
  else 
  {
    
    if( xSemaphoreTake( xRFIDTxMutex, cmdMAX_MUTEX_WAIT ) == pdPASS )
    {
      
      vRFIDSerialPutString2( xPort, ( signed char * ) pData, ( unsigned short ) (pData[1]+2) );
      xSemaphoreGive( xRFIDTxMutex );
      
    }
  }
  
}

uint8_t UART_ReceiveByte(void)
{
  uint8_t data = 0;
  RxDataPacketTypeDef   RxRFIDPacket0;
  if ( uxQueueMessagesWaiting( xRFIDPackedRX ) != 0) {
    if( xQueueReceive( xRFIDPackedRX, &( RxRFIDPacket0 ), ( TickType_t ) 10 ) )
    {
      data = (RxRFIDPacket0.Command);
    }
  }    
  return data;
}

void UART_ReceiveBuffer	(uint8_t *pData)
{
  RxDataPacketTypeDef   RxRFIDPacket0;
  uint8_t               ir=0;
  
  
  while ( uxQueueMessagesWaiting( xRFIDPackedRX ) == 0);
  if( xQueueReceive( xRFIDPackedRX, &( RxRFIDPacket0 ), ( TickType_t ) 10 ) )
  {
    // pcRxedMessage now points to the struct AMessage variable posted
    // by vATask.
    tRFIDPacket0 = RxRFIDPacket0;
    if (RxRFIDPacket0.Command == ECHO) {
      pData[0] = RxRFIDPacket0.Command;
    } else {
      pData[0] = RxRFIDPacket0.Command;
      pData[1] = RxRFIDPacket0.Length;                      
      for (ir=0;ir<RxRFIDPacket0.Length;ir++){
        pData[ir+2] = RxRFIDPacket0.Data[ir];
      }   
    }
  }
}
/*-----------------------------------------------------------*/
