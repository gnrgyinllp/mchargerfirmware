/* StateMachine.h */


#ifndef STATEMACHINE_H
#define STATEMACHINE_H


#define MELT  // Define MELT DETECTION in order to detect the contacto position and rise an Error Flag

#define STATESCHARGER_READY                     0x00
#define STATESCHARGER_DELAY                     0x01 //T1a
#define STATESCHARGER_VEHICLECONNECTED          0x02
#define STATESCHARGER_PRECHARGING               0x03
#define STATESCHARGER_CHARGING                  0x04
#define STATESCHARGER_CHARGINGVENTILATION       0x05
#define STATESCHARGER_POSTCHARGING              0x06
#define STATESCHARGER_ENDCHARGINGSESSION        0x07
#define STATESCHARGER_DELAY_VENTILATION         0x08
#define STATESCHARGER_AUTHORIZATION             0x09
#define STATESCHARGER_AUTHORIZED                0x0A
#define STATESCHARGER_DELAY_DISCONNECTION       0x0B
#define STATESCHARGER_WAKEUP                    0x0C
#define STATESCHARGER_CPTOGND                   0x0D
#define STATESCHARGER_NODIODE                   0x0E
#define STATESCHARGER_POSTCHARGING_1            0x0F
#define STATESCHARGER_POSTCHARGING_2            0x10


/*
#define STATESVEHICLE_NONE                      0x00
#define STATESVEHICLE_CONNECTED                 0x01
#define STATESVEHICLE_READYCHARGING             0x03
#define STATESVEHICLE_READYCHARGINGVENTILATION  0x04
#define STATESVEHICLE_ERROR                     0x05
#define STATESVEHICLE_EVSENOTAVAILABLE          0x06
*/
// ERRORS

#define MAX_NUM_ERROR                      16

#define ERROR_IN_VEHICLE                   1
#define ERROR_EVSE_NOT_AVAILABLE           2
#define ERROR_WHILE_CHARGING               3
#define ERROR_VENTILATION_REQUIREMENT      4
#define ERROR_TIMER_CONTACTOR_ON           5
#define ERROR_MELT_CONTACTOR               6
#define ERROR_POWERFAILURE                 7
#define ERROR_CPTOGND                      8
#define ERROR_NOAC                         9
#define ERROR_NODIODE                      10
#define ERROR_UNKNOWN                      11
#define ERROR_NOAUTH                       12

// END of ERRORS

#ifdef MITSUBISHI
  #define DELAY_STAGE_MS 200ul //10ul //3000ul
#else
  #define DELAY_STAGE_CS 150ul //50ul // 500ms
#endif

#define DELAY_WAKE_UP_STATE_B1 3000ul  // 30secs
#define DELAY_WAKE_UP_STATE_F 300ul  // 3secs

#define DELAY_TIMER_ACON 50ul      // 50 = 500ms
#define DELAY_TIMER_VENTILATION 300ul
#define DELAY_TIMER_COFF 5ul//100ul
#define DELAY_POWER_FAILURE 10ul
#define DELAY_DISCONNECTION 10ul
#define DELAY_NOAC_CHECK    100ul
#define DELAY_READ_STABLE_VEHICLE_STATE_MS 5ul//200ul
#define DELAY_POSTCHARGE_B2_B1  12000ul  // 120secs


#define REALTIME_PERIOD_30_S  3000ul
#define REALTIME_PERIOD_5_S   500ul
#define REALTIME_PERIOD_2_MS  200ul
#define REALTIME_PERIOD_1_MS   100ul

#define REALTIME_PERIOD_500_MS    50ul
#define REALTIME_PERIOD_300_MS    30ul
#define REALTIME_PERIOD_200_MS    20ul
#define REALTIME_PERIOD_150_MS    15ul
#define REALTIME_PERIOD_100_MS    10ul

#define REALTIME_LONG_PERIOD_ZIGBEE_MS   30000ul
#define REALTIME_SMALL_PERIOD_ZIGBEE_MS  5000ul
#define REALTIME_LONG_PERIOD_PC_MS   30000ul
#define REALTIME_SMALL_PERIOD_PC_MS  5000ul
#define REALTIME_LONG_PERIOD_GPRS_MS   30000ul
#define REALTIME_SMALL_PERIOD_GPRS_MS  10000ul



#define INITIAL_CURRENT_CHARGE 16ul//12ul
#define MAX_CURRENT_ALLOWED 16

#define WH_PER_PULSE_METER    1  

////////////    EVSE EVENT/////////////////////
#define EVSE_EVENT_CHANGE_CURRENT_CHARGING  0x00000001
#define EVSE_EVENT_SEND_REALTIMEPACKET      0x00000002


////////////////LOGIN PARAMETERS//////////////
#define         LOGIN_TIMEOUT_PERIOD         60000   // 1000 = 10 seconds // 12000 = 2 min //60000 = 10min


////////////////LEDS PARAMETERS//////////////
#define         LED_BLINK_PERIOD_MS         100   // = 3 seconds
#define         WHITELEDDUTYADDITION        3
#define         WHITELEDMAXDUTY             30

////////////////BUZZER PARAMETERS//////////////
#define         BUZZER_LONG_PERIOD_MS           50   // = 1 seconds
#define         BUZZER_SHORT_PERIOD_MS          10      //20
#define         BUZZER_BEEP_ALERT_MAX_COUNT     4
#define         BEEP_OFF                        0
#define         BEEP_SHORT                      1
#define         BEEP_LONG                       2
#define         BEEP_ALERT                      3

/////////////Charging ADC States////////////
#define         STATE_A                     0ul           //12V Steady
#define         STATE_A2                    1ul           //12V PWM  
#define         STATE_B1                    2ul           //9V steady
#define         STATE_B2                    3ul           //9V PWM    
#define         STATE_C0                    4ul           //6V PWM
#define         STATE_C                     5ul           //6V PWM
#define         STATE_D                     6ul           //3v PWM
#define         STATE_E                     7ul           //0v Steady CP to GND
#define         STATE_F                     8ul           //-12V Steady
#define         STATE_G                     9ul           //No diode precences PWM with -12V
#define         STATE_H                     10ul           //State no identified
        

////////// ADC CP Comparator trigers//////////////
#define         CP_NON_VALUE_MAX                3
#define         CP_STEADY_COUNTER_TRIGGER       950
#define         CP_PWM_ACTIVITY_MIN             25

//////// LED STATES ////////////////////////////
#define LED_OFF 0
#define LED_ON 1
#define LED_BLINK 2
#define LED_CHARGE_FLASH 3
#define LED_BLINK_PERIOD_10MS 25ul


//////////CONTACTOR STATE/////////////////////////
#define CLOSE_CONTACTOR_STATE                   1
#define OPEN_CONTACTOR_STATE                   0


/////  CURRENT RATED EVSE /////////////////////////
#define MAX_CURRENT_RATED                       16


///////  PP - cable coding ADC reading///////////////
#define ADC_63A_LEVEL_MIN     70
#define ADC_63A_LEVEL_MAX     100
#define ADC_32A_LEVEL_MIN     110
#define ADC_32A_LEVEL_MAX     150
#define ADC_20A_LEVEL_MIN     250
#define ADC_20A_LEVEL_MAX     310
#define ADC_13A_LEVEL_MIN     420
#define ADC_13A_LEVEL_MAX     530

typedef enum {
  Credit,
  RFID,
  CellPhone    
} ePaymentMethod;

typedef enum {
  USDollar,
  Euro,
  Sterling,
  Nis,
  Rad    
} eCurrency;

typedef struct
{
  RTC_TimeTypeDef timeStart;
  RTC_DateTypeDef dateStart;
  RTC_TimeTypeDef timeEnd;
  RTC_DateTypeDef dateEnd;  
  uint32_t meterStart;
  uint32_t meterEnd; 
  uint32_t userId;
  uint32_t SessionId; 
  uint32_t vehicleId;
  uint32_t ConsumptionWH;
  uint32_t Power;
  uint32_t priceUnit;
  eCurrency currency;
  ePaymentMethod paymentMethod;
  GPIO_PinState isAuthorized;  
} SessionObject; 




///declaration function//

void InitEvseStateMachine(void);
void WhiteLedFlash(void);
void CheckLedsState(void);
uint16_t CurrentToDutyCycle(uint16_t i_Current);
void CP_State_Machine_Handler( uint8_t CP_new_State );
void EnablePWM(void);
void DisablePWM(void);
TickType_t ChargerStateTimePassed(void); 
void SetError(uint8_t i_Error);
void ClearError(uint8_t i_Error);
void SetStateCharger(char i_StateCharger);
uint32_t readCableCurrentCoding(void);
void CheckCableStatus(void);
void CheckBuzzerState(void);
void CheckLoginTime(void);
void DisableAlarmA(void);
void DisableAlarmB(void);
void reportError();

void resetSessionObject(void);
void initializeSessionObject();
void EndSessionObject();


#endif