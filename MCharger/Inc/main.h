/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "stm32f4xx_hal.h"
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define WP_EEPROM_Pin GPIO_PIN_2
#define WP_EEPROM_GPIO_Port GPIOE
#define AC_Sensor_Pin GPIO_PIN_3
#define AC_Sensor_GPIO_Port GPIOC
#define CP_ADC_Pin GPIO_PIN_0
#define CP_ADC_GPIO_Port GPIOA
#define PP_ADC_Pin GPIO_PIN_1
#define PP_ADC_GPIO_Port GPIOA
#define CP_CHECK_Pin GPIO_PIN_2
#define CP_CHECK_GPIO_Port GPIOA
#define DC_SENSOR_Pin GPIO_PIN_3
#define DC_SENSOR_GPIO_Port GPIOA
#define CS_METER_Pin GPIO_PIN_4
#define CS_METER_GPIO_Port GPIOA
#define IRQ0_METER_Pin GPIO_PIN_5
#define IRQ0_METER_GPIO_Port GPIOC
#define IRQ1_METER_Pin GPIO_PIN_0
#define IRQ1_METER_GPIO_Port GPIOB
#define RST_METER_Pin GPIO_PIN_1
#define RST_METER_GPIO_Port GPIOB
#define BUZZER_PWM_Pin GPIO_PIN_9
#define BUZZER_PWM_GPIO_Port GPIOE
#define GPRS_PRESENCE_Pin GPIO_PIN_10
#define GPRS_PRESENCE_GPIO_Port GPIOE
#define GPRS_SHDN_Pin GPIO_PIN_11
#define GPRS_SHDN_GPIO_Port GPIOE
#define GPRS_IGT_Pin GPIO_PIN_12
#define GPRS_IGT_GPIO_Port GPIOE
#define GPRS_SYNC_Pin GPIO_PIN_13
#define GPRS_SYNC_GPIO_Port GPIOE
#define BT_UART3_TX_Pin GPIO_PIN_10
#define BT_UART3_TX_GPIO_Port GPIOB
#define BT_UART3_RX_Pin GPIO_PIN_11
#define BT_UART3_RX_GPIO_Port GPIOB
#define BLUE_LED_Pin GPIO_PIN_12
#define BLUE_LED_GPIO_Port GPIOB
#define RED_LED_Pin GPIO_PIN_13
#define RED_LED_GPIO_Port GPIOB
#define WHITE_LED_PWM_Pin GPIO_PIN_14
#define WHITE_LED_PWM_GPIO_Port GPIOB
#define CONTACTOR_OUT_Pin GPIO_PIN_15
#define CONTACTOR_OUT_GPIO_Port GPIOB
#define TTLor485_Pin GPIO_PIN_8
#define TTLor485_GPIO_Port GPIOD
#define EN_rs485_Pin GPIO_PIN_9
#define EN_rs485_GPIO_Port GPIOD
#define DEBUG_UART1_TX_Pin GPIO_PIN_9
#define DEBUG_UART1_TX_GPIO_Port GPIOA
#define DEBUG_UART1_RX_Pin GPIO_PIN_10
#define DEBUG_UART1_RX_GPIO_Port GPIOA
#define USART2_TX_GPRS_Pin GPIO_PIN_5
#define USART2_TX_GPRS_GPIO_Port GPIOD
#define USART2_RX_GPRS_Pin GPIO_PIN_6
#define USART2_RX_GPRS_GPIO_Port GPIOD
#define SD_MON_Pin GPIO_PIN_7
#define SD_MON_GPIO_Port GPIOD
#define CP_IN_Pin GPIO_PIN_4
#define CP_IN_GPIO_Port GPIOB
#define CP_PWM_Pin GPIO_PIN_5
#define CP_PWM_GPIO_Port GPIOB
#define CONTACTOR_MON_Pin GPIO_PIN_6
#define CONTACTOR_MON_GPIO_Port GPIOB
#define LOCK_MON_Pin GPIO_PIN_9
#define LOCK_MON_GPIO_Port GPIOB
#define LOCK1_Pin GPIO_PIN_0
#define LOCK1_GPIO_Port GPIOE
#define LOCK2_Pin GPIO_PIN_1
#define LOCK2_GPIO_Port GPIOE

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
typedef enum
{
  MAIN_STATE_IDLE,
  MAIN_STATE_INITAL,
  MAIN_STATE_INVALID_RFID,
  MAIN_STATE_ENABLE_RFID,
  MAIN_STATE_DISABLE_RFID,
  MAIN_STATE_PROCESS_CP,
  MAIN_STATE_INDEX
} MainState_Tag;

extern void MX_ADC1_Init(void);
extern void MX_ADC2_Init(void);
extern void MX_RNG_Init(void);
extern void MX_RTC_Init(void);
extern void MX_SDIO_SD_Init(void);
extern void MX_SPI1_Init(void);
extern void MX_TIM1_Init(void);
extern void MX_TIM3_Init(void);
extern void MX_TIM12_Init(void);
extern void MX_USART1_UART_Init(void);
extern void MX_USART3_UART_Init(void);
extern void MX_USART2_UART_Init(void);
extern void MX_USART6_UART_Init(void);
extern void MX_I2C1_Init(void);
/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
