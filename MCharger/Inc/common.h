/**
  ******************************************************************************
  * @file    common.h
  * @author  MMY Application Team
  * @version $Revision: 1507 $
  * @date    $Date: 2016-01-08 09:48:35 +0100 (Fri, 08 Jan 2016) $
  * @brief   This file provides all the headers of the common functions.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under ST MYLIBERTY SOFTWARE LICENSE AGREEMENT (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/myliberty  
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
  * AND SPECIFICALLY DISCLAIMING THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _COMMON_H
#define _COMMON_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Nfc Mandatory define ------------------------------------------------------*/

#define RESULTOK		0x00 
#define ERRORCODE_GENERIC	1 
#define ACTION_COMPLETED	0x9000

/*  status and erroc code --------------------------------------------------- */
#define MAIN_SUCCESS_CODE	RESULTOK

/* Enum for ST95 state aligned with new lib Ndef*/
typedef enum {UNDEFINED_MODE=0,PICC,PCD}DeviceMode_t;
typedef enum {UNDEFINED_TAG_TYPE=0,TT1,TT2,TT3,TT4A,TT4B,TT5}TagType_t;

/* external constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
typedef  void (*pFunction)(void);

/* Exported constants --------------------------------------------------------*/
/* Constants used by Serial Command Line Mode */
#define NFC_TAG_MEMORY          512

#define CMD_STRING_SIZE         128

/* TT1 */
#define NFCT1_MAX_TAGMEMORY	(120+2)

/* TT2 */
#define NFCT2_MAX_TAGMEMORY	NFC_TAG_MEMORY /*(must be a multiple of 8) */ 
#define NFCT2_MAX_CC_SIZE	4	
#define NFCT2_MAX_CONFIG	12	
#define NFCT2_MAX_NDEFFILE	(NFCT2_MAX_TAGMEMORY-NFCT2_MAX_CC_SIZE-NFCT2_MAX_CONFIG)

/* TT3 */
#define NFCT3_ATTRIB_INFO_SIZE 	16
#define NFCT3_MAX_NDEFFILE	NFC_TAG_MEMORY
#define NFCT3_MAX_TAGMEMORY 	(NFCT3_MAX_NDEFFILE+NFCT3_ATTRIB_INFO_SIZE)
#define NFCT3_NB_BLOC_MSB	((NFCT3_MAX_TAGMEMORY/16)>>8)
#define NFCT3_NB_BLOC_LSB	((NFCT3_MAX_TAGMEMORY/16)&0x00FF)

/* TT4 */
#define NFCT4_MAX_CCMEMORY	16
#define NFCT4A_MAX_NDEFMEMORY	NFC_TAG_MEMORY
#define NFCT4B_MAX_NDEFMEMORY	NFC_TAG_MEMORY
#define NFCT4_MAX_NDEFMEMORY	NFCT4A_MAX_NDEFMEMORY

/* TT5 */
#define NFCT5_MAX_TAGMEMORY	NFC_TAG_MEMORY

#define NFC_DEVICE_MAX_NDEFMEMORY	NFCT4_MAX_NDEFMEMORY

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef const uint32_t uc32;  /*!< Read Only */
typedef const uint16_t uc16;  /*!< Read Only */
typedef const uint8_t  uc8;   /*!< Read Only */

	 
#define MAX(x,y) 				((x > y)? x : y)
#define MIN(x,y) 				((x < y)? x : y)  
#define ABS(x) 					((x)>0 ? (x) : -(x))  
#define CHECKVAL(val, min,max) 	((val < min || val > max) ? false : true) 
	 
#define GETMSB(val) 		((val & 0xFF00 )>>8 ) 
#define GETLSB(val) 		( val & 0x00FF ) 
 
#define RESULTOK 							0x00 
#define ERRORCODE_GENERIC 		1 

#ifndef errchk
#define errchk(fCall) if (status = (fCall), status != RESULTOK) \
	{goto Error;}
#endif

/******************  PCD  ******************/
/* ISO14443A */
#define PCD_TYPEA_ARConfigA	        0x01
#define PCD_TYPEA_ARConfigB	        0xDF

#define PCD_TYPEA_TIMERW                0x5A

/* ISO14443B */
#define PCD_TYPEB_ARConfigA	        0x01
#define PCD_TYPEB_ARConfigB	        0x51

/* Felica */
#define PCD_TYPEF_ARConfigA	        0x01
#define PCD_TYPEF_ARConfigB	        0x51

/* ISO15693 */
#define PCD_TYPEV_ARConfigA	        0x01
#define PCD_TYPEV_ARConfigB	        0xD1

/******************  PICC  ******************/
/* ISO14443A */
#define PICC_TYPEA_ACConfigA            0x27  /* backscaterring */

/* ISO14443B */
#define PICC_TYPEB_ARConfigD            0x0E  /* card demodulation gain */
#define PICC_TYPEB_ACConfigA            0x17  /* backscaterring */

/* Felica */
#define PICC_TYPEF_ACConfigA            0x17  /* backscaterring */


/* Exported macro ------------------------------------------------------------*/
/* Common routines */
/* Exported functions ------------------------------------------------------- */
uint16_t random(uint16_t min, uint16_t max);

#endif /* _COMMON_H */
