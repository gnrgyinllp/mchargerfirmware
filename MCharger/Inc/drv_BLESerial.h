#ifndef DRV_BLE_SERIAL_H_
#define DRV_BLE_SERIAL_H_

#include <stdint.h>
#include <stdbool.h>
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#include "stm32f4xx_hal_uart.h"

#define DEVICE_NAME_LEN          12
#define STATUS_LED_LEN           8
#define ADVERTISING_DURATION_LEN 4
#define ADVERTISING_POWER_LEN    3

typedef __packed struct {
  char strTransmitPower[ADVERTISING_POWER_LEN];
  char strAdvertisingDuration[ADVERTISING_DURATION_LEN];
  char strDeviceName[DEVICE_NAME_LEN];
  char strStatusLedConfig[STATUS_LED_LEN];
} BLEConfig_t;

void drv_BLECheckFirstBoot(void);
void drv_BLESetStreamMode(void);
void drv_BLEGetConfig(void);
#endif /* DRV_BLE_SERIAL_H_ */
