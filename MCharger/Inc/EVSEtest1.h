/**
  ******************************************************************************
  * @file    EVSEtest1.h
  * @author  Josep Pinto
  * @version V1.0.0
  * @date    4-Jan-2017
  * @brief   This files contains all functions declarations and definitions 
  *          for the EV charging functionality test
  ******************************************************************************
  * 
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EVSEtest1_H
#define __EVSEtest1_H
  /* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "EVSEtest1.h"
#include "FreeRTOS.h"
#include "StateMachine.h"
#include "queue.h"
#include "main.h"
#include "eeprom.h"

/* USER CODE BEGIN Includes */
void User_Init(void); 
void BlueButton_Led(void);
void DutyLoop(void);
void FreqLoop(void);
void GetAdc(void);
int CloseContactorCH1(void);
void PWM_TIMER_init(void);
void PWM_Start(void);
void HAL_ADC_USER_ConvCpltCallback(ADC_HandleTypeDef* hadc);
signed portBASE_TYPE xCP_GetStatus(  signed portCHAR *CPStatusChar, portTickType xBlockTime );
int CloseContactor(void);
int OpenContactor(void);
void Lock(void);
void UnLock(void);
int BlueLedTurnON(void);
int BlueLedTurnOFF(void);
int RedLedTurnON(void);
int RedLedTurnOFF(void);
int WhiteLedTurnON(void);
int WhiteLedTurnOFF(void);
void WhiteLedSetDuty(uint8_t DC);
GPIO_PinState ReadContactorMonitoring(void);
void ReadEepromEVSEValues(void);
void InitWriteEepromEVSEValues(void);
void WriteEepromEVSEValues(char WAdd[], char WDat[], size_t WLen);
void WriteEepromEVSEValuesInit(uint16_t WAddInt, char WDat[], size_t WLen);
void ReadInitialCurrent(void);
void ReadMaxCurrent(void);


#define      ADC_ARRAY_LENGHT     1000  
#define      LOWER_LIMIT_12V      40
#define      UPPER_LIMIT_12V      55
#define      LOWER_LIMIT_9V      65
#define      UPPER_LIMIT_9V      79
#define      LOWER_LIMIT_6V      85
#define      UPPER_LIMIT_6V      99
#define      LOWER_LIMIT_3V      110
#define      UPPER_LIMIT_3V      124
#define      LOWER_LIMIT_0V      128
#define      UPPER_LIMIT_0V      150
#define      LOWER_LIMIT_MINUS12V      225
#define      UPPER_LIMIT_MINUS12V      240
#define      PWM_F_MINUS12V           -12
#define      PWM_E_0V                 0
#define      PWM_D_3V                 3
#define      PWM_C_6V                 6
#define      PWM_B_9V                 9
#define      PWM_A_12V                12
#define      PWM_NOVALID              -33

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/
#define CP_QUEUE_LENGHT         10        

/* USER CODE BEGIN Private defines */

#define HZ_INT_CLK              12288000
#define PERIOD_INIT_PWM1        9870
#define PULSE_INIT_PWM1         4935   
#define OPEN_CH1_INS            "o1"
#define CLOSE_CH1_INS           "c1"
#define OPEN_CH2_INS            "o2"
#define CLOSE_CH2_INS           "c2"



/////////////////EEPROM PARAMETERS////////////////
/* Variables' number */
#define EEPROM_NUM_VAR                  ((uint8_t)32)
#define EEPROM_START_ADDRESS            0x5000
#define EEPROM_FIRST_TIME               0
#define PASS_NUM0_ADDRESS               1
#define PASS_NUM1_ADDRESS               2
#define PASS_NUM2_ADDRESS               3
#define PASS_NUM3_ADDRESS               4
#define PASS_NUM4_ADDRESS               5
#define PASS_NUM5_ADDRESS               6
#define SERIAL_NUM0_ADDRESS             7
#define SERIAL_NUM1_ADDRESS             8
#define SERIAL_NUM2_ADDRESS             9
#define SERIAL_NUM3_ADDRESS             10
#define SERIAL_NUM4_ADDRESS             11
#define SERIAL_NUM5_ADDRESS             12
#define LOGIN_ENABLE                    13
#define MAX_CURRENT_EEPROM_ADDRESS      14
#define CURRENT_EEPROM_ADDRESS          15
#define CHARGER_TYPE_EEPROM_ADDRESS     16
#define OUTPUT_TYPE_ADDRESS             17
#define METER_EEPROM_ADDRESS            18
#define BLE_EEPROM_ENABLE               19
#define RS485_EEPROM_ENABLE             20
#define RS485ID_EEPROM_ENABLE           21
#define RFID_EEPROM_ENABLE              22
#define AUTHORIZATION_ENABLE            23
#define FIRMWARE_VERSION_ADDRESS0       24
#define FIRMWARE_VERSION_ADDRESS1       25
#define FIRMWARE_VERSION_ADDRESS2       26
#define DATE_PROGRAM_ADDRESS0           27
#define DATE_PROGRAM_ADDRESS1           28
#define DATE_PROGRAM_ADDRESS2           29
#define DATE_PROGRAM_ADDRESS3           30
#define AUDI_CHARGING_ADDRESS0          31

/////////////////INITIAL EEPROM PARAMETERS////////////////
/* Factory reset values */

#define EEPROM_FIRST_TIME_INIT          "01" //first eeprom flashed, 01:Yes
#define PASS_EEPROM_INIT                "MakeLotsOf@s"//"KoalaBear5pm"
#define SERIAL_NUM_INIT                 "GN0000000000"
#define LOGIN_ENABLE_INIT               "01"
#define MAX_CURRENT_INIT                "16" //{'2','5'}
#define CURRENT_INIT                    "16"
#define CHARGER_TYPE_INIT               "02"
#define OUTPUT_TYPE_INIT                "00"
#define METER_INIT                      "00"
#define BLE_ENABLE_INIT                 "01"
#define RS485_ENABLE_INIT               "00"
#define RS485ID_INIT                    "00"
#define RFID_ENABLE_INIT                "00"
#define AUTHORIZATION_ENABLE_INIT       "00"
#define FIRMWARE_VERSION_INIT           "M00001"
#define DATE_PROGRAM_INIT               "06-02-18"
#define AUDI_CHARGE_INIT                "00"
/////////////////INITIAL EEPROM PARAMETER one single constant////////////////
/* Factory reset values */



///// INIT VALUES
#define INIT_CURRENT                    16
#define INIT_CHARGER_TYPE               0 // 0:Basic charger cable, 1:Basic charger socket
#define INIT_METER_PARAMETER            0 // 0:Meter not enabled    1: Internal Meter Enabled

/// MESSAGES
#define NO_LOGGED_MESSAGE               "No Logged In, Please Log In to Continue..."


#endif /* __EVSEtest1_H */