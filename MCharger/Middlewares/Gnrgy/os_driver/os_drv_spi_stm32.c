#include <stdint.h>
#include <stdbool.h>
#include "os_drv_spi.h"
#include "os_layer/os_layer.h"
#include "hal_spi.h"
#include "log/log.h"
#include "utils/zz.h"

/**********************************************************************************************************************/

#define HAL_SPI_USE_IFACE_1    1
#define HAL_SPI_USE_IFACE_2    0
#define HAL_SPI_USE_IFACE_3    0

extern SPI_HandleTypeDef hspi1;
/**********************************************************************************************************************/

typedef struct osDrvSpi_STM32_TAG
{
  osDrvSpi_t baseDrv;
  halSpi_Handle_t* const pHalSpi;
  volatile osLayer_ThreadId_t threadId;
  volatile bool flagError;
} osDrvSpi_STM32_t;

/**********************************************************************************************************************/

static osDrvSpi_STM32_t g_osDrvSpi_arrOfDrv[
#if HAL_SPI_USE_IFACE_1
                                       + 1
#endif
#if HAL_SPI_USE_IFACE_2
                                       + 1
#endif
#if HAL_SPI_USE_IFACE_3
                                       + 1
#endif
                                       ] = {
#if HAL_SPI_USE_IFACE_1
                                           {.pHalSpi = &hspi1},
#endif
#if HAL_SPI_USE_IFACE_2
                                           {.pHalSpi = &hspi2},
#endif
#if HAL_SPI_USE_IFACE_3
                                           {.pHalSpi = &hspi3},
#endif
                                       };

#if HAL_SPI_USE_IFACE_1
#define OS_DRV_SPI_IFACE_1  1
#else
#define OS_DRV_SPI_IFACE_1  0
#endif
#if HAL_SPI_USE_IFACE_2
#define OS_DRV_SPI_IFACE_2  1
#else
#define OS_DRV_SPI_IFACE_2  0
#endif
#if HAL_SPI_USE_IFACE_3
#define OS_DRV_SPI_IFACE_3  1
#else
#define OS_DRV_SPI_IFACE_3  0
#endif

#define OS_DRV_SPI_IFACE_1_IDX  (OS_DRV_SPI_IFACE_1 - 1)
#define OS_DRV_SPI_IFACE_2_IDX  (OS_DRV_SPI_IFACE_1 + OS_DRV_SPI_IFACE_2 - 1)
#define OS_DRV_SPI_IFACE_3_IDX  (OS_DRV_SPI_IFACE_1 + OS_DRV_SPI_IFACE_2 + OS_DRV_SPI_IFACE_3 - 1)

/**********************************************************************************************************************/

static osDrvSpi_STM32_t* osDrvSpi_getDrv(
    const osDrvSpi_InterfaceNum_t ifaceNum
)
{
  osDrvSpi_STM32_t* pDrv = NULL;
  switch(ifaceNum)
  {
#if HAL_SPI_USE_IFACE_1
    case 1:
      pDrv = &g_osDrvSpi_arrOfDrv[OS_DRV_SPI_IFACE_1_IDX];
      break;
#endif
#if HAL_SPI_USE_IFACE_2
    case 2:
      pDrv = &g_osDrvSpi_arrOfDrv[OS_DRV_SPI_IFACE_2_IDX];
      break;
#endif
#if HAL_SPI_USE_IFACE_3
    case 3:
      pDrv = &g_osDrvSpi_arrOfDrv[OS_DRV_SPI_IFACE_3_IDX];
      break;
#endif
    default:
      break;
  }
  return pDrv;
}

static osDrvSpi_STM32_t* osDrvSpi_getDrvByHandle(
    halSpi_Handle_t* const handle
)
{
  for (unsigned i = 0; i < ZZ_COUNT_OF(g_osDrvSpi_arrOfDrv); ++i)
  {
    if (g_osDrvSpi_arrOfDrv[i].pHalSpi == handle)
    {
      return &g_osDrvSpi_arrOfDrv[i];
    }
  }
  return NULL;
}


osDrvSpi_t* osDrvSpi_init(
    const osDrvSpi_InterfaceNum_t ifaceNum
)
{
  osDrvSpi_STM32_t* const pDrv = osDrvSpi_getDrv(ifaceNum);
  if (NULL == pDrv)
  {
    return NULL;
  }

  if (!osDrvSpiBase_init(&pDrv->baseDrv, ifaceNum))
  {
    return NULL;
  }

  return &pDrv->baseDrv;
}

void osDrvSpi_deinit(
    osDrvSpi_t* const pDrvSpi
)
{
  osDrvSpi_STM32_t* const pDrv = (osDrvSpi_STM32_t*)pDrvSpi;
  osDrvSpiBase_deinit(&pDrv->baseDrv);
  pDrv->threadId.threadId = 0;
  pDrv->flagError = false;
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
  osDrvSpi_STM32_t* const pDrv = osDrvSpi_getDrvByHandle(hspi);
  if (NULL == pDrv)
  {
    return;
  }
  const osThreadId threadId = pDrv->threadId.threadId;
  if (0 != threadId)
  {
    pDrv->threadId.threadId = 0;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(threadId, &xHigherPriorityTaskWoken);

    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
     should be performed to ensure the interrupt returns directly to the highest
     priority task.  The macro used for this purpose is dependent on the port in
     use and may be called portEND_SWITCHING_ISR(). */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
  osDrvSpi_STM32_t* const pDrv = osDrvSpi_getDrvByHandle(hspi);
  if (NULL == pDrv)
  {
    return;
  }
  const osThreadId threadId = pDrv->threadId.threadId;
  if (0 != threadId)
  {
    pDrv->threadId.threadId = 0;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(threadId, &xHigherPriorityTaskWoken);

    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
     should be performed to ensure the interrupt returns directly to the highest
     priority task.  The macro used for this purpose is dependent on the port in
     use and may be called portEND_SWITCHING_ISR(). */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
  osDrvSpi_STM32_t* const pDrv = osDrvSpi_getDrvByHandle(hspi);
  if (NULL == pDrv)
  {
    return;
  }
  pDrv->flagError = true;
  const osThreadId threadId = pDrv->threadId.threadId;
  if (0 != threadId)
  {
    pDrv->threadId.threadId = 0;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(threadId, &xHigherPriorityTaskWoken);

    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
     should be performed to ensure the interrupt returns directly to the highest
     priority task.  The macro used for this purpose is dependent on the port in
     use and may be called portEND_SWITCHING_ISR(). */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

void HAL_SPI_AbortCpltCallback(SPI_HandleTypeDef *hspi)
{

}

bool osDrvSpi_masterTransmit(
    osDrvSpi_t* const pDrvSpi,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  osDrvSpi_STM32_t* const pDrv = (osDrvSpi_STM32_t*)pDrvSpi;
  pDrv->threadId = osLayer_getCurThreadId();
  pDrv->flagError = false;

  if (!halSpi_masterTransmitDMA(
      pDrv->pHalSpi,
      txbuf,
      txbytes))
  {
    LOGF_OS_DRV_SPI_ERR("%s failed", "halSpi_masterTransmitDMA");
    return false;
  }

  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  while (osLayer_getElapsedTicks(t0) < timeoutTicks)
  {
    const osLayer_DelayUTicks_t delayTicks =
        osLayer_convDeltaTicksToDelayUTicks(timeoutTicks - osLayer_getElapsedTicks(t0));
    if (delayTicks > 0)
    {
      ulTaskNotifyTake(pdTRUE, (TickType_t)delayTicks);
    }
    if (0 == pDrv->threadId.threadId)
    {
      break;
    }
  }
  if (0 != pDrv->threadId.threadId)
  {
    pDrv->threadId.threadId = 0;
    LOGF_OS_DRV_SPI_ERR("%s timeout", "halSpi_masterTransmitDMA");
    return false;
  }
  if (pDrv->flagError)
  {
    LOGF_OS_DRV_SPI_ERR("%s error", "halSpi_masterTransmitDMA");
    return false;
  }
  return true;
}

bool osDrvSpi_masterReceive(
    osDrvSpi_t* const pDrvSpi,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  osDrvSpi_STM32_t* const pDrv = (osDrvSpi_STM32_t*)pDrvSpi;
  pDrv->threadId = osLayer_getCurThreadId();
  pDrv->flagError = false;

  if (!halSpi_masterReceiveDMA(
      pDrv->pHalSpi,
      rxbuf,
      rxbytes))
  {
    LOGF_OS_DRV_SPI_ERR("%s failed", "halSpi_masterReceiveDMA");
    return false;
  }

  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  while (osLayer_getElapsedTicks(t0) < timeoutTicks)
  {
    const osLayer_DelayUTicks_t delayTicks =
        osLayer_convDeltaTicksToDelayUTicks(timeoutTicks - osLayer_getElapsedTicks(t0));
    if (delayTicks > 0)
    {
      ulTaskNotifyTake(pdTRUE, (TickType_t)delayTicks);
    }
    if (0 == pDrv->threadId.threadId)
    {
      break;
    }
  }
  if (0 != pDrv->threadId.threadId)
  {
    pDrv->threadId.threadId = 0;
    LOGF_OS_DRV_SPI_ERR("%s timeout", "halSpi_masterReceiveDMA");
    return false;
  }
  if (pDrv->flagError)
  {
    LOGF_OS_DRV_SPI_ERR("%s error", "halSpi_masterReceiveDMA");
    return false;
  }
  return true;
}

