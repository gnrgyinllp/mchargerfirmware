#include <stdint.h>
#include "uart.h"
#include "os_layer/os_layer.h"
#include "hal_uart.h"

void uartTransmit(
        UART_HandleTypeDef* huart,
        const void* const txBuf,
        const uint16_t txBufLen)
{
    while ((huart->gState == HAL_UART_STATE_BUSY_TX) || (huart->gState == HAL_UART_STATE_BUSY_TX_RX))
    {
        osLayer_sleepTicks(1);
    }
    halUART_TransmitIT(huart, (uint8_t*)txBuf, txBufLen);
}
