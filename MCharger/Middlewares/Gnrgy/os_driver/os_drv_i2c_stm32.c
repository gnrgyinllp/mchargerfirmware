#include <stdint.h>
#include <stdbool.h>
#include "os_drv_i2c.h"
#include "os_layer/os_layer.h"
#include "hal_i2c.h"
#include "log/log.h"
#include "utils/zz.h"

/**********************************************************************************************************************/

#define HAL_I2C_USE_IFACE_1    1
#define HAL_I2C_USE_IFACE_2    0
#define HAL_I2C_USE_IFACE_3    0

extern I2C_HandleTypeDef hi2c1;
/**********************************************************************************************************************/

typedef struct osDrvI2c_STM32_TAG
{
  osDrvI2c_t baseDrv;
  halI2c_Handle_t* const pHalI2c;
  volatile osLayer_ThreadId_t threadId;
  volatile bool flagError;
} osDrvI2c_STM32_t;

/**********************************************************************************************************************/

static osDrvI2c_STM32_t g_osDrvI2c_arrOfDrv[
#if HAL_I2C_USE_IFACE_1
                                       + 1
#endif
#if HAL_I2C_USE_IFACE_2
                                       + 1
#endif
#if HAL_I2C_USE_IFACE_3
                                       + 1
#endif
                                       ] = {
#if HAL_I2C_USE_IFACE_1
                                           {.pHalI2c = &hi2c1},
#endif
#if HAL_I2C_USE_IFACE_2
                                           {.pHalI2c = &hi2c2},
#endif
#if HAL_I2C_USE_IFACE_3
                                           {.pHalI2c = &hi2c3},
#endif
                                       };

#if HAL_I2C_USE_IFACE_1
#define OS_DRV_I2C_IFACE_1  1
#else
#define OS_DRV_I2C_IFACE_1  0
#endif
#if HAL_I2C_USE_IFACE_2
#define OS_DRV_I2C_IFACE_2  1
#else
#define OS_DRV_I2C_IFACE_2  0
#endif
#if HAL_I2C_USE_IFACE_3
#define OS_DRV_I2C_IFACE_3  1
#else
#define OS_DRV_I2C_IFACE_3  0
#endif

#define OS_DRV_I2C_IFACE_1_IDX  (OS_DRV_I2C_IFACE_1 - 1)
#define OS_DRV_I2C_IFACE_2_IDX  (OS_DRV_I2C_IFACE_1 + OS_DRV_I2C_IFACE_2 - 1)
#define OS_DRV_I2C_IFACE_3_IDX  (OS_DRV_I2C_IFACE_1 + OS_DRV_I2C_IFACE_2 + OS_DRV_I2C_IFACE_3 - 1)

/**********************************************************************************************************************/

static osDrvI2c_STM32_t* osDrvI2c_getDrv(
    const osDrvI2c_InterfaceNum_t ifaceNum
)
{
  osDrvI2c_STM32_t* pDrv = NULL;
  switch(ifaceNum)
  {
#if HAL_I2C_USE_IFACE_1
    case 1:
      pDrv = &g_osDrvI2c_arrOfDrv[OS_DRV_I2C_IFACE_1_IDX];
      break;
#endif
#if HAL_I2C_USE_IFACE_2
    case 2:
      pDrv = &g_osDrvI2c_arrOfDrv[OS_DRV_I2C_IFACE_2_IDX];
      break;
#endif
#if HAL_I2C_USE_IFACE_3
    case 3:
      pDrv = &g_osDrvI2c_arrOfDrv[OS_DRV_I2C_IFACE_3_IDX];
      break;
#endif
    default:
      break;
  }
  return pDrv;
}

static osDrvI2c_STM32_t* osDrvI2c_getDrvByHandle(
    halI2c_Handle_t* const handle
)
{
  for (unsigned i = 0; i < ZZ_COUNT_OF(g_osDrvI2c_arrOfDrv); ++i)
  {
    if (g_osDrvI2c_arrOfDrv[i].pHalI2c == handle)
    {
      return &g_osDrvI2c_arrOfDrv[i];
    }
  }
  return NULL;
}


osDrvI2c_t* osDrvI2c_init(
    const osDrvI2c_InterfaceNum_t ifaceNum
)
{
  osDrvI2c_STM32_t* const pDrv = osDrvI2c_getDrv(ifaceNum);
  if (NULL == pDrv)
  {
    return NULL;
  }

  if (!osDrvI2cBase_init(&pDrv->baseDrv, ifaceNum))
  {
    return NULL;
  }

  return &pDrv->baseDrv;
}

void osDrvI2c_deinit(
    osDrvI2c_t* const pDrvI2c
)
{
  osDrvI2c_STM32_t* const pDrv = (osDrvI2c_STM32_t*)pDrvI2c;
  osDrvI2cBase_deinit(&pDrv->baseDrv);
  pDrv->threadId.threadId = 0;
  pDrv->flagError = false;
}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  osDrvI2c_STM32_t* const pDrv = osDrvI2c_getDrvByHandle(hi2c);
  if (NULL == pDrv)
  {
    return;
  }
  const osThreadId threadId = pDrv->threadId.threadId;
  if (0 != threadId)
  {
    pDrv->threadId.threadId = 0;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(threadId, &xHigherPriorityTaskWoken);

    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
     should be performed to ensure the interrupt returns directly to the highest
     priority task.  The macro used for this purpose is dependent on the port in
     use and may be called portEND_SWITCHING_ISR(). */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  osDrvI2c_STM32_t* const pDrv = osDrvI2c_getDrvByHandle(hi2c);
  if (NULL == pDrv)
  {
    return;
  }
  const osThreadId threadId = pDrv->threadId.threadId;
  if (0 != threadId)
  {
    pDrv->threadId.threadId = 0;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(threadId, &xHigherPriorityTaskWoken);

    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
     should be performed to ensure the interrupt returns directly to the highest
     priority task.  The macro used for this purpose is dependent on the port in
     use and may be called portEND_SWITCHING_ISR(). */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
  osDrvI2c_STM32_t* const pDrv = osDrvI2c_getDrvByHandle(hi2c);
  if (NULL == pDrv)
  {
    return;
  }
  pDrv->flagError = true;
  const osThreadId threadId = pDrv->threadId.threadId;
  if (0 != threadId)
  {
    pDrv->threadId.threadId = 0;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(threadId, &xHigherPriorityTaskWoken);

    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
     should be performed to ensure the interrupt returns directly to the highest
     priority task.  The macro used for this purpose is dependent on the port in
     use and may be called portEND_SWITCHING_ISR(). */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

void HAL_I2C_AbortCpltCallback(I2C_HandleTypeDef *hi2c)
{

}

bool osDrvI2c_masterTransmit(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  osDrvI2c_STM32_t* const pDrv = (osDrvI2c_STM32_t*)pDrvI2c;
  pDrv->threadId = osLayer_getCurThreadId();
  pDrv->flagError = false;

  if (!halI2c_masterTransmitDMA(
      pDrv->pHalI2c,
      addr,
      txbuf,
      txbytes))
  {
    LOGF_OS_DRV_I2C_ERR("%s failed", "halI2c_masterTransmitDMA");
    return false;
  }

  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  while (osLayer_getElapsedTicks(t0) < timeoutTicks)
  {
    const osLayer_DelayUTicks_t delayTicks =
        osLayer_convDeltaTicksToDelayUTicks(timeoutTicks - osLayer_getElapsedTicks(t0));
    if (delayTicks > 0)
    {
      ulTaskNotifyTake(pdTRUE, (TickType_t)delayTicks);
    }
    if (0 == pDrv->threadId.threadId)
    {
      break;
    }
  }
  if (0 != pDrv->threadId.threadId)
  {
    pDrv->threadId.threadId = 0;
    LOGF_OS_DRV_I2C_ERR("%s timeout", "halI2c_masterTransmitDMA");
    return false;
  }
  if (pDrv->flagError)
  {
    LOGF_OS_DRV_I2C_ERR("%s error", "halI2c_masterTransmitDMA");
    return false;
  }
  return true;
}

bool osDrvI2c_masterReceive(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  osDrvI2c_STM32_t* const pDrv = (osDrvI2c_STM32_t*)pDrvI2c;
  pDrv->threadId = osLayer_getCurThreadId();
  pDrv->flagError = false;

  if (!halI2c_masterReceiveDMA(
      pDrv->pHalI2c,
      addr,
      rxbuf,
      rxbytes))
  {
    LOGF_OS_DRV_I2C_ERR("%s failed", "halI2c_masterReceiveDMA");
    return false;
  }

  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  while (osLayer_getElapsedTicks(t0) < timeoutTicks)
  {
    const osLayer_DelayUTicks_t delayTicks =
        osLayer_convDeltaTicksToDelayUTicks(timeoutTicks - osLayer_getElapsedTicks(t0));
    if (delayTicks > 0)
    {
      ulTaskNotifyTake(pdTRUE, (TickType_t)delayTicks);
    }
    if (0 == pDrv->threadId.threadId)
    {
      break;
    }
  }
  if (0 != pDrv->threadId.threadId)
  {
    pDrv->threadId.threadId = 0;
    LOGF_OS_DRV_I2C_ERR("%s timeout", "halI2c_masterTransmitDMA");
    return false;
  }
  if (pDrv->flagError)
  {
    LOGF_OS_DRV_I2C_ERR("%s error", "halI2c_masterReceiveDMA");
    return false;
  }
  return true;
}

