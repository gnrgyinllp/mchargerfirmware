#ifndef HAL_SPI_H
#define HAL_SPI_H

#include <stdint.h>
#include <stdbool.h>
#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

#define METER_CS_Port  GPIOA
#define METER_CS_Pin   GPIO_PIN_4
  
typedef SPI_HandleTypeDef halSpi_Handle_t;

/**
  * @brief Init SPI driver instance
  * @retval true if successful
  */
bool halSpi_init(halSpi_Handle_t* const handle);

/**
  * @brief Reset SPI hardware
  */
bool halSpi_reset(halSpi_Handle_t* const handle);

bool halSpi_abortIT(
    halSpi_Handle_t* const handle
);

bool halSpi_masterTransmitDMA(
    halSpi_Handle_t* const handle,
    const uint8_t* const txbuf,
    const size_t txbytes
);

bool halSpi_masterReceiveDMA(
    halSpi_Handle_t* const handle,
    uint8_t* const rxbuf,
    const size_t rxbytes
);

#ifdef __cplusplus
}
#endif

#endif /* HAL_SPI_H */
