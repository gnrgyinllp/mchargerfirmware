#ifndef HAL_I2C_H
#define HAL_I2C_H

#include <stdint.h>
#include <stdbool.h>
#include "stm32f4xx_hal.h"
#ifdef __cplusplus
extern "C" {
#endif

typedef I2C_HandleTypeDef halI2c_Handle_t;

typedef uint8_t halI2c_Addr_t;

/**
  * @brief Init I2C driver instance
  * @retval true if successful
  */
bool halI2c_init(halI2c_Handle_t* const handle);

/**
  * @brief Reset I2C hardware
  */
bool halI2c_reset(halI2c_Handle_t* const handle);

bool halI2c_abortIT(
    halI2c_Handle_t* const handle
);

bool halI2c_masterTransmitDMA(
    halI2c_Handle_t* const handle,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes
);

bool halI2c_masterReceiveDMA(
    halI2c_Handle_t* const handle,
    const halI2c_Addr_t addr,
    uint8_t* const rxbuf,
    const size_t rxbytes
);

#ifdef __cplusplus
}
#endif

#endif /* HAL_I2C_H */
