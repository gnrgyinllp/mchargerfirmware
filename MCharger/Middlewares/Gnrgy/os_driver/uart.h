#if !defined(UART_H_)
#define UART_H_

#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "os_layer/os_layer.h"


void uartTransmit(
        UART_HandleTypeDef* huart,
        const void* const txBuf,
        const uint16_t txBufLen);


#endif /* UART_H_ */
