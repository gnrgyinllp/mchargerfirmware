#ifndef HAL_UART_H
#define HAL_UART_H

#include <stdint.h>
#include "main.h"

void halUART_waitTxReady(UART_HandleTypeDef* const huart);

/**
  * @brief Send an amount of data in interrupt mode.
  * @param huart: UART handle.
  * @param pData: pointer to data buffer.
  * @param size: amount of data to be sent.
  * @retval HAL status
  */
HAL_StatusTypeDef halUART_TransmitIT(UART_HandleTypeDef* const huart, const uint8_t* const pData, const uint16_t size);

/**
 * @brief Receive an amount of data in interrupt mode.
 * @param huart: UART handle.
 * @param pData: pointer to data buffer.
 * @param size: amount of data to be received.
 * @retval HAL status
 */
void halUART_ReceiveIT_withoutLock(UART_HandleTypeDef* const huart, uint8_t* const pData, const uint16_t size);

/**
  * @brief Receive an amount of data in interrupt mode.
  * @param huart: UART handle.
  * @param pData: pointer to data buffer.
  * @param size: amount of data to be received.
  * @retval HAL status
  */
HAL_StatusTypeDef halUART_ReceiveIT(UART_HandleTypeDef* const huart, uint8_t* const pData, const uint16_t size);

#endif /* HAL_UART_H */

