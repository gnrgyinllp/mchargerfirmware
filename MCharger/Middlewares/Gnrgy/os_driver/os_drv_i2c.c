#include <stdint.h>
#include <stdbool.h>
#include "os_drv_i2c.h"
#include "utils/attribs.h"
#include "log/log.h"
#include "os_layer/os_layer.h"
#include "stm32f4xx.h"
#include "main.h"

#if !defined(OS_DRV_I2C_NUM_RETRIES)
#define OS_DRV_I2C_NUM_RETRIES      3
#endif

bool osDrvI2cBase_init(
    osDrvI2c_t* const pDrv,
    const osDrvI2c_InterfaceNum_t ifaceNum
)
{
  pDrv->ifaceNum = ifaceNum;
  if (!osLayer_mutexInit(&pDrv->mutex))
  {
    LOGF_OS_DRV_I2C_ERR("osLayer_mutexInit failed");
    return false;
  }
  return true;
}

void osDrvI2cBase_deinit(
    osDrvI2c_t* const pDrv
)
{
  osLayer_mutexDeinit(&pDrv->mutex);
}

bool osDrvI2c_acquireBus(
    osDrvI2c_t* const pDrv
)
{
  return osLayer_mutexEnter(&pDrv->mutex);
}

bool osDrvI2c_releaseBus(
    osDrvI2c_t* const pDrv
)
{
  return osLayer_mutexExit(&pDrv->mutex);
}

static bool osDrvI2c_masterTransmitReceiveWithRetries(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  for (unsigned i = 0; i < OS_DRV_I2C_NUM_RETRIES; ++i)
  {
    if (0 != txbytes)
    {
      if (!osDrvI2c_masterTransmit(pDrvI2c, addr, txbuf, txbytes, timeoutTicks))
      {
        LOGF_OS_DRV_I2C_WARN("%s failed for slave=0x%02x time %d", "osDrvI2c_masterTransmit", addr, i);
        continue;
      }
    }

    if (0 != rxbytes)
    {
      if (!osDrvI2c_masterReceive(pDrvI2c, addr, rxbuf, rxbytes, timeoutTicks))
      {
        LOGF_OS_DRV_I2C_WARN("%s failed for slave=0x%02x time %d", "osDrvI2c_masterReceive", addr, i);
        continue;
      }
    }
    return true;
  }
  return false;
}

bool osDrvI2c_masterTransaction(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  if (!osDrvI2c_acquireBus(pDrvI2c))
  {
    LOGF_OS_DRV_I2C_ERR("%s failed", "osDrvI2c_acquireBus");
    return false;
  }
  if (!osDrvI2c_masterTransmitReceiveWithRetries(pDrvI2c, addr, txbuf, txbytes, rxbuf, rxbytes, timeoutTicks))
  {
    LOGF_OS_DRV_I2C_ERR("%s failed for slave=0x%02x", "osDrvI2c_masterReceiveWithRetries", addr);
    osDrvI2c_releaseBus(pDrvI2c);
    return false;
  }
  if (!osDrvI2c_releaseBus(pDrvI2c))
  {
    LOGF_OS_DRV_I2C_ERR("%s failed", "osDrvI2c_releaseBus");
    return false;
  }
  return true;
}

bool osDrvI2c_masterTransactionTransmit(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  return osDrvI2c_masterTransaction(pDrvI2c, addr, txbuf, txbytes, NULL, 0, timeoutTicks);
}

bool osDrvI2c_masterTransactionReceive(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  return osDrvI2c_masterTransaction(pDrvI2c, addr, NULL, 0, rxbuf, rxbytes, timeoutTicks);
}

