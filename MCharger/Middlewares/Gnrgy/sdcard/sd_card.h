#if !defined(_SD_CARD_H_)
#define _SD_CARD_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "file.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum sdCard_FSType_TAG
{
  sdCard_FSType_None = 0,
  sdCard_FSType_FAT12,
  sdCard_FSType_FAT16,
  sdCard_FSType_FAT32,
  sdCard_FSType_Unknown,
} sdCard_FSType_e;

bool sdCard_powerOn(void);

void sdCard_init(void);

bool sdCard_lock(void);

void sdCard_unlock(void);

bool sdCard_mount(void);

bool sdCard_unmount(void);

bool sdCard_unmountUnsafe(void);

void sdCard_emergencyStop(void);

bool sdCard_isReady(void);

bool sdCard_getNumSectors(uint32_t* pNumSectors);

bool sdCard_getDiskSizeMb(uint32_t* pDiskSizeMb);

bool sdCard_getPartitionInfo(uint32_t* const pPartitionSizeMb,
    uint32_t* const pFreeSpaceMb,
    sdCard_FSType_e* const pFsType);


#ifdef __cplusplus
}
#endif

#endif /* _SD_CARD_H_ */
