#if !defined(_FILE_H_)
#define _FILE_H_

#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "os_layer/os_layer.h"
#include "ff.h"
#include "utils/zz.h"
#include "utils/sys_queue1.h"

#define OS_FILE_SUPPORT_SUSPEND     1

#ifdef __cplusplus
extern "C" {
#endif

/**********************************************************************************************************************/

typedef struct FileName_TAG
{
#define MAX_FILENAME_LEN     64
  char buf[MAX_FILENAME_LEN];
} FileName_t;

typedef struct FileInfo_TAG FileInfo_t;
struct FileInfo_TAG
{
#if OS_FILE_SUPPORT_SUSPEND
  ZTAILQ_ENTRY(FileInfo_TAG) list;
#endif
  uint8_t mode;
  FileName_t fileName;
  bool flagReady;
  bool flagFlushNeeded;
  FIL fil;
  uint32_t curPos;
};

/**********************************************************************************************************************/

bool fileInit(void);

#if OS_FILE_SUPPORT_SUSPEND
void fileSuspend(void);
void fileResume(void);
bool fileTryLockSemaSuspend(void);
void fileUnlockSemaSuspend(void);
#endif

bool fileLock(void);
void fileUnlock(void);

FileName_t fileNameVprintf(const char* fmt, va_list args);

ZZ_ATTR_FORMAT_PRINTF(1, 2)
FileName_t fileNamePrintf(const char* fmt, ...);

void fileNamePVprintf(FileName_t* const pFileName, const char* fmt, va_list args);

ZZ_ATTR_FORMAT_PRINTF(2, 3)
void fileNamePPrintf(FileName_t* const pFileName, const char* fmt, ...);

const char* fileErrInfo(FRESULT fres);

bool fileGetSize(const FileName_t* pFileName, size_t* const pFileSize);

size_t fileInfoGetSize(FileInfo_t* const pFileInfo);

const char* fileInfoGetFileName(FileInfo_t* const pFileInfo);

bool fileOpen(FileInfo_t* const pFileInfo, const FileName_t* const pFileName, const int fcntl_mode);

bool fileOpenForRead(FileInfo_t* const pFileInfo, const FileName_t* const pFileName);

bool fileOpenForWrite(FileInfo_t* const pFileInfo, const FileName_t* const pFileName);

bool fileOpenAppend(FileInfo_t* const pFileInfo, const FileName_t* const pFileName);

bool fileClose(FileInfo_t* const pFileInfo);

bool fileIsExist(const FileName_t* const pFileName);

bool fileIsExistEx(const char* const filePath, bool* const pFlagIsDir);

bool fileRemove(const FileName_t* const pFileName);

bool fileRemoveEx(const char* const filePath);

bool fileRename(const FileName_t* const pOldName, const FileName_t* const pNewName);

bool fileCopy(FileInfo_t* const pSrc, FileInfo_t* const pDst);

bool fileRenameSafe(const FileName_t* const pOldName, const FileName_t* const pNewName);

bool fileCloseAndRemove(FileInfo_t* const pFileInfo);

bool fileIsReady(FileInfo_t* pFileInfo);

bool fileSeek(FileInfo_t* const pFileInfo, const uint32_t offset);

bool fileReadEx(FileInfo_t* const pFileInfo, char* const buf, const size_t bufSize, uint32_t* const pNumBytesRead);

bool fileRead(FileInfo_t* const pFileInfo, char* const buf, const size_t bufSize);

char* fileGets(FileInfo_t* const pFileInfo, char* const buf, const size_t bufSize);

bool fileWrite(FileInfo_t* pFileInfo, const char* buf, size_t bufLen);

bool fileVprintf(FileInfo_t* pFileInfo, const char* fmt, va_list args);

bool filePuts(FileInfo_t* pFileInfo, const char* const lineBuf);

bool filePrintf(FileInfo_t* pFileInfo, const char* fmt, ...);

bool fileVprintfExt(FileInfo_t* pFileInfo, const char* prefix, const char* fmt, va_list args, const char* suffix);

bool fileFlush(FileInfo_t* pFileInfo);

bool fileIsEof(const FileInfo_t* const pFileInfo);

#ifdef __cplusplus
}
#endif

#endif /* _FILE_H_ */
