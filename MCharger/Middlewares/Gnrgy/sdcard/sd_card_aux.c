#include <string.h>
#include "sd_card.h"
#include "os_layer/os_layer.h"
#include "ff.h"
#include "utils/t_snprintf.h"
#include "log/log.h"
#include "file.h"
#include "hal_driver/hal_sdcard.h"

extern FATFS g_FAT_FS;

void sdCard_readDiskInfoUpdateDb(void)
{
#if defined(_WIN32)
  const BYTE fs_type = FS_FAT32;
#else
  const BYTE fs_type = g_FAT_FS.fs_type;
#endif

  switch (fs_type)
  {
  case 0:
    LOG_SDCARD_MSG("not mounted");
    return;
  case FS_FAT12:
    LOG_SDCARD_MSG("mounted successfully: %s", "FAT12");
    LOG_COMMON_INFO("Success (%s)", "FAT12");
    break;
  case FS_FAT16:
    LOG_SDCARD_MSG("mounted successfully: %s", "FAT16");
    LOG_COMMON_INFO("Success (%s)", "FAT16");
    break;
  case FS_FAT32:
    LOG_SDCARD_MSG("mounted successfully: %s", "FAT32");
    LOG_COMMON_INFO("Success (%s)", "FAT32");
    break;
  default:
    LOG_SDCARD_MSG("mounted successfully: %s", "Unknown");
    LOG_COMMON_INFO("Unknown file system");
    return;
  }
  LOG_COMMON_INFO("all tests successful");

  uint32_t diskSizeMb = 0;
  if (!sdCard_getDiskSizeMb(&diskSizeMb))
  {
	  LOG_COMMON_INFO("???GB");
    return;
  }
  LOG_COMMON_INFO("%u.%02uGB",
      (unsigned)(diskSizeMb / 1024),
      (unsigned)(((diskSizeMb % 1024) * 1000) / 1024 / 10));
  {
    uint32_t partitionSizeMb = 0;
    uint32_t freeSpaceMb = 0;
    sdCard_FSType_e fsType = sdCard_FSType_None;
    if (!sdCard_getPartitionInfo(&partitionSizeMb, &freeSpaceMb, &fsType))
    {
    	LOG_COMMON_INFO("???GB");
      return;
    }
    LOG_COMMON_INFO("%u.%02uGB of %u.%02uGB",
        (unsigned)(freeSpaceMb / 1024),
        (unsigned)(((freeSpaceMb % 1024) * 1000) / 1024 / 10),
        (unsigned)(partitionSizeMb / 1024),
        (unsigned)(((partitionSizeMb % 1024) * 1000) / 1024 / 10));
  }
}
