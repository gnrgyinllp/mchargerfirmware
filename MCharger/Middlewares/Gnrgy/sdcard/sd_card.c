#include <string.h>
#include "sd_card.h"
#include "ff.h"
#include "file.h"
#include "log/log.h"
#include "os_layer/os_layer.h"
#include "hal_driver/hal_sdcard.h"
#include "utils/t_snprintf.h"
#include "app/gnrgy_config.h"
#if !defined(_WIN32)
#include "fatfs.h"
#include "diskio.h"
#endif


#if !defined(PROJECT_USE_SD_CARD_LOCKING)
#error Macro PROJECT_USE_SD_CARD_LOCKING is not defined.
#endif

FATFS g_FAT_FS;
static bool g_sdCard_syncInit;
static bool g_sdCard_isDriverInitialized;
static bool g_sdCard_isMounted;
static uint32_t g_sdCard_numSectors;
#if PROJECT_USE_SD_CARD_LOCKING
static osLayer_Mutex_t g_sdCard_mutex;
static osLayer_Semaphore_t g_sdCard_semaAvailable;
static osLayer_Mutex_t g_sdCard_mutexForMountAndUnmount;
#endif

bool sdCard_powerOn(void)
{
  LOG_SDCARD_INFO("Power on SD-Card");
  osLayer_sleepTicks(100);
  return true;
}

void sdCard_init(void)
{
  if (!g_sdCard_syncInit)
  {
    g_sdCard_syncInit = true;
#if PROJECT_USE_SD_CARD_LOCKING
    osLayer_mutexInit(&g_sdCard_mutex);
    osLayer_semaInit(&g_sdCard_semaAvailable);
    osLayer_mutexInit(&g_sdCard_mutexForMountAndUnmount);
#endif
  }
}

bool sdCard_lock(void)
{
#if PROJECT_USE_SD_CARD_LOCKING
  LOG_SDCARD_NOISE("LOCK");
  LOG_SDCARD_TRACE("osLayer_mutexEnter(&g_sdCard_mutex)");
  osLayer_mutexEnter(&g_sdCard_mutex);
  if (!sdCard_isReady())
  {
    osLayer_mutexExit(&g_sdCard_mutex);
    return false;
  }
  LOG_SDCARD_TRACE("osLayer_semaWaitInfinite(&g_sdCard_semaAvailable)");
  osLayer_semaWaitInfinite(&g_sdCard_semaAvailable);
  LOG_SDCARD_TRACE("acquired semaAvailable");
#endif
  return true;
}

void sdCard_unlock(void)
{
#if PROJECT_USE_SD_CARD_LOCKING
  LOG_SDCARD_NOISE("UNLOCK");
  LOG_SDCARD_TRACE("osLayer_semaSignal(&g_sdCard_semaAvailable)");
  osLayer_semaSignal(&g_sdCard_semaAvailable);

  LOG_SDCARD_TRACE("osLayer_mutexExit(&g_sdCard_mutex)");
  osLayer_mutexExit(&g_sdCard_mutex);
#endif
}

static bool sdCard_createPartition(void)
{
#if !defined(_WIN32)
  BYTE* pWorkArea = osLayer_malloc(_MAX_SS);
  if (NULL == pWorkArea)
  {
    return false;
  }
  /* Initialize a brand-new disk drive mapped to physical drive 0 */
  DWORD plist[] = {100, 0, 0, 0}; /* Create one primary partition */
  const FRESULT fres = f_fdisk(0, plist, pWorkArea); /* Divide physical drive 0 */
  osLayer_free(pWorkArea);
  if (FR_OK != fres)
  {
    LOG_SDCARD_ERR("fdisk failed: %s", fileErrInfo(fres));
    return false;
  }
#endif
  LOG_SDCARD_INFO("primary partition created successfully");
  return true;
}

static bool sdCard_mkfs(void)
{
  const size_t wordAreaSize = _MAX_SS;
  void* pWorkArea = osLayer_malloc(wordAreaSize);
  if (NULL == pWorkArea)
  {
    LOG_SDCARD_ERR("Can't allocate mem for working area");
    return false;
  }

  const FRESULT fres = f_mkfs("0:", 0, 0, pWorkArea, wordAreaSize);
  osLayer_free(pWorkArea);
  if (FR_OK != fres)
  {
    LOG_SDCARD_ERR("mkfs failed: %s", fileErrInfo(fres));
    return false;
  }
  LOG_SDCARD_INFO("The drive was formatted in FAT successfully");
  return true;
}

static bool sdCard_readNumSectors(uint32_t* pNumSectors)
{
#if defined(_WIN32)
  uint32_t numSectorsOnDisk = 16384000;
#else
  DWORD numSectorsOnDisk = 0;
  const BYTE pdrv = 0;
  const DRESULT dres = disk_ioctl(pdrv, GET_SECTOR_COUNT, &numSectorsOnDisk);
  if (RES_OK != dres) {
    LOG_SDCARD_ERR("sdCard_getDiskSizeMb failed: dres=%d", (int) dres);
    return false;
  }
#endif
  *pNumSectors = numSectorsOnDisk;
  return true;
}


static bool sdCard_mountUnsafe(void)
{
  #if !defined(_WIN32)
  MX_FATFS_Init();
  #endif
  g_sdCard_isDriverInitialized = true;
  FRESULT fres = f_mount(&g_FAT_FS, "0:", 1);
  if (FR_OK != fres)
  {
    LOG_SDCARD_ERR("mount failed: %s", fileErrInfo(fres));
    if (FR_NO_FILESYSTEM != fres)
    {
      goto err;
    }
    if (!sdCard_createPartition())
    {
      goto err;
    }
    if (!sdCard_mkfs())
    {
      goto err;
    }
    fres = f_mount(&g_FAT_FS, "0:", 1);
    if (FR_OK != fres)
    {
      LOG_SDCARD_ERR("mount failed: %s", fileErrInfo(fres));
      goto err;
    }
  }
  if (!sdCard_readNumSectors(&g_sdCard_numSectors))
  {
    LOG_SDCARD_ERR("mount failed: can't get num sectors");
    goto err;
  }
  LOG_SDCARD_INFO("Mounted successfully");
  g_sdCard_isMounted = true;

  return true;

err:
  g_sdCard_isDriverInitialized = false;
  return false;
}

bool sdCard_mount(void)
{
  LOG_SDCARD_INFO("Try to mount");
#if PROJECT_USE_SD_CARD_LOCKING
  LOG_SDCARD_DBG("%s: osLayer_mutexEnter(&g_sdCard_mutexForMountAndUnmount)", __func__);
  osLayer_mutexEnter(&g_sdCard_mutexForMountAndUnmount);
  LOG_SDCARD_DBG("%s: acquired mutexForMountAndUnmount", __func__);
#endif
  bool res = false;

#if PROJECT_USE_SD_CARD_LOCKING
  LOG_SDCARD_DBG("%s: osLayer_mutexEnter(&g_sdCard_mutex)", __func__);
  osLayer_mutexEnter(&g_sdCard_mutex);
  LOG_SDCARD_DBG("%s: acquired g_sdCard_mutex", __func__);
#endif

#if PROJECT_USE_SD_CARD_LOCKING
  if (osLayer_semaWaitImmediate(&g_sdCard_semaAvailable))
  {
    osLayer_semaSignal(&g_sdCard_semaAvailable);
    LOG_SDCARD_WARN("mount: already mounted");
    LOG_SDCARD_DBG("%s: osLayer_mutexExit(&g_sdCard_mutex)", __func__);
    osLayer_mutexExit(&g_sdCard_mutex);
    LOG_SDCARD_DBG("%s: osLayer_mutexExit(&g_sdCard_mutexForMountAndUnmount)", __func__);
    osLayer_mutexExit(&g_sdCard_mutexForMountAndUnmount);
    return true;
  }
#endif

  res = sdCard_mountUnsafe();

#if PROJECT_USE_SD_CARD_LOCKING
  LOG_SDCARD_DBG("sdCard_mount: set semaAvailable");
  osLayer_semaSignal(&g_sdCard_semaAvailable);

  LOG_SDCARD_DBG("%s: osLayer_mutexExit(&g_sdCard_mutex)", __func__);
  osLayer_mutexExit(&g_sdCard_mutex);
  LOG_SDCARD_DBG("%s: osLayer_mutexExit(&g_sdCard_mutexForMountAndUnmount)", __func__);
  osLayer_mutexExit(&g_sdCard_mutexForMountAndUnmount);
#endif
  return res;
}

bool sdCard_unmountUnsafe(void)
{
  bool res = false;
  if (g_sdCard_isMounted)
  {
    g_sdCard_isMounted = false;
    FRESULT fres = f_mount(0, "0:", 1);
    if (fres != FR_OK)
    {
      LOG_SDCARD_WARN("unmount failed: %s", fileErrInfo(fres));
    }
    else
    {
      res = true;
    }
  }
  if (g_sdCard_isDriverInitialized)
  {
    g_sdCard_isDriverInitialized = false;
  }
  return res;
}

bool sdCard_unmount(void)
{
  bool res = false;
  LOG_SDCARD_INFO("sdCard_unmount");

#if PROJECT_USE_SD_CARD_LOCKING
  LOG_SDCARD_DBG("%s: osLayer_mutexEnter(&g_sdCard_mutexForMountAndUnmount)", __func__);
  osLayer_mutexEnter(&g_sdCard_mutexForMountAndUnmount);
  {
    LOG_SDCARD_DBG("%s: osLayer_mutexEnter(&g_sdCard_mutex)", __func__);
    osLayer_mutexEnter(&g_sdCard_mutex);
    LOG_SDCARD_DBG("%s: acquired g_sdCard_mutex", __func__);
    {
      LOG_SDCARD_DBG("osLayer_semaWaitImmediate(&g_sdCard_semaAvailable)");
      if (!osLayer_semaWaitImmediate(&g_sdCard_semaAvailable))
      {
        LOG_SDCARD_ERR("unmount: SD-Card is not mounted");
      }
      else
      {
        res = sdCard_unmountUnsafe();
      }
    }
    LOG_SDCARD_DBG("%s: osLayer_mutexExit(&g_sdCard_mutex)", __func__);
    osLayer_mutexExit(&g_sdCard_mutex);
  }
  LOG_SDCARD_DBG("%s: osLayer_mutexExit(&g_sdCard_mutexForMountAndUnmount)", __func__);
  osLayer_mutexExit(&g_sdCard_mutexForMountAndUnmount);
#else
  res = sdCard_unmountUnsafe();
#endif
  LOG_SDCARD_INFO("Unmounted successfully");
  return res;
}

bool sdCard_isReady(void)
{
  return g_sdCard_isMounted;
}

void sdCard_getStatus(void)
{
  g_sdCard_isMounted = false;
}

bool sdCard_getNumSectors(uint32_t* pNumSectors)
{
  *pNumSectors = g_sdCard_numSectors;
  return true;
}

bool sdCard_getDiskSizeMb(uint32_t* pDiskSizeMb)
{
  *pDiskSizeMb = 0;
  uint32_t numSectorsOnDisk = 0;
  if (!sdCard_getNumSectors(&numSectorsOnDisk))
  {
    return false;
  }
  const uint32_t diskSizeMb = numSectorsOnDisk / 1024 * _MIN_SS / 1024;
  LOG_SDCARD_INFO("sdCard_getDiskSizeMb: %lu sectors on disk", (unsigned long) numSectorsOnDisk);
  LOG_SDCARD_INFO("sdCard_getDiskSizeMb: %lu MiB", (unsigned long) diskSizeMb);
  *pDiskSizeMb = diskSizeMb;
  return true;
}

bool sdCard_getPartitionInfo(uint32_t* const pPartitionSizeMb,
    uint32_t* const pFreeSpaceMb,
    sdCard_FSType_e* const pFsType)
{
  *pFreeSpaceMb = 0;
#if defined(_WIN32)
  const uint32_t partitionSizeMb = 4923;
  const uint32_t freeSpaceMb = 3456;
  const sdCard_FSType_e fsType = sdCard_FSType_FAT32;
#else

  FATFS *fs = NULL;
  DWORD freeClust = 0;
  /* Get volume information and free clusters of drive 1 */
  if (!sdCard_lock())
  {
    LOG_FILE_ERR("SD-Card is absent");
    return false;
  }
  const FRESULT fres = f_getfree("0:", &freeClust, &fs);
  sdCard_unlock();
  if(FR_OK != fres)
  {
    LOG_SDCARD_WARN("sdCard_getPartitionInfo: f_getfree failed: %s", fileErrInfo(fres));
    return false;
  }

  /* Get total sectors and free sectors */
  const DWORD totalSect = (fs->n_fatent - 2) * fs->csize;
  const DWORD freeSect = freeClust * fs->csize;

#if _MAX_SS != _MIN_SS    /* Get disk sector size */
#error not supported.
#endif
  LOG_SDCARD_INFO("sdCard_getPartitionInfo: %lu free clusters", (unsigned long) freeClust);
  LOG_SDCARD_INFO("sdCard_getPartitionInfo: %lu free sectors", (unsigned long) freeSect);
  LOG_SDCARD_INFO("sdCard_getPartitionInfo: %lu total sectors", (unsigned long) totalSect);
  const uint32_t partitionSizeMb = totalSect / 1024 * _MIN_SS / 1024;
  const uint32_t freeSpaceMb = freeSect / 1024 * _MIN_SS / 1024;
  sdCard_FSType_e fsType = sdCard_FSType_Unknown;
  switch (fs->fs_type)
  {
  case FS_FAT12:
    LOG_SDCARD_INFO("sdCard_getPartitionInfo: file system: %s", "FAT12");
    fsType = sdCard_FSType_FAT12;
    break;
  case FS_FAT16:
    LOG_SDCARD_INFO("sdCard_getPartitionInfo: file system: %s", "FAT16");
    fsType = sdCard_FSType_FAT16;
    break;
  case FS_FAT32:
    LOG_SDCARD_INFO("sdCard_getPartitionInfo: file system: %s", "FAT32");
    fsType = sdCard_FSType_FAT32;
    break;
  default:
    LOG_SDCARD_INFO("sdCard_getPartitionInfo: file system: %s", "Unknown");
    break;
  }
#endif
  LOG_SDCARD_INFO("sdCard_getPartitionInfo: partition size: %lu MiB", (unsigned long) partitionSizeMb);
  LOG_SDCARD_INFO("sdCard_getPartitionInfo: free space    : %lu MiB", (unsigned long) freeSpaceMb);
  *pPartitionSizeMb = partitionSizeMb;
  *pFreeSpaceMb = freeSpaceMb;
  *pFsType = fsType;
  return true;
}

void sdCard_emergencyStop(void)
{
}
