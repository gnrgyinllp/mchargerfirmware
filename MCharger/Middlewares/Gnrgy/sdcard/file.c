#include "file.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "log/log.h"
#include "sdcard/sd_card.h"
#include "ff.h"
#include "utils/t_snprintf.h"
#include "utils/sys_queue1.h"
#include "os_layer/os_layer.h"
#include "os_layer/os_dir.h"

static char g_fileLineBuf[1024];
static osLayer_Mutex_t g_fileLineMutex;

static osLayer_Mutex_t g_file_fnoMutex;
static FILINFO g_file_fno;
static osLayer_Mutex_t g_fileMutex;
#if OS_FILE_SUPPORT_SUSPEND
static ZTAILQ_HEAD(FileHeadOfListOfOpenedFiles, FileInfo_TAG) g_fileListOfOpenedFiles;
static osLayer_Semaphore_t g_fileSemaSuspend;
#endif

#define	O_RDONLY	0		/* +1 == FREAD */
#define	O_WRONLY	1		/* +1 == FWRITE */
#define	O_RDWR		2		/* +1 == FREAD|FWRITE */
#define	O_APPEND	0x0008          /* append (writes guaranteed at the end) */
#define	O_CREAT		0x0200          /* open with file create */
#define	O_TRUNC		0x0400	        /* open with truncation */
/**********************************************************************************************************************/

bool fileInit(void)
{
  osLayer_mutexInit(&g_fileMutex);
  osLayer_mutexInit(&g_fileLineMutex);
  osLayer_mutexInit(&g_file_fnoMutex);
#if OS_FILE_SUPPORT_SUSPEND
  ZTAILQ_INIT(&g_fileListOfOpenedFiles);
  osLayer_semaInit(&g_fileSemaSuspend);
  osLayer_semaSignal(&g_fileSemaSuspend);
#endif
  return true;
}

#if OS_FILE_SUPPORT_SUSPEND
bool fileTryLockSemaSuspend(void)
{
  return osLayer_semaWaitImmediate(&g_fileSemaSuspend);
}

void fileUnlockSemaSuspend(void)
{
  osLayer_semaSignal(&g_fileSemaSuspend);
}
#endif

bool fileLock(void)
{
  LOG_FILE_NOISE("fileLock");
  if (!osLayer_mutexEnter(&g_fileMutex))
  {
    LOG_FILE_ERR("osLayer_mutexEnter failed");
    return false;
  }
  if (!sdCard_lock())
  {
    LOG_FILE_ERR("SD-Card is absent");
    osLayer_mutexExit(&g_fileMutex);
    return false;
  }
  return true;
}

void fileUnlock(void)
{
  LOG_FILE_NOISE("fileUnlock");
  sdCard_unlock();
  osLayer_mutexExit(&g_fileMutex);
}

const char* fileErrInfo(FRESULT fres)
{
  static char unknownErrBuf[32];
  switch (fres)
  {
    case FR_OK: /* (0) Succeeded */
      return "[0] Succeeded";
    case FR_DISK_ERR: /* (1) A hard error occurred in the low level disk I/O layer */
      return "[1] Hardware disk error";
    case FR_INT_ERR: /* (2) Assertion failed */
      return "[2] Assertion failed";
    case FR_NOT_READY: /* (3) The physical drive cannot work */
      return "[3] Not ready";
    case FR_NO_FILE: /* (4) Could not find the file */
      return "[4] Could not find the file";
    case FR_NO_PATH: /* (5) Could not find the path */
      return "[5] Could not find the path";
    case FR_INVALID_NAME: /* (6) The path name format is invalid */
      return "[6] The path name format is invalid";
    case FR_DENIED: /* (7) Access denied due to prohibited access or directory full */
      return "[7] Access denied due to prohibited access or directory full";
    case FR_EXIST: /* (8) Access denied due to prohibited access */
      return "[8] Already exist";
    case FR_INVALID_OBJECT: /* (9) The file/directory object is invalid */
      return "[9] The file/directory object is invalid";
    case FR_WRITE_PROTECTED: /* (10) The physical drive is write protected */
      return "[10] The physical drive is write protected";
    case FR_INVALID_DRIVE: /* (11) The logical drive number is invalid */
      return "[11] The logical drive number is invalid";
    case FR_NOT_ENABLED: /* (12) The volume has no work area */
      return "[12] The volume has no work area";
    case FR_NO_FILESYSTEM: /* (13) There is no valid FAT volume */
      return "[13] There is no valid FAT volume";
    case FR_MKFS_ABORTED: /* (14) The f_mkfs() aborted due to any parameter error */
      return "[14] The f_mkfs aborted due to any parameter error";
    case FR_TIMEOUT: /* (15) Could not get a grant to access the volume within defined period */
      return "[15] Timeout";
    case FR_LOCKED: /* (16) The operation is rejected according to the file sharing policy */
      return "[16] Locked";
    case FR_NOT_ENOUGH_CORE: /* (17) LFN working buffer could not be allocated */
      return "[17] LFN working buffer could not be allocated";
    case FR_TOO_MANY_OPEN_FILES: /* (18) Number of open files > _FS_LOCK */
      return "[18] Number of open files > _FS_LOCK";
    case FR_INVALID_PARAMETER: /* (19) Given parameter is invalid */
      return "[19] Invalid paramener";
    default:
      snprintf(unknownErrBuf, sizeof(unknownErrBuf), "[%d] Unknown error", fres);
      return unknownErrBuf;
  }
}

void fileNamePVprintf(FileName_t* const pFileName, const char* fmt, va_list args)
{
  vsnprintf(pFileName->buf, sizeof(pFileName->buf), fmt, args);
}

void fileNamePPrintf(FileName_t* const pFileName, const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  fileNamePVprintf(pFileName, fmt, args);
  va_end(args);
}

FileName_t fileNameVprintf(const char* fmt, va_list args)
{
  FileName_t fileName = {{0}};
  vsnprintf(fileName.buf, sizeof(fileName.buf), fmt, args);
  return fileName;
}

FileName_t fileNamePrintf(const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  FileName_t fileName = fileNameVprintf(fmt, args);
  va_end(args);
  return fileName;
}

void fileNameCopy(FileName_t* const dst, const FileName_t* const src)
{
  strncpy(dst->buf, src->buf, sizeof(dst->buf));
  dst->buf[sizeof(dst->buf) - 1] = '\0';
}

bool fileGetSize(const FileName_t* pFileName, size_t* const pFileSize)
{
  *pFileSize = 0;
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  osLayer_mutexEnter(&g_file_fnoMutex);
  const FRESULT fres = f_stat(pFileName->buf, &g_file_fno);
  if (FR_OK != fres)
  {
    osLayer_mutexExit(&g_file_fnoMutex);
    fileUnlock();
    return false;
  }
  *pFileSize = g_file_fno.fsize;
  osLayer_mutexExit(&g_file_fnoMutex);
  fileUnlock();
  return true;
}

size_t fileInfoGetSize(FileInfo_t* const pFileInfo)
{
  return f_size(&pFileInfo->fil);
}

const char* fileInfoGetFileName(FileInfo_t* const pFileInfo)
{
  const char* p = strrchr(pFileInfo->fileName.buf, '/');
  if (NULL == p)
  {
    return pFileInfo->fileName.buf;
  }
  else
  {
    return p + 1;
  }
}

bool fileOpen(FileInfo_t* const pFileInfo, const FileName_t* const pFileName, const int fcntl_mode)
{
  LOG_FILE_NOISE("Open file %s", pFileName->buf);
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  if (pFileInfo->flagReady)
  {
    fileUnlock();
    LOG_FILE_ERR("FileInfo is busy");
    return false;
  }
  uint8_t f_mode = 0;
  if ((O_WRONLY == (fcntl_mode & O_WRONLY)) || (O_RDWR == (fcntl_mode & O_RDWR)))
  {
    f_mode |= FA_WRITE;
    if ((O_CREAT | O_TRUNC) == (fcntl_mode & (O_CREAT | O_TRUNC)))
    {
      f_mode |= FA_CREATE_ALWAYS;
    }
    else if (fcntl_mode & O_CREAT)
    {
      f_mode |= FA_CREATE_NEW;
    }
  }
  else
  {
    f_mode |= FA_READ;
    if (fcntl_mode & O_APPEND)
    {
      f_mode |= FA_OPEN_ALWAYS;
    }
  }

  fileNameCopy(&pFileInfo->fileName, pFileName);
  pFileInfo->mode = f_mode;

  const FRESULT fres = f_open(&pFileInfo->fil, pFileInfo->fileName.buf, pFileInfo->mode);
  if (FR_OK != fres)
  {
    fileUnlock();
    LOG_FILE_ERR("Can't open file '%s' with mode=0x%02x: %s",
        pFileInfo->fileName.buf,
        pFileInfo->mode,
        fileErrInfo(fres));
    return false;
  }
#if OS_FILE_SUPPORT_SUSPEND
  ZTAILQ_INSERT_TAIL(&g_fileListOfOpenedFiles, pFileInfo, list);
#endif
  pFileInfo->flagReady = true;
  fileUnlock();
  return true;
}

bool fileOpenForRead(FileInfo_t* const pFileInfo, const FileName_t* const pFileName)
{
  const int fcntl_mode = O_RDONLY;
  const bool res = fileOpen(pFileInfo, pFileName, fcntl_mode);
  return res;
}

bool fileOpenForWrite(FileInfo_t* const pFileInfo, const FileName_t* const pFileName)
{
  const int fcntl_mode = O_WRONLY | O_CREAT | O_TRUNC;
  const bool res = fileOpen(pFileInfo, pFileName, fcntl_mode);
  return res;
}

bool fileOpenAppend(FileInfo_t* const pFileInfo, const FileName_t* const pFileName)
{
  const int fcntl_mode = O_WRONLY | O_APPEND;
  if (!fileOpen(pFileInfo, pFileName, fcntl_mode))
  {
    return false;
  }

  /* Seek to end of the file to append data */
  if (!fileSeek(pFileInfo, fileInfoGetSize(pFileInfo)))
  {
    fileClose(pFileInfo);
    return false;
  }
  return true;
}

bool fileClose(FileInfo_t* const pFileInfo)
{
  LOG_FILE_NOISE("Close file %s", pFileInfo->fileName.buf);
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  if (!pFileInfo->flagReady)
  {
    fileUnlock();
    LOG_FILE_WARN("%s: File is not opened", __func__);
    return true;
  }

#if OS_FILE_SUPPORT_SUSPEND
  ZTAILQ_REMOVE(&g_fileListOfOpenedFiles, pFileInfo, list);
#endif
  pFileInfo->fileName.buf[0] = '\0';
  pFileInfo->flagReady = false;
  const FRESULT fres = f_close(&pFileInfo->fil);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOG_FILE_ERR("Can't close file %s: %s", pFileInfo->fileName.buf, fileErrInfo(fres));
    return false;
  }
  return true;
}

#if OS_FILE_SUPPORT_SUSPEND
void fileSuspend(void)
{
  LOG_FILE_INFO("SUSPEND");
  if (!osLayer_semaWaitInfinite(&g_fileSemaSuspend))
  {
    LOG_FILE_ERR("osLayer_semaWaitInfinite(g_fileSemaSuspend) failed");
    return;
  }
  if (!osLayer_mutexEnter(&g_fileMutex))
  {
    LOG_FILE_ERR("osLayer_mutexEnter(g_fileMutex) failed");
    return;
  }
  if (!sdCard_lock())
  {
    LOG_FILE_ERR("sdCard_lock failed");
    osLayer_mutexExit(&g_fileMutex);
    return;
  }

  {
    FileInfo_t* pFileInfo = NULL;
    ZTAILQ_FOREACH(pFileInfo, &g_fileListOfOpenedFiles, list)
    {
      pFileInfo->curPos = f_tell(&pFileInfo->fil);
      if (pFileInfo->mode & FA_WRITE)
      {
        pFileInfo->mode &= ~(FA_CREATE_ALWAYS | FA_CREATE_NEW);
        pFileInfo->mode |= FA_OPEN_ALWAYS;
      }
      else
      {
        pFileInfo->mode |= FA_OPEN_ALWAYS;
      }
      LOG_FILE_INFO("SUSPEND: close file %s at offset %u with mode 0x%02x",
          pFileInfo->fileName.buf,
          (unsigned )pFileInfo->curPos,
          (unsigned )pFileInfo->mode);
      const FRESULT fres = f_close(&pFileInfo->fil);
      if (FR_OK != fres)
      {
        LOG_FILE_ERR("Can't close file %s: %s", pFileInfo->fileName.buf, fileErrInfo(fres));
      }
    }
  }
  osDir_closeAllOpenedDirsOnSuspend();
  sdCard_unlock();
  sdCard_unmount();
}

void fileResume(void)
{
  LOG_FILE_INFO("RESUME");

  sdCard_mount();

  if (!sdCard_lock())
  {
    LOG_FILE_ERR("sdCard_lock failed");
    return;
  }

  {
    FileInfo_t* pFileInfo = NULL;
    ZTAILQ_FOREACH(pFileInfo, &g_fileListOfOpenedFiles, list)
    {
      {
        LOG_FILE_INFO("RESUME: reopen file %s at offset %u with mode 0x%02x", pFileInfo->fileName.buf,
                      (unsigned)pFileInfo->curPos, (unsigned)pFileInfo->mode);
        const FRESULT fres = f_open(&pFileInfo->fil, pFileInfo->fileName.buf, pFileInfo->mode);
        if (FR_OK != fres)
        {
          LOG_FILE_ERR("Can't reopen file '%s' with mode=0x%02x: %s",
                       pFileInfo->fileName.buf,
                       pFileInfo->mode,
                       fileErrInfo(fres));

          continue;
        }
      }
      {
        const FRESULT fres = f_lseek(&pFileInfo->fil, pFileInfo->curPos);
        if (FR_OK != fres)
        {
          LOG_FILE_ERR("Can't repos file '%s' to offset %u", pFileInfo->fileName.buf, (unsigned)pFileInfo->curPos);
          continue;
        }
      }
    }
  }
  osLayer_mutexEnter(&g_file_fnoMutex);
  osDir_openAllOpenedDirsOnResume(&g_file_fno);
  osLayer_mutexExit(&g_file_fnoMutex);

  sdCard_unlock();
  if (!osLayer_mutexExit(&g_fileMutex))
  {
    LOG_FILE_ERR("osLayer_mutexEnter failed");
  }
  if (!osLayer_semaSignal(&g_fileSemaSuspend))
  {
    LOG_FILE_ERR("osLayer_semaSignal(g_fileSemaSuspend) failed");
  }
}
#endif

bool fileIsExistEx(const char* const filePath, bool* const pFlagIsDir)
{
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  osLayer_mutexEnter(&g_file_fnoMutex);
  const FRESULT fres = f_stat(filePath, &g_file_fno);
  if (NULL != pFlagIsDir)
  {
    *pFlagIsDir = (0 != (g_file_fno.fattrib & AM_DIR)) ? true : false;
  }
  osLayer_mutexExit(&g_file_fnoMutex);
  fileUnlock();
  if (FR_OK != fres)
  {
    return false;
  }
  return true;
}

bool fileIsExist(const FileName_t* const pFileName)
{
  return fileIsExistEx(pFileName->buf, NULL);
}

bool fileRemoveEx(const char* const filePath)
{
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_unlink(filePath);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOG_FILE_ERR("Can't remove file %s: %s", filePath, fileErrInfo(fres));
    return false;
  }
  return true;
}

bool fileRemove(const FileName_t* const pFileName)
{
  return fileRemoveEx(pFileName->buf);
}

bool fileRename(const FileName_t* const pOldName, const FileName_t* const pNewName)
{
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_rename(pOldName->buf, pNewName->buf);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOG_FILE_ERR("Can't rename file %s to %s: %s", pOldName->buf, pNewName->buf, fileErrInfo(fres));
    return false;
  }
  return true;
}

bool fileCopy(FileInfo_t* const pSrc, FileInfo_t* const pDst)
{
  const size_t srcSize = fileInfoGetSize(pSrc);
  size_t remLen = srcSize;
  osLayer_mutexEnter(&g_fileLineMutex);
  while (remLen > 0)
  {
    const size_t blockLen = remLen < sizeof(g_fileLineBuf) ? remLen : sizeof(g_fileLineBuf);
    if (!fileRead(pSrc, g_fileLineBuf, blockLen))
    {
      LOG_COMMON_ERR("Can't read from file %s", pSrc->fileName.buf);
      return false;
    }
    if (!fileWrite(pDst, g_fileLineBuf, blockLen))
    {
      LOG_COMMON_ERR("Can't write to file %s", pDst->fileName.buf);
      return false;
    }
    remLen -= blockLen;
  }
  osLayer_mutexExit(&g_fileLineMutex);
  return true;
}

bool fileRenameSafe(const FileName_t* const pOldName, const FileName_t* const pNewName)
{
  const FileName_t fileNameBak = fileNamePrintf("%s.bak", pNewName->buf);

  if (fileIsExist(&fileNameBak))
  {
    fileRemove(&fileNameBak);
  }
  const bool isFileExist = fileIsExist(pNewName);
  if (isFileExist)
  {
    if (!fileRename(pNewName, &fileNameBak))
    {
      LOG_COMMON_WARN("Can't rename file from %s to %s", pNewName->buf, fileNameBak.buf);
    }
  }
  if (!fileRename(pOldName, pNewName))
  {
    LOG_COMMON_WARN("Can't rename file from %s to %s", pOldName->buf, pNewName->buf);
    return false;
  }
  if (isFileExist)
  {
    if (!fileRemove(&fileNameBak))
    {
      LOG_COMMON_WARN("Can't remove file %s", fileNameBak.buf);
    }
  }
  return true;
}

bool fileCloseAndRemove(FileInfo_t* const pFileInfo)
{
  const FileName_t fileName = pFileInfo->fileName;
  fileClose(pFileInfo);
  const bool res = fileRemove(&fileName);
  return res;
}

bool fileIsReady(FileInfo_t* pFileInfo)
{
  return pFileInfo->flagReady;
}

bool fileSeek(FileInfo_t* const pFileInfo, const uint32_t offset)
{
  if (!pFileInfo->flagReady)
  {
    LOGF_FILE_ERR("File is not opened");
    return false;
  }
  if (!fileLock())
  {
    LOGF_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_lseek(&pFileInfo->fil, (DWORD)offset);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOGF_FILE_ERR("Seek for file %s failed: %s", pFileInfo->fileName.buf, fileErrInfo(fres));
    return false;
  }
  return true;
}

bool fileReadEx(FileInfo_t* const pFileInfo, char* const buf, const size_t bufSize, uint32_t* const pNumBytesRead)
{
  UINT bw = 0; // variable to return number of bytes written
  if (!pFileInfo->flagReady)
  {
    LOGF_FILE_ERR("File is not opened");
    return false;
  }
  if (!fileLock())
  {
    LOGF_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_read(&pFileInfo->fil, buf, bufSize, &bw);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOGF_FILE_ERR("read from file %s failed: %s", pFileInfo->fileName.buf, fileErrInfo(fres));
    return false;
  }
  *pNumBytesRead = bw;
  return true;
}

bool fileRead(FileInfo_t* const pFileInfo, char* const buf, const size_t bufSize)
{
  uint32_t numBytesRead = 0;
  if (!fileReadEx(pFileInfo, buf, bufSize, &numBytesRead))
  {
    return false;
  }
  if (numBytesRead != bufSize)
  {
    LOG_FILE_ERR(
        "Read from file %s failed: read %lu bytes from %lu",
        pFileInfo->fileName.buf,
        (unsigned long)numBytesRead,
        (unsigned long)bufSize);
    return false;
  }
  return true;
}

char* fileGets(FileInfo_t* const pFileInfo, char* const buf, const size_t bufSize)
{
  if (!pFileInfo->flagReady)
  {
    LOGF_FILE_ERR("File is not opened");
    return false;
  }
  if (!fileLock())
  {
    LOGF_FILE_ERR("fileLock failed");
    return false;
  }
  char* const ret = f_gets(buf, bufSize, &pFileInfo->fil);
  fileUnlock();
  return ret;
}

bool fileWrite(FileInfo_t* pFileInfo, const char* buf, size_t bufLen)
{
  UINT bw = 0; // variable to return number of bytes written
  pFileInfo->flagFlushNeeded = true;
  if (!pFileInfo->flagReady)
  {
    LOGF_FILE_ERR("File is not opened");
    return false;
  }
  if (!fileLock())
  {
    LOGF_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_write(&pFileInfo->fil, buf, bufLen, &bw);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOGF_FILE_ERR("Write to file '%s' failed: %s", pFileInfo->fileName.buf, fileErrInfo(fres));
    return false;
  }
  return true;
}

bool fileVprintfExt(FileInfo_t* pFileInfo, const char* prefix, const char* fmt, va_list args, const char* suffix)
{
  int idx = 0;
  osLayer_mutexEnter(&g_fileLineMutex);
  if (NULL != prefix)
  {
    idx += t_snprintf(&g_fileLineBuf[idx], sizeof(g_fileLineBuf) - idx, "%s", prefix);
  }
  idx += t_vsnprintf(&g_fileLineBuf[idx], sizeof(g_fileLineBuf) - idx, fmt, args);
  if (NULL != suffix)
  {
    idx += t_snprintf(&g_fileLineBuf[idx], sizeof(g_fileLineBuf) - idx, "%s", suffix);
  }
  const bool res = fileWrite(pFileInfo, g_fileLineBuf, (size_t)idx);
  osLayer_mutexExit(&g_fileLineMutex);
  return res;
}

bool fileVprintf(FileInfo_t* pFileInfo, const char* fmt, va_list args)
{
  return fileVprintfExt(pFileInfo, NULL, fmt, args, NULL);
}

bool filePrintf(FileInfo_t* pFileInfo, const char* fmt, ...)
{
  bool res = false;
  va_list args;
  va_start(args, fmt);
  if (fileVprintf(pFileInfo, fmt, args))
  {
    res = true;
  }
  va_end(args);
  return res;

}

bool filePuts(FileInfo_t* pFileInfo, const char* const lineBuf)
{
  return filePrintf(pFileInfo, "%s", lineBuf);
}


bool fileFlush(FileInfo_t* pFileInfo)
{
  if (!pFileInfo->flagFlushNeeded)
  {
    return true;
  }
  pFileInfo->flagFlushNeeded = false;
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_sync(&pFileInfo->fil);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOG_FILE_ERR("Flush file %s failed: %s", pFileInfo->fileName.buf, fileErrInfo(fres));
    return false;
  }
  return true;
}

bool fileIsEof(const FileInfo_t* const pFileInfo)
{
  if (!pFileInfo->flagReady)
  {
    LOGF_FILE_ERR("File is not opened");
    return false;
  }
  const bool ret = (bool)f_eof(&pFileInfo->fil);
  return ret;
}
