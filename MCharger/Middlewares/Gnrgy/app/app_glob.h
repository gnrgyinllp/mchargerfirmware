
#ifndef APP_GLOB_H_
#define APP_GLOB_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

extern void app_traceMalloc(void* pvAddress, unsigned long uiSize);
extern void app_traceFree(void* pvAddress, unsigned long uiSize);

#ifdef __cplusplus
}
#endif
#endif /* APP_GLOB_H_ */
