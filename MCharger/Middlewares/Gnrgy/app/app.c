#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "gnrgy_config.h"
#include "log/log.h"
#include "buffer/cycle_buf.h"
#include "gsm/at_uart.h"
#include "gsm/at_cmd.h"
#include "os_driver/hal_uart.h"
#include "stm32f4xx_hal.h"
#include "sdcard/file.h"
#include "sdcard/sd_card.h"
#include "app/gnrgy_config.h"
#include "eeprom/drv_eeprom.h"
#include "led/led_rgb.h"
#include "temp/temp.h"
#include "meter/meter.h"
#include "temp/drv_temp.h"
#include "os_driver/os_drv_i2c.h"
#include "sdcard/file.h"
#include "eeprom/user_eeprom.h"
#include "EVSEtest1.h"
#include "serial.h"
#include "queue.h"
#include "FreeRTOS_CLI.h"
#include "eeprom.h"
#include "lib_95HFConfigManager.h"
#include "rfid.h"
#include "Meter7858A.h"
#include "drv_BLESerial.h"

/* The priority used by the UART command console task. */
#define mainUART_COMMAND_CONSOLE_TASK_PRIORITY	( configMAX_PRIORITIES - 2 )
#define mainUART_COMMAND_CONSOLE_STACK_SIZE	( configMINIMAL_STACK_SIZE * 3UL )
/* Private variables ---------------------------------------------------------*/
extern void vRegisterSampleCLICommands( void );
extern void vUARTCommandConsoleStart( uint16_t usStackSize, UBaseType_t uxPriority );
extern void vBLECommandConsoleStart( uint16_t usStackSize, UBaseType_t uxPriority );
extern void vRFIDOperationTaskStart( uint16_t usStackSize, UBaseType_t uxPriority );
extern void WhiteLedFlash(void);
extern char Rx_data[100],RfidEE[2];;
extern char CP_FinalStatus;
extern char FirstEE[2],BackBoneSNEE[12],PasswordEE[12],LoginEE[2],MaxCurrentEE[2],CurrentEE[2],CSTypeEE[2],MeterEE[2],BleEE[2],RS485EE[2],RS485AddEE[2],RfidEE[2],
AuthEE[2],OutputTypeEE[2],FirmwareEE[6],DateFlashEE[8];
/* Virtual address defined by the user: 0xFFFF value is prohibited
for the eeprom emmulation in FLASH memory*/
uint16_t Vir,VirtAddVarTab[EEPROM_NUM_VAR];
uint16_t VarDataTab[EEPROM_NUM_VAR];
uint16_t VarValue,VarDataTmp = 0;
extern uint8_t TagUID[];
extern ISO14443A_CARD ISO14443A_Card;
extern uint8_t         IdAllowed;
extern uint8_t         Buzzer_State;
extern CycleBuf_t* g_gsmTask_cycleBufUART;
extern UART_HandleTypeDef huart2;
static osLayer_Mutex_t g_mutexMainThread;
FileInfo_t g_file;
uint8_t g_whitelist[4];
osDrvI2c_t* g_pDrvI2c;
bool flagEnableCharger = false;
osThreadId CP_CheckHandle;
osThreadId RFIDtaskHandle;
osThreadId GSMTaskHandle;
MainState_Tag g_MainState = MAIN_STATE_IDLE;
bool g_blFlagAuthenEnable = false;
void fCp_Check(void const * argument);
void fRFIDtask(void const * argument);
void gsmTask_Check(void const * argument);

void mainThread_mutexInit(void)
{
  osLayer_mutexInit(&g_mutexMainThread);
}

void mainThread_mutexEnter(void)
{
  osLayer_mutexEnter(&g_mutexMainThread);
}

void mainThread_mutexExit(void)
{
  osLayer_mutexExit(&g_mutexMainThread);
}

bool I2C_Init(void)
{
  bool LenStatus = false;
  g_pDrvI2c = osDrvI2c_init(1);
  if (NULL == g_pDrvI2c)
  {
    LOG_COMMON_CRIT("osDrvI2c_init failed");
    return false;
  }
  LenStatus = eeprom_init();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("eeprom_init failed");
    return false;
  }
  return true;
}

bool File_CreateExample(void)
{
  char fileBuf[4];
  const FileName_t fileName = fileNamePrintf("whiteblacklist.db");
  if (!fileOpenForWrite(&g_file, &fileName))
  {
    LOG_COMMON_ERR("fileOpenForWrite failed");
    return false;
  }
  /* White dummy while list */
  fileBuf[0] = 'A';
  fileBuf[1] = 'A';
  fileBuf[2] = 'B';
  fileBuf[3] = 'B';
  if (!fileWrite(&g_file, fileBuf, sizeof(fileBuf)))
  {
    LOG_COMMON_ERR("fileWrite failed");
    fileClose(&g_file);
    return false;
  }
  if (!fileClose(&g_file))
  {
    LOG_COMMON_ERR("fileClose failed");
    return false;
  }
  
  return true;
}

bool File_ReadBlackWhiteList(void)
{
  const FileName_t fileName = fileNamePrintf("whiteblacklist.db");
  if (!fileOpenForRead(&g_file, &fileName))
  {
    LOG_COMMON_ERR("fileOpenForWrite failed");
    return false;
  }

  if (!fileRead(&g_file, g_whitelist, sizeof(g_whitelist)))
  {
    LOG_COMMON_ERR("fileWrite failed");
    fileClose(&g_file);
    return false;
  }
  if (!fileClose(&g_file))
  {
    LOG_COMMON_ERR("fileClose failed");
    return false;
  }
  
  return true;
}

bool RFID_Check(uint8_t * input)
{
  for (int i = 0; i < sizeof(g_whitelist); i++)
  {
    if (g_whitelist[i] != input[i]) return false;
  }
  return true;
}

int mainTaskFunc(void)
{
  int i;
  log_init();
  mainThread_mutexInit();
  LOG_COMMON_INFO("System Console Log Gnrgy");
  LOG_COMMON_INFO("Initial I2C driver");
  if (false == I2C_Init())
  {
    LOG_COMMON_ERR("Cannot initial I2C_Init");
    return 0;
  }
  
  for (Vir=0;Vir<EEPROM_NUM_VAR;Vir++){
#ifdef DEVELOPMENT
    VirtAddVarTab[Vir] = Vir;
#else
    VirtAddVarTab[Vir] = EEPROM_START_ADDRESS + Vir;
#endif
    VarDataTab[Vir] = 0;
  }

#ifndef DEVELOPMENT
  /* Unlock the Flash Program Erase controller */
  HAL_FLASH_Unlock();
  /* EEPROM Init */
  if( EE_Init() != EE_OK)
  {
    Error_Handler();
  }

#endif
  LOG_COMMON_INFO("Read EEPROM data");
  ReadEepromEVSEValues();
  LOG_COMMON_INFO("Finished read EEPROM data");

  LOG_COMMON_INFO("Check Bluetooth");         
  drv_BLECheckFirstBoot();
  drv_BLEGetConfig();
  InitWriteEepromEVSEValues();
#if 1
  LOG_COMMON_MSG("Mounting SD-Card");
  if (!halSDcard_init())
  {
    LOG_COMMON_ERR("halSDcard_init failed");
  }
  LOG_COMMON_MSG("halSDcard_init");
  sdCard_init();
  if (sdCard_mount())
  {
    fileInit();
    sdCard_readDiskInfoUpdateDb();
    configFileStructure();
  }
  else
  {
    LOG_COMMON_ERR("sdCard_mount failed");
  }
  File_CreateExample();
#endif
  GSMTask_init();
  Gsm_Init();
  vRegisterSampleCLICommands();
  LOG_COMMON_INFO("Start thread vUARTCommandConsoleStart");
  vUARTCommandConsoleStart( mainUART_COMMAND_CONSOLE_STACK_SIZE, osPriorityLow );
  LOG_COMMON_INFO("Start thread vBLECommandConsoleStart");
  vBLECommandConsoleStart( mainUART_COMMAND_CONSOLE_STACK_SIZE, osPriorityLow );
  LOG_COMMON_INFO("Start thread vRFIDOperationTaskStart");

  vRFIDOperationTaskStart( mainUART_COMMAND_CONSOLE_STACK_SIZE, osPriorityLow );
  LOG_COMMON_INFO("Start thread RFIDtask");
  osThreadDef(RFIDtask, fRFIDtask, osPriorityBelowNormal, 0, 1024);
  RFIDtaskHandle = osThreadCreate(osThread(RFIDtask), NULL);

  osThreadDef(GSMTask, gsmTask_Check, osPriorityBelowNormal,0, 1024);
  GSMTaskHandle = osThreadCreate(osThread(GSMTask), NULL);
  LOG_COMMON_INFO("Finished create tasks");
  LOG_COMMON_INFO("Start thread CP Check");
  osThreadDef(CP_Check, fCp_Check, osPriorityRealtime, 0, 1024);
  CP_CheckHandle = osThreadCreate(osThread(CP_Check), NULL);
  /* add threads, ... */
  
  User_Init();
  if ((RfidEE[0] == '0') && (RfidEE[1] == '1'))
  {
    IdAllowed = 0;
    g_MainState = MAIN_STATE_INITAL;
    g_blFlagAuthenEnable = false;
  }
  else if ((RfidEE[0] == '0') && (RfidEE[1] == '0'))
  {
    IdAllowed = 1;
    g_MainState = MAIN_STATE_PROCESS_CP;
    g_blFlagAuthenEnable = true;
  }
  else
  {}
  while(1)
  {
    switch (g_MainState)
    {
    case MAIN_STATE_INITAL:
      {
        g_blFlagAuthenEnable = false;
        break;
      }
    case MAIN_STATE_ENABLE_RFID:
      {
        Buzzer_State = BEEP_LONG;
        g_blFlagAuthenEnable = true;
        g_MainState = MAIN_STATE_PROCESS_CP;
        break;
      }
    case MAIN_STATE_DISABLE_RFID:
      {
        Buzzer_State = BEEP_LONG;
        g_blFlagAuthenEnable = false;
        g_MainState = MAIN_STATE_PROCESS_CP;
        break;
      }
    case MAIN_STATE_INVALID_RFID:
      {
        g_blFlagAuthenEnable = false;
        Buzzer_State = BEEP_ALERT;
        break;
      }
    case MAIN_STATE_PROCESS_CP:
      {
        if (true == g_blFlagAuthenEnable)
        {
          IdAllowed = 1;
        }
        else
        {
          IdAllowed = 0;
        }
      }
    }
  
    ADC_Process();
    CheckLedsState();
    CheckBuzzerState();
    CheckLoginTime();
    osDelay(10);
  }
}

/* fCp_Check function */
void fCp_Check(void const * argument)
{
  /* USER CODE BEGIN fCp_Check */
  uint8_t CPState_Char;

  /* Infinite loop */
  for(;;)
  {
    
    CPState_Char = CP_FinalStatus;
    CP_State_Machine_Handler( CPState_Char);
    
    osDelay(10);
  }
  /* USER CODE END fCp_Check */
}

/* fRFIDtask function */
void fRFIDtask(void const * argument)
{
  /* USER CODE BEGIN fRFIDtask */
  int8_t TagType = TRACK_NOTHING;
  bool TagDetected = false;
  
  /* 95HF HW Init */
  ConfigManager_HWInit();
  /* Infinite loop */
  for(;;)
  {

    /* Scan to find if there is a tag */
    TagType = ConfigManager_TagHunting(TRACK_NFCTYPE2|TRACK_NFCTYPE4A|TRACK_NFCTYPE5);
    
    switch(TagType)
    {
      /* ISO14443A */
    case TRACK_NFCTYPE2:
      LOG_COMMON_INFO("Enable charger function");
      TagDetected = true;
      break;
    case TRACK_NFCTYPE4A:
      LOG_COMMON_INFO("Enable charger function");
      TagDetected = true;
      break;
    case TRACK_NFCTYPE5:
      LOG_COMMON_INFO("Enable charger function");
      TagDetected = true;
      break;
    }
    
    if ((TagDetected == true) && (g_blFlagAuthenEnable == true))
    {
      g_MainState = MAIN_STATE_DISABLE_RFID;
      TagDetected = false;
    } else if ((TagDetected == true) && (g_blFlagAuthenEnable == false))
    {
      g_MainState = MAIN_STATE_ENABLE_RFID;
      TagDetected = false;
    }
    
    osDelay(500);
  }
  /* USER CODE END fRFIDtask */
}

void gsmTask_Check(void const * argument)
{
  for (;;)
  {
    bool flagReady = false;
    if (atUart_doActions(&flagReady))
    {
      continue;
    }
    osDelay(100);
  }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if (&huart2 == huart) // ESP32 WiFi
  {
    CycleBuf_t* const pCycleBuf = g_gsmTask_cycleBufUART;

    cycleBuf_moveForwardLastIdx(pCycleBuf);
    uint8_t* const pNextVal = cycleBuf_getPtrToLast(pCycleBuf);
    halUART_ReceiveIT_withoutLock(huart, pNextVal, 1);

    if (0 != pCycleBuf->threadId.threadId)
    {
      BaseType_t xHigherPriorityTaskWoken = pdFALSE;
      vTaskNotifyGiveFromISR(pCycleBuf->threadId.threadId, &xHigherPriorityTaskWoken);

      /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
       should be performed to ensure the interrupt returns directly to the highest
       priority task.  The macro used for this purpose is dependent on the port in
       use and may be called portEND_SWITCHING_ISR(). */
      portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
  }
}
