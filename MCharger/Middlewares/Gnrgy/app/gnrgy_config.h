
#ifndef GNRGY_CONFIG_H_
#define GNRGY_CONFIG_H_

#include <stdint.h>
#include <stdbool.h>

#define SERIAL_DEVICE(x_)       SERIAL_DEVICE_(x_)
#define SERIAL_DEVICE_(x_)      (&huart##x_)

#define I2C_DEVICE(x_)          I2C_DEVICE_(x_)
#define I2C_DEVICE_(x_)         (&hi2c##x_)

#define GNRGY_DIR_TMP                   "tmp"
#define GNRGY_DIR_TMP_PPP               GNRGY_DIR_TMP "/ppp"
#define GNRGY_DIR_JFFS2                 "jffs2"
#define GNRGY_DIR_JFFS2_ETC             GNRGY_DIR_JFFS2 "/etc"
#define GNRGY_DIR_JFFS2_FILES           GNRGY_DIR_JFFS2 "/files"
#define GNRGY_DIR_JFFS2_LOG             GNRGY_DIR_JFFS2 "/log"

#define GNRGY_DIR_FILES_CS              GNRGY_DIR_JFFS2_FILES "/cs"
#define GNRGY_DIR_FILES_LCC             GNRGY_DIR_JFFS2_FILES "/lcc"

#define GNRGY_DIR_CS_CSLOG              GNRGY_DIR_FILES_CS "/cs_log"
#define GNRGY_DIR_CS_INIFILES           GNRGY_DIR_FILES_CS "/ini_files"
#define GNRGY_DIR_CS_LOGFILES           GNRGY_DIR_FILES_CS "/log_files"

#define GNRGY_DIR_LCC_CSLOG             GNRGY_DIR_FILES_LCC "/cs_log"
#define GNRGY_DIR_LCC_INIFILES          GNRGY_DIR_FILES_LCC "/ini_files"
#define GNRGY_DIR_LCC_LOGFILES          GNRGY_DIR_FILES_LCC "/log_files"

#define TARGET_I2C_EEPROM                  (1)
#define TARGET_SPI_METER                   (1)
#define EEPROM_ADDRESS                     (0x50 << 1)
#define TEMP_ADDRESS                       (0x4C << 1)

#define TARGET_UART_RS232             1
#define TARGET_UART_DBG               6
#define TARGET_UART_GSM               2
#define TARGET_UART_BT                3
#define TARGET_UART_RFID              6

#define PROJECT_USE_SD_CARD_LOCKING                 1

#define M_CHARGER_CODE_VERSION_MAJOR  1
#define M_CHARGER_CODE_VERSION_MINOR  1

#define GSM_APN  "oneapn.com"
#define GSM_USER "mms"
#define GSM_PASS "mms"
#define TCP_SERVER_IP "171.249.60.171"
#define TCP_SERVER_PORT 123
bool configFileStructure(void);
#endif /* GNRGY_CONFIG_H_ */
