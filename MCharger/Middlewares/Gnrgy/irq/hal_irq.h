#ifndef HAL_IRQ_H
#define HAL_IRQ_H

#include <stdint.h>
#include <stdbool.h>

#include "utils/attribs.h"

#if defined(_FREERTOS)
#include "cmsis_os.h"
#if defined(USE_HAL_DRIVER)
#include "stm32f4xx.h"
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief A type for storing the value of CPU's 'Priority Mask Register'.
 */
typedef uint32_t HAL_IRQ_PriorityMask;


#if defined(_FREERTOS)

static inline bool halIrq_init(void)
{
  return true;
}

static inline bool halIrq_deinit(void)
{
  return true;
}

/**
 * @brief Disable all interrupts.
 * @note This function is used in timer's calibration,
 *       so we must disable all IRQ, but not only the ones handled by FreeRTOS.
 *       We should not use taskENTER_CRITICAL/taskEXIT_CRITICAL,
 *       which disables only IRQs with lower priority than MAX_SYSCALL_INTERRUPT_PRIORITY.
 *
 * @return the current state of the priority mask bit from the Priority Mask Register.
 */
ZZ_ATTR_ALWAYS_INLINE
ZZ_ATTR_WARN_UNUSED_RESULT
static inline HAL_IRQ_PriorityMask halIrq_disable(void)
{
    /* Read PRIMASK register, check interrupt status before you disable them */
    /* Returns 0 if they are enabled, or non-zero if disabled */
    HAL_IRQ_PriorityMask priMask = __get_PRIMASK();

    __disable_irq(); /* Disable interrupts */

    return priMask;
}

/**
 * @brief Restore previous state of interrupt enabling.
 * @param priMask - previous value of Priority Mask Register.
 */

ZZ_ATTR_ALWAYS_INLINE 
static inline void halIrq_restore(const HAL_IRQ_PriorityMask priMask)
{
    /* Enable interrupts back */
    if (!priMask) {
        __enable_irq();
    }
}

#endif // _FREERTOS

#ifdef __cplusplus
}
#endif

#endif // HAL_IRQ_H
