#include "cycle_buf.h"
#include "os_layer/os_layer.h"
#include "irq/hal_irq.h"
#include "utils/zz.h"
void cycleBuf_init(CycleBuf_t* const pCycleBuf, uint8_t* const buf, const uint16_t bufSize)
{
  pCycleBuf->firstIdx = 0;
  pCycleBuf->lastIdx = 0;
  pCycleBuf->cycleBuf = buf;
  pCycleBuf->cycleBufSize = bufSize;
  pCycleBuf->threadId.threadId = 0;
}

CycleBuf_t* cycleBuf_create(const uint16_t bufSize)
{
  CycleBuf_t* const pCycleBuf = osLayer_malloc(sizeof(*pCycleBuf) + bufSize);
  if (NULL == pCycleBuf)
  {
    return NULL;
  }
  cycleBuf_init(pCycleBuf, (uint8_t*)(pCycleBuf + 1), bufSize);
  return pCycleBuf;
}

void cycleBuf_setCurThreadId(CycleBuf_t* const pCycleBuf)
{
  pCycleBuf->threadId = osLayer_getCurThreadId();
}

bool cycleBuf_isEmpty(CycleBuf_t* const pCycleBuf)
{
  if (pCycleBuf->firstIdx == pCycleBuf->lastIdx)
  {
    return true;
  }
  return false;
}

static inline uint16_t cycleBuf_calcNextIdx(CycleBuf_t* const pCycleBuf, const uint16_t idx)
{
  const uint16_t nextIdx = idx + 1;
  if (nextIdx == pCycleBuf->cycleBufSize)
  {
    return 0;
  }
  return nextIdx;
}

uint8_t* cycleBuf_getPtrToLast(CycleBuf_t* const pCycleBuf)
{
  return &pCycleBuf->cycleBuf[pCycleBuf->lastIdx];
}

bool cycleBuf_moveForwardLastIdx(CycleBuf_t* const pCycleBuf)
{
  const uint16_t nextIdx = cycleBuf_calcNextIdx(pCycleBuf, pCycleBuf->lastIdx);
  if (nextIdx == pCycleBuf->firstIdx)
  {
    return false;
  }
  pCycleBuf->lastIdx = nextIdx;
  return true;
}

bool cycleBuf_append(CycleBuf_t* const pCycleBuf, const uint8_t val)
{
  const uint16_t nextIdx = cycleBuf_calcNextIdx(pCycleBuf, pCycleBuf->lastIdx);
  if (nextIdx == pCycleBuf->firstIdx)
  {
    return false;
  }
  pCycleBuf->cycleBuf[pCycleBuf->lastIdx] = val;
  pCycleBuf->lastIdx = nextIdx;
  return true;
}

uint8_t cycleBuf_read(CycleBuf_t* const pCycleBuf)
{
  const uint8_t val = pCycleBuf->cycleBuf[pCycleBuf->firstIdx];
  if (pCycleBuf->firstIdx != pCycleBuf->lastIdx)
  {
    pCycleBuf->firstIdx = cycleBuf_calcNextIdx(pCycleBuf, pCycleBuf->firstIdx);
  }
  return val;
}


