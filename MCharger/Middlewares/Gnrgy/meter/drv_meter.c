#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "os_driver/os_drv_spi.h"
#include "log/log.h"
#include "app/gnrgy_config.h"
#include "drv_meter.h"

#define METER_TIMEOUT_MS               (4)
/**
 * @brief Create new DrvMeter_t object for Meter with specified address on the SPI bus.
 * @param pDrvSpi       - ptr to SPI-bus driver - @ref osDrvSpi_t
 * @return ptr to created object @ref DrvMeter_t
 */
DrvMeter_t* drvMeter_create(
    osDrvSpi_t* const pDrvSpi
)
{
  DrvMeter_t* const pObj = osLayer_calloc(1, sizeof(*pObj));
  if (NULL == pObj)
  {
    LOG_METER_ERR("Can't allocate mem");
    return NULL;
  }
  pObj->pDrvSpi = pDrvSpi;
  pObj->flagReady = false;
  pObj->timeoutTicks = osLayer_convMillisecondsToTicks(METER_TIMEOUT_MS);
  return pObj;
}

/**
 * @brief Check if sensor was successfully initialized.
 * @param pObj          - ptr to @ref DrvMeter_t
 * @return true if successful
 */
bool drvMeter_isReady(
    DrvMeter_t* const pObj
)
{
  return pObj->flagReady;
}

/**
 * @brief Delete DrvMeter_t object.
 * @param pObj          - ptr to @ref DrvMeter_t
 */
void drvMeter_delete(DrvMeter_t* const pObj)
{
  osLayer_free(pObj);
}

/**
 * @brief Write buffer with configured size to Meter
 * @param pObj       - ptr to @ref DrvMeter_t
 * @param usAddress  - Address will be written
 * @param txbuf      - Value will be written to address
 * @param txbytes    - Size of rx buffer
 * @return true if successful
 */
bool drvMeter_writeToMeter(
    DrvMeter_t* const pObj,
    uint16_t const usAddress,
    const uint8_t* const txbuf,
    const size_t txbytes
)
{
  uint8_t ucLowAddr, ucHighAddr;
  ucLowAddr = usAddress & 0x00FF;
  ucHighAddr = (uint8_t)((usAddress & 0xFF00) >> 8);

  uint8_t* txBuf = osLayer_calloc(txbytes + 3, sizeof(*txBuf));
  if (NULL == txBuf)
  {
    LOG_METER_ERR("Can't allocate mem");
    return false;
  }
  txBuf[0] = 0x00;
  txBuf[1] = ucHighAddr;
  txBuf[2] = ucLowAddr;
  memcpy(&txBuf[3], txbuf, txbytes);

  LOG_METER_INFO("Write to regAddr=0x%02x",usAddress);
  if (!osDrvSpi_masterTransactionTransmit(pObj->pDrvSpi, txBuf, txbytes + 3, pObj->timeoutTicks))
  {
    LOG_METER_ERR("regAddr=0x%02x", usAddress);
    return false;
  }
  return true;
}

/**
 * @brief Read data from Meter based on Meter Registers List
 * @param pObj         - ptr to @ref DrvMeter_t
 * @param usAddress    - Start Address will be written
 * @param rxbuf        - ptr to array value will be stored read from meter
 * @param rxbytes      - Length of array value
 * @return true if successful
 */
bool drvMeter_readFromMeter(
    DrvMeter_t* const pObj,
    uint16_t usAddress,
    uint8_t* const rxbuf,
    const size_t rxbytes
)
{
  uint8_t ucLowAddr, ucHighAddr;
  uint8_t txBuf[3];
  ucLowAddr = usAddress & 0x00FF;
  ucHighAddr = (uint8_t)((usAddress & 0xFF00) >> 8);
  txBuf[0] = 0x71;
  txBuf[1] = ucHighAddr;
  txBuf[2] = ucLowAddr;
  LOG_METER_INFO("Read to regAddr=0x%02x",usAddress);
  if (!osDrvSpi_masterTransaction(pObj->pDrvSpi, txBuf, sizeof(txBuf), rxbuf, sizeof(rxbuf), pObj->timeoutTicks))
  {
    LOG_METER_ERR("regAddr=0x%02x", usAddress);
    return false;
  }
  return true;
}

