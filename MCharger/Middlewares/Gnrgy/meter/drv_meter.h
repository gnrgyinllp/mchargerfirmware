#ifndef DRV_METER_H_
#define DRV_METER_H_

#include <stdint.h>
#include <stdbool.h>
#include "os_driver/os_drv_spi.h"

struct DrvMeter_TAG
{
  osDrvSpi_t* pDrvSpi;
  bool flagReady;
  osLayer_DeltaTicks_t timeoutTicks;
};

typedef struct DrvMeter_TAG DrvMeter_t;

/**
 * @brief Create new DrvMeter_t object for Meter with specified address on the SPI bus.
 * @param pDrvSpi       - ptr to SPI-bus driver - @ref osDrvSpi_t
 * @return ptr to created object @ref DrvMeter_t
 */
DrvMeter_t* drvMeter_create(
    osDrvSpi_t* const pDrvSpi
);

/**
 * @brief Initial check and configure Meter sensor.
 * @param pObj          - ptr to @ref DrvMeter_t
 * @return true if successful
 */
bool drvMeter_init(
    DrvMeter_t* const pObj
);

/**
 * @brief Check if sensor was successfully initialized.
 * @param pObj          - ptr to @ref DrvMeter_t
 * @return true if successful
 */
bool drvMeter_isReady(
    DrvMeter_t* const pObj
);

/**
 * @brief Delete DrvMeter_t object.
 * @param pObj          - ptr to @ref DrvMeter_t
 */
void drvMeter_delete(DrvMeter_t* const pObj);

/**
 * @brief Write buffer with configured size to Meter
 * @param pObj       - ptr to @ref DrvMeter_t
 * @param usAddress  - Address will be written
 * @param txbuf      - Value will be written to address
 * @param txbytes    - Size of rx buffer
 * @return true if successful
 */
bool drvMeter_writeToMeter(
    DrvMeter_t* const pObj,
    uint16_t const usAddress,
    const uint8_t* const txbuf,
    const size_t txbytes
);

/**
 * @brief Read data from Meter based on Meter Registers List
 * @param pObj         - ptr to @ref DrvMeter_t
 * @param usAddress    - Start Address will be written
 * @param rxbuf        - ptr to array value will be stored read from meter
 * @param rxbytes      - Length of array value
 * @return true if successful
 */
bool drvMeter_readFromMeter(
    DrvMeter_t* const pObj,
    uint16_t usAddress,
    uint8_t* const rxbuf,
    const size_t rxbytes
);

/**
 * @brief Burst Read data from Meter based on Meter Registers List
 * @param pObj         - ptr to @ref DrvMeter_t
 * @param usAddress    - Start Address will be written
 * @param rxbuf        - ptr to array value will be stored read from meter
 * @param rxbytes      - Length of array value
 * @return true if successful
 */
bool drvMeter_readBurstFromMeter(
    DrvMeter_t* const pObj,
    uint16_t usAddress,
    uint8_t* const rxbuf,
    const size_t rxbytes
);
#endif /* DRV_METER_H_ */
