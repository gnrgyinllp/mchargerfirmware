#include "log/log.h"
#include "temp.h"
#include "drv_temp.h"
#include "app/gnrgy_config.h"

static DrvTemp_t* g_pDrvTemp;
extern osDrvI2c_t* g_pDrvI2c;

bool temp_init(void)
{
  if (NULL != g_pDrvTemp)
  {
    LOG_COMMON_ERR("Temperature sensor was already initialized");
    return false;
  }
  g_pDrvTemp = drvTemp_create(g_pDrvI2c, TEMP_ADDRESS);
  if (NULL == g_pDrvTemp)
  {
    LOG_COMMON_ERR("drvTemp_create failed");
    return false;
  }
  return true;
}

bool temp_readCurrentTemp(uint8_t *pValue)
{
  if (NULL == g_pDrvTemp)
  {
    LOG_COMMON_ERR("Temperature sensor didn't initialize");
    return false;
  }

  if (false == drvTemp_read(g_pDrvTemp, REMOTE_TEMPERATURE_REG, pValue))
  {
    LOG_COMMON_ERR("drvTemp_read failed");
    return false;
  }

  return true;
}

bool temp_readManufacture(uint8_t *pValue)
{
  if (NULL == g_pDrvTemp)
  {
    LOG_COMMON_ERR("Temperature sensor didn't initialize");
    return false;
  }

  if (false == drvTemp_read(g_pDrvTemp, MANUFACTURE_IDENTIFICATION_REG, pValue))
  {
    LOG_COMMON_ERR("drvTemp_read failed");
    return false;
  }

  return true;
}

bool temp_readDeviceRevision(uint8_t *pValue)
{
  if (NULL == g_pDrvTemp)
  {
    LOG_COMMON_ERR("Temperature sensor didn't initialize");
    return false;
  }

  if (false == drvTemp_read(g_pDrvTemp, DEVICE_REVISION_REG, pValue))
  {
    LOG_COMMON_ERR("drvTemp_read failed");
    return false;
  }

  return true;
}

bool temp_remove(void)
{
  if (NULL == g_pDrvTemp)
  {
    LOG_COMMON_ERR("Temperature sensor didn't initialize");
    return false;
  }
  drvTemp_delete(g_pDrvTemp);
  return true;
}

bool temp_setConfig(TEMP_CONFIG_e enConfig)
{
  return true;
}