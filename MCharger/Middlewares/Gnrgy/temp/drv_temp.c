#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "os_driver/os_drv_i2c.h"
#include "log/log.h"
#include "app/gnrgy_config.h"
#include "drv_temp.h"

#define TEMP_TIMEOUT_MS               (100)

/**
 * @brief Create new DrvTemp_t object for temperature with specified address on the I2C bus.
 * @param pDrvI2c       - ptr to I2C-bus driver - @ref osDrvI2c_t
 * @param slaveAddr     - address of sensor on the I2C bus
 * @return ptr to created object @ref DrvTemp_t
 */
DrvTemp_t* drvTemp_create(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t slaveAddr
)
{
  DrvTemp_t* const pObj = osLayer_calloc(1, sizeof(*pObj));
  if (NULL == pObj)
  {
    LOG_TEMP_ERR("Can't allocate mem");
    return NULL;
  }
  pObj->pDrvI2c = pDrvI2c;
  pObj->flagReady = false;
  pObj->slaveAddr = slaveAddr;
  pObj->timeoutTicks = osLayer_convMillisecondsToTicks(TEMP_TIMEOUT_MS);
  return pObj;
}

/**
* @brief Check if sensor was successfully initialized.
* @param pObj          - ptr to @ref DrvTemp_t
* @return true if successful
*/
bool drvTemp_isReady(
    DrvTemp_t* const pObj
)
{
  return pObj->flagReady;
}

/**
* @brief Delete DrvTemp_t object.
* @param pObj          - ptr to @ref DrvTemp_t
*/
void drvTemp_delete(DrvTemp_t* const pObj)
{
  osLayer_free(pObj);
}

/**
* @brief Write One Byte.
* @param pObj       - ptr to @ref DrvTemp_t
* @param usAddress  - Address will be written
* @param ucValue    - Value will be written to address
* @return true if successful
*/
bool drvTemp_read(
    DrvTemp_t* const pObj,
    uint8_t ucAddress,
    uint8_t* pValue
)
{
  const uint8_t txbuf[1] = {
      ucAddress,
  };
  uint8_t rxbuf[1] = {0};

  *pValue = 0;

  if (!osDrvI2c_masterTransaction(pObj->pDrvI2c,
      pObj->slaveAddr,
      txbuf,
      sizeof(txbuf),
      NULL,
      0,
      pObj->timeoutTicks))
  {
    LOGFL_TEMP_ERR("slave=0x%02x, regAddr=0x%02x", pObj->slaveAddr, ucAddress);
    return false;
  }

  if (!osDrvI2c_masterTransaction(pObj->pDrvI2c,
      pObj->slaveAddr,
      NULL,
      0,
      rxbuf,
      sizeof(rxbuf),
      pObj->timeoutTicks))
  {
    LOGFL_TEMP_ERR("slave=0x%02x, regAddr=0x%02x", pObj->slaveAddr, ucAddress);
    return false;
  }

  *pValue = rxbuf[0];
  LOG_TEMP_MSG("slave=0x%02x, regAddr=0x%02x, val=0x%02x (%u)", pObj->slaveAddr, ucAddress, *pValue, *pValue);
  return true;
}
