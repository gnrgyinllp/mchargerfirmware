#if !defined(SYS_QUEUE1_H_)
#define SYS_QUEUE1_H_

#ifdef __cplusplus
extern "C" {
#endif


#define ZTAILQ_HEAD(name, type)                                        \
struct name {                                                           \
        struct type *cqh_first;         /* first element */             \
        struct type *cqh_last;          /* last element */              \
}

#define ZTAILQ_HEAD_INITIALIZER(head)                                  \
        { NULL, NULL }

#define ZTAILQ_ENTRY(type)                                             \
struct {                                                                \
        struct type *cqe_next;          /* next element */              \
        struct type *cqe_prev;          /* previous element */          \
}

/*
 * queue functions.
 */
#define ZTAILQ_INIT(head) do {                                         \
        (head)->cqh_first = NULL;                                      \
        (head)->cqh_last = NULL;                                       \
} while (/*CONSTCOND*/0)

#define ZTAILQ_INSERT_AFTER(head, listelm, elm, field) do {             \
        (elm)->field.cqe_next = (listelm)->field.cqe_next;              \
        (elm)->field.cqe_prev = (listelm);                              \
        if ((listelm)->field.cqe_next == NULL)                          \
                (head)->cqh_last = (elm);                               \
        else                                                            \
                (listelm)->field.cqe_next->field.cqe_prev = (elm);      \
        (listelm)->field.cqe_next = (elm);                              \
} while (/*CONSTCOND*/0)

#define ZTAILQ_INSERT_BEFORE(head, listelm, elm, field) do {            \
        (elm)->field.cqe_next = (listelm);                              \
        (elm)->field.cqe_prev = (listelm)->field.cqe_prev;              \
        if ((listelm)->field.cqe_prev == NULL)                          \
                (head)->cqh_first = (elm);                              \
        else                                                            \
                (listelm)->field.cqe_prev->field.cqe_next = (elm);      \
        (listelm)->field.cqe_prev = (elm);                              \
} while (/*CONSTCOND*/0)

#define ZTAILQ_INSERT_HEAD(head, elm, field) do {                       \
        (elm)->field.cqe_next = (head)->cqh_first;                      \
        (elm)->field.cqe_prev = NULL;                                   \
        if ((head)->cqh_last == NULL)                                   \
                (head)->cqh_last = (elm);                               \
        else                                                            \
                (head)->cqh_first->field.cqe_prev = (elm);              \
        (head)->cqh_first = (elm);                                      \
} while (/*CONSTCOND*/0)

#define ZTAILQ_INSERT_TAIL(head, elm, field) do {                       \
        (elm)->field.cqe_next = NULL;                                   \
        (elm)->field.cqe_prev = (head)->cqh_last;                       \
        if ((head)->cqh_first == NULL)                                  \
                (head)->cqh_first = (elm);                              \
        else                                                            \
                (head)->cqh_last->field.cqe_next = (elm);               \
        (head)->cqh_last = (elm);                                       \
} while (/*CONSTCOND*/0)

#define ZTAILQ_REMOVE(head, elm, field) do {                            \
        if ((elm)->field.cqe_next == NULL)                              \
                (head)->cqh_last = (elm)->field.cqe_prev;               \
        else                                                            \
                (elm)->field.cqe_next->field.cqe_prev =                 \
                    (elm)->field.cqe_prev;                              \
        if ((elm)->field.cqe_prev == NULL)                              \
                (head)->cqh_first = (elm)->field.cqe_next;              \
        else                                                            \
                (elm)->field.cqe_prev->field.cqe_next =                 \
                    (elm)->field.cqe_next;                              \
} while (/*CONSTCOND*/0)

#define ZTAILQ_REMOVE_FIRST(head, elm, field) do {                      \
        if ((elm)->field.cqe_next == NULL)                              \
                (head)->cqh_last = NULL;                                \
        else                                                            \
                (elm)->field.cqe_next->field.cqe_prev = NULL;           \
        (head)->cqh_first = (elm)->field.cqe_next;                      \
} while (/*CONSTCOND*/0)

#define ZTAILQ_REMOVE_NOT_FIRST(head, elm, field) do {                  \
        if ((elm)->field.cqe_next == NULL)                              \
                (head)->cqh_last = (elm)->field.cqe_prev;               \
        else                                                            \
                (elm)->field.cqe_next->field.cqe_prev =                 \
                    (elm)->field.cqe_prev;                              \
        (elm)->field.cqe_prev->field.cqe_next = (elm)->field.cqe_next;  \
} while (/*CONSTCOND*/0)

#define ZTAILQ_FOREACH(var, head, field)                                \
        for ((var) = ((head)->cqh_first);                               \
                (var) != NULL;                                          \
                (var) = ((var)->field.cqe_next))

#define ZTAILQ_FOREACHT(type, var, head, field)                         \
        for (type* var = ((head)->cqh_first);                           \
                (var) != NULL;                                          \
                (var) = ((var)->field.cqe_next))

#define ZTAILQ_FOREACH_REVERSE(var, head, field)                        \
        for ((var) = ((head)->cqh_last);                                \
                (var) != NULL;                                          \
                (var) = ((var)->field.cqe_prev))

/*
 * Circular queue access methods.
 */
#define ZTAILQ_EMPTY(head)              ((head)->cqh_first == NULL)
#define ZTAILQ_FIRST(head)             ((head)->cqh_first)
#define ZTAILQ_LAST(head)              ((head)->cqh_last)
#define ZTAILQ_NEXT(elm, field)        ((elm)->field.cqe_next)
#define ZTAILQ_PREV(elm, field)        ((elm)->field.cqe_prev)

#define ZTAILQ_LOOP_NEXT(head, elm, field)                              \
        (((elm)->field.cqe_next == NULL)                                \
            ? ((head)->cqh_first)                                       \
            : (elm->field.cqe_next))
#define ZTAILQ_LOOP_PREV(head, elm, field)                             \
        (((elm)->field.cqe_prev == NULL)                                \
            ? ((head)->cqh_last)                                        \
            : (elm->field.cqe_prev))


#ifdef __cplusplus
}
#endif

#endif /*SYS_QUEUE1_H_*/
