#if !defined(ATTRIBS_H_)
#define ATTRIBS_H_

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__GNUC__)

// Common Attributes

/**
 * @brief Used for annotate objects which may be not used in code.
 */
#define ZZ_ATTR_UNUSED __attribute__((unused))

#define ZZ_ATTR_ALIGNED(x)            __attribute__((aligned(x)))
#define ZZ_ATTR_DEPRECATED(msg)       __attribute__((deprecated(msg)))

// Common Function Attributes
#define ZZ_ATTR_ALWAYS_INLINE         __attribute__((always_inline))
#define ZZ_ATTR_NOINLINE              __attribute__((noinline))
#define ZZ_ATTR_CONST                 __attribute__((const))
#define ZZ_ATTR_PURE                  __attribute__((pure))
#define ZZ_ATTR_RETURNS_NONNULL       __attribute__((returns_nonnull))
#define ZZ_ATTR_ERROR(msg)            __attribute__((error(msg)))
#define ZZ_ATTR_WARNING(msg)          __attribute__((warning(msg)))
#define ZZ_ATTR_WARN_UNUSED_RESULT    __attribute__((warn_unused_result))

#define ZZ_ATTR_PACK                  __attribute__((__packed__))

/**
 * @brief Used for annotate functions that never return.
 */
#define ZZ_ATTR_NORETURN __attribute__((noreturn))

/**
 * @brief   The format attribute specifies that a function takes printf, scanf,
 *          strftime or strfmon style arguments that should be type-checked against a format string.
 */
#define ZZ_ATTR_FORMAT_PRINTF(fmtIdx_, firstArgIdx_) __attribute__((format(printf, fmtIdx_, firstArgIdx_)))
#else
#define ZZ_ATTR_UNUSED
#define ZZ_ATTR_ALIGNED(x)
#define ZZ_ATTR_DEPRECATED(msg)
#define ZZ_ATTR_NORETURN
#define ZZ_ATTR_ALWAYS_INLINE
#define ZZ_ATTR_NOINLINE
#define ZZ_ATTR_CONST
#define ZZ_ATTR_PURE
#define ZZ_ATTR_RETURNS_NONNULL
#define ZZ_ATTR_ERROR(msg)
#define ZZ_ATTR_WARNING(msg)
#define ZZ_ATTR_WARN_UNUSED_RESULT
#define ZZ_ATTR_FORMAT_PRINTF(stringIdx_, firstToCheck_)
#define ZZ_ATTR_PACK
#endif

#ifdef __cplusplus
}
#endif

#endif /* ATTRIBS_H_ */
