#ifndef ZZ_H
#define ZZ_H

#include <assert.h>

#include "utils/attribs.h"


#ifdef __clang__
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedMacroInspection"
#endif // __clang__

#ifdef __cplusplus
extern "C" {
#endif


#define ZZ_TYPECHECK(type, x) \
({      type dummy1; \
        __typeof__(x) dummy2; \
        (void)(&dummy1 == &dummy2); \
        1; \
})


/**
 * @brief Calculates (at compile time) the maximum value of integer which can be placed to the specified type.
 */
#define ZZ_MAX_VAL_OF_TYPE(type_) \
    (((type_)~0 < 0) ? (type_)(1 << (sizeof(type_)*8-1)) : (type_)~0)

/**
 * @brief Tests condition at compile time (can be used outside of functions)
 */
#define ZZ_COMPILE_TIME_ASSERT(EXP_) \
    ZZ_COMPILE_TIME_ASSERT_I(EXP_, __LINE__)

#define ZZ_COMPILE_TIME_ASSERT_I(EXP_, LN_) \
    ZZ_COMPILE_TIME_ASSERT_II(EXP_, LN_)

#define ZZ_COMPILE_TIME_ASSERT_II(EXP_, LN_) \
    typedef char _static_assertion_at_line_##LN_[(EXP_) ? 1 : -1]


/**
 * @brief Tests condition at compile time (can be used inside functions)
 */
#define ZZ_COMPILE_TIME_ASSERT_FUNC(EXP_) \
    XX_COMPILE_TIME_ASSERT_I_FUNC(EXP_, __LINE__)

#define ZZ_COMPILE_TIME_ASSERT_I_FUNC(EXP_, LN_) \
    ZZ_COMPILE_TIME_ASSERT_II_FUNC(EXP_, LN_)

#define ZZ_COMPILE_TIME_ASSERT_II_FUNC(EXP_, LN_) \
    typedef char _static_assertion_at_line_##LN_[(EXP_) ? 1 : -1]; \
    (void)(_static_assertion_at_line_##LN_*)0

/**
 * @brief Checks that size of specified type is matched (at compile time).
 */
#define ZZ_ASSERT_SIZE(nameOfType_, expSizeOfType_) \
    ZZ_COMPILE_TIME_ASSERT(sizeof(nameOfType_) == (expSizeOfType_))

#if defined(_TDD)
#define ZZ_TDD_ASSERT(expr_) \
    HW_ASSERT(expr_)
#define ZZ_TDD_ASSERT_FMT(expr_, fmt_, ...) \
    HW_ASSERT_FMT(expr_, fmt_, ##__VA_ARGS__)
#else
#define ZZ_TDD_ASSERT(expr_)
#define ZZ_TDD_ASSERT_FMT(expr_, fmt_, ...)
#endif

/**
 * @brief Gets number of elements in static array.
 */
#define ZZ_COUNT_OF(array_)     (sizeof(array_)/sizeof((array_)[0]))


#if !defined(static_assert)
#if !defined __cplusplus
#   if __STDC_VERSION__ >= 201112L
//      See http://clang.llvm.org/docs/LanguageExtensions.html#feature_check
#       ifdef __has_feature         // Check if clang is used
#           if __has_feature(c_static_assert)
#               define static_assert(expr_, msg_) _Static_assert(expr_, msg_)
#           else
//#               define static_assert(expr_, msg_) ZZ_COMPILE_TIME_ASSERT(expr_)
#               define _Static_assert(expr_, msg_) ZZ_COMPILE_TIME_ASSERT(expr_)
#           endif
#       else
#           define static_assert(expr_, msg_) _Static_assert(expr_, msg_)
#       endif
#   else
#       define static_assert(expr_, msg_) ZZ_COMPILE_TIME_ASSERT(expr_)
#       define _Static_assert(expr_, msg_) ZZ_COMPILE_TIME_ASSERT(expr_)
#   endif
#endif
#endif

#define STATIC_INLINE static inline

#ifdef __cplusplus
}
#endif

#ifdef __clang__
#pragma clang diagnostic pop
#endif // __clang__


#endif //ZZ_H
