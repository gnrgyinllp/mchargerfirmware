#if !defined(T_SNPRINTF_H_)
#define T_SNPRINTF_H_

#include <stdarg.h>
#include <stddef.h>

#include "utils/attribs.h"

#ifdef __cplusplus
extern "C" {
#endif

int t_vsnprintf(char* str, size_t count, const char* fmt, va_list args);

ZZ_ATTR_FORMAT_PRINTF(3, 4)
int t_snprintf(char* str, size_t count, const char* fmt, ...);


#ifdef __cplusplus
}
#endif

#endif /* T_SNPRINTF_H_ */
