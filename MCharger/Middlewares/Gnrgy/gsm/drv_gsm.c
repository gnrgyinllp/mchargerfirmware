#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "app/gnrgy_config.h"
#include "log/log.h"
#include "buffer/cycle_buf.h"
#include "gsm/at_uart.h"
#include "gsm/at_cmd.h"
#include "os_driver/hal_uart.h"
#include "FreeRTOS_CLI.h"
extern UART_HandleTypeDef huart2;
CycleBuf_t* g_gsmTask_cycleBufUART;
UART_HandleTypeDef* const g_gsmTask_pSerialDrv = &huart2;
#define TXBUF_LEN 2048
uint8_t txBuf[TXBUF_LEN];
static bool gsmTask_checkConfigureAT(void)
{
  /* AT */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }
  /* ATE0 to turn off echo */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_ATE0(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_ATE0");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }
  /* AT+CPIN? */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CPIN(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CPIN");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  return true;
}

static bool gsmTask_enableGPRS(bool blStatus)
{
  if (blStatus)
  {
    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_CIPSHUT(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_CIPSHUT");
        return false;
      }
      if (AT_Cmd_FinalResultCode_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }

    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_OpenCGATT(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_OpenCGATT");
        return false;
      }
      if (AT_Cmd_FinalResultCode_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }

    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_CSTT(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_CSTT");
        return false;
      }
      if (AT_Cmd_FinalResultCode_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }

    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_OpenSAPBR(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_OpenSAPBR");
        return false;
      }
      if (AT_Cmd_FinalResultCode_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }

    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_CIICR(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_CIICR");
        return false;
      }
      if (AT_Cmd_FinalResultCode_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }
  }
  else
  {
    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_CIPSHUT(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_CIPSHUT");
        return false;
      }
      if (AT_Cmd_FinalResultCode_SHUT_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }

    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_CloseSAPBR(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_CloseSAPBR");
        return false;
      }
      if (AT_Cmd_FinalResultCode_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }

    {
      AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
      if (!atCmd_AT_CloseCGATT(&finResCode))
      {
        LOG_COMMON_ERR("atCmd_AT_CloseCGATT");
        return false;
      }
      if (AT_Cmd_FinalResultCode_OK != finResCode)
      {
        LOG_COMMON_CRIT("finResCode=%d", finResCode);
        return false;
      }
    }
  }
}

static bool gsmTask_enableTCPServer(void)
{
  /* AT+CIPSHUT */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPSHUT(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPSHUT");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  /* AT+CIPMUX */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPMUX(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPMUX");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  /* AT+CIPRXGET */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPRXGET(&finResCode,CIPRXGET_MODE_ENABLE,0, NULL,NULL))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPRXGET");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  /* AT+CSTT */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CSTT(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CSTT");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }
  
  /* AT+CIICR */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIICR(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIICR");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  /* AT+CIFSR -> Get IP */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIFSR(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIFSR");
      return false;
    }
  }

  /* AT+CIPSTATUS */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPSTATUS(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPSTATUS");
      return false;
    }
    if (AT_Cmd_FinalResultCode_IP_STATUS != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }
  
  /* AT+CIPSERVER=1,[PORT] -> Start server with PORT input  */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPSERVER(&finResCode, TCP_SERVER_PORT))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPSERVER");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  /* AT+CIPSTATUS after invoked SIPSERVER */
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPSTATUS(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPSTATUS");
      return false;
    }
    if (AT_Cmd_FinalResultCode_SERVER_LISTENING != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }
  return true;
}

static bool gsmTask_enableTCPClient(void)
{
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPSHUT(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPSHUT");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPMUX(&finResCode))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPMUX");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPRXGET(&finResCode,CIPRXGET_MODE_ENABLE,0, NULL,NULL))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPRXGET");
      return false;
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPSTART(&finResCode, TCP_SERVER_IP, TCP_SERVER_PORT))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPSTART");
      return false;
    }
    if (AT_Cmd_FinalResultCode_CONNECT_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
      return false;
    }
  }

  return true;
}

void gsmTask_ReadProcessMessage(void)
{
#define GSM_MAX_LEN 1024
  BaseType_t xReturned;
  char *pcOutputString;
  int len = 0;
  int remain = 0;
  uint16_t cnflen = 0;
  uint8_t partPacket = 0;
  memset(txBuf,'\0', TXBUF_LEN);
  AT_Cmd_String_t reqMessage;
  /* Get global output buffer to avoid over lap data */
  pcOutputString = FreeRTOS_CLIGetOutputBuffer();
  {

    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPRXGET(&finResCode,CIPRXGET_MODE_READ_LEN,0, NULL,&cnflen))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPRXGET");
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
    }
  }

  if (cnflen != 0)
  {
    AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
    if (!atCmd_AT_CIPRXGET(&finResCode,CIPRXGET_MODE_READ_DATA, cnflen, &reqMessage,NULL))
    {
      LOG_COMMON_ERR("atCmd_AT_CIPRXGET");
    }
    if (AT_Cmd_FinalResultCode_OK != finResCode)
    {
      LOG_COMMON_CRIT("finResCode=%d", finResCode);
    }
    LOG_COMMON_INFO("Message %s", reqMessage.buf);

    do
    {
      /* Get the next output string from the command interpreter. */
      xReturned = FreeRTOS_CLIProcessCommand( reqMessage.buf, pcOutputString, 256 );
      len += t_snprintf(&txBuf[len], sizeof(txBuf) - len, "%s", pcOutputString);
    } while( xReturned != pdFALSE );
    
    LOG_COMMON_INFO("Size of message %d", len);
    LOG_COMMON_INFO("Message Output %s", txBuf);
    if (len == 0)
    {
      len = t_snprintf(&txBuf[len], sizeof(txBuf) - len, "Command not recognised.  Enter 'help' to view a list of available commands.");
    }
    {
      if (len > GSM_MAX_LEN)
      {
        partPacket = len/GSM_MAX_LEN;

        for (int packetid = 0; packetid < partPacket; packetid++)
        {
          AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
          if (!atCmd_AT_CIPSEND(&finResCode, &txBuf[GSM_MAX_LEN*packetid], GSM_MAX_LEN))
          {
            LOG_COMMON_ERR("atCmd_AT_CIPSEND");
          }
          if (AT_Cmd_FinalResultCode_OK != finResCode)
          {
            LOG_COMMON_CRIT("finResCode=%d", finResCode);
          }

          remain = len - GSM_MAX_LEN;
        }

        {
          AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
          if (!atCmd_AT_CIPSEND(&finResCode, &txBuf[len - remain], remain))
          {
            LOG_COMMON_ERR("atCmd_AT_CIPSEND");
          }
          if (AT_Cmd_FinalResultCode_OK != finResCode)
          {
            LOG_COMMON_CRIT("finResCode=%d", finResCode);
          }
        }
      }
      else
      {
        AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
        if (!atCmd_AT_CIPSEND(&finResCode, txBuf, len))
        {
          LOG_COMMON_ERR("atCmd_AT_CIPSEND");
        }
        if (AT_Cmd_FinalResultCode_OK != finResCode)
        {
          LOG_COMMON_CRIT("finResCode=%d", finResCode);
        }
      }
    }
  }
}

static const char* atCmd_isPrefixMatch(const char* const prefix, const char* const textResponse)
{
  const char* p = prefix;
  const char* q = textResponse;
  while ('\0' != *p)
  {
    if (*p++ != *q++)
    {
      return NULL;
    }
  }
  return q;
}

static void gsmTask_CallBackHandler(void* const param, const char* const responseText)
{
  (void)param;
  const char* const pData = atCmd_isPrefixMatch("+CIPRXGET: 1", responseText);
  if (NULL != pData)
  {
    gsmTask_ReadProcessMessage();
  }
}

bool GSMTask_init(void)
{
  g_gsmTask_cycleBufUART = cycleBuf_create(2048);
  if (NULL == g_gsmTask_cycleBufUART)
  {
    LOG_COMMON_CRIT("Can't allocate memory for CycleBuf");
    return false;
  }
  
  halUART_ReceiveIT_withoutLock(g_gsmTask_pSerialDrv, cycleBuf_getPtrToLast(g_gsmTask_cycleBufUART), 1);
  
  if (!atUart_createInstance(g_gsmTask_cycleBufUART))
  {
    LOG_COMMON_CRIT("atUart_createInstance");
  }

  if (!atCmd_init())
  {
    LOG_COMMON_CRIT("atCmd_init");
  }

  cycleBuf_setCurThreadId(g_gsmTask_cycleBufUART);
  
  return true;
}

void Gsm_Init(void)
{
  if (gsmTask_checkConfigureAT())
  {
    LOG_COMMON_INFO("Passed gsmTask_checkConfigureAT");
  }
  else
  {
    LOG_COMMON_INFO("Failed gsmTask_checkConfigureAT");
  }

  if (gsmTask_enableTCPServer())
  {
    LOG_COMMON_INFO("Passed gsmTask_enableTCPServer");
  }
  else
  {
    LOG_COMMON_INFO("Failed gsmTask_enableTCPServer");
  }
  
  atUart_subscribeToUnsolicitedResponses(&gsmTask_CallBackHandler, NULL);
}