#ifndef AT_UART_PRIVATE_H
#define AT_UART_PRIVATE_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define AT_UART_SIGNAL__CHAR_READY             osLayer_SignalNum_1
#define AT_UART_SIGNAL__AT_REQ                 osLayer_SignalNum_2
#define AT_UART_SIGNAL__WAIT_LINE              osLayer_SignalNum_3
#define AT_UART_SIGNAL__WAIT_LINE_TIMEOUT      osLayer_SignalNum_4
#define AT_UART_SIGNAL__STOP_READING_RESPONSE  osLayer_SignalNum_5

#define AT_UART_SIGNAL__NOTIFY_READY           osLayer_SignalNum_1

/**
 * @brief Declaration of AT_UART_t class.
 */
typedef struct AT_UART_TAG AT_UART_t;


/**
 * @brief Init instance of UartGsm class.
 * @param pUartGsm                  - ptr to the AT_UART_t instance.
 * @return true if success.
 */
bool atUart_init(AT_UART_t* const pUartGsm);

/**
 * @brief Deinit instance of UartGsm class.
 * @param pUartGsm                  - ptr to the AT_UART_t instance.
 * @return true if success.
 */
void atUart_deinit(AT_UART_t* const pUartGsm);

/**
 * @brief Wait while thread task started and initialized.
 * @param pThreadEncoder   - ptr to ThreadEncoder_t instance
 * @return none
 */
void atUart_waitThreadReady(AT_UART_t* const pUartGsm);

bool atUart_putReqSendCmdAT(
        AT_UART_t* const pUartGsm,
        const char* const atCmd);

bool atUart_putReqSendCmdWithData(
        AT_UART_t* const pUartGsm,
        const char* const atCmd,
        const uint8_t* const buf,
        const uint16_t bufLen);

bool atUart_putReqWaitLineWithTimeout(
        AT_UART_t* const pUartGsm,
        char* const buf,
        const size_t bufLen,
        const uint32_t timeout_ms);

bool atUart_putReqStopReadingResponse(
        AT_UART_t* const pUartGsm);

#ifdef __cplusplus
}
#endif

#endif //AT_UART_PRIVATE_H
