#ifndef AT_CMD_H
#define AT_CMD_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ********************************************************************************************************************/

typedef enum AT_Cmd_FinalResultCode_TAG
{
    AT_Cmd_FinalResultCode_Undef,
    AT_Cmd_FinalResultCode_OK,
    AT_Cmd_FinalResultCode_ERROR,
    AT_Cmd_FinalResultCode_SHUT_OK,
    AT_Cmd_FinalResultCode_CONNECT_OK,
    AT_Cmd_FinalResultCode_AngleBracket,
    AT_Cmd_FinalResultCode_IP_INITIAL,
    AT_Cmd_FinalResultCode_IP_STATUS,
    AT_Cmd_FinalResultCode_SERVER_LISTENING,
} AT_Cmd_FinalResultCode_e;

typedef enum AT_CmdCIPRXGET_TAG
{
  CIPRXGET_MODE_ENABLE = 1,
  CIPRXGET_MODE_READ_LEN = 4,
  CIPRXGET_MODE_READ_DATA = 2
} AT_CmdCIPRXGET_e;

#define AT_CMD_MAX_STRING_LEN  32

typedef struct AT_Cmd_String_TAG
{
  char buf[AT_CMD_MAX_STRING_LEN];
} AT_Cmd_String_t;

#define AT_CMD_STRING_INIT_EMPTY()   {{0}}


typedef void (*AT_Cmd_Callback)(void* const param, const bool isSuccessful);

/* ********************************************************************************************************************/

bool atCmd_init(void);

bool atCmd_handleUnsolicitedResponse(
        const char* const respStr);

bool atCmd_AT(
        AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CFUN(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CPIN(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIPMUX(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CSQ(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CREG(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_OpenCGATT(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CloseCGATT(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_SetSAPBR(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_OpenSAPBR(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CloseSAPBR(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CSTT(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIICR(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIFSR(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIPSHUT(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIPSTATUS(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIPSTART(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const host,
    const uint16_t port);

bool atCmd_AT_CIPRXGET(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    AT_CmdCIPRXGET_e mode,
    uint16_t reqlen,
    AT_Cmd_String_t* const pMessage,
    uint16_t* cnflen);

bool atCmd_AT_CheckCSTT(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady);

bool atCmd_AT_RST(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_CWQAP(void);

bool atCmd_ATE0(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_GMR(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pVersionOfAT,
    AT_Cmd_String_t* const pVersionOfSDK,
    AT_Cmd_String_t* const pCompileTime);

bool atCmd_AT_FW_VER(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pFirmwareVer);

bool atCmd_AT_SET_HOSTNAME(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const hostname);

bool atCmd_SDCARD_SENDFILE(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const connType,
    const char* const serverName,
    const uint16_t serverPort,
    const uint32_t timeout_sec,
    const char* const filePath,
    const char* const prefixPath,
    const char* const suffixPath);

bool atCmd_AT_CWMODE1(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CWAUTOCONN1(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CWLAPOPT(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const bool flagSortByRSSI);

bool atCmd_AT_CWLAP(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const apName,
    bool* const pFlagFound);

bool atCmd_List_AT_CWLAP(void (*cb)(const char* const ssid, int strength));

bool atCmd_AT_CWJAP(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const ssid,
    const char* const pwd,
    int* const pCwjapStatus);

bool atCmd_AT_CWJAPQ(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pSSID,
    AT_Cmd_String_t* const pBSSID_MAC,
    unsigned* const pChannel,
    int* const pRSSI);

bool atCmd_AT_CIPSTAQ(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pIP,
    AT_Cmd_String_t* const pGateway,
    AT_Cmd_String_t* const pNetmask);

bool atCmd_AT_CIPSTAMACQ(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pMAC);

bool atCmd_AT_CIPCLOSE(
    AT_Cmd_FinalResultCode_e* const pFinResCode);



bool atCmd_AT_CIPSTART_SSL(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const host,
    const uint16_t port);

bool atCmd_AT_CIPMODE_setTransmissionModePassthrough(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIPMODE_setTransmissionModeNormal(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

bool atCmd_AT_CIPSEND_startTransparentTransmission(
    AT_Cmd_FinalResultCode_e* const pFinResCode);

void atCmd_AT_CIPSEND_finishTransparentTransmission(void);

bool atCmd_AT_CIPSEND(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const uint8_t* const buf,
    const uint16_t bufLen);

bool atCmd_sendString(
    const char* const pStr);

bool atCmd_sendBuf(
    const uint8_t* const buf,
    const uint16_t bufLen);

bool atCmd_readBuf(
    uint8_t* const buf,
    const uint16_t bufLen,
    const uint32_t timeout_ms);

bool atCmd_readLineWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout);

bool atCmd_waitIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD);

bool atCmd_readIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    uint16_t* const pLenIPD);

bool atCmd_waitSDIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD);

bool atCmd_readSDIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    uint16_t* const pLenIPD);

#ifdef __cplusplus
}
#endif

#endif //AT_CMD_H
