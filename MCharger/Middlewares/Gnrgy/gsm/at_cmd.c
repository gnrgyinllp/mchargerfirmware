#include "at_cmd.h"
#include "at_uart.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "utils/zz.h"
#include "utils/t_snprintf.h"
#include "log/log.h"
#include "os_layer/os_layer.h"
#include "app/gnrgy_config.h"

typedef struct AT_Cmd_TAG AT_Cmd_t;
struct AT_Cmd_TAG
{
#define AT_CMD_RESP_BUF_SIZE        128
  char respBuf[AT_CMD_RESP_BUF_SIZE];
  uint32_t timeoutLineWait_ms;
  uint32_t defaultTimeoutLineWait_ms;
};

typedef void (* AT_Cmd_callbackOnInformationTextResponse_t)(const char* const textResponse, void* param);

typedef struct AT_Cmd_FinalResultCodeMap_TAG
{
  const char* const finalResultStr;
  const AT_Cmd_FinalResultCode_e finalResultCode;
} AT_Cmd_FinalResultCodeMap_t;

typedef void (* AT_Cmd_URC_CallbackHanlder)(const char* const urc, const char* const argsStr);

typedef struct AT_Cmd_URC_Map_TAG
{
  const char* const urcPrefix;
  AT_Cmd_URC_CallbackHanlder cbHandler;
} AT_Cmd_URC_Map_t;

/* ********************************************************************************************************************/

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMap_AT[] = {
    {"OK",    AT_Cmd_FinalResultCode_OK},
    {"ERROR", AT_Cmd_FinalResultCode_ERROR},
};

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMapScanWifi[] = {
    {"OK",    AT_Cmd_FinalResultCode_OK},
};

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMap_AT_ERROR[] = {
    {"ERROR", AT_Cmd_FinalResultCode_ERROR},
};

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMap_AT_CIPSHUT[] = {
    {"ERROR", AT_Cmd_FinalResultCode_ERROR},
    {"SHUT OK", AT_Cmd_FinalResultCode_OK},
};

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMap_AT_CIPSTART[] = {
    {"ERROR", AT_Cmd_FinalResultCode_ERROR},
    {"CONNECT OK", AT_Cmd_FinalResultCode_CONNECT_OK},
};

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMap_AT_CIPSTATUS[] = {
    {"STATE: IP INITIAL", AT_Cmd_FinalResultCode_IP_INITIAL},
    {"STATE: IP STATUS", AT_Cmd_FinalResultCode_IP_STATUS},
    {"STATE: SERVER LISTENING", AT_Cmd_FinalResultCode_SERVER_LISTENING},
};

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMap_AT_CIPSERVER[] = {
    {"ERROR", AT_Cmd_FinalResultCode_ERROR},
    {"SERVER OK", AT_Cmd_FinalResultCode_OK},
};

const AT_Cmd_FinalResultCodeMap_t g_atCmd_resCodeMap_AT_CIPSEND[] = {
    {"SEND OK",    AT_Cmd_FinalResultCode_OK},
    {"ERROR", AT_Cmd_FinalResultCode_ERROR},
};

static void atCmd_urcHandler_IPD(const char* const urc, const char* const argsStr);
static void atCmd_urcHandler_SSID(const char* const urc, const char* const argsStr);
const AT_Cmd_URC_Map_t g_atCmd_urcMap[] = {
    {"+IPD,", &atCmd_urcHandler_IPD},
    {"+CWLAP:",&atCmd_urcHandler_SSID}
};

AT_Cmd_t g_atCmd;

/* ********************************************************************************************************************/

static void atCmd_urcHandler_IPD(const char* const urc, const char* const argsStr)
{
  (void)urc;
  (void)argsStr;
}

static void atCmd_urcHandler_SSID(const char* const urc, const char* const argsStr)
{
  (void)urc;
  (void)argsStr;
}

void atCmd_stringInit(AT_Cmd_String_t* const pStr, const char* const msg)
{
  strncpy(pStr->buf, msg, sizeof(pStr->buf) - 1);
  pStr->buf[sizeof(pStr->buf) - 1] = '\0';
}

void atCmd_stringVprintf(AT_Cmd_String_t* const pStr, const char* const fmt, va_list args)
{
  t_vsnprintf(pStr->buf, sizeof(pStr->buf), fmt, args);
}

void atCmd_stringPrintf(AT_Cmd_String_t* const pStr, const char* const fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  atCmd_stringVprintf(pStr, fmt, args);
  va_end(args);
}

static bool atCmd_parseQuotedString(const char* const text, AT_Cmd_String_t* const pStr, const char** const ppNext)
{
  atCmd_stringInit(pStr, "");
  const char* p = text;
  if('"' != *p)
  {
    return false;
  }
  const char* q = strchr(p + 1, '"');
  if(NULL == q)
  {
    return false;
  }
  const size_t len = (size_t)(q - p - 1);
  atCmd_stringPrintf(pStr, "%.*s", len, p + 1);
  if (NULL != ppNext)
  {
    *ppNext = q + 1;
  }
  return true;
}


static const char* atCmd_isPrefixMatch(const char* const prefix, const char* const textResponse)
{
  const char* p = prefix;
  const char* q = textResponse;
  while ('\0' != *p)
  {
    if (*p++ != *q++)
    {
      return NULL;
    }
  }
  return q;
}

static bool atCmd_privInit(AT_Cmd_t* const pObj)
{
  pObj->defaultTimeoutLineWait_ms = 1000;
  pObj->timeoutLineWait_ms = pObj->defaultTimeoutLineWait_ms;
  return true;
}

bool atCmd_init(void)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_privInit(pObj);
}

bool atCmd_handleUnsolicitedResponse(
    const char* const respStr)
{
  for (unsigned i = 0; i < ZZ_COUNT_OF(g_atCmd_urcMap); ++i)
  {
    const AT_Cmd_URC_Map_t* const pMap = &g_atCmd_urcMap[i];
    const char* const val = atCmd_isPrefixMatch(pMap->urcPrefix, respStr);
    if (NULL != val)
    {
      pMap->cbHandler(respStr, val);
      return true;
    }
  }
  return false;
}

bool atCmd_handleResponse(
    const char* const Prefix,
    const char* const respStr)
{
  const char* const val = atCmd_isPrefixMatch(Prefix, respStr);
  if (NULL != val)
  {
    SYSLOG_INFO("Val %s",val);
    return true;
  }
  return false;
}

static bool atCmd_checkIsFinalResult(
    const char* const respStr,
    const AT_Cmd_FinalResultCodeMap_t* const finalResultCodeMap,
    const uint16_t finalResultCodeMapSize,
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  *pFinResCode = AT_Cmd_FinalResultCode_Undef;
  for (unsigned i = 0; i < finalResultCodeMapSize; ++i)
  {
    const AT_Cmd_FinalResultCodeMap_t* const pFinRes = &finalResultCodeMap[i];
    if (0 == strcmp(respStr, pFinRes->finalResultStr))
    {
      *pFinResCode = pFinRes->finalResultCode;
      return true;
    }
  }
  return false;
}

static void atCmd_setTemporaryTimeout(AT_Cmd_t* const pObj, uint32_t timeoutLineWait_ms)
{
  pObj->timeoutLineWait_ms = timeoutLineWait_ms;
}

static bool atCmd_transaction(
    AT_Cmd_t* const pObj,
    const char* const at_cmd,
    const AT_Cmd_FinalResultCodeMap_t* const finalResultCodeMap,
    const uint16_t finalResultCodeMapSize,
    AT_Cmd_callbackOnInformationTextResponse_t callbackOnInformationTextResponse,
    void* const cbParam,
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  *pFinResCode = AT_Cmd_FinalResultCode_Undef;
  if (!atUart_sendCmdAT(at_cmd))
  {
    return false;
  }
  osLayer_Ticks_t t0 = osLayer_getSysTick();
  for (;;)
  {
    bool isTimeout = false;
    const osLayer_Ticks_t elapsedTime = osLayer_getSysTick() - t0;
    const osLayer_Ticks_t remainingTime =
        pObj->timeoutLineWait_ms > elapsedTime ? pObj->timeoutLineWait_ms - elapsedTime : 1;
    if (!atUart_waitLineWithTimeout(pObj->respBuf, sizeof(pObj->respBuf), remainingTime, &isTimeout))
    {
      return false;
    }
    if (isTimeout)
    {
      *pFinResCode = AT_Cmd_FinalResultCode_Undef;
      break;
    }
    if (!atCmd_handleUnsolicitedResponse(pObj->respBuf))
    {
      if (atCmd_checkIsFinalResult(
          pObj->respBuf,
          finalResultCodeMap,
          finalResultCodeMapSize,
          pFinResCode))
      {
        break;
      }
    }
    if (NULL != callbackOnInformationTextResponse)
    {
      callbackOnInformationTextResponse(pObj->respBuf, cbParam);
    }
  }
  pObj->timeoutLineWait_ms = pObj->defaultTimeoutLineWait_ms;
  if (!atUart_stopReadingResponse())
  {
    return false;
  }
  return true;
}

static bool atCmd_transactionWithData(
    AT_Cmd_t* const pObj,
    const char* const at_cmd,
    const uint8_t* const buf,
    const uint16_t bufLen,
    const AT_Cmd_FinalResultCodeMap_t* const finalResultCodeMap,
    const uint16_t finalResultCodeMapSize,
    AT_Cmd_callbackOnInformationTextResponse_t callbackOnInformationTextResponse,
    void* const cbParam,
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  *pFinResCode = AT_Cmd_FinalResultCode_Undef;
  if (!atUart_sendCmdAT(at_cmd))
  {
    LOG_ATCMD_ERR("atUart_sendCmdAT");
    return false;
  }
  bool isAngleBracketTimeout = false;
  if (!atUart_waitAngleBracketWithTimeout(
      pObj->respBuf,
      sizeof(pObj->respBuf),
      pObj->timeoutLineWait_ms,
      &isAngleBracketTimeout))
  {
    LOG_ATCMD_ERR("atUart_waitAngleBracketWithTimeout");
    return false;
  }
  if (isAngleBracketTimeout)
  {
    LOG_ATCMD_ERR("Timeout waiting '>'");
    return false;
  }
  if ('>' == pObj->respBuf[0])
  {
    LOG_ATCMD_ERR("Unexpected resp (waiting '>'): %s", pObj->respBuf);
    return false;
  }
  if (!atCmd_sendBuf(buf, bufLen))
  {
    LOG_ATCMD_ERR("atCmd_sendBuf");
    return false;
  }

  for (;;)
  {
    bool isTimeout = false;
    if (!atUart_waitLineWithTimeout(pObj->respBuf, sizeof(pObj->respBuf), pObj->timeoutLineWait_ms, &isTimeout))
    {
      LOG_ATCMD_ERR("atUart_waitLineWithTimeout");
      return false;
    }
    if (isTimeout)
    {
      *pFinResCode = AT_Cmd_FinalResultCode_Undef;
      break;
    }
    if (atCmd_checkIsFinalResult(
        pObj->respBuf,
        finalResultCodeMap,
        finalResultCodeMapSize,
        pFinResCode))
    {
      break;
    }
    if (NULL != callbackOnInformationTextResponse)
    {
      callbackOnInformationTextResponse(pObj->respBuf, cbParam);
    }
  }
  pObj->timeoutLineWait_ms = pObj->defaultTimeoutLineWait_ms;
  if (!atUart_stopReadingResponse())
  {
    LOG_ATCMD_ERR("atUart_stopReadingResponse");
    return false;
  }
  return true;
}

static bool atCmd_transactionWaitAngleBracket(
    AT_Cmd_t* const pObj,
    const char* const at_cmd,
    const AT_Cmd_FinalResultCodeMap_t* const finalResultCodeMap,
    const uint16_t finalResultCodeMapSize,
    AT_Cmd_callbackOnInformationTextResponse_t callbackOnInformationTextResponse,
    void* const cbParam,
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  *pFinResCode = AT_Cmd_FinalResultCode_Undef;
  if (!atUart_sendCmdAT(at_cmd))
  {
    LOG_AT_CMD_ERR("atUart_sendCmdWithData");
    return false;
  }
  for (;;)
  {
    bool isTimeout = false;
    if (!atUart_waitAngleBracketWithTimeout(pObj->respBuf, sizeof(pObj->respBuf), pObj->timeoutLineWait_ms, &isTimeout))
    {
      LOG_AT_CMD_ERR("atUart_waitAngleBracketWithTimeout");
      return false;
    }
    if (isTimeout)
    {
      *pFinResCode = AT_Cmd_FinalResultCode_Undef;
      break;
    }
    if ('>' == pObj->respBuf[0])
    {
      *pFinResCode = AT_Cmd_FinalResultCode_AngleBracket;
      break;
    }
    if (atCmd_checkIsFinalResult(
        pObj->respBuf,
        finalResultCodeMap,
        finalResultCodeMapSize,
        pFinResCode))
    {
      break;
    }
    if (NULL != callbackOnInformationTextResponse)
    {
      callbackOnInformationTextResponse(pObj->respBuf, cbParam);
    }
  }
  pObj->timeoutLineWait_ms = pObj->defaultTimeoutLineWait_ms;
  if (!atUart_stopReadingResponse())
  {
    LOG_AT_CMD_ERR("atUart_stopReadingResponse");
    return false;
  }
  return true;
}

/**********************************************************************************************************************/

bool atCmd_AT(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CFUN(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CFUN=1,1",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}


bool atCmd_AT_CPIN(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CPIN?",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CIPMUX(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CIPMUX=0",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CSQ(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CSQ",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CREG(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CREG?",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_OpenCGATT(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CGATT=1",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CloseCGATT(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CGATT=0",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_SetSAPBR(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_OpenSAPBR(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+SAPBR=1,1",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CloseSAPBR(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+SAPBR=0,1",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CSTT(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CSTT=\"%s\"", GSM_APN);
  atCmd_setTemporaryTimeout(pObj, 10 * 1000);
  return atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CIICR(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  atCmd_setTemporaryTimeout(pObj, 10 * 1000);
  return atCmd_transaction(
      pObj,
      "AT+CIICR",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CIFSR(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{

  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[64];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIFSR");

  atCmd_setTemporaryTimeout(pObj, 1 * 1000);

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode))
  {
    return false;
  }
  return true;
}

bool atCmd_AT_CIPSHUT(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[64];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPSHUT");

  atCmd_setTemporaryTimeout(pObj, 20 * 1000);

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT_CIPSHUT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT_CIPSHUT),
      NULL,
      NULL,
      pFinResCode))
  {
    return false;
  }
  return true;
}

bool atCmd_AT_CIPSTATUS(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPSTATUS");

  atCmd_setTemporaryTimeout(pObj, 30 * 1000);

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT_CIPSTATUS,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT_CIPSTATUS),
      NULL,
      NULL,
      pFinResCode))
  {
    return false;
  }

  return true;
}

bool atCmd_AT_CIPSTART(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const host,
    const uint16_t port)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPSTART=\"TCP\",\"%s\",%u", host, port);

  atCmd_setTemporaryTimeout(pObj, 30 * 1000);

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT_CIPSTART,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT_CIPSTART),
      NULL,
      NULL,
      pFinResCode))
  {
    return false;
  }

  return true;
}

bool atCmd_AT_CIPSERVER(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const uint16_t port)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPSERVER=1,%u", port);

  atCmd_setTemporaryTimeout(pObj, 30 * 1000);

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT_CIPSERVER,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT_CIPSERVER),
      NULL,
      NULL,
      pFinResCode))
  {
    return false;
  }

  return true;
}

typedef struct AT_Cmd_InfoCIPRXGET_LEN_ASK_TAG
{
  bool flagReady;
  uint16_t cnflen;
} AT_Cmd_InfoCIPRXGET_LEN_ASK_t;

typedef struct AT_Cmd_InfoCIPRXGET_DATA_ASK_TAG
{
  AT_Cmd_String_t* const pMessage;
  uint16_t reqlen;
  bool flagReady;
} AT_Cmd_InfoCIPRXGET_DATA_ASK_t;


static void atCmd_cbCIPRXGET_LEN_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCIPRXGET_LEN_ASK_t* const pInfo = param;

  const char* const pData = atCmd_isPrefixMatch("+CIPRXGET:", textResponse);
  if (NULL == pData)
  {
    return;
  }
  const char* q = pData;
  const char* p = NULL;
  p = q + 3;
  const long cnflen = strtol(p, &q, 10);
  if('\0' != *q)
  {
    return;
  }
  pInfo->cnflen = cnflen;
  pInfo->flagReady = true;
}

static void atCmd_cbCIPRXGET_DATA_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCIPRXGET_DATA_ASK_t* const pInfo = param;

  const char* const pData = atCmd_isPrefixMatch("+CIPRXGET:", textResponse);
  if (NULL != pData)
  {
    return;
  }

  if (strlen(textResponse) == (pInfo->reqlen - 2))
  {
    atCmd_stringPrintf(pInfo->pMessage, "%.*s", pInfo->reqlen, textResponse);
    pInfo->flagReady = true;
  }
}

bool atCmd_AT_CIPRXGET(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    AT_CmdCIPRXGET_e mode,
    uint16_t reqlen,
    AT_Cmd_String_t* const pMessage,
    uint16_t* cnflen)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];


  atCmd_setTemporaryTimeout(pObj, 10 * 1000);
  if (CIPRXGET_MODE_ENABLE == mode)
  {
    t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPRXGET=%u", CIPRXGET_MODE_ENABLE);

    if (!atCmd_transaction(
        pObj,
        atCmdBuf,
        g_atCmd_resCodeMap_AT,
        ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
        NULL,
        NULL,
        pFinResCode))
    {
      return false;
    }
  }
  else if (CIPRXGET_MODE_READ_LEN == mode)
  {

    t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPRXGET=%u", CIPRXGET_MODE_READ_LEN);

    AT_Cmd_InfoCIPRXGET_LEN_ASK_t info = {
        .flagReady = false,
        .cnflen = 0
    };

    if (!atCmd_transaction(
        pObj,
        atCmdBuf,
        g_atCmd_resCodeMap_AT,
        ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
        &atCmd_cbCIPRXGET_LEN_ASK,
        &info,
        pFinResCode))
    {
      return false;
    }
    else
    {
      *cnflen = info.cnflen;
    }
  }

  else if (CIPRXGET_MODE_READ_DATA == mode)
  {
    t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPRXGET=%u, %u", CIPRXGET_MODE_READ_DATA, reqlen);

    AT_Cmd_InfoCIPRXGET_DATA_ASK_t info = {
        .flagReady = false,
        .reqlen = reqlen,
        .pMessage = pMessage,
    };

    if (!atCmd_transaction(
        pObj,
        atCmdBuf,
        g_atCmd_resCodeMap_AT,
        ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
        &atCmd_cbCIPRXGET_DATA_ASK,
        &info,
        pFinResCode))
    {
      return false;
    }
  }
  return true;
}

typedef struct AT_Cmd_InfoCSTT_ASK_TAG
{
  bool flagReady;
} AT_Cmd_InfoCSTT_ASK_t;

static void atCmd_cbCSTT_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCSTT_ASK_t* const pInfo = param;
  AT_Cmd_String_t gsm_apn;
  AT_Cmd_String_t gsm_user;
  AT_Cmd_String_t gsn_pass;

  const char* const pData = atCmd_isPrefixMatch("+CSTT:", textResponse);
  if (NULL == pData)
  {
    return;
  }
  const char* p = pData;
  const char* q = NULL;
  if (!atCmd_parseQuotedString(p, &gsm_apn, &q))
  {
    return;
  }
  if(NULL == q)
  {
    return;
  }
  if(',' != *q)
  {
    return;
  }

  p = q + 1;
  q = NULL;
  if (!atCmd_parseQuotedString(p, &gsm_user, &q))
  {
    return;
  }
  if(NULL == q)
  {
    return;
  }
  if(',' != *q)
  {
    return;
  }
  p = q + 1;
  q = NULL;
  pInfo->flagReady = true;
}

bool atCmd_AT_CheckCSTT(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady)
{
  *pFlagReady = false;
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CSTT?");

  AT_Cmd_InfoCSTT_ASK_t info = {.flagReady = false};

  atCmd_setTemporaryTimeout(pObj, 10 * 1000);

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      &atCmd_cbCSTT_ASK,
      &info,
      pFinResCode))
  {
    return false;
  }
  if (AT_Cmd_FinalResultCode_OK == *pFinResCode)
  {
    if (info.flagReady)
    {
      *pFlagReady =  true;
    }
  }
  return true;
}

/**********************************************************************************************************************/

bool atCmd_AT_RST(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+RST",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

/**********************************************************************************************************************/

bool atCmd_SDCARD_SENDFILE(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const connType,
    const char* const serverName,
    const uint16_t serverPort,
    const uint32_t timeout_sec,
    const char* const filePath,
    const char* const prefixPath,
    const char* const suffixPath)
{
  AT_Cmd_t* const pObj = &g_atCmd;

  LOG_AT_CMD_INFO("Cmd##: atCmd_SDCARD_SENDFILE");

  const size_t len = t_snprintf(NULL, 0,
      "AT+SDCARD_SENDFILE=\"%s\",\"%s\",%u,%u,\"%s\",\"%s\",\"%s\"",
        connType,
        serverName,
        serverPort,
        (unsigned)timeout_sec,
        filePath,
        prefixPath,
        suffixPath);
  LOG_AT_CMD_INFO("Cmd##: len=%u", (unsigned)len);
  char* const atCmd = osLayer_malloc(len + 1);
  if (NULL == atCmd)
  {
    LOG_AT_CMD_ERR("Can't allocate %u bytes for atCmd", (unsigned)(len + 1));
    return false;
  }
  t_snprintf(atCmd, len + 1,
      "AT+SDCARD_SENDFILE=\"%s\",\"%s\",%u,%u,\"%s\",\"%s\",\"%s\"",
        connType,
        serverName,
        serverPort,
        (unsigned)timeout_sec,
        filePath,
        prefixPath,
        suffixPath);
  LOG_AT_CMD_INFO("Send cmd: %s", atCmd);
  const int res = atCmd_transaction(
      pObj,
      atCmd,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
  osLayer_free(atCmd);
  return res;
}

/**********************************************************************************************************************/

bool atCmd_ATE0(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "ATE0",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

/**********************************************************************************************************************/

typedef struct AT_Cmd_InfoGMR_TAG
{
  bool flagReady;
  AT_Cmd_String_t* const pVersionOfAT;
  AT_Cmd_String_t* const pVersionOfSDK;
  AT_Cmd_String_t* const pCompileTime;
} AT_Cmd_InfoGMR_t;

static void atCmd_cbGMR(const char* const textResponse, void* param)
{
#define AT_GMR_PREFIX_AT_VERSION      "AT version:"
#define AT_GMR_PREFIX_SDK_VERSION     "SDK version:"
#define AT_GMR_PREFIX_COMPILE_TIME    "compile time:"

  AT_Cmd_InfoGMR_t* const pInfo = param;

  {
    const char* const pData = atCmd_isPrefixMatch(AT_GMR_PREFIX_AT_VERSION, textResponse);
    if (NULL != pData)
    {
      t_snprintf(pInfo->pVersionOfAT->buf, sizeof(pInfo->pVersionOfAT->buf), "%s", pData);
      SYSLOG_ESP32_INFO("AT_GMR: AT version:", pInfo->pVersionOfAT->buf);
      pInfo->flagReady = true;
      return;
    }
  }
  {
    const char* const pData = atCmd_isPrefixMatch(AT_GMR_PREFIX_SDK_VERSION, textResponse);
    if (NULL != pData)
    {
      t_snprintf(pInfo->pVersionOfSDK->buf, sizeof(pInfo->pVersionOfSDK->buf), "%s", pData);
      SYSLOG_ESP32_INFO("AT_GMR: SDK version:", pInfo->pVersionOfSDK->buf);
      pInfo->flagReady = true;
      return;
    }
  }
  {
    const char* const pData = atCmd_isPrefixMatch(AT_GMR_PREFIX_COMPILE_TIME, textResponse);
    if (NULL != pData)
    {
      t_snprintf(pInfo->pCompileTime->buf, sizeof(pInfo->pCompileTime->buf), "%s", pData);
      SYSLOG_ESP32_INFO("AT_GMR: Compile time:", pInfo->pCompileTime->buf);
      pInfo->flagReady = true;
      return;
    }
  }
}

bool atCmd_AT_GMR(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pVersionOfAT,
    AT_Cmd_String_t* const pVersionOfSDK,
    AT_Cmd_String_t* const pCompileTime)
{
  *pFlagReady = false;
  pVersionOfAT->buf[0] = '\0';
  pVersionOfSDK->buf[0] = '\0';
  pCompileTime->buf[0] = '\0';

  AT_Cmd_t* const pObj = &g_atCmd;

  atCmd_setTemporaryTimeout(pObj, 1 * 1000);

  AT_Cmd_InfoGMR_t info = {
      .flagReady = false,
      .pVersionOfAT = pVersionOfAT,
      .pVersionOfSDK = pVersionOfSDK,
      .pCompileTime = pCompileTime,
  };

  if (!atCmd_transaction(
      pObj,
      "AT+GMR",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      &atCmd_cbGMR,
      &info,
      pFinResCode))
  {
    return false;
  }
  if (info.flagReady)
  {
    *pFlagReady = true;
  }
  return true;
}

/**********************************************************************************************************************/

typedef struct AT_Cmd_InfoFirmwareVersion_TAG
{
  bool flagReady;
  AT_Cmd_String_t* const pFirmwareVersion;
} AT_Cmd_InfoFirmwareVersion_t;

static void atCmd_cbFW_VER(const char* const textResponse, void* param)
{
#define AT_FW_VER_PREFIX_AT_VERSION      "Firmware version: "

  AT_Cmd_InfoFirmwareVersion_t* const pInfo = param;

  {
    const char* const pData = atCmd_isPrefixMatch(AT_FW_VER_PREFIX_AT_VERSION, textResponse);
    if (NULL != pData)
    {
      t_snprintf(pInfo->pFirmwareVersion->buf, sizeof(pInfo->pFirmwareVersion->buf), "%s", pData);
      SYSLOG_ESP32_INFO("AT_FW_VER: Firmware version: ", pInfo->pFirmwareVersion->buf);
      pInfo->flagReady = true;
      return;
    }
  }
}

bool atCmd_AT_FW_VER(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pFirmwareVer)
{
  *pFlagReady = false;
  pFirmwareVer->buf[0] = '\0';

  AT_Cmd_t* const pObj = &g_atCmd;

  atCmd_setTemporaryTimeout(pObj, 1 * 1000);

  AT_Cmd_InfoFirmwareVersion_t info = {
      .flagReady = false,
      .pFirmwareVersion = pFirmwareVer,
  };

  if (!atCmd_transaction(
      pObj,
      "AT+FW_VER",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      &atCmd_cbFW_VER,
      &info,
      pFinResCode))
  {
    return false;
  }
  *pFinResCode = AT_Cmd_FinalResultCode_OK; // ignore error for old firmware which doesn't support 'AT+FW_VER'
  if (info.flagReady)
  {
    *pFlagReady = true;
  }
  return true;
}


/**********************************************************************************************************************/

bool atCmd_AT_SET_HOSTNAME(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const hostname)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+SET_HOSTNAME=\"%s\"", hostname);
  return atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

/**********************************************************************************************************************/

bool atCmd_AT_CWMODE1(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CWMODE=1",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

/**********************************************************************************************************************/

bool atCmd_AT_CWAUTOCONN1(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CWAUTOCONN=1",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

/**********************************************************************************************************************/

bool atCmd_AT_CWLAPOPT(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const bool flagSortByRSSI)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[20];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CWLAPOPT=%d,31", flagSortByRSSI ? 0 : 1);
  return atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

/**********************************************************************************************************************/

typedef struct AT_Cmd_InfoCWLAP_ASK_TAG
{
  bool flagReady;
} AT_Cmd_InfoCWLAP_ASK_t;

static void atCmd_cbCWLAP_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCWLAP_ASK_t* const pInfo = param;
  const char* const pData = atCmd_isPrefixMatch("+CWLAP:", textResponse);
  if (NULL == pData)
  {
    return;
  }
  pInfo->flagReady = true;
}

bool atCmd_AT_CWLAP(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const apName,
    bool* const pFlagFound)
{
  *pFlagFound = false;

  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CWLAP=\"%s\"", apName);

  AT_Cmd_InfoCWLAP_ASK_t info = {.flagReady = false};

  atCmd_setTemporaryTimeout(pObj, 10 * 1000);

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      &atCmd_cbCWLAP_ASK,
      &info,
      pFinResCode))
  {
    return false;
  }
  if (AT_Cmd_FinalResultCode_OK == *pFinResCode)
  {
    if (info.flagReady)
    {
      *pFlagFound = info.flagReady;
    }
  }
  return true;
}

/**********************************************************************************************************************/
bool atCmd_CWQAP(void)
{
  AT_Cmd_FinalResultCode_e pFinResCode = AT_Cmd_FinalResultCode_Undef;
  AT_Cmd_t* const pObj = &g_atCmd;
  if (!atCmd_transaction(
      pObj,
      "AT+CWQAP",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      &pFinResCode))
  {
    return false;
  }
  return true;
}
/**********************************************************************************************************************/

typedef struct AT_Cmd_InfoCWJAP_ASK_TAG
{
  bool flagReady;
  int cwjapStatus;
} AT_Cmd_InfoCWJAP_ASK_t;

static void atCmd_cbCWJAP_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCWJAP_ASK_t* const pInfo = param;
  const char* const pData = atCmd_isPrefixMatch("+CWJAP:", textResponse);
  if (NULL == pData)
  {
    return;
  }
  pInfo->cwjapStatus = atoi(pData);
  pInfo->flagReady = true;
}

bool atCmd_AT_CWJAP(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const ssid,
    const char* const pwd,
    int* const pCwjapStatus)
{
  *pCwjapStatus = -1;

  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[64];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CWJAP=\"%s\",\"%s\"", ssid, pwd);

  atCmd_setTemporaryTimeout(pObj, 30 * 1000);

  AT_Cmd_InfoCWJAP_ASK_t info = {.flagReady = false};

  if (!atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      &atCmd_cbCWJAP_ASK,
      &info,
      pFinResCode))
  {
    return false;
  }
  if (info.flagReady)
  {
    *pCwjapStatus = info.cwjapStatus;
  }
  return true;
}

/**********************************************************************************************************************/

typedef struct AT_Cmd_InfoCWJAPQ_ASK_TAG
{
  bool flagReady;
  AT_Cmd_String_t* const pSSID;
  AT_Cmd_String_t* const pBSSID_MAC;
  unsigned channel;
  int rssi;
} AT_Cmd_InfoCWJAPQ_ASK_t;

static void atCmd_cbCWJAPQ_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCWJAPQ_ASK_t* const pInfo = param;
  const char* const pData = atCmd_isPrefixMatch("+CWJAP:", textResponse);
  if (NULL == pData)
  {
    return;
  }
  const char* p = pData;
  const char* q = NULL;
  if (!atCmd_parseQuotedString(p, pInfo->pSSID, &q))
  {
    return;
  }
  if(NULL == q)
  {
    return;
  }
  if(',' != *q)
  {
    return;
  }

  p = q + 1;
  q = NULL;
  if (!atCmd_parseQuotedString(p, pInfo->pBSSID_MAC, &q))
  {
    return;
  }
  if(NULL == q)
  {
    return;
  }
  if(',' != *q)
  {
    return;
  }

  p = q + 1;
  char* end = NULL;
  const unsigned long channel = strtoul(p, &end, 10);
  if(',' != *end)
  {
    return;
  }
  pInfo->channel = channel;

  p = end + 1;
  end = NULL;
  const long rssi = strtol(p, &end, 10);
  if('\0' != *end)
  {
    return;
  }
  pInfo->rssi = rssi;

  SYSLOG_ESP32_INFO("SSID:%s, BSSID(MAC):%s, chan=%u, RSSI=%d",
                    pInfo->pSSID->buf,
                    pInfo->pBSSID_MAC->buf,
                    (unsigned)channel,
                    (int)rssi);
  pInfo->flagReady = true;
}

/**********************************************************************************************************************/

typedef struct AT_Cmd_InfoCIPSTAQ_ASK_TAG
{
  bool flagReady;
  AT_Cmd_String_t* const pIP;
  AT_Cmd_String_t* const pGateway;
  AT_Cmd_String_t* const pNetmask;
} AT_Cmd_InfoCIPSTAQ_ASK_t;

static void atCmd_cbCIPSTAQ_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCIPSTAQ_ASK_t* const pInfo = param;
  {
    const char* const pData = atCmd_isPrefixMatch("+CIPSTA:ip:", textResponse);
    if (NULL != pData)
    {
      if (!atCmd_parseQuotedString(pData, pInfo->pIP, NULL))
      {
        return;
      }
      pInfo->flagReady = true;
      return;
    }
  }
  {
    const char* const pData = atCmd_isPrefixMatch("+CIPSTA:gateway:", textResponse);
    if (NULL != pData)
    {
      if (!atCmd_parseQuotedString(pData, pInfo->pGateway, NULL))
      {
        return;
      }
      pInfo->flagReady = true;
      return;
    }
  }
  {
    const char* const pData = atCmd_isPrefixMatch("+CIPSTA:netmask:", textResponse);
    if (NULL != pData)
    {
      if (!atCmd_parseQuotedString(pData, pInfo->pNetmask, NULL))
      {
        return;
      }
      pInfo->flagReady = true;
      return;
    }
  }
}

bool atCmd_AT_CIPSTAQ(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pIP,
    AT_Cmd_String_t* const pGateway,
    AT_Cmd_String_t* const pNetmask)
{
  *pFlagReady = false;
  pIP->buf[0] = '\0';
  pGateway->buf[0] = '\0';
  pNetmask->buf[0] = '\0';

  AT_Cmd_t* const pObj = &g_atCmd;

  atCmd_setTemporaryTimeout(pObj, 3 * 1000);

  AT_Cmd_InfoCIPSTAQ_ASK_t info = {
      .flagReady = false,
      .pIP = pIP,
      .pGateway = pGateway,
      .pNetmask = pNetmask,
  };

  if (!atCmd_transaction(
      pObj,
      "AT+CIPSTA?",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      &atCmd_cbCIPSTAQ_ASK,
      &info,
      pFinResCode))
  {
    return false;
  }
  *pFlagReady = info.flagReady;
  return true;
}

/**********************************************************************************************************************/

typedef struct AT_Cmd_InfoCIPSTAMACQ_ASK_TAG
{
  bool flagReady;
  AT_Cmd_String_t* const pMAC;
} AT_Cmd_InfoCIPSTAMACQ_ASK_t;

static void atCmd_cbCIPSTAMACQ_ASK(const char* const textResponse, void* param)
{
  AT_Cmd_InfoCIPSTAMACQ_ASK_t* const pInfo = param;
  {
    const char* const pData = atCmd_isPrefixMatch("+CIPSTAMAC:", textResponse);
    if (NULL != pData)
    {
      if (!atCmd_parseQuotedString(pData, pInfo->pMAC, NULL))
      {
        return;
      }
      pInfo->flagReady = true;
      return;
    }
  }
}

bool atCmd_AT_CIPSTAMACQ(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    bool* const pFlagReady,
    AT_Cmd_String_t* const pMAC)
{
  *pFlagReady = false;
  pMAC->buf[0] = '\0';

  AT_Cmd_t* const pObj = &g_atCmd;

  atCmd_setTemporaryTimeout(pObj, 3 * 1000);

  AT_Cmd_InfoCIPSTAMACQ_ASK_t info = {
      .flagReady = false,
      .pMAC = pMAC,
  };

  if (!atCmd_transaction(
      pObj,
      "AT+CIPSTAMAC?",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      &atCmd_cbCIPSTAMACQ_ASK,
      &info,
      pFinResCode))
  {
    return false;
  }
  *pFlagReady = info.flagReady;
  return true;
}

/**********************************************************************************************************************/

bool atCmd_AT_CIPCLOSE(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CIPCLOSE",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}



bool atCmd_AT_CIPSTART_SSL(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const char* const host,
    const uint16_t port)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[48];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPSTART=\"SSL\",\"%s\",%u", host, port);
  atCmd_setTemporaryTimeout(pObj, 5 * 1000);
  return atCmd_transaction(
      pObj,
      atCmdBuf,
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CIPMODE_setTransmissionModePassthrough(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CIPMODE=1",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CIPMODE_setTransmissionModeNormal(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  return atCmd_transaction(
      pObj,
      "AT+CIPMODE=0",
      g_atCmd_resCodeMap_AT,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_AT_CIPSEND_startTransparentTransmission(
    AT_Cmd_FinalResultCode_e* const pFinResCode)
{
  AT_Cmd_t* const pObj = &g_atCmd;

  AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
  if (!atCmd_AT_CIPMODE_setTransmissionModePassthrough(&finResCode))
  {
    SYSLOG_ESP32_ERR("atCmd_AT_CIPMODE_setTransmissionModePassthrough");
  }
  if (AT_Cmd_FinalResultCode_OK != finResCode)
  {
    SYSLOG_ESP32_ERR("atCmd_AT_CIPMODE_setTransmissionModePassthrough, res=%d", finResCode);
  }
  return atCmd_transactionWaitAngleBracket(
      pObj,
      "AT+CIPSEND",
      g_atCmd_resCodeMap_AT_ERROR,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT_ERROR),
      NULL,
      NULL,
      pFinResCode);
}

void atCmd_AT_CIPSEND_finishTransparentTransmission(void)
{
  osLayer_sleepTicks(100);
  const char* const termSeq = "+++";
  SYSLOG_ESP32_INFO("> terminationSeq: %s", termSeq);
  atCmd_sendString(termSeq);
  osLayer_sleepTicks(100);

  AT_Cmd_FinalResultCode_e finResCode = AT_Cmd_FinalResultCode_Undef;
  if (!atCmd_AT_CIPMODE_setTransmissionModeNormal(&finResCode))
  {
    SYSLOG_ESP32_ERR("atCmd_AT_CIPMODE_setTransmissionModeNormal");
  }
  if (AT_Cmd_FinalResultCode_OK != finResCode)
  {
    SYSLOG_ESP32_ERR("atCmd_AT_CIPMODE_setTransmissionModeNormal, res=%d", finResCode);
  }
}

bool atCmd_AT_CIPSEND(
    AT_Cmd_FinalResultCode_e* const pFinResCode,
    const uint8_t* const buf,
    const uint16_t bufLen)
{
  AT_Cmd_t* const pObj = &g_atCmd;
  char atCmdBuf[20];
  t_snprintf(atCmdBuf, sizeof(atCmdBuf), "AT+CIPSEND=%u", (unsigned)bufLen);
  return atCmd_transactionWithData(
      pObj,
      atCmdBuf,
      buf,
      bufLen,
      g_atCmd_resCodeMap_AT_CIPSEND,
      ZZ_COUNT_OF(g_atCmd_resCodeMap_AT_CIPSEND),
      NULL,
      NULL,
      pFinResCode);
}

bool atCmd_sendString(
    const char* const pStr)
{
  return atUart_writeString(pStr);
}

bool atCmd_sendBuf(
    const uint8_t* const buf,
    const uint16_t bufLen)
{
  return atUart_writeBuf(buf, bufLen);
}

bool atCmd_readBuf(
    uint8_t* const buf,
    const uint16_t bufLen,
    const uint32_t timeout_ms)
{
  return atUart_readBuf(buf, bufLen, timeout_ms);
}

bool atCmd_readLineWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout)
{
  return atUart_waitLineWithTimeout(buf, bufLen, timeout_ms, pIsTimeout);
}

bool atCmd_waitIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD)
{
  return atUart_waitIPD(buf, bufLen, timeout_ms, pIsTimeout, pLenIPD);
}

bool atCmd_readIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    uint16_t* const pLenIPD)
{
  *pLenIPD = 0;
  bool flagTimeout = false;
  uint16_t lenIPD = 0;
  if (!atCmd_waitIPD(buf, bufLen, timeout_ms, &flagTimeout, &lenIPD))
  {
    LOG_AT_CMD_ERR("atCmd_waitIPD");
    return false;
  }
  if (flagTimeout)
  {
    return false;
  }
  LOG_AT_CMD_INFO("readIPD: got IPD len=%u", lenIPD);
  if (0 == lenIPD)
  {
    LOG_AT_CMD_INFO("atCmd_waitIPD zero len");
    return false;
  }
  if (lenIPD > bufLen)
  {
    LOG_AT_CMD_ERR("IPD len=%u > max len=%u, drop packet", lenIPD, bufLen);
    for (unsigned i = 0; i < lenIPD; ++i)
    {
      uint8_t ch;
      if (!atCmd_readBuf(&ch, 1, timeout_ms))
      {
        LOG_AT_CMD_ERR("atCmd_readBuf");
        return false;
      }
    }
    return false;
  }
  if (!atCmd_readBuf((uint8_t*)buf, lenIPD, timeout_ms))
  {
    LOG_AT_CMD_ERR("atCmd_readBuf");
    return false;
  }
  *pLenIPD = lenIPD;
  return true;
}

bool atCmd_waitSDIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD)
{
  return atUart_waitSDIPD(buf, bufLen, timeout_ms, pIsTimeout, pLenIPD);
}

bool atCmd_readSDIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    uint16_t* const pLenIPD)
{
  *pLenIPD = 0;
  bool flagTimeout = false;
  uint16_t lenIPD = 0;
  if (!atCmd_waitSDIPD(buf, bufLen, timeout_ms, &flagTimeout, &lenIPD))
  {
    LOG_AT_CMD_ERR("atCmd_waitSDIPD");
    return false;
  }
  if (flagTimeout)
  {
    return false;
  }
  LOG_AT_CMD_INFO("readSDIPD: got SDIPD len=%u", lenIPD);
  if (0 == lenIPD)
  {
    LOG_AT_CMD_INFO("atCmd_waitSDIPD zero len");
    return false;
  }
  if (lenIPD > bufLen)
  {
    LOG_AT_CMD_ERR("SDIPD len=%u > max len=%u, drop packet", lenIPD, bufLen);
    for (unsigned i = 0; i < lenIPD; ++i)
    {
      uint8_t ch;
      if (!atCmd_readBuf(&ch, 1, timeout_ms))
      {
        LOG_AT_CMD_ERR("atCmd_readBuf");
        return false;
      }
    }
    return false;
  }
  if (!atCmd_readBuf((uint8_t*)buf, lenIPD, timeout_ms))
  {
    LOG_AT_CMD_ERR("atCmd_readBuf");
    return false;
  }
  *pLenIPD = lenIPD;
  return true;
}
