// this file must be include in "log.h"

/*** LOG for FACILITY_TEMP ******************************************************************************************/

/**
 * @brief Prints log with 'TRACE' severity for specified 'facility'
 */
#define LOG_TEMP_TRACE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_TRACE, LOG_FACILITY_TEMP, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'NOISE' severity for specified 'facility'
 */
#define LOG_TEMP_NOISE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_NOISE, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOG_TEMP_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOGV_TEMP_DBG(fmt_, args_)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, args_)

#define LOGF_TEMP_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_TEMP_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_TEMP, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_TEMP_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_TEMP, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOG_TEMP_INFO(fmt_, ...)   \
    LOG(LOG_SEVERITY_INFO, LOG_FACILITY_TEMP, fmt_, ##__VA_ARGS__)

#define LOGF_TEMP_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_TEMP_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_TEMP, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_TEMP_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_TEMP, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGP_TEMP_INFO(prefix_, fmt_, ...)    \
    LOGP(LOG_SEVERITY_INFO, LOG_FACILITY_TEMP, prefix_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGV_TEMP_INFO(fmt_, args_)    \
    LOGV(LOG_SEVERITY_INFO, LOG_FACILITY_TEMP, fmt_, args_)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGVP_TEMP_INFO(prefix_, fmt_, args_)    \
    LOGVP(LOG_SEVERITY_INFO, LOG_FACILITY_TEMP, prefix_, fmt_, args_)

/**
 * @brief Prints log with 'WARN' severity for specified 'facility'
 */
#define LOG_TEMP_WARN(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_TEMP, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_TEMP_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_TEMP_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_TEMP, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_TEMP_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_TEMP, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_TEMP_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_TEMP, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_TEMP_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_TEMP_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_TEMP, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_TEMP_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_TEMP, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_TEMP_ERR_FUNC(func_, fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_TEMP, NULL, 0, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'CRIT' severity for specified 'facility'
 */
#define LOG_TEMP_CRIT(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_TEMP, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

#define LOGF_TEMP_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_TEMP, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_TEMP_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_TEMP, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_TEMP_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_TEMP, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_TEMP_MSG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_MSG, LOG_FACILITY_TEMP, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

