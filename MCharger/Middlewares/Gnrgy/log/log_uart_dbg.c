#include "log_uart_dbg.h"
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include "utils/t_snprintf.h"
#include "os_driver/hal_uart.h"
#include "irq/hal_irq.h"

void logUartDbg_vprintf(const char* fmt, va_list args)
{
#if LOG_UART && defined(TARGET_UART_DBG)
#if !defined(_WIN32)
  static bool g_logUartDbgStarted;
  static halUART_Handle_t* g_logUartDbgHandle;
  static char logBuf[200]; // we can use static buf because IRQ is disabled, so no other thread can interrupt execution
  int idx = 0;
  if (!g_logUartDbgStarted)
  {
    const HAL_IRQ_PriorityMask irqMask = halIrq_disable();
    (void)irqMask;
    g_logUartDbgStarted = true;
    g_logUartDbgHandle = osDrvUart_getHandle(TARGET_UART_DBG);
    if (NULL != g_logUartDbgHandle)
    {
      halUART_stop(g_logUartDbgHandle);
    }
    idx += t_snprintf(&logBuf[idx], sizeof(logBuf) - idx, "\r\n");
  }
  if (NULL == g_logUartDbgHandle)
  {
    return;
  }
  idx += t_vsnprintf(&logBuf[idx], sizeof(logBuf) - idx, fmt, args);
  idx += t_snprintf(&logBuf[idx], sizeof(logBuf) - idx, "\r\n");

  halUART_Transmit(g_logUartDbgHandle, (uint8_t*)logBuf, idx);
#else
  fflush(stdout);
  fflush(stderr);
  vfprintf(stderr, fmt, args);
  fprintf(stderr, "\n");
  fflush(stderr);
#endif
#endif
}

void logUartDbg_printf(const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  logUartDbg_vprintf(fmt, args);
  va_end(args);
}
