#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "log/log_ll.h"
#include "os_layer/os_layer.h"

#if defined(LOG_USB)
#include "usbd_cdc_if.h"
#endif

void logLL_usbTransmitWaitAsyncComplete(void)
{
#if defined(LOG_USB)
    while (usbCDC_isHostPortOpened() && !usbCDC_isReadyToTransmit())
    {
      osLayer_sleepTicks(1);
    }
#endif
}

bool logLL_usbTransmitAsync(
        const void* const txBuf,
        const uint16_t txBufLen)
{
    (void)txBuf;
    (void)txBufLen;
#if defined(LOG_USB)
    if (!usbCDC_isHostPortOpened())
    {
      return true;
    }
    logLL_usbTransmitWaitAsyncComplete();
    const USBD_StatusTypeDef res = (USBD_StatusTypeDef)CDC_Transmit_FS((uint8_t*)txBuf, txBufLen);
    if (USBD_OK != res)
    {
        return false;
    }
#endif
    return true;
}

bool logLL_usbTransmitSync(
        const void* const txBuf,
        const uint16_t txBufLen)
{
    (void)txBuf;
    (void)txBufLen;

#if defined(LOG_USB)
    if (!logLL_usbTransmitAsync(txBuf, txBufLen))
    {
        return false;
    }
    logLL_usbTransmitWaitAsyncComplete();
#endif
    return true;
}
