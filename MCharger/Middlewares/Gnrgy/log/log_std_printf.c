#include "log.h"
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>


#if !defined(_WIN32)
/**
 * @brief To redirect printf output for Keil uVision we need to override fputc.
 */
int fputc(int ch, FILE* f)
{
  log_lock();
  log_putcWithoutLock((char)ch);
  log_unlock();
  return ch;
}

/**
 * @brief To redirect printf output for GCC we need to override _write_r.
 */
int _write_r(void* r, int file, const uint8_t* ptr, size_t len)
{
  log_lock();
  while (len--)
  {
    log_putcWithoutLock(*ptr++);
  }
  log_unlock();
  return len;
}
#endif

