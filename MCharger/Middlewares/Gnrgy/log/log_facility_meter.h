// this file must be include in "log.h"

/*** LOG for FACILITY_METER ******************************************************************************************/

/**
 * @brief Prints log with 'TRACE' severity for specified 'facility'
 */
#define LOG_METER_TRACE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_TRACE, LOG_FACILITY_METER, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'NOISE' severity for specified 'facility'
 */
#define LOG_METER_NOISE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_NOISE, LOG_FACILITY_METER, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOG_METER_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_METER, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOGV_METER_DBG(fmt_, args_)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_METER, NULL, 0, __func__, fmt_, args_)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOG_METER_INFO(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_METER, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'WARN' severity for specified 'facility'
 */
#define LOG_METER_WARN(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_METER, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_METER_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_METER, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_METER_ERR_FUNC(func_, fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_METER, NULL, 0, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'CRIT' severity for specified 'facility'
 */
#define LOG_METER_CRIT(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_METER, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_METER_MSG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_MSG, LOG_FACILITY_METER, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

