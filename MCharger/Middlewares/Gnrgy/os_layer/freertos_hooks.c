#include "FreeRTOS.h"
#include "log/log.h"
#include "irq/hal_irq.h"
#include "app/app_glob.h"

void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
  const HAL_IRQ_PriorityMask sr = halIrq_disable();
  (void)sr;
  for (;;)
  {
  }
}

#if configUSE_MALLOC_FAILED_HOOK
void vApplicationMallocFailedHook(void)
{
  const HAL_IRQ_PriorityMask sr = halIrq_disable();
  (void)sr;
  while (1)
  {
  }
}
#endif

void vApplicationIdleHook(void)
{
  __asm volatile( "dsb" );
  __asm volatile( "wfi" );
  __asm volatile( "isb" );
}

