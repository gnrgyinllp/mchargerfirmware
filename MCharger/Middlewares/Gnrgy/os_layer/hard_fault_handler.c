#include <stdint.h>

#include "log/log.h"

#define HARDFAULT_PREFIX "[HARDFAULT]"

#define __STATIC_INLINE static inline
#define __ASM __asm

/**
  \brief   Get Main Stack Pointer
  \details Returns the current value of the Main Stack Pointer (MSP).
  \return               MSP Register value
 */
#if defined(__KEIL__)
__STATIC_INLINE uint32_t hfh__get_MSP(void)
{
  register uint32_t __regMainStackPointer     __ASM("msp");
  return(__regMainStackPointer);
}

#else
__STATIC_INLINE uint32_t hfh__get_MSP(void)
{
  register uint32_t result;

  __ASM volatile ("MRS %0, msp\n" : "=r" (result) );

  return(result);
}
#endif

/**
  \brief   Get Process Stack Pointer
  \details Returns the current value of the Process Stack Pointer (PSP).
  \return               PSP Register value
 */
#if defined(__KEIL__)
__STATIC_INLINE uint32_t hfh__get_PSP(void)
{
  register uint32_t __regProcessStackPointer  __ASM("psp");
  return(__regProcessStackPointer);
}

#else
__STATIC_INLINE  uint32_t hfh__get_PSP(void)
{
  register uint32_t result;

  __ASM volatile ("MRS %0, psp\n"  : "=r" (result) );
  return(result);
}
#endif

void printStackTrace(void)
{
    #define CODE_BASE 0x08040000
    #define CODE_END  (0x08000000 + 0x00200000)

    const uint32_t* const stackBegin = (const uint32_t*)hfh__get_MSP();
    const uint32_t* const stackEnd = (const uint32_t *)(stackBegin + 512);

    logDbg_printf("HardFault stack trace:\n");

    for (const uint32_t* ptr = stackBegin; ptr < stackEnd; ++ptr)
    {
        if ((((*ptr) >= (uint32_t)CODE_BASE) && ((*ptr) <= (uint32_t)CODE_END) && ((*ptr) & 0x00000001)))
        {
            logDbg_printf("    Address: 0x%08X\n", (unsigned int)(*(ptr) - 1));
        }
    }
}

void usrHardFaultHandler(const uint32_t* const registers)
{
    const uint32_t reg_r0 = registers[ 0 ];
    const uint32_t reg_r1 = registers[ 1 ];
    const uint32_t reg_r2 = registers[ 2 ];
    const uint32_t reg_r3 = registers[ 3 ];
    const uint32_t reg_r12 = registers[ 4 ];
    const uint32_t regLR = registers[5];
    const uint32_t regPC = registers[6];
    const uint32_t regPSR = registers[7];
    const uint32_t regMSP = hfh__get_MSP();
    const uint32_t regPSP = hfh__get_PSP();
    const uint32_t regCFSR = *((volatile unsigned long *)0xE000ED28);
    const uint32_t regHFSR = *((volatile unsigned long *)0xE000ED2C);
    const uint32_t regDFSR = *((volatile unsigned long *)0xE000ED30);
    const uint32_t regMMAR = *((volatile unsigned long *)0xE000ED34);
    const uint32_t regBFAR = *((volatile unsigned long *)0xE000ED38);
    const uint32_t regAFSR = *((volatile unsigned long *)0xE000ED3C);

    logDbg_printf("====================\n");
    logDbg_printf("==== HARD FAULT ====\n");
    logDbg_printf("====================\n");

    if (0 == regPC)
    {
        logDbg_printf("%s NULL pointer was called as function at 0x%08X\n", HARDFAULT_PREFIX, (unsigned int)(regLR - 1));
    }

    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "PC  ", (unsigned int)regPC);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "LR  ", (unsigned int)regLR);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "PSR ", (unsigned int)regPSR);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "MSP ", (unsigned int)regMSP);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "PSP ", (unsigned int)regPSP);

    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "r0  ", (unsigned int)reg_r0);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "r1  ", (unsigned int)reg_r1);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "r2  ", (unsigned int)reg_r2);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "r3  ", (unsigned int)reg_r3);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "r12 ", (unsigned int)reg_r12);

    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "CFSR", (unsigned int)regCFSR);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "HFSR", (unsigned int)regHFSR);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "DFSR", (unsigned int)regDFSR);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "MMAR", (unsigned int)regMMAR);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "BFAR", (unsigned int)regBFAR);
    logDbg_printf("%s %s: 0x%08X\n", HARDFAULT_PREFIX, "AFSR", (unsigned int)regAFSR);

    printStackTrace();

    while (1)
    {
    }
}
