#ifndef OSLAYER_H
#define OSLAYER_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t osLayer_TimeIntervalInSeconds_t;

typedef struct osLayer_Timer_TAG osLayer_Timer_t;

typedef struct osLayer_Signal_TAG osLayer_Signal_t;

typedef void* osLayer_QueueItem_t;

typedef enum osLayer_SignalNum_TAG
{
    osLayer_SignalNum_None = 0,
    osLayer_SignalNum_0,
    osLayer_SignalNum_1,
    osLayer_SignalNum_2,
    osLayer_SignalNum_3,
    osLayer_SignalNum_4,
    osLayer_SignalNum_5,
    osLayer_SignalNum_6,
    osLayer_SignalNum_7,
    osLayer_SignalNum_8,
    osLayer_SignalNum_9,
    osLayer_SignalNum_10,
    osLayer_SignalNum_11,
    osLayer_SignalNum_12,
    osLayer_SignalNum_13,
    osLayer_SignalNum_14,
    osLayer_SignalNum_15,
    osLayer_SignalNum_16,
    osLayer_SignalNum_17,
    osLayer_SignalNum_18,
    osLayer_SignalNum_19,
    osLayer_SignalNum_20,
    osLayer_SignalNum_21,
    osLayer_SignalNum_22,
    osLayer_SignalNum_23,
    osLayer_SignalNum_24,
    osLayer_SignalNum_25,
    osLayer_SignalNum_26,
    osLayer_SignalNum_27,
    osLayer_SignalNum_28,
    osLayer_SignalNum_29,
    osLayer_SignalNum_30,
} osLayer_SignalNum_e;

typedef struct osLayer_SignalBitMask_TAG
{
    int32_t bitMask;
    unsigned lastOfs;
} osLayer_SignalBitMask_t;

typedef struct osLayer_SignalSubscriber_TAG
{
    osLayer_Signal_t* pSigObj;
    osLayer_SignalNum_e sigNum;
} osLayer_SignalNotifier_t;

typedef struct osLayer_TimerSignal_TAG
{
    osLayer_Timer_t* pTimerObj;
    osLayer_Signal_t* pSignalObj;
    osLayer_SignalNum_e sigNum;
} osLayer_TimerSignal_t;

typedef struct osLayer_Queue_TAG osLayer_Queue_t;

typedef struct osLayer_Pool_TAG osLayer_Pool_t;

typedef struct osLayer_MailQueue_TAG osLayer_MailQueue_t;

typedef void (*osLayer_TimerFunc)(void* const argument);
/**********************************************************************************************************************/

#if defined(STM32F407xx) || defined(STM32F429xx)
#include "stm32f4xx_hal.h"
#endif

#if defined(_FREERTOS)
  #include "os_layer/os_layer_cmsis.h"
#endif

/**********************************************************************************************************************/

void* osLayer_malloc(const size_t size);

void osLayer_free(void* const ptr);

void* osLayer_calloc(const size_t nmemb, const size_t size);

char* osLayer_strdup(const char* const pStr);
char* osLayer_strdupWithLen(const char* const pStr, const int len);
char* osLayer_strdupVprintf(const char* const fmt, va_list args);

char* osLayer_strdupPrintf(const char* const fmt, ...);

static inline osLayer_DeltaTicks_t osLayer_convMillisecondsToTicks(
    const osLayer_DeltaMilliseconds_t deltaMilliseconds
)
{
    return (osLayer_DeltaTicks_t)OS_LAYER_MS_TO_TICKS(deltaMilliseconds);
}

static inline osLayer_DeltaMilliseconds_t osLayer_convTicksToMilliseconds(
    const osLayer_DeltaTicks_t deltaTicks
)
{
    return (osLayer_DeltaMilliseconds_t)OS_LAYER_TICKS_TO_MS(deltaTicks);
}

static inline osLayer_DeltaTicks_t osLayer_getElapsedTicks(const osLayer_Ticks_t t0)
{
    return (osLayer_DeltaTicks_t)(osLayer_getSysTick() - t0);
}

static inline bool osLayer_isTimeoutExpiredWithFixedUpdate(
    osLayer_Ticks_t* const pLastTick,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
    const osLayer_Ticks_t curTick = osLayer_getSysTick();
    const osLayer_DeltaTicks_t elapsedTicks = curTick - *pLastTick;
    if (elapsedTicks >= timeoutTicks)
    {
        *pLastTick += timeoutTicks;
        return true;
    }
    return false;
}

static inline bool osLayer_isTimeoutExpiredWithFloatingUpdate(
    osLayer_Ticks_t* const pLastTick,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
    const osLayer_Ticks_t curTick = osLayer_getSysTick();
    const osLayer_DeltaTicks_t elapsedTicks = curTick - *pLastTick;
    if (elapsedTicks >= timeoutTicks)
    {
        *pLastTick = curTick;
        return true;
    }
    return false;
}

static inline bool osLayer_isTimeoutExpired(const osLayer_Ticks_t t0, const osLayer_DeltaTicks_t timeoutTicks)
{
    const osLayer_DeltaTicks_t elapsedTicks = osLayer_getElapsedTicks(t0);
    if (elapsedTicks >= timeoutTicks)
    {
        return true;
    }
    return false;
}

static inline bool osLayer_isTimeoutExpiredMilliSeconds(const osLayer_Ticks_t t0,
      const osLayer_DeltaMilliseconds_t timeoutMilliSeconds)
{
    const osLayer_DeltaTicks_t timeoutTicks = osLayer_convMillisecondsToTicks(timeoutMilliSeconds);
    return osLayer_isTimeoutExpired(t0, timeoutTicks);
}

static inline bool osLayer_isTimeoutExpiredSeconds(const osLayer_Ticks_t t0,
      const osLayer_DeltaSeconds_t timeoutSeconds)
{
    return osLayer_isTimeoutExpiredMilliSeconds(t0, timeoutSeconds * 1000);
}

bool osLayer_threadCreate(
        const osLayer_ThreadDef_t* const pThreadDef,
        void* const argument,
        osLayer_ThreadId_t* const pThreadId);

bool osLayer_threadCancel(osLayer_ThreadId_t* const pThreadId);

/**
 * @brief Create osLayer_Timer_t instance of one shot software timer.
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param timerFunc     - the callback function to call when timer expires
 * @param argument      - the argument to be passed to callback timerFunc
 * @return ptr to new Timer object
 */
osLayer_Timer_t* osLayer_timerCreateOneShot(
        const char* const timerName,
        const osLayer_DeltaTicks_t delayTicks,
        const osLayer_TimerFunc timerFunc,
        void* const argument
);

/**
 * @brief Create osLayer_Timer_t instance of periodic software timer.
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param timerFunc     - the callback function to call when timer expires
 * @param argument      - the argument to be passed to callback timerFunc
 * @return ptr to new Timer object
 */
osLayer_Timer_t* osLayer_timerCreatePeriodic(
        const char* const timerName,
        const osLayer_DeltaTicks_t delayTicks,
        const osLayer_TimerFunc timerFunc,
        void* const argument
);

/**
 * @brief Delete timer
 * @param pTimer        - ptr to osLayer_Timer_t
 */
void osLayer_timerDelete(
    osLayer_Timer_t* const pTimer
);

/**
 * @brief Start the one shot timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @return true if success
 */
bool osLayer_timerStartOneShot(
        osLayer_Timer_t* const pTimer
);

/**
 * @brief Start the one shot timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @param delayTicks    - the period of the timer in ticks.
 * @return
 */
bool osLayer_timerStartOneShotEx(
        osLayer_Timer_t* const pTimer,
        const osLayer_DeltaTicks_t delayTicks
);

/**
 * @brief Start the periodic timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @return true if success
 */
bool osLayer_timerStartPeriodic(
        osLayer_Timer_t* const pTimer
);

/**
 * @brief Start the periodic timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @param delayTicks    - the period of the timer in ticks.
 * @return
 */
bool osLayer_timerStartPeriodicEx(
        osLayer_Timer_t* const pTimer,
        const osLayer_DeltaTicks_t delayTicks
);

/**
 * @brief Stop timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @return true if success
 */
bool osLayer_timerStop(
        osLayer_Timer_t* const pTimer
);

/**
 * @brief Simulate the operation of the timer.
 * @param pTimer - ptr to the instance of osLayer_Timer_t
 * @return none
 */
void osLayer_timerSimulate(osLayer_Timer_t* const pTimer);

/**
 * @brief Create new osLayer_Signal_t object.
 * @return instance of osLayer_Signal_t object
 */
osLayer_Signal_t* osLayer_signalCreate(void);

/**
 * @brief Delete osLayer_Signal_t object.
 */
void osLayer_signalDelete(osLayer_Signal_t* const pSigObj);

/**
 * @brief Register current thread in osLayer_Signal_t object.
 * @return true if success
 */
bool osLayer_signalRegisterCurThread(osLayer_Signal_t* const pSigObj);

/**
 * @brief Unregister thread
 * @param pSigObj
 */
void osLayer_signalUnregisterThread(osLayer_Signal_t* const pSigObj);

/**
 * @brief Check if thread regstered
 * @return true if thread registered
 */
bool osLayer_signalIsThreadRegistered(osLayer_Signal_t* const pSigObj);

/**
 * @brief Deinit instance of osLayer_Signal_t
 * @param pSigObj   - ptr to osLayer_Signal_t instance
 */
void osLayer_signalDeinit(osLayer_Signal_t* const pSigObj);

/**
 * @brief Register new signal to handle by osLayer_Signal_t
 * @param pSigObj   - ptr to osLayer_Signal_t instance
 * @param sigNum    - osLayer_SignalNum_e
 * @return true if success
 */
bool osLayer_signalAdd(
        osLayer_Signal_t* const pSigObj,
        const osLayer_SignalNum_e sigNum);

/**
 * @brief Send signal to the thread which created osLayer_Signal_t object.
 * @param pSigObj       - ptr to osLayer_Signal_t
 * @param sigNum        - osLayer_SignalNum_e
 * @return true if successful
 */
bool osLayer_signalSend(
        osLayer_Signal_t* const pSigObj,
        const osLayer_SignalNum_e sigNum);

/**
 * @brief Clear signal for the thread.
 * @param pSigObj       - ptr to osLayer_Signal_t
 * @param sigNum        - osLayer_SignalNum_e
 * @return true if successful
 */
bool osLayer_signalClear(
    osLayer_Signal_t* const pSigObj,
    const osLayer_SignalNum_e sigNum);

/**
 * @brief Wait for the calling task to receive one or more signals.
 * @param pSigObj           - ptr to osLayer_Signal_t
 * @param[OUT] pSigBitMask  - ptr to osLayer_SignalBitMask_t
 * @return true if success
 */
bool osLayer_signalWait(
        osLayer_Signal_t* const pSigObj,
        osLayer_SignalBitMask_t* const pSigBitMask);

/**
 * @brief Wait for the calling task to receive one or more signals.
 * @param pSigObj           - ptr to osLayer_Signal_t
 * @param timeoutTicks      - timeout in system ticks or OS_LAYER_TIME_IMMEDIATE / OS_LAYER_TIME_INFINITE
 * @param[OUT] pSigBitMask  - ptr to osLayer_SignalBitMask_t
 * @return true if success
 */
bool osLayer_signalWaitWithTimeout(
        osLayer_Signal_t* const pSigObj,
        osLayer_DeltaTicks_t timeoutTicks,
        osLayer_SignalBitMask_t* const pSigBitMask);

/**
 * @brief Get next signal from a set of received signals.
 * @param pSigBitMask    - ptr to osLayer_SignalBitMask_t
 * @return osLayer_SignalNum_e
 */
osLayer_SignalNum_e osLayer_signalGetNext(
        osLayer_SignalBitMask_t* const pSigBitMask);

/**********************************************************************************************************************/

osLayer_SignalNotifier_t osLayer_sigNotiMake(
    osLayer_Signal_t* const pSigObj,
    const osLayer_SignalNum_e sigNum
);

void osLayer_sigNotify(
    const osLayer_SignalNotifier_t* const pSigNoti
);

/**********************************************************************************************************************/

/**
 * @brief Create new one shot software timer which will send specified signal when the timeout expires.
 * @param pTimerSig     - ptr to osLayer_TimerSignal_t
 * @param pSignalObj    - ptr to osLayer_Signal_t
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param sigNum        - signal to be sent when timer expires
 * @return true if success
 */
bool osLayer_timerSignalCreateOneShot(
        osLayer_TimerSignal_t* const pTimerSig,
        osLayer_Signal_t* const pSignalObj,
        const char* const timerName,
        const osLayer_DeltaTicks_t delayTicks,
        const osLayer_SignalNum_e sigNum
);

/**
 * @brief Create new periodic software timer which sends specified signal with specified period.
 * @param pTimerSig     - ptr to osLayer_TimerSignal_t
 * @param pSignalObj    - ptr to osLayer_Signal_t
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param sigNum        - signal to be sent when timer expires
 * @return true if success
 */
bool osLayer_timerSignalCreatePeriodic(
    osLayer_TimerSignal_t* const pTimerSig,
    osLayer_Signal_t* const pSignalObj,
    const char* const timerName,
    const osLayer_DeltaTicks_t delayTicks,
    const osLayer_SignalNum_e sigNum
);

/**
 * @brief Start a one shot timerSignal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @return true if success
 */
bool osLayer_timerSignalStartOneShot(
        osLayer_TimerSignal_t* const pTimerSig
);

/**
 * @brief Start a periodic timerSignal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @return true if success
 */
bool osLayer_timerSignalStartPeriodic(
    osLayer_TimerSignal_t* const pTimerSig
);

/**
 * @brief Start a one shot timerSignal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @param delayTicks    - the period of the timer in ticks.
 * @return true if success
 */
bool osLayer_timerSignalStartOneShotEx(
        osLayer_TimerSignal_t* const pTimerSig,
        const osLayer_DeltaTicks_t delayTicks
);

/**
 * @brief Stop timer signal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @return true if success
 */
bool osLayer_timerSignalStop(
        osLayer_TimerSignal_t* const pTimerSig
);

/**
 * @brief Simulate the operation of the timerSignal.
 * @param pTimer - ptr to the instance of osLayer_TimerSignal_t
 * @return none
 */
void osLayer_timerSignalSimulate(osLayer_TimerSignal_t* const pTimerSig);

osLayer_Queue_t* osLayer_queueCreate(const uint32_t queueSize);
void osLayer_queueDelete(osLayer_Queue_t* const pQueue);
void osLayer_queueSend(osLayer_Queue_t* const pQueue, const osLayer_QueueItem_t item);
bool osLayer_queueRecv(osLayer_Queue_t* const pQueue, osLayer_QueueItem_t* const pItem);
bool osLayer_queueRecvImmediate(osLayer_Queue_t* const pQueue, osLayer_QueueItem_t* const pItem);

osLayer_Pool_t* osLayer_poolCreate(const uint32_t numItems, const size_t itemSize);
void osLayer_poolDelete(osLayer_Pool_t* const pPool);
void* osLayer_poolAlloc(osLayer_Pool_t* const pPool);
void* osLayer_poolCalloc(osLayer_Pool_t* const pPool);
bool osLayer_poolFree(osLayer_Pool_t* const pPool, void* const block);

osLayer_MailQueue_t* osLayer_mailQueueCreate(
    const uint32_t queueSize,
    const size_t itemSize
);

void osLayer_mailQueueDelete(
    osLayer_MailQueue_t* pMailQueue
);

bool osLayer_mailQueueSendImmediate(
    osLayer_MailQueue_t* pMailQueue,
    const void* const pMsgBuf
);

bool osLayer_mailQueueRecvInfinite(
    osLayer_MailQueue_t* pMailQueue,
    void* const pMsgBuf
);

bool osLayer_mailQueueRecvImmediate(
    osLayer_MailQueue_t* pMailQueue,
    void* const pMsgBuf
);

#ifdef __cplusplus
}
#endif

#endif //OSLAYER_H
