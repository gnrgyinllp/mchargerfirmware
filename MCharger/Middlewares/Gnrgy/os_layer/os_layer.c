#include "os_layer/os_layer.h"
#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "utils/t_snprintf.h"

char* osLayer_strdup(const char* const pStr)
{
    const size_t len = strlen(pStr) + 1;
    char* const p = osLayer_malloc(len);
    if (NULL == p)
    {
        return NULL;
    }
    strcpy(p, pStr);
    return p;
}

char* osLayer_strdupWithLen(const char* const pStr, const int len)
{
    char* const p = osLayer_malloc(len);
    if (NULL == p)
    {
        return NULL;
    }
    strcpy(p, pStr);
    return p;
}

char* osLayer_strdupVprintf(const char* const fmt, va_list args)
{
    const size_t len = (size_t)t_vsnprintf(NULL, 0, fmt, args);
    char* const text = osLayer_malloc(len + 1);
    if (NULL == text)
    {
        return NULL;
    }
    t_vsnprintf(text, len + 1, fmt, args);
    return text;
}

char* osLayer_strdupPrintf(const char* const fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    char* const p = osLayer_strdupVprintf(fmt, args);
    va_end(args);
    return p;
}

/**
 * @brief Get next signal from a set of received signals.
 * @param pSigBitMask    - ptr to osLayer_Signal_t
 * @return osLayer_SignalNum_e
 */
osLayer_SignalNum_e osLayer_signalGetNext(osLayer_SignalBitMask_t* const pSigBitMask)
{
    if (0 == pSigBitMask->bitMask)
    {
        return osLayer_SignalNum_None;
    }
    for (unsigned i = pSigBitMask->lastOfs; i < 31; ++i)
    {
        const int32_t mask = 1 << i;
        if (pSigBitMask->bitMask & mask)
        {
            pSigBitMask->bitMask ^= mask;
            pSigBitMask->lastOfs = i;
            return (osLayer_SignalNum_e)(osLayer_SignalNum_0 + i);
        }
    }
    return osLayer_SignalNum_None;
}

osLayer_SignalNotifier_t osLayer_sigNotiMake(
    osLayer_Signal_t* const pSigObj,
    const osLayer_SignalNum_e sigNum
)
{
    const osLayer_SignalNotifier_t sigNoti = {
        .pSigObj = pSigObj,
        .sigNum = sigNum,
    };
    return sigNoti;
};

void osLayer_sigNotify(
    const osLayer_SignalNotifier_t* const pSigNoti
)
{
    if (NULL == pSigNoti)
    {
        return;
    }
    if (NULL == pSigNoti->pSigObj)
    {
        return;
    }
    osLayer_signalSend(pSigNoti->pSigObj, pSigNoti->sigNum);
}


static void osLayer_cbTimerSignalFunc(void* const argument)
{
    osLayer_TimerSignal_t* const pTimerSig = argument;
    osLayer_signalSend(pTimerSig->pSignalObj, pTimerSig->sigNum);
}

/**
 * @brief Create new one shot software timer which will send specified signal when the timeout expires.
 * @param pTimerSig     - ptr to osLayer_TimerSignal_t
 * @param pSignalObj    - ptr to osLayer_Signal_t
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param sigNum        - signal to be sent when timer expires
 * @return true if success
 */
bool osLayer_timerSignalCreateOneShot(
        osLayer_TimerSignal_t* const pTimerSig,
        osLayer_Signal_t* const pSignalObj,
        const char* const timerName,
        const osLayer_DeltaTicks_t delayTicks,
        const osLayer_SignalNum_e sigNum
)
{
    memset(pTimerSig, 0, sizeof(*pTimerSig));

    pTimerSig->pSignalObj = pSignalObj;
    pTimerSig->sigNum = sigNum;

    pTimerSig->pTimerObj = osLayer_timerCreateOneShot(
        timerName,
        delayTicks,
        &osLayer_cbTimerSignalFunc,
        pTimerSig);
    if (NULL == pTimerSig->pTimerObj)
    {
        return false;
    }

    return true;
}

/**
 * @brief Create new periodic software timer which sends specified signal with specified period.
 * @param pTimerSig     - ptr to osLayer_TimerSignal_t
 * @param pSignalObj    - ptr to osLayer_Signal_t
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param sigNum        - signal to be sent when timer expires
 * @return true if success
 */
bool osLayer_timerSignalCreatePeriodic(
    osLayer_TimerSignal_t* const pTimerSig,
    osLayer_Signal_t* const pSignalObj,
    const char* const timerName,
    const osLayer_DeltaTicks_t delayTicks,
    const osLayer_SignalNum_e sigNum
)
{
    memset(pTimerSig, 0, sizeof(*pTimerSig));

    pTimerSig->pSignalObj = pSignalObj;
    pTimerSig->sigNum = sigNum;

    pTimerSig->pTimerObj = osLayer_timerCreatePeriodic(
        timerName,
        delayTicks,
        &osLayer_cbTimerSignalFunc,
        pTimerSig);
    if (NULL == pTimerSig->pTimerObj)
    {
        return false;
    }

    return true;
}

/**
 * @brief Start a one shot timerSignal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @return true if success
 */
bool osLayer_timerSignalStartOneShot(
        osLayer_TimerSignal_t* const pTimerSig
)
{
    return osLayer_timerStartOneShot(pTimerSig->pTimerObj);
}

/**
 * @brief Start a periodic timerSignal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @return true if success
 */
bool osLayer_timerSignalStartPeriodic(
    osLayer_TimerSignal_t* const pTimerSig
)
{
    return osLayer_timerStartPeriodic(pTimerSig->pTimerObj);
}

/**
 * @brief Start a one shot timerSignal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @param delayTicks    - the period of the timer in ticks.
 * @return true if success
 */
bool osLayer_timerSignalStartOneShotEx(
        osLayer_TimerSignal_t* const pTimerSig,
        const osLayer_DeltaTicks_t delayTicks
)
{
    osLayer_timerSignalStop(pTimerSig);
    return osLayer_timerStartOneShotEx(pTimerSig->pTimerObj, delayTicks);
}

/**
 * @brief Stop timer signal
 * @param pTimer        - ptr to osLayer_TimerSignal_t
 * @return true if success
 */
bool osLayer_timerSignalStop(
        osLayer_TimerSignal_t* const pTimerSig
)
{
    osLayer_timerStop(pTimerSig->pTimerObj);
    osLayer_signalClear(pTimerSig->pSignalObj, pTimerSig->sigNum);
    return true;
}

/**
 * @brief Simulate the operation of the timerSignal.
 * @param pTimer - ptr to the instance of osLayer_TimerSignal_t
 * @return none
 */
void osLayer_timerSignalSimulate(osLayer_TimerSignal_t* const pTimerSig)
{
    osLayer_timerSimulate(pTimerSig->pTimerObj);
}

