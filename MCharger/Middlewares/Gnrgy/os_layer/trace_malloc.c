#include "FreeRTOS.h"
#include "app/app_glob.h"
#include "log/log.h"

void app_traceMalloc(void* pvAddress, unsigned long uiSize)
{
#if TARGET_ENABLE_TRACE_MALLOC
  if (!log_isEnabled(LOG_SEVERITY_TRACE, LOG_FACILITY_MEMORY))
  {
    return;
  }
  logUartDbg_printf(
      "traceMalloc(%u): 0x%p [free: %u, %u]",
      (unsigned)uiSize,
      pvAddress,
      (unsigned)xPortGetFreeHeapSize(),
      (unsigned)xPortGetMinimumEverFreeHeapSize());
#endif
}

void app_traceFree(void* pvAddress, unsigned long uiSize)
{
#if TARGET_ENABLE_TRACE_MALLOC
  if (!log_isEnabled(LOG_SEVERITY_TRACE, LOG_FACILITY_MEMORY))
  {
    return;
  }
  logUartDbg_printf(
      "traceFree(%u): 0x%p [free: %u, %u]",
      (unsigned)uiSize,
      pvAddress,
      (unsigned)xPortGetFreeHeapSize(),
      (unsigned)xPortGetMinimumEverFreeHeapSize());
#endif
}

