#include "os_layer.h"
#include "os_layer_cmsis.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "log/log.h"
#include "main.h"


typedef struct osLayer_Signal_TAG
{
  osThreadId threadId;
  int32_t sigMask;
} osLayer_Signal_t;

/**
 * @breif Timer type value for the timer definition.
 */
typedef enum osLayer_TimerType_TAG {
    osLayer_TimerType_Ones = osTimerOnce,           ///< one-shot timer
    osLayer_TimerType_Periodic = osTimerPeriodic,   ///< repeating timer
} osLayer_TimerType_e;


struct osLayer_Timer_TAG
{
    const char* timerName;
    bool isPeriodic;
    bool isActive;
    osLayer_TimerFunc timerFunc;
    void* argument;
    osLayer_DeltaTicks_t delayTicks;
    osTimerId timerId;
    osStatus errCode;
};


void* osLayer_malloc(const size_t size)
{
    void* const p = pvPortMalloc(size);
    if (NULL == p)
    {
        Error_Handler();
    }
    return p;
}

void osLayer_free(void* const ptr)
{
    vPortFree(ptr);
}

void* osLayer_calloc(const size_t nmemb, const size_t size)
{
    const size_t memSize = nmemb * size;
    void* const p = pvPortMalloc(memSize);
    if (NULL == p)
    {
        return NULL;
    }
    memset(p, 0, memSize);
    return p;
}

bool osLayer_threadCreate(
        const osLayer_ThreadDef_t* const pThreadDef,
        void* const argument,
        osLayer_ThreadId_t* const pThreadId)
{

    pThreadId->threadId = osThreadCreate(pThreadDef, argument);
    if (NULL == pThreadId->threadId)
    {
        return false;
    }
    return true;
}

bool osLayer_threadCancel(osLayer_ThreadId_t* const pThreadId)
{
    if (osOK != osThreadTerminate(pThreadId->threadId))
    {
        return false;
    }
    return true;
}

/**
 * @brief Internal function which will be called when the timer expires or simulation is executed.
 * @param argument - ptr to osLayer_Timer_t
 * @return none
 */
static void osLayer_timerHandler(osLayer_Timer_t* const pTimer)
{
    if (!pTimer->isPeriodic)
    {
        pTimer->isActive = false;
    }
    if (NULL != pTimer->timerFunc)
    {
        pTimer->timerFunc(pTimer->argument);
    }
}

/**
 * @brief Internal function which will be called from the timer thread when the timer expires.
 * @note @ref TimerCallbackFunction_t
 * @param argument - ptr to osLayer_Timer_t
 * @return none
 */
static void osLayer_timerHandlerCMSIS(const void* const argument)
{
    const TimerHandle_t xTimer = (TimerHandle_t)argument;
    osLayer_Timer_t* const pTimer = pvTimerGetTimerID(xTimer);
    osLayer_timerHandler(pTimer);
}

/**
 * @brief Create osLayer_Timer_t instance of software timer.
 * @param timerName     - optional timer name (for debug purposes)
 * @param isPeriodic    - true if periodic timer should be created
 * @param delayTicks    - the period of the timer in ticks.
 * @param timerFunc     - the callback function to call when timer expires
 * @param argument      - the argument to be passed to callback timerFunc
 * @return true if success
 */
static osLayer_Timer_t* osLayer_timerCreate(
        const char* const timerName,
        const bool isPeriodic,
        const osLayer_DeltaTicks_t delayTicks,
        const osLayer_TimerFunc timerFunc,
        void* const argument
)
{
    osLayer_Timer_t* const pTimer = osLayer_calloc(1, sizeof(*pTimer));
    if (NULL == pTimer)
    {
      return NULL;
    }
    pTimer->timerName = timerName;
    pTimer->delayTicks = delayTicks;
    pTimer->timerFunc = timerFunc;
    pTimer->argument = argument;
    pTimer->isPeriodic = isPeriodic;
    pTimer->isActive = false;

    const osTimerDef_t timerDef = {
            .ptimer = &osLayer_timerHandlerCMSIS,
    };
    pTimer->timerId = osTimerCreate(&timerDef, isPeriodic ? osTimerPeriodic : osTimerOnce, pTimer);
    if (NULL == pTimer->timerId)
    {
        osLayer_free(pTimer);
        return NULL;
    }
    return pTimer;
}

/**
 * @brief Create osLayer_Timer_t instance of one shot software timer.
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param timerFunc     - the callback function to call when timer expires
 * @param argument      - the argument to be passed to callback timerFunc
 * @return ptr to new Timer object
 */
osLayer_Timer_t* osLayer_timerCreateOneShot(
        const char* const timerName,
        const osLayer_DeltaTicks_t delayTicks,
        const osLayer_TimerFunc timerFunc,
        void* const argument
)
{
  return osLayer_timerCreate(timerName, false, delayTicks, timerFunc, argument);
}

/**
 * @brief Create osLayer_Timer_t instance of periodic software timer.
 * @param timerName     - optional timer name (for debug purposes)
 * @param delayTicks    - the period of the timer in ticks.
 * @param timerFunc     - the callback function to call when timer expires
 * @param argument      - the argument to be passed to callback timerFunc
 * @return ptr to new Timer object
 */
osLayer_Timer_t* osLayer_timerCreatePeriodic(
        const char* const timerName,
        const osLayer_DeltaTicks_t delayTicks,
        const osLayer_TimerFunc timerFunc,
        void* const argument
)
{
  return osLayer_timerCreate(timerName, true, delayTicks, timerFunc, argument);
}

/**
 * @brief Delete timer
 * @param pTimer        - ptr to osLayer_Timer_t
 */
void osLayer_timerDelete(
    osLayer_Timer_t* const pTimer
)
{
  osLayer_timerStop(pTimer);
  osLayer_free(pTimer);
}

/**
 * @brief Start the one shot timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @return true if success
 */
bool osLayer_timerStartOneShot(
        osLayer_Timer_t* const pTimer
)
{
    if (pTimer->isPeriodic)
    {
      return false;
    }
    pTimer->isActive = true;
    for (;;)
    {
        pTimer->errCode = osTimerStart(pTimer->timerId, osLayer_convDeltaTicksToDelayUMilliseconds(pTimer->delayTicks));
        if (osOK == pTimer->errCode)
        {
            break;
        }
        osLayer_sleepTicks(1);
    }
    return true;
}

/**
 * @brief Start the one shot timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @param delayTicks    - the period of the timer in ticks.
 * @return
 */
bool osLayer_timerStartOneShotEx(
        osLayer_Timer_t* const pTimer,
        const osLayer_DeltaTicks_t delayTicks
)
{
    pTimer->delayTicks = delayTicks;
    return osLayer_timerStartOneShot(pTimer);
}

/**
 * @brief Start the periodic timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @return true if success
 */
bool osLayer_timerStartPeriodic(
        osLayer_Timer_t* const pTimer
)
{
    if (!pTimer->isPeriodic)
    {
      return false;
    }
    pTimer->isActive = true;
    for (;;)
    {
        pTimer->errCode = osTimerStart(pTimer->timerId, osLayer_convDeltaTicksToDelayUMilliseconds(pTimer->delayTicks));
        if (osOK == pTimer->errCode)
        {
            break;
        }
        LOG_OS_WARN("osTimerStart res=%d", pTimer->errCode);
        osLayer_sleepTicks(1);
    }
    return true;
}

/**
 * @brief Start the periodic timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @param delayTicks    - the period of the timer in ticks.
 * @return
 */
bool osLayer_timerStartPeriodicEx(
        osLayer_Timer_t* const pTimer,
        const osLayer_DeltaTicks_t delayTicks
)
{
    pTimer->delayTicks = delayTicks;
    return osLayer_timerStartPeriodic(pTimer);
}


/**
 * @brief Stop timer
 * @param pTimer        - ptr to osLayer_Timer_t
 * @return true if success
 */
bool osLayer_timerStop(
        osLayer_Timer_t* const pTimer
)
{
    pTimer->isActive = false;
    for (;;)
    {
        pTimer->errCode = osTimerStop(pTimer->timerId);
        if (osOK == pTimer->errCode)
        {
            break;
        }
        LOG_OS_WARN("osTimerStop res=%d", pTimer->errCode);
        osLayer_sleepTicks(1);
    }
    return true;
}

/**
 * @brief Simulate the operation of the timer.
 * @param pTimer - ptr to the instance of osLayer_Timer_t
 * @return none
 */
void osLayer_timerSimulate(
        osLayer_Timer_t* const pTimer)
{
    osLayer_timerHandler(pTimer);
}


/**
 * @brief Create new osLayer_Signal_t object.
 * @return instance of osLayer_Signal_t object
 */
osLayer_Signal_t* osLayer_signalCreate(void)
{
    osLayer_Signal_t* pSigObj = osLayer_calloc(1, sizeof(*pSigObj));
    pSigObj->threadId = 0;
    pSigObj->sigMask = 0;
    return pSigObj;
}

/**
 * @brief Delete osLayer_Signal_t object.
 */
void osLayer_signalDelete(osLayer_Signal_t* const pSigObj)
{
  osLayer_signalDeinit(pSigObj);
  osLayer_free(pSigObj);
}

/**
 * @brief Register current thread in osLayer_Signal_t object.
 * @return true if success
 */
bool osLayer_signalRegisterCurThread(osLayer_Signal_t* const pSigObj)
{
    if (NULL == pSigObj)
    {
      return false;
    }
    pSigObj->threadId = osThreadGetId();
    return true;
}

/**
 * @brief Unregister thread
 * @param pSigObj
 */
void osLayer_signalUnregisterThread(osLayer_Signal_t* const pSigObj)
{
  pSigObj->threadId = 0;
}

/**
 * @brief Check if thread regstered
 * @return true if thread registered
 */
bool osLayer_signalIsThreadRegistered(osLayer_Signal_t* const pSigObj)
{
  if (0 != pSigObj->threadId)
  {
    return true;
  }
  return false;
}

/**
 * @brief Deinit instance of osLayer_Signal_t
 * @param pSigObj   - ptr to osLayer_Signal_t instance
 */
void osLayer_signalDeinit(osLayer_Signal_t* const pSigObj)
{
    pSigObj->threadId = 0;
    pSigObj->sigMask = 0;
}

/**
 * @brief Register new signal to handle by osLayer_Signal_t
 * @param pSigObj   - ptr to osLayer_Signal_t instance
 * @param sigNum    - osLayer_SignalNum_e
 * @return true if success
 */
bool osLayer_signalAdd(
        osLayer_Signal_t* const pSigObj,
        const osLayer_SignalNum_e sigNum)
{
    const unsigned sigIdx = sigNum - osLayer_SignalNum_0;
    if (sigIdx >= 31)
    {
        return false;
    }
    if ((0 != (pSigObj->sigMask & (1 << sigIdx))))
    {
        return false;
    }
    pSigObj->sigMask |= 1 << sigIdx;
    return true;
}

/**
 * @brief Send signal to the thread which created osLayer_Signal_t object.
 * @param pSigObj       - ptr to osLayer_Signal_t
 * @param sigNum        - osLayer_SignalNum_e
 * @return true if successful
 */
bool osLayer_signalSend(
        osLayer_Signal_t* const pSigObj,
        const osLayer_SignalNum_e sigNum)
{
    if (NULL == pSigObj)
    {
      return false;
    }
    const unsigned sigIdx = sigNum - osLayer_SignalNum_0;
    if (sigIdx >= 31)
    {
      return false;
    }
    const int32_t sigMask = 1 << sigIdx;
    if (0 == pSigObj->threadId)
    {
      return false;
    }
    int32_t res = osSignalSet(pSigObj->threadId, sigMask);
    if (osOK != res)
    {
        return false;
    }
    return true;
}

/**
 * @brief Clear signal for the thread.
 * @param pSigObj       - ptr to osLayer_Signal_t
 * @param sigNum        - osLayer_SignalNum_e
 * @return true if successful
 */
bool osLayer_signalClear(
    osLayer_Signal_t* const pSigObj,
    const osLayer_SignalNum_e sigNum)
{
  if (NULL == pSigObj)
  {
    return false;
  }
  const unsigned sigIdx = sigNum - osLayer_SignalNum_0;
  if (sigIdx >= 31)
  {
    return false;
  }
  const int32_t sigMask = 1 << sigIdx;
  if (0 == pSigObj->threadId)
  {
    return false;
  }
  int32_t res = osSignalClear(pSigObj->threadId, sigMask);
  if (0x80000000UL == (uint32_t)res)
  {
    return false;
  }
  return true;
}

/**
 * @brief Wait for the calling task to receive one or more signals.
 * @param pSigObj           - ptr to osLayer_Signal_t
 * @param[OUT] pSigBitMask  - ptr to osLayer_SignalBitMask_t
 * @return true if success
 */
bool osLayer_signalWait(
        osLayer_Signal_t* const pSigObj,
        osLayer_SignalBitMask_t* const pSigBitMask)
{
    return osLayer_signalWaitWithTimeout(pSigObj, OS_LAYER_TIME_INFINITE, pSigBitMask);
}

/**
 * @brief Wait for the calling task to receive one or more signals.
 * @param pSigObj           - ptr to osLayer_Signal_t
 * @param timeoutTicks      - timeout in system ticks or OS_LAYER_TIME_IMMEDIATE / OS_LAYER_TIME_INFINITE
 * @param[OUT] pSigBitMask  - ptr to osLayer_SignalBitMask_t
 * @return true if success
 */
bool osLayer_signalWaitWithTimeout(
        osLayer_Signal_t* const pSigObj,
        osLayer_DeltaTicks_t timeoutTicks,
        osLayer_SignalBitMask_t* const pSigBitMask)
{
    if (0 == pSigObj->threadId)
    {
      return false;
    }
    const osEvent ev = osSignalWait(pSigObj->sigMask, osLayer_convDeltaTicksToDelayUMilliseconds(timeoutTicks));
    if (osEventSignal != ev.status)
    {
        return false;
    }
    pSigBitMask->bitMask = ev.value.signals & pSigObj->sigMask;
    pSigBitMask->lastOfs = 0;
    return true;
}

struct osLayer_MailQueue_TAG
{
    size_t itemSize;
    osMailQId mailQ;
    struct os_mailQ_cb *os_mailQ_cb;
    osMailQDef_t os_mailQ_def;
};

osLayer_MailQueue_t* osLayer_mailQueueCreate(
    const uint32_t queueSize,
    const size_t itemSize
)
{
  osLayer_MailQueue_t* pMailQueue = osLayer_malloc(sizeof(*pMailQueue));
  if (NULL == pMailQueue)
  {
    return false;
  }
  pMailQueue->itemSize = itemSize;
  pMailQueue->os_mailQ_cb = NULL;
  pMailQueue->os_mailQ_def.queue_sz = queueSize;
  pMailQueue->os_mailQ_def.item_sz = itemSize;
  pMailQueue->os_mailQ_def.cb = &pMailQueue->os_mailQ_cb;

  pMailQueue->mailQ = osMailCreate(&pMailQueue->os_mailQ_def, NULL);
  if (!pMailQueue->mailQ)
  {
    osLayer_free(pMailQueue);
    return false;
  }
  return pMailQueue;
}

bool osLayer_mailQueueSendImmediate(
    osLayer_MailQueue_t* pMailQueue,
    const void* const pMsgBuf
)
{
  if (NULL == pMailQueue)
  {
    return false;
  }
  const uint32_t millisec = 0;
  void* const pMailBuf = osMailAlloc(pMailQueue->mailQ, millisec);
  if (NULL == pMailBuf)
  {
    return false;
  }
  memcpy(pMailBuf, pMsgBuf, pMailQueue->itemSize);

  const osStatus status = osMailPut(pMailQueue->mailQ, pMailBuf);
  if (osOK != status)
  {
    return false;
  }
  return true;
}

bool osLayer_mailQueueRecvInfinite(
    osLayer_MailQueue_t* pMailQueue,
    void* const pMsgBuf
)
{
  if (NULL == pMailQueue)
  {
    return false;
  }
  const osEvent event = osMailGet(pMailQueue->mailQ, osWaitForever);
  if (osEventMail != event.status)
  {
    return false;
  }
  const void* const pMailBuf = event.value.p;
  memcpy(pMsgBuf, pMailBuf, pMailQueue->itemSize);
  const osStatus status = osMailFree(pMailQueue->mailQ, (void*)pMailBuf);
  if (osOK != status)
  {
    return false;
  }
  return true;
}

bool osLayer_mailQueueRecvImmediate(
    osLayer_MailQueue_t* pMailQueue,
    void* const pMsgBuf
)
{
  if (NULL == pMailQueue)
  {
    return false;
  }
  const osEvent event = osMailGet(pMailQueue->mailQ, 0);
  if (osEventMail != event.status)
  {
    return false;
  }
  const void* const pMailBuf = event.value.p;
  memcpy(pMsgBuf, pMailBuf, pMailQueue->itemSize);
  const osStatus status = osMailFree(pMailQueue->mailQ, (void*)pMailBuf);
  if (osOK != status)
  {
    return false;
  }
  return true;
}
