#include "hal_gpio.h"
#include <string.h>
#include "irq/hal_irq.h"


void halGPIO_usedPortsInit(
    halGPIO_UsedPorts_t* const pUsedPorts
)
{
  memset(pUsedPorts, 0, sizeof(*pUsedPorts));
  pUsedPorts->bmUsedPorts = 0;
  for (halGPIO_Port_e portIdx = halGPIO_Port_A; portIdx < halGPIO_NUM_PORTS; ++portIdx)
  {
    halGPIO_UsedPins_t* const pUsedPins = &pUsedPorts->usedPins[portIdx];
    pUsedPins->bmUsedPins = 0;
    pUsedPins->maskModer = 0;
  }
  pUsedPorts->usedPins[halGPIO_Port_A].pGPIOx = GPIOA;
  pUsedPorts->usedPins[halGPIO_Port_B].pGPIOx = GPIOB;
  pUsedPorts->usedPins[halGPIO_Port_C].pGPIOx = GPIOC;
  pUsedPorts->usedPins[halGPIO_Port_D].pGPIOx = GPIOD;
  pUsedPorts->usedPins[halGPIO_Port_E].pGPIOx = GPIOE;
  pUsedPorts->usedPins[halGPIO_Port_F].pGPIOx = GPIOF;
  pUsedPorts->usedPins[halGPIO_Port_G].pGPIOx = GPIOG;
  pUsedPorts->usedPins[halGPIO_Port_H].pGPIOx = GPIOH;
}

bool halGPIO_usedPortsAddPins(
    halGPIO_UsedPorts_t* const pUsedPorts,
    GPIO_TypeDef* const pGPIOx,
    const halGPIO_BitMaskPins_t bmPins
)
{
  for (halGPIO_Port_e portIdx = halGPIO_Port_A; portIdx < halGPIO_NUM_PORTS; ++portIdx)
  {
    halGPIO_UsedPins_t* const pUsedPins = &pUsedPorts->usedPins[portIdx];
    if (pUsedPins->pGPIOx == pGPIOx)
    {
      pUsedPorts->bmUsedPorts |= 1 << portIdx;
      pUsedPins->bmUsedPins |= bmPins;
      for (unsigned pinIdx = 0; pinIdx < 16; ++pinIdx)
      {
        if (0 != (bmPins & (1 << pinIdx)))
        {
          pUsedPins->maskModer |= 0x3 << (pinIdx * 2);
        }
      }
      return true;
    }
  }
  return false;
}

void halGPIO_modeSwitchToInput(const halGPIO_UsedPorts_t* const pUsedPorts)
{
  halGPIO_BitMaskUsedPorts_t bmUsedPorts = pUsedPorts->bmUsedPorts;
  while (0 != bmUsedPorts)
  {
    const unsigned portIdx = 31 - __CLZ(bmUsedPorts);
    const halGPIO_UsedPins_t* const pUsedPins = &pUsedPorts->usedPins[portIdx];
    HAL_IRQ_PriorityMask priMask = halIrq_disable();
    pUsedPins->pGPIOx->MODER &= ~pUsedPins->maskModer;
    halIrq_restore(priMask);
    bmUsedPorts ^= 1 << portIdx;
  }
}

void halGPIO_modeRestoreBackToOutput(const halGPIO_UsedPorts_t* const pUsedPorts)
{
  halGPIO_BitMaskUsedPorts_t bmUsedPorts = pUsedPorts->bmUsedPorts;
  while (0 != bmUsedPorts)
  {
    const unsigned portIdx = 31 - __CLZ(bmUsedPorts);
    const halGPIO_UsedPins_t* const pUsedPins = &pUsedPorts->usedPins[portIdx];
    HAL_IRQ_PriorityMask priMask = halIrq_disable();
    pUsedPins->pGPIOx->MODER |= 0x55555555 & pUsedPins->maskModer;
    halIrq_restore(priMask);
    bmUsedPorts ^= 1 << portIdx;
  }
}


/**
 * @brief   Pads mode setup.
 * @details This function programs a pads group belonging to the same port
 *          with the specified mode.
 * @note    @p PAL_MODE_UNCONNECTED is implemented as push pull output at 2MHz.
 * @note    Writing on pads programmed as pull-up or pull-down has the side
 *          effect to modify the resistor setting because the output latched
 *          data is used for the resistor selection.
 *
 * @param[in] port      the port identifier
 * @param[in] mask      the group mask
 * @param[in] mode      the mode
 *
 * @notapi
 */
void halGPIO_setGroupMode(
    GPIO_TypeDef* const port,
    halGPIO_BitMaskPins_t mask,
    const halGPIO_MODE_t mode)
{
  uint32_t moder = (mode & halGPIO_STM32_MODE_MASK) >> 0;
  uint32_t otyper = (mode & halGPIO_STM32_OTYPE_MASK) >> 2;
  uint32_t ospeedr = (mode & halGPIO_STM32_OSPEED_MASK) >> 3;
  uint32_t pupdr = (mode & halGPIO_STM32_PUDR_MASK) >> 5;
  uint32_t altr = (mode & halGPIO_STM32_ALTERNATE_MASK) >> 7;
  uint32_t bit = 0;
  for (;;)
  {
    if ((mask & 1) != 0)
    {
      uint32_t altrmask, m1, m2, m4;

      altrmask = altr << ((bit & 7) * 4);
      m4 = 15 << ((bit & 7) * 4);
      if (bit < 8)
        port->AFR[0] = (port->AFR[0] & ~m4) | altrmask;
      else
        port->AFR[1] = (port->AFR[1] & ~m4) | altrmask;
      m1 = 1 << bit;
      port->OTYPER = (port->OTYPER & ~m1) | otyper;
      m2 = 3 << (bit * 2);
      port->OSPEEDR = (port->OSPEEDR & ~m2) | ospeedr;
      port->PUPDR = (port->PUPDR & ~m2) | pupdr;
      port->MODER = (port->MODER & ~m2) | moder;
    }
    mask >>= 1;
    if (!mask)
      return;
    otyper <<= 1;
    ospeedr <<= 2;
    pupdr <<= 2;
    moder <<= 2;
    bit++;
  }
}
