#include "asserts/hw_assert.h"

#include <stdarg.h>
#include <stddef.h>

#include "log/log.h"
#include "utils/attribs.h"
#include "utils/t_snprintf.h"

ZZ_ATTR_NORETURN
static void hw_vassert(const char* expr,
    const char* file,
    const int line,
    const char* func,
    const char* fmt,
    va_list args)
{
  char prefixBuf[48];
  if (NULL != fmt)
  {
    t_snprintf(prefixBuf, sizeof(prefixBuf), "HW_ASSERT: %s: ", expr);
  }
  else
  {
    t_snprintf(prefixBuf, sizeof(prefixBuf), "HW_ASSERT: %s", expr);
  }
  log_vprintfExtWithPrefix(
      LOG_SEVERITY_MSG,
      LOG_FACILITY_COMMON,
      file,
      line,
      func,
      prefixBuf,
      fmt,
      args
  );
  while(1)
  {
  }

}

ZZ_ATTR_FORMAT_PRINTF(5, 6)
void hw_assert(const char* expr, const char* file, const int line, const char* func, const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  hw_vassert(expr, file, line, func, fmt, args);
  va_end(args);
}