#if !defined(HW_ASSERT_H_)
#define HW_ASSERT_H_

#include "../../Gnrgy/utils/attribs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define HW_ASSERT_TYPECHECK(type, x) \
({      type dummy1; \
        __typeof__(x) dummy2; \
        (void)(&dummy1 == &dummy2); \
        1; \
})

#if defined(NDEBUG)
/**
 * @brief assert-statements, used in release-mode (if NDEBUG is defined)
 */
#define HW_ASSERT(expr_)

#define HW_ASSERT_RET(expr_) \
    do { if(!(expr_)){return;} } while(0)

#define HW_ASSERT_RETVAL(expr_, val_) \
    do { if(!(expr_)){return val_;} } while(0)

#define HW_ASSERT_NOT_NULL(ptr_)

#define HW_ASSERT_NOT_NULL_RET(ptr_) \
    do { if((NULL == (ptr_))){return;}} while(0);

#define HW_ASSERT_NOT_NULL_RETVAL(ptr_, val_) \
    do { if((NULL == (ptr_))){return val_;}} while(0);

#define HW_ASSERT_MSG(expr_, msg_)

#define HW_ASSERT_FMT(expr_, fmt_, ...)

#define HW_ASSERT_RET_FMT(expr_,  fmt_, ...) \
    do { if(!(expr_)){return;}} while(0);

#define HW_ASSERT_RETVAL_FMT(expr_, val_, fmt_, ...) \
    do { if(!(expr_)){return val_;}} while(0);

#define HW_ASSERT_FILE(expr_, file_, line_, func_)

#define HW_ASSERT_FILE_FMT(expr_, file_, line_, func_, fmt_, ...)

#else


extern ZZ_ATTR_NORETURN void hw_assert(const char* expr,
    const char* file,
    const int line,
    const char* func,
    const char* fmt, ...);

/**
 * @brief assert-statements, used in debug-mode (if NDEBUG is not defined)
 */
#define HW_ASSERT(expr_) \
    do { if(!(expr_)){hw_assert(#expr_, __FILE__, __LINE__, __func__, NULL);} } while(0);

#define HW_ASSERT_RET(expr_) \
    do { if(!(expr_)){hw_assert(#expr_, __FILE__, __LINE__, __func__, NULL);}} while(0);

#define HW_ASSERT_RETVAL(expr_, val_) \
    do { if(!(expr_)){hw_assert(#expr_, __FILE__, __LINE__, __func__, NULL);}} while(0);

#define HW_ASSERT_NOT_NULL(ptr_) \
    do { if((NULL == (ptr_))){hw_assert(#ptr_ " is NULL", __FILE__, __LINE__, __func__, NULL);} } while(0);

#define HW_ASSERT_NOT_NULL_RET(ptr_) \
    do { if((NULL == (ptr_))){hw_assert(#ptr_ " is NULL", __FILE__, __LINE__, __func__, NULL);}} while(0);

#define HW_ASSERT_NOT_NULL_RETVAL(ptr_, val_) \
    do { if((NULL == (ptr_))){hw_assert(#ptr_ " is NULL", __FILE__, __LINE__, __func__, NULL);}} while(0);

#define HW_ASSERT_MSG(expr_, msg_) \
    do { if(!(expr_)){hw_assert(#expr_, __FILE__, __LINE__, __func__, "%s", msg_);} } while(0);

#define HW_ASSERT_FMT(expr_, fmt_, ...) \
    do { if(!(expr_)){hw_assert(#expr_, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__);} } while(0);

#define HW_ASSERT_RET_FMT(expr_,  fmt_, ...) \
    do { if(!(expr_)){hw_assert(#expr_, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__);}} while(0);

#define HW_ASSERT_RETVAL_FMT(expr_, val_, fmt_, ...) \
    do { if(!(expr_)){hw_assert(#expr_, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__);}} while(0);

#define HW_ASSERT_FILE(expr_, file_, line_, func_) \
    do { if(!(expr_)){hw_assert(#expr_, file_, line_, func_, NULL);} } while(0);

#define HW_ASSERT_FILE_FMT(expr_, file_, line_, func_, fmt_, ...) \
    do { if(!(expr_)){hw_assert(#expr_, file_, line_, func_, fmt_, ##__VA_ARGS__);} } while(0);

#endif

/**
 * @brief Calculates (at compile time) the maximum value of integer which can be placed to the specified type.
 */
#define HW_ASSERT_MAX_VAL_OF_TYPE(type_) \
    (((type_)~0 < 0) ? (type_)(1 << (sizeof(type_)*8-1)) : (type_)~0)

/**
 * @brief Tests condition at compile time (can be used outside of functions)
 */
#define HW_ASSERT_COMPILE_TIME(EXP_) \
    HW_ASSERT_COMPILE_TIME_I(EXP_, __LINE__)

#define HW_ASSERT_COMPILE_TIME_I(EXP_, LN_) \
    HW_ASSERT_COMPILE_TIME_II(EXP_, LN_)

#define HW_ASSERT_COMPILE_TIME_II(EXP_, LN_) \
    typedef char _static_assertion_at_line_##LN_[(EXP_) ? 1 : -1]


/**
 * @brief Tests condition at compile time (can be used inside functions)
 */
#define HW_ASSERT_COMPILE_TIME_FUNC(EXP_) \
    HW_ASSERT_COMPILE_TIME_I_FUNC(EXP_, __LINE__)

#define HW_ASSERT_COMPILE_TIME_I_FUNC(EXP_, LN_) \
    HW_ASSERT_COMPILE_TIME_II_FUNC(EXP_, LN_)

#define HW_ASSERT_COMPILE_TIME_II_FUNC(EXP_, LN_) \
    typedef char _static_assertion_at_line_##LN_[(EXP_) ? 1 : -1]; \
    (void)(_static_assertion_at_line_##LN_*)0

/**
 * @brief Checks that size of specified type is matched (at compile time).
 */
#define HW_ASSERT_SIZE(nameOfType_, expSizeOfType_) \
    HW_ASSERT_COMPILE_TIME(sizeof(nameOfType_) == (expSizeOfType_))


#ifdef __cplusplus
}
#endif

#endif /* HW_ASSERT_H_ */
