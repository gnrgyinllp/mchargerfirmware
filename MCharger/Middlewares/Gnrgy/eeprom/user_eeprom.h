#ifndef EEPROM_H_
#define EEPROM_H_

#include <stdint.h>
#include <stdbool.h>

bool eeprom_init(void);
bool eeprom_writebyte(uint16_t usAddress, uint8_t  ucValue);
bool eeprom_writebytes(uint16_t usAddress, uint8_t*  pValueArr, uint8_t  ucLength);
bool eeprom_readbyte(uint16_t usAddress, uint8_t*  pReadValue);
bool eeprom_readbytes(uint16_t usAddress, uint8_t* pArrayValue, uint8_t ucLength);
bool eeprom_remove(void);
#endif