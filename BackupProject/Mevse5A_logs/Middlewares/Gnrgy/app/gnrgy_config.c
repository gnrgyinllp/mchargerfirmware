#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "log/log.h"
#include "sdcard/file.h"
#include "os_layer/os_dir.h"
#include "gnrgy_config.h"

bool configFileStructure(void)
{
  LOG_COMMON_INFO("Create directory structure");
	if (!osDir_createIfNotExist(GNRGY_DIR_TMP))
	{
		return false;
	}
  if (!osDir_createIfNotExist(GNRGY_DIR_TMP_PPP))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_JFFS2))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_JFFS2_ETC))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_JFFS2_FILES))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_JFFS2_LOG))
	{
		return false;
	}  
	if (!osDir_createIfNotExist(GNRGY_DIR_JFFS2_ETC))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_FILES_CS))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_CS_CSLOG))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_CS_INIFILES))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_CS_LOGFILES))
	{
		return false;
	} 
	if (!osDir_createIfNotExist(GNRGY_DIR_FILES_LCC))
	{
		return false;
	} 
  if (!osDir_createIfNotExist(GNRGY_DIR_LCC_CSLOG))
	{
		return false;
	}
	if (!osDir_createIfNotExist(GNRGY_DIR_LCC_INIFILES))
	{
		return false;
	}
	return true;
}
