#if !defined(UART_DBG_H_)
#define UART_DBG_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*uartDbg_cbCmdHandler)(const char* const cmd);

void uartDbg_init(void);
void uartDbg_handleCmds(uartDbg_cbCmdHandler cbCmdHandler);

#ifdef __cplusplus
}
#endif

#endif /* UART_DBG_H_ */
