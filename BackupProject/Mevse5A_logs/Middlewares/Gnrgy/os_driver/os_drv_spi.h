#ifndef OS_DRV_SPI_H
#define OS_DRV_SPI_H

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include "os_layer/os_layer.h"
#include "hal_spi.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef uint16_t osDrvSpi_InterfaceNum_t;

struct osDrvSpi_TAG
{
  osDrvSpi_InterfaceNum_t ifaceNum;
  osLayer_Mutex_t mutex;
};

typedef struct osDrvSpi_TAG osDrvSpi_t;

osDrvSpi_t* osDrvSpi_init(
    const osDrvSpi_InterfaceNum_t ifaceNum
);

void osDrvSpiBase_deinit(
    osDrvSpi_t* const pDrv
);

bool osDrvSpi_masterTransmit(
    osDrvSpi_t* const pDrvSpi,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvSpi_masterReceive(
    osDrvSpi_t* const pDrvSpi,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvSpi_masterTransaction(
    osDrvSpi_t* const pDrvSpi,
    const uint8_t* const txbuf,
    const size_t txbytes,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvSpi_masterTransactionTransmit(
    osDrvSpi_t* const pDrvSpi,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvSpi_masterTransactionReceive(
    osDrvSpi_t* const pDrvSpi,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

/* Private functions **************************************************************************************************/

/**
 * Init base class of osDrvSpi_t
 * @note This is protected function.
 * @param pDrv
 * @param ifaceNum
 * @param rxBufSize
 * @param txBufSize
 * @param baud
 * @return
 */
bool osDrvSpiBase_init(
    osDrvSpi_t* const pDrv,
    const osDrvSpi_InterfaceNum_t ifaceNum
);

/**
 * Deinit base class of osDrvSpi_t
 * @param pDrv
 */
void osDrvSpiBase_deinit(
    osDrvSpi_t* const pDrv
);

#ifdef __cplusplus
}
#endif


#endif //OS_DRV_SPI_H
