#ifndef OS_DRV_I2C_H
#define OS_DRV_I2C_H

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include "os_layer/os_layer.h"
#include "hal_i2c.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef uint16_t osDrvI2c_InterfaceNum_t;

struct osDrvI2c_TAG
{
  osDrvI2c_InterfaceNum_t ifaceNum;
  osLayer_Mutex_t mutex;
};

typedef struct osDrvI2c_TAG osDrvI2c_t;

osDrvI2c_t* osDrvI2c_init(
    const osDrvI2c_InterfaceNum_t ifaceNum
);

void osDrvI2c_deinit(
    osDrvI2c_t* const pDrvUart
);

bool osDrvI2c_masterTransmit(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvI2c_masterReceive(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvI2c_masterTransaction(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvI2c_masterTransactionTransmit(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

bool osDrvI2c_masterTransactionReceive(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t addr,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
);

/* Private functions **************************************************************************************************/

/**
 * Init base class of osDrvI2c_t
 * @note This is protected function.
 * @param pDrv
 * @param ifaceNum
 * @param rxBufSize
 * @param txBufSize
 * @param baud
 * @return
 */
bool osDrvI2cBase_init(
    osDrvI2c_t* const pDrv,
    const osDrvI2c_InterfaceNum_t ifaceNum
);

/**
 * Deinit base class of osDrvI2c_t
 * @param pDrv
 */
void osDrvI2cBase_deinit(
    osDrvI2c_t* const pDrv
);

#ifdef __cplusplus
}
#endif


#endif //OS_DRV_I2C_H
