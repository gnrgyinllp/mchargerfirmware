#include "hal_i2c.h"
#include "log/log.h"


/**
  * @brief Init I2C driver instance
  */
bool halI2c_init(halI2c_Handle_t* const handle)
{
#if !defined(_WIN32)
  const HAL_StatusTypeDef res = HAL_I2C_Init(handle);
  if (HAL_OK != res)
  {
    LOGF_HAL_I2C_ERR("HAL_I2C_Init failed, res=%d", (int)res);
    return false;
  }
#endif
  return true;
}

bool halI2c_reset(halI2c_Handle_t* const handle)
{
#if !defined(_WIN32)
  // workaround for I2C busy flag strange behavior
  // https://electronics.stackexchange.com/questions/267972/i2c-busy-flag-strange-behaviour
  handle->Instance->CR1 |= I2C_CR1_SWRST;
  __NOP();
  handle->Instance->CR1 &= ~I2C_CR1_SWRST;
  {
    const HAL_StatusTypeDef res = HAL_I2C_DeInit(handle);
    if (HAL_OK != res)
    {
      LOGF_HAL_I2C_ERR("HAL_I2C_DeInit failed, res=%d", (int)res);
      return false;
    }
  }
  {
    const HAL_StatusTypeDef res = HAL_I2C_Init(handle);
    if (HAL_OK != res)
    {
      LOGF_HAL_I2C_ERR("HAL_I2C_Init failed, res=%d", (int)res);
      return false;
    }
  }
#endif
  return true;
}

bool halI2c_abortIT(
    halI2c_Handle_t* const handle
)
{
#if !defined(_WIN32)
  const HAL_StatusTypeDef res = HAL_I2C_Master_Abort_IT(handle, 0);
  if (HAL_OK != res)
  {
    LOG_HAL_I2C_ERR("HAL_I2C_Master_Abort_IT failed, res=%d", res);
    return false;
  }
#endif
  return true;
}

bool halI2c_masterTransmitDMA(
    halI2c_Handle_t* const handle,
    const halI2c_Addr_t addr,
    const uint8_t* const txbuf,
    const size_t txbytes
)
{
  LOG_DUMP_NOISE(LOG_FACILITY_HAL_I2C, txbuf, txbytes, "HAL_I2C_Master_Transmit_DMA: addr=0x%02x", addr);
  const HAL_StatusTypeDef res = HAL_I2C_Master_Transmit_DMA(handle, addr, (uint8_t*)txbuf, txbytes);
  if (HAL_OK != res)
  {
    LOG_HAL_I2C_ERR("HAL_I2C_Master_Transmit_DMA failed, res=%d", res);
    if (HAL_TIMEOUT == res)
    {
      // workaround for I2C busy flag strange behavior
      // https://electronics.stackexchange.com/questions/267972/i2c-busy-flag-strange-behaviour
      LOGF_HAL_I2C_WARN("### Deinit/init I2C");
      if (!halI2c_reset(handle))
      {
        LOGF_HAL_I2C_ERR("halI2c_reset");
      }
    }
    return false;
  }
  return true;
}

bool halI2c_masterReceiveDMA(
    halI2c_Handle_t* const handle,
    const halI2c_Addr_t addr,
    uint8_t* const rxbuf,
    const size_t rxbytes
)
{
  LOG_HAL_I2C_NOISE("HAL_I2C_Master_Receive_DMA: addr=0x%02x, rxbytes=%u", addr, rxbytes);
  const HAL_StatusTypeDef res = HAL_I2C_Master_Receive_DMA(handle, addr, rxbuf, rxbytes);
  if (HAL_OK != res)
  {
    LOG_HAL_I2C_ERR("HAL_I2C_Master_Receive_DMA failed, res=%d", res);
    if (HAL_TIMEOUT == res)
    {
      // workaround for I2C busy flag strange behavior
      // https://electronics.stackexchange.com/questions/267972/i2c-busy-flag-strange-behaviour
      LOGF_HAL_I2C_WARN("### Deinit/init I2C");
      if (!halI2c_reset(handle))
      {
        LOGF_HAL_I2C_ERR("halI2c_reset");
      }
    }
    return false;
  }
  return true;
}
