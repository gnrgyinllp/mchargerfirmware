#include "hal_uart.h"
#include "uart.h"
#include "irq/hal_irq.h"
#include "os_layer/os_layer.h"

void halUART_waitTxReady(UART_HandleTypeDef* const huart)
{
  while (huart->gState != HAL_UART_STATE_READY)
  {
    osLayer_sleepTicks(1);
  }
}

/**
 * @brief Send an amount of data in interrupt mode.
 * @param huart: UART handle.
 * @param pData: pointer to data buffer.
 * @param Size: amount of data to be sent.
 * @retval HAL status
 */
HAL_StatusTypeDef halUART_TransmitIT(UART_HandleTypeDef* const huart, const uint8_t* const pData, const uint16_t size)
{
  /* Check that a Tx process is not already ongoing */
  if (huart->gState == HAL_UART_STATE_READY)
  {
    if ((pData == NULL) || (size == 0))
    {
      return HAL_ERROR;
    }

    /* Process Locked */
    const HAL_IRQ_PriorityMask priMask = halIrq_disable();

    huart->pTxBuffPtr = (uint8_t*)pData;
    huart->TxXferSize = size;
    huart->TxXferCount = size;

    huart->ErrorCode = HAL_UART_ERROR_NONE;
    huart->gState = HAL_UART_STATE_BUSY_TX;

    /* Enable the UART Transmit Data Register Empty Interrupt */
    SET_BIT(huart->Instance->CR1, USART_CR1_TXEIE);

    /* Process Unlocked */
    halIrq_restore(priMask);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

/**
 * @brief Receive an amount of data in interrupt mode.
 * @param huart: UART handle.
 * @param pData: pointer to data buffer.
 * @param size: amount of data to be received.
 */
void halUART_ReceiveIT_withoutLock(UART_HandleTypeDef* const huart, uint8_t* const pData, const uint16_t size)
{
  huart->pRxBuffPtr = pData;
  huart->RxXferSize = size;
  huart->RxXferCount = size;

  huart->ErrorCode = HAL_UART_ERROR_NONE;
  huart->RxState = HAL_UART_STATE_BUSY_RX;

  /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
  SET_BIT(huart->Instance->CR3, USART_CR3_EIE);

  /* Enable the UART Parity Error and Data Register not empty Interrupts */
  SET_BIT(huart->Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);
}

/**
 * @brief Receive an amount of data in interrupt mode.
 * @param huart: UART handle.
 * @param pData: pointer to data buffer.
 * @param size: amount of data to be received.
 * @retval HAL status
 */
HAL_StatusTypeDef halUART_ReceiveIT(UART_HandleTypeDef* const huart, uint8_t* const pData, const uint16_t size)
{
  /* Check that a Rx process is not already ongoing */
  if (huart->RxState == HAL_UART_STATE_READY)
  {
    if ((pData == NULL) || (size == 0))
    {
      return HAL_ERROR;
    }

    const HAL_IRQ_PriorityMask priMask = halIrq_disable();

    halUART_ReceiveIT_withoutLock(huart, pData, size);

    halIrq_restore(priMask);

    return HAL_OK;
  }
  else
  {
    return HAL_BUSY;
  }
}

