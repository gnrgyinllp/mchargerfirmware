#include <stdint.h>
#include <stdbool.h>
#include "os_drv_spi.h"
#include "utils/attribs.h"
#include "log/log.h"
#include "os_layer/os_layer.h"
#include "stm32f4xx.h"
#include "main.h"

#if !defined(OS_DRV_SPI_NUM_RETRIES)
#define OS_DRV_SPI_NUM_RETRIES      1
#endif


bool osDrvSpiBase_init(
    osDrvSpi_t* const pDrv,
    const osDrvSpi_InterfaceNum_t ifaceNum
)
{
  pDrv->ifaceNum = ifaceNum;
  if (!osLayer_mutexInit(&pDrv->mutex))
  {
    LOGF_OS_DRV_SPI_ERR("osLayer_mutexInit failed");
    return false;
  }
  return true;
}

void osDrvSpiBase_deinit(
    osDrvSpi_t* const pDrv
)
{
  osLayer_mutexDeinit(&pDrv->mutex);
}

bool osDrvSpi_acquireBus(
    osDrvSpi_t* const pDrv
)
{
  return osLayer_mutexEnter(&pDrv->mutex);
}

bool osDrvSpi_releaseBus(
    osDrvSpi_t* const pDrv
)
{
  return osLayer_mutexExit(&pDrv->mutex);
}

static bool osDrvSpi_masterTransmitReceiveWithRetries(
    osDrvSpi_t* const pDrvSpi,
    const uint8_t* const txbuf,
    const size_t txbytes,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  for (unsigned i = 0; i < OS_DRV_SPI_NUM_RETRIES; ++i)
  {
    HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET );
    if (0 != txbytes)
    {
      if (!osDrvSpi_masterTransmit(pDrvSpi, txbuf, txbytes, timeoutTicks))
      {
        LOGF_OS_DRV_SPI_WARN("%s failed for slave", "osDrvSpi_masterTransmit");
        continue;
      }
    }

    if (0 != rxbytes)
    {
      if (!osDrvSpi_masterReceive(pDrvSpi, rxbuf, rxbytes, timeoutTicks))
      {
        LOGF_OS_DRV_SPI_WARN("%s failed for slave", "osDrvSpi_masterReceive");
        continue;
      }
    }
    HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET );
    return true;
  }
  return false;
}

bool osDrvSpi_masterTransaction(
    osDrvSpi_t* const pDrvSpi,
    const uint8_t* const txbuf,
    const size_t txbytes,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  if (!osDrvSpi_acquireBus(pDrvSpi))
  {
    LOGF_OS_DRV_SPI_ERR("%s failed", "osDrvSpi_acquireBus");
    return false;
  }
  if (!osDrvSpi_masterTransmitReceiveWithRetries(pDrvSpi, txbuf, txbytes, rxbuf, rxbytes, timeoutTicks))
  {
    LOGF_OS_DRV_SPI_ERR("%s failed for slave", "osDrvSpi_masterTransmitReceiveWithRetries");
    osDrvSpi_releaseBus(pDrvSpi);
    return false;
  }
  if (!osDrvSpi_releaseBus(pDrvSpi))
  {
    LOGF_OS_DRV_SPI_ERR("%s failed", "osDrvSpi_releaseBus");
    return false;
  }
  return true;
}

bool osDrvSpi_masterTransactionTransmit(
    osDrvSpi_t* const pDrvSpi,
    const uint8_t* const txbuf,
    const size_t txbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  return osDrvSpi_masterTransaction(pDrvSpi, txbuf, txbytes, NULL, 0, timeoutTicks);
}

bool osDrvSpi_masterTransactionReceive(
    osDrvSpi_t* const pDrvSpi,
    uint8_t* const rxbuf,
    const size_t rxbytes,
    const osLayer_DeltaTicks_t timeoutTicks
)
{
  return osDrvSpi_masterTransaction(pDrvSpi, NULL, 0, rxbuf, rxbytes, timeoutTicks);
}

