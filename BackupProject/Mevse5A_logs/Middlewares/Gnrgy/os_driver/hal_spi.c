#include "hal_spi.h"
#include "log/log.h"

/**
  * @brief Init SPI driver instance
  */
bool halSpi_init(halSpi_Handle_t* const handle)
{
#if !defined(_WIN32)
  const HAL_StatusTypeDef res = HAL_SPI_Init(handle);
  if (HAL_OK != res)
  {
    LOGF_HAL_I2C_ERR("HAL_SPI_Init failed, res=%d", (int)res);
    return false;
  }
#endif
  return true;
}

bool halSpi_reset(halSpi_Handle_t* const handle)
{
#if !defined(_WIN32)
  __NOP();
  {
    const HAL_StatusTypeDef res = HAL_SPI_DeInit(handle);
    if (HAL_OK != res)
    {
      LOGF_HAL_SPI_ERR("HAL_SPI_DeInit failed, res=%d", (int)res);
      return false;
    }
  }
  {
    const HAL_StatusTypeDef res = HAL_SPI_Init(handle);
    if (HAL_OK != res)
    {
      LOGF_HAL_SPI_ERR("HAL_SPI_Init failed, res=%d", (int)res);
      return false;
    }
  }
#endif
  return true;
}

bool halSpi_abortIT(
    halSpi_Handle_t* const handle
)
{
#if !defined(_WIN32)
  const HAL_StatusTypeDef res = HAL_SPI_Abort_IT(handle);
  if (HAL_OK != res)
  {
    LOG_HAL_I2C_ERR("HAL_SPI_Abort_IT failed, res=%d", res);
    return false;
  }
#endif
  return true;
}

bool halSpi_masterTransmitDMA(
    halSpi_Handle_t* const handle,
    const uint8_t* const txbuf,
    const size_t txbytes
)
{
  const HAL_StatusTypeDef res = HAL_SPI_Transmit_DMA(handle, (uint8_t*)txbuf, txbytes);
  if (HAL_OK != res)
  {
    LOG_HAL_SPI_ERR("HAL_SPI_Transmit_DMA failed, res=%d", res);
    if (HAL_TIMEOUT == res)
    {
      LOGF_HAL_SPI_WARN("### Deinit/init SPI");
      if (!halSpi_reset(handle))
      {
        LOGF_HAL_SPI_ERR("halSpi_reset");
      }
    }
    return false;
  }
  return true;
}

bool halSpi_masterReceiveDMA(
    halSpi_Handle_t* const handle,
    uint8_t* const rxbuf,
    const size_t rxbytes
)
{
  const HAL_StatusTypeDef res = HAL_SPI_Receive_DMA(handle, rxbuf, rxbytes);
  if (HAL_OK != res)
  {
    LOG_HAL_SPI_ERR("HAL_SPI_Receive_DMA failed, res=%d", res);
    if (HAL_TIMEOUT == res)
    {
      LOGF_HAL_SPI_WARN("### Deinit/init SPI");
      if (!halSpi_reset(handle))
      {
        LOGF_HAL_SPI_ERR("halSpi_reset");
      }
    }
    return false;
  }
  return true;
}
