#if !defined(HAL_SDCARD_H_)
#define HAL_SDCARD_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

bool halSDcard_init(void);

#ifdef __cplusplus
}
#endif

#endif /* HAL_SDCARD_H_ */
