#if !defined(HAL_GPIO_H_)
#define HAL_GPIO_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "stm32f4xx.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef uint16_t halGPIO_BitMaskPins_t;
typedef uint16_t halGPIO_BitMaskUsedPorts_t;
typedef uint32_t halGPIO_MaskMODER_t;

typedef uint32_t halGPIO_MODE_t;

typedef enum halGPIO_Port_TAG
{
  halGPIO_Port_A = 0,
  halGPIO_Port_B,
  halGPIO_Port_C,
  halGPIO_Port_D,
  halGPIO_Port_E,
  halGPIO_Port_F,
  halGPIO_Port_G,
  halGPIO_Port_H,
  halGPIO_NUM_PORTS,
} halGPIO_Port_e;

typedef struct halGPIO_UsedPins_TAG
{
  GPIO_TypeDef* pGPIOx;
  halGPIO_BitMaskPins_t bmUsedPins;
  halGPIO_MaskMODER_t maskModer;
} halGPIO_UsedPins_t;

typedef struct halGPIO_UsedPorts_TAG
{
  halGPIO_UsedPins_t usedPins[halGPIO_NUM_PORTS];
  halGPIO_BitMaskUsedPorts_t bmUsedPorts;
} halGPIO_UsedPorts_t;


void halGPIO_usedPortsInit(
    halGPIO_UsedPorts_t* const pUsedPorts
);

bool halGPIO_usedPortsAddPins(
    halGPIO_UsedPorts_t* const pUsedPorts,
    GPIO_TypeDef* const pGPIOx,
    const halGPIO_BitMaskPins_t bmPins
);

void halGPIO_modeSwitchToInput(const halGPIO_UsedPorts_t* const pUsedPorts);

void halGPIO_modeRestoreBackToOutput(const halGPIO_UsedPorts_t* const pUsedPorts);

/**
 * @name    STM32-specific I/O mode flags
 * @{
 */
#define halGPIO_STM32_MODE_MASK             (3 << 0)
#define halGPIO_STM32_MODE_INPUT            (0 << 0)
#define halGPIO_STM32_MODE_OUTPUT           (1 << 0)
#define halGPIO_STM32_MODE_ALTERNATE        (2 << 0)
#define halGPIO_STM32_MODE_ANALOG           (3 << 0)

#define halGPIO_STM32_OTYPE_MASK            (1 << 2)
#define halGPIO_STM32_OTYPE_PUSHPULL        (0 << 2)
#define halGPIO_STM32_OTYPE_OPENDRAIN       (1 << 2)

#define halGPIO_STM32_OSPEED_MASK           (3 << 3)
#define halGPIO_STM32_OSPEED_LOWEST         (0 << 3)
#if defined(STM32F0XX) || defined(STM32F30X) || defined(STM32F37X)
#define halGPIO_STM32_OSPEED_MID            (1 << 3)
#else
#define halGPIO_STM32_OSPEED_MID1           (1 << 3)
#define halGPIO_STM32_OSPEED_MID2           (2 << 3)
#endif
#define halGPIO_STM32_OSPEED_HIGHEST        (3 << 3)

#define halGPIO_STM32_PUDR_MASK             (3 << 5)
#define halGPIO_STM32_PUDR_FLOATING         (0 << 5)
#define halGPIO_STM32_PUDR_PULLUP           (1 << 5)
#define halGPIO_STM32_PUDR_PULLDOWN         (2 << 5)

#define halGPIO_STM32_ALTERNATE_MASK        (15 << 7)
#define halGPIO_STM32_ALTERNATE(n)          ((n) << 7)

/**
 * @brief   Alternate function.
 *
 * @param[in] n         alternate function selector
 */
#define halGPIO_MODE_ALTERNATE(n)           (halGPIO_STM32_MODE_ALTERNATE |         \
                                         halGPIO_STM32_ALTERNATE(n))
/** @} */

/**
 * @name    Standard I/O mode flags
 * @{
 */
/**
 * @brief   This mode is implemented as input.
 */
#define halGPIO_MODE_RESET                  halGPIO_STM32_MODE_INPUT

/**
 * @brief   This mode is implemented as input with pull-up.
 */
#define halGPIO_MODE_UNCONNECTED            halGPIO_MODE_INPUT_PULLUP

/**
 * @brief   Regular input high-Z pad.
 */
#define halGPIO_MODE_INPUT                  halGPIO_STM32_MODE_INPUT

/**
 * @brief   Input pad with weak pull up resistor.
 */
#define halGPIO_MODE_INPUT_PULLUP           (halGPIO_STM32_MODE_INPUT |             \
                                         halGPIO_STM32_PUDR_PULLUP)

/**
 * @brief   Input pad with weak pull down resistor.
 */
#define halGPIO_MODE_INPUT_PULLDOWN         (halGPIO_STM32_MODE_INPUT |             \
                                         halGPIO_STM32_PUDR_PULLDOWN)

/**
 * @brief   Analog input mode.
 */
#define halGPIO_MODE_INPUT_ANALOG           halGPIO_STM32_MODE_ANALOG

/**
 * @brief   Push-pull output pad.
 */
#define halGPIO_MODE_OUTPUT_PUSHPULL        (halGPIO_STM32_MODE_OUTPUT |            \
                                         halGPIO_STM32_OTYPE_PUSHPULL)

/**
 * @brief   Open-drain output pad.
 */
#define halGPIO_MODE_OUTPUT_OPENDRAIN       (halGPIO_STM32_MODE_OUTPUT |            \
                                         halGPIO_STM32_OTYPE_OPENDRAIN)



/**
 * @brief   Reads an I/O port.
 * @details This function is implemented by reading the GPIO IDR register, the
 *          implementation has no side effects.
 * @note    This function is not meant to be invoked directly by the application
 *          code.
 *
 * @param[in] port      port identifier
 * @return              The port bits.
 */
static inline halGPIO_BitMaskPins_t halGPIO_readPort(GPIO_TypeDef* const port)
{
  return port->IDR;
}

/**
 * @brief   Reads the output latch.
 * @details This function is implemented by reading the GPIO ODR register, the
 *          implementation has no side effects.
 * @note    This function is not meant to be invoked directly by the application
 *          code.
 *
 * @param[in] port      port identifier
 * @return              The latched logical states.
 */
static inline halGPIO_BitMaskPins_t halGPIO_readLatch(GPIO_TypeDef* const port)
{
  return port->ODR;
}

/**
 * @brief   Writes on a I/O port.
 * @details This function is implemented by writing the GPIO ODR register, the
 *          implementation has no side effects.
 * @note    Writing on pads programmed as pull-up or pull-down has the side
 *          effect to modify the resistor setting because the output latched
 *          data is used for the resistor selection.
 *
 * @param[in] port      port identifier
 * @param[in] bitMask   bits to be written on the specified port
 */
static inline void halGPIO_writePort(GPIO_TypeDef* const port, const halGPIO_BitMaskPins_t bitMask)
{
  port->ODR = bitMask;
}

/**
 * @brief   Sets a bits mask on a I/O port.
 * @details This function is implemented by writing the GPIO BSRR register, the
 *          implementation has no side effects.
 * @note    Writing on pads programmed as pull-up or pull-down has the side
 *          effect to modify the resistor setting because the output latched
 *          data is used for the resistor selection.
 *
 * @param[in] port      port identifier
 * @param[in] bitMask   bits to be ORed on the specified port
 */
static inline void halGPIO_setPort(GPIO_TypeDef* const port, const halGPIO_BitMaskPins_t bitMask)
{
  port->BSRR = (uint32_t)bitMask;
}

/**
 * @brief   Clears a bits mask on a I/O port.
 * @details This function is implemented by writing the GPIO BRR register, the
 *          implementation has no side effects.
 * @note    Writing on pads programmed as pull-up or pull-down has the side
 *          effect to modify the resistor setting because the output latched
 *          data is used for the resistor selection.
 *
 * @param[in] port      port identifier
 * @param[in] bitMask   bits to be cleared on the specified port
 */
static inline void halGPIO_clearPort(GPIO_TypeDef* const port, const halGPIO_BitMaskPins_t bitMask)
{
  port->BSRR = ((uint32_t)bitMask) << 16;
}

/**
 * @brief   Writes a group of bits.
 * @details This function is implemented by writing the GPIO BSRR register, the
 *          implementation has no side effects.
 * @note    Writing on pads programmed as pull-up or pull-down has the side
 *          effect to modify the resistor setting because the output latched
 *          data is used for the resistor selection.
 *
 * @param[in] port      port identifier
 * @param[in] mask      group mask
 * @param[in] bits      bits to be written. Values exceeding the group
 *                      width are masked.
 */
static inline void halGPIO_writeGroup(
    GPIO_TypeDef* const port,
    const halGPIO_BitMaskPins_t mask,
    const halGPIO_BitMaskPins_t bits)
{
  port->BSRR = (((uint32_t)(~(bits) & (mask))) << 16) | (uint32_t)((bits) & (mask));
}

/**
 * @brief   Pads group mode setup.
 * @details This function programs a pads group belonging to the same port
 *          with the specified mode.
 * @note    Writing on pads programmed as pull-up or pull-down has the side
 *          effect to modify the resistor setting because the output latched
 *          data is used for the resistor selection.
 *
 * @param[in] port      port identifier
 * @param[in] mask      group mask
 * @param[in] mode      group mode
 */
extern void halGPIO_setGroupMode(
    GPIO_TypeDef* const port,
    halGPIO_BitMaskPins_t mask,
    const halGPIO_MODE_t mode);


#ifdef __cplusplus
}
#endif

#endif
