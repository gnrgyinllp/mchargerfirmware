#include "hal_sdcard.h"
#include <stdint.h>
#include <stdbool.h>
#include "os_layer/os_layer.h"
#include "log/log.h"
#include "main.h"


bool halSDcard_init(void)
{
  MX_SDIO_SD_Init();
  return true;
}

