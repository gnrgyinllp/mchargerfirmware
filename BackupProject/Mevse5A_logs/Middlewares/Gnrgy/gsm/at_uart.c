#include "at_uart.h"
#include "at_uart_private.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "os_layer/os_layer.h"
#include "utils/t_snprintf.h"
#include "log/log.h"
#include "buffer/cycle_buf.h"
#include "os_driver/hal_uart.h"
#include "app/gnrgy_config.h"

extern UART_HandleTypeDef huart2;

static void atUart_handleEventAT_Req(AT_UART_t* const pAtUart);
static void atUart_handleEventStopReadingResponse(AT_UART_t* const pAtUart);

/**
 * @brief Declaration of ThreadEncoder class.
 */
struct AT_UART_TAG
{
  CycleBuf_t* pCycleBufUART;
  UART_HandleTypeDef* pSerialDrv;
  osLayer_Semaphore_t sema;
  char atCmdBuf[128];
  bool flagSendData;
  const uint8_t* atCmdData;
  uint16_t atCmdDataLen;
  char lineBuf[128];
  unsigned lineBufIdx;
  bool flagLineReady;
  bool flagWaitingLine;
  bool flagReadingResponse;
  char* reqLineBuf;
  size_t reqLineLen;
  uint32_t reqLineTimeout_ms;
  bool reqLineIsTimeout;

  osLayer_SignalNum_e sigNotify;

  atUart_CallbackUnsolicitedResponse cbUnsolicitedResponseHandler;
  void* cbUnsolicitedResponseHandlerParam;
};

/* Globals definitions ************************************************************************************************/

/**
 * @brief Instance of AT_UART_t class
 */
AT_UART_t g_atUartInstance;

/* Static function declarations ***************************************************************************************/

static void atUart_sendReceivedLine(AT_UART_t* const pAtUart);

static void atUart_sendBuf(
    AT_UART_t* const pAtUart,
    const uint8_t* const buf,
    const uint16_t bufLen)
{
  (void)pAtUart;
  (void)buf;
  LOG_ATCMD_INFO("send data: len=%u", (unsigned)bufLen);
  halUART_waitTxReady(pAtUart->pSerialDrv);
  halUART_TransmitIT(pAtUart->pSerialDrv, (uint8_t*)buf, (size_t)bufLen);
  halUART_waitTxReady(pAtUart->pSerialDrv);
}

static size_t atUart_readWithTimeout(
    AT_UART_t* const pAtUart,
    uint8_t* const buf,
    const uint16_t bufLen,
    const uint32_t timeout_ms)
{
  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  uint32_t cnt = 0;
  while(cnt < bufLen)
  {
    const uint32_t delta = osLayer_getSysTick() - t0;
    if (cycleBuf_isEmpty(pAtUart->pCycleBufUART))
    {
      if (delta < timeout_ms)
      {
        ulTaskNotifyTake(pdTRUE, timeout_ms - delta);
      }
    }
    if (!cycleBuf_isEmpty(pAtUart->pCycleBufUART))
    {
      buf[cnt++] = cycleBuf_read(pAtUart->pCycleBufUART);
    }
    if (delta >= timeout_ms)
    {
      break;
    }
  }
  return cnt;
}

static bool atUart_readBuf_(
    AT_UART_t* const pAtUart,
    uint8_t* const buf,
    const uint16_t bufLen,
    const uint32_t timeout_ms)
{
  (void)pAtUart;
  (void)buf;
  LOG_ATCMD_INFO("read data: len=%u", (unsigned)bufLen);
  const size_t retLen = atUart_readWithTimeout(pAtUart, buf, (size_t)bufLen, timeout_ms);
  if (0 != retLen)
  {
    LOG_ATCMD_DEBUG("Recv buf: %.*s",retLen, buf);
  }
  else
  {
    LOG_ATCMD_INFO("Recv buf: timeout");
  }
  if (bufLen != retLen)
  {
    LOG_ATCMD_ERR("Recv buf: expBufLen=%u, actBufLen=%u", (unsigned)bufLen, (unsigned)retLen);
    return false;
  }
  return true;
}

/* Public functions ***************************************************************************************************/

/**
 * @brief Create instance of UartGsm class.
 * @return true if success.
 */
bool atUart_createInstance(CycleBuf_t* const pCycleBuf)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;

  pAtUart->pCycleBufUART = NULL;
  pAtUart->pSerialDrv = NULL;
  pAtUart->pCycleBufUART = pCycleBuf;
  pAtUart->pSerialDrv = SERIAL_DEVICE(TARGET_UART_GSM);

  if (!atUart_init(pAtUart))
  {
    return false;
  }

  return true;
}

/**
 * @brief Destroy instance of UartGsm class.
 * @return true if success.
 */
bool atUart_destroyInstance(void)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  atUart_deinit(pAtUart);
  return true;
}

/**
 * @brief Subscribe to get unsolicited responses
 * @param cbUnsolicitedResponseHandler      - callback handler
 * @param param                             - ptr to param for callback
 */
void atUart_subscribeToUnsolicitedResponses(
    const atUart_CallbackUnsolicitedResponse cbUnsolicitedResponseHandler,
    void* const param)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  pAtUart->cbUnsolicitedResponseHandler = cbUnsolicitedResponseHandler;
  pAtUart->cbUnsolicitedResponseHandlerParam = param;
}

/**
 * @brief Send AT-command request
 * @param atCmd     - ptr to string with AT-command
 * @return true if command was successfully sent
 */
bool atUart_sendCmdAT(const char* const atCmd)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  return atUart_putReqSendCmdAT(pAtUart, atCmd);
}


/**
 * @brief Send AT-command request with data buffer
 * @param atCmd     - ptr to string with AT-command
 * @param buf       - ptr to buffer with data
 * @param bufLen    - length of the buf
 * @return true if command was successfully sent
 */
bool atUart_sendCmdWithData(
    const char* const atCmd,
    const uint8_t* const buf,
    const uint16_t bufLen)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  return atUart_putReqSendCmdWithData(pAtUart, atCmd, buf, bufLen);
}

/**
 * @brief Write buffer
 * @param buf       - ptr to buffer
 * @param bufLen    - length of buffer
 * @return true if command was successfully sent
 */
bool atUart_writeBuf(const uint8_t* const buf, const uint16_t bufLen)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  atUart_sendBuf(pAtUart, buf, bufLen);
  return true;
}

/**
 * @brief Read buffer
 * @param buf           - ptr to buffer
 * @param bufLen        - length of buffer
 * @param timeout_ms    -   timeout ms
 * @return true if command was successfully sent
 */
bool atUart_readBuf(uint8_t* const buf, const uint16_t bufLen, uint32_t timeout_ms)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  return atUart_readBuf_(pAtUart, buf, bufLen, timeout_ms);
}

/**
 * @brief Write string
 * @param pStr     - ptr to string
 * @return true if command was successfully sent
 */
bool atUart_writeString(const char* const pStr)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  LOG_ATCMD_INFO("send string:\r\n%s", pStr);
  atUart_sendBuf(pAtUart, (const uint8_t*)pStr, (uint16_t)strlen(pStr));
  return true;
}

/**
 * @brief Wait line with response to AT-command
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @return true if success
 */
bool atUart_waitLineWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  if (0 == bufLen)
  {
    return false;
  }
  buf[0] = '\0';
  pAtUart->reqLineBuf = buf;
  pAtUart->reqLineLen = bufLen;
  pAtUart->reqLineTimeout_ms = timeout_ms;

  pAtUart->flagReadingResponse = true;

  *pIsTimeout = false;
  osLayer_Ticks_t t0 = osLayer_getSysTick();
  pAtUart->flagWaitingLine = true;
  for (;;)
  {
    bool flagReady = false;
    if (atUart_doActions(&flagReady))
    {
      if (flagReady)
      {
        *pIsTimeout = false;
        break;
      }
      continue;
    }
    osLayer_Ticks_t elapsedTime = osLayer_getSysTick() - t0;
    if (elapsedTime > timeout_ms)
    {
      *pIsTimeout = true;
      break;
    }
  }
  pAtUart->flagWaitingLine = false;
  return true;
}

/**
 * @brief Get line with response to AT-command "+CWLAP:"
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @return true if success
 */
bool atUart_getLineWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  if (0 == bufLen)
  {
    return false;
  }
  buf[0] = '\0';
  pAtUart->reqLineBuf = buf;
  pAtUart->reqLineLen = bufLen;
  pAtUart->reqLineTimeout_ms = timeout_ms;

  pAtUart->flagReadingResponse = true;

  *pIsTimeout = false;
  osLayer_Ticks_t t0 = osLayer_getSysTick();
  pAtUart->flagWaitingLine = true;
  for (;;)
  {
    bool flagReady = false;
    if (atUart_doActions(&flagReady))
    {
      if (flagReady)
      {
        *pIsTimeout = false;
        break;
      }
      continue;
    }
    osLayer_Ticks_t elapsedTime = osLayer_getSysTick() - t0;
    if (elapsedTime > timeout_ms)
    {
      *pIsTimeout = true;
      break;
    }
  }
  pAtUart->flagWaitingLine = false;
  return true;
}

/**
 * @brief Wait line with response to AT-command or '>'
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @return true if success
 */
bool atUart_waitAngleBracketWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  if (0 == bufLen)
  {
    return false;
  }
  buf[0] = '\0';
  pAtUart->reqLineTimeout_ms = timeout_ms;

  *pIsTimeout = false;
  osLayer_Ticks_t t0 = osLayer_getSysTick();
  pAtUart->flagWaitingLine = true;
  for (;;)
  {
    bool flagReady = false;
    if (atUart_doActions(&flagReady))
    {
      if (flagReady)
      {
        *pIsTimeout = false;
        break;
      }
      continue;
    }
    if (1 == pAtUart->lineBufIdx)
    {
      if ('>' == pAtUart->lineBuf[0])
      {
        pAtUart->lineBufIdx = 0;
        pAtUart->lineBuf[1] = '\0';
        *pIsTimeout = false;
        atUart_sendReceivedLine(pAtUart);
        pAtUart->sigNotify = osLayer_SignalNum_None;
        break;
      }
    }
    osLayer_Ticks_t elapsedTime = osLayer_getSysTick() - t0;
    if (elapsedTime > timeout_ms)
    {
      *pIsTimeout = true;
      break;
    }
  }

  pAtUart->flagWaitingLine = false;
  return true;
}

/**
 * @brief Wait '+IPD,<len>:'
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @param pLenIPD       - ptr to variable where length of folowing IPD is stored (if zero, IPD is not received yet)
 * @return true if success
 */
bool atUart_waitIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD)
{
  *pLenIPD = 0;

  AT_UART_t* const pAtUart = &g_atUartInstance;
  if (0 == bufLen)
  {
    return false;
  }
  buf[0] = '\0';
  pAtUart->reqLineBuf = buf;
  pAtUart->reqLineLen = bufLen;
  pAtUart->reqLineTimeout_ms = timeout_ms;

  *pIsTimeout = false;
  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  osLayer_Ticks_t t1 = osLayer_getSysTick();
  pAtUart->flagWaitingLine = true;
  int state = 0;
  unsigned ipdLenStartIdx = 0;
  for (;;)
  {
    bool flagLineReady = false;
    const bool res  = atUart_doActions(&flagLineReady);
    if (!res)
    {
      const osLayer_Ticks_t elapsedTime = osLayer_getSysTick() - t0;
      if (elapsedTime > timeout_ms)
      {
        if (0 == state)
        {
          *pIsTimeout = true;
          break;
        }
        else
        {
          // if "+IPD" already started we should wait until end of packet
          const osLayer_Ticks_t elapsedTimeSinceLastChar = osLayer_getSysTick() - t1;
          if (elapsedTimeSinceLastChar > 2000)
          {
            // "+IPD" started but seems that some bytes were dropped and inter-char timeout occurred
            *pIsTimeout = true;
            break;
          }
        }
      }
    }
    else
    {
      if (flagLineReady)
      {
        // drop line
        flagLineReady = false;
        continue;
      }
      {
        if (pAtUart->lineBufIdx > 0)
        {
          const int ch = (int)pAtUart->lineBuf[pAtUart->lineBufIdx-1];
          LOG_ATCMD_DEBUG("waitIPD: Got ch '%c' 0x%02x (state=%d), lineBufIdx=%u",
                          (char)(((ch > 32) && (ch < 128)) ? ch : '.'),
                          (unsigned)ch,
                          (int)state,
                          (unsigned)pAtUart->lineBufIdx);
        }
        else
        {
          LOG_ATCMD_DEBUG("waitIPD: EOL");
        }
      }
      t1 = osLayer_getSysTick();
      switch (state)
      {
        case 0:
          if ((1 == pAtUart->lineBufIdx) && ('+' == pAtUart->lineBuf[0]))
          {
            state += 1;
          }
          break;
        case 1:
          if ((2 == pAtUart->lineBufIdx) && ('I' == pAtUart->lineBuf[1]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 2:
          if ((3 == pAtUart->lineBufIdx) && ('P' == pAtUart->lineBuf[2]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 3:
          if ((4 == pAtUart->lineBufIdx) && ('D' == pAtUart->lineBuf[3]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 4:
          if ((5 == pAtUart->lineBufIdx) && (',' == pAtUart->lineBuf[4]))
          {
            ipdLenStartIdx = pAtUart->lineBufIdx;
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 5:
        {
          const char ch = pAtUart->lineBuf[pAtUart->lineBufIdx - 1];
          if (':' == ch)
          {
            state += 1;
          }
          else
          {
            if ((ch < '0') || (ch > '9'))
            {
              state = 0;
            }
          }
          break;
        }
        default:
          state = 0;
          break;
      }
      if (6 == state)
      {
        char* end = &pAtUart->lineBuf[pAtUart->lineBufIdx - 1];
        *pLenIPD = (uint16_t)strtoul(&pAtUart->lineBuf[ipdLenStartIdx], &end, 10);
        pAtUart->lineBufIdx = 0;
        pAtUart->lineBuf[pAtUart->lineBufIdx] = '\0';
        *pIsTimeout = false;
        atUart_sendReceivedLine(pAtUart);
        pAtUart->sigNotify = osLayer_SignalNum_None;
        break;
      }
    }
  }
  pAtUart->flagWaitingLine = false;
  return true;
}

/**
 * @brief Wait '+SDIPD,<len>:'
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @param pLenIPD       - ptr to variable where length of folowing IPD is stored (if zero, IPD is not received yet)
 * @return true if success
 */
bool atUart_waitSDIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD)
{
  *pLenIPD = 0;

  AT_UART_t* const pAtUart = &g_atUartInstance;
  if (0 == bufLen)
  {
    return false;
  }
  buf[0] = '\0';
  pAtUart->reqLineBuf = buf;
  pAtUart->reqLineLen = bufLen;
  pAtUart->reqLineTimeout_ms = timeout_ms;

  *pIsTimeout = false;
  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  osLayer_Ticks_t t1 = osLayer_getSysTick();
  pAtUart->flagWaitingLine = true;
  int state = 0;
  unsigned ipdLenStartIdx = 0;
  for (;;)
  {
    bool flagLineReady = false;
    const bool res  = atUart_doActions(&flagLineReady);
    if (!res)
    {
      const osLayer_Ticks_t elapsedTime = osLayer_getSysTick() - t0;
      if (elapsedTime > timeout_ms)
      {
        if (0 == state)
        {
          *pIsTimeout = true;
          break;
        }
        else
        {
          // if "+SDIPD" already started we should wait until end of packet
          const osLayer_Ticks_t elapsedTimeSinceLastChar = osLayer_getSysTick() - t1;
          if (elapsedTimeSinceLastChar > 1000)
          {
            // "+SDIPD" started but seems that some bytes were dropped and inter-char timeout occurred
            *pIsTimeout = true;
            break;
          }
        }
      }
    }
    else
    {
      if (flagLineReady)
      {
        // drop line
        flagLineReady = false;
        continue;
      }
      {
        if (pAtUart->lineBufIdx > 0)
        {
          const int ch = (int)pAtUart->lineBuf[pAtUart->lineBufIdx-1];
          LOG_ATCMD_DEBUG("waitSDIPD: Got ch '%c' 0x%02x (state=%d), lineBufIdx=%u",
                          (char)(((ch > 32) && (ch < 128)) ? ch : '.'),
                          (unsigned)ch,
                          (int)state,
                          (unsigned)pAtUart->lineBufIdx);
        }
        else
        {
          LOG_ATCMD_DEBUG("waitSDIPD: EOL");
        }
      }
      t1 = osLayer_getSysTick();
      switch (state)
      {
        case 0:
          if ((1 == pAtUart->lineBufIdx) && ('+' == pAtUart->lineBuf[pAtUart->lineBufIdx - 1]))
          {
            state += 1;
          }
          break;
        case 1:
          if ((2 == pAtUart->lineBufIdx) && ('S' == pAtUart->lineBuf[pAtUart->lineBufIdx - 1]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 2:
          if ((3 == pAtUart->lineBufIdx) && ('D' == pAtUart->lineBuf[pAtUart->lineBufIdx - 1]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 3:
          if ((4 == pAtUart->lineBufIdx) && ('I' == pAtUart->lineBuf[pAtUart->lineBufIdx - 1]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 4:
          if ((5 == pAtUart->lineBufIdx) && ('P' == pAtUart->lineBuf[pAtUart->lineBufIdx - 1]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 5:
          if ((6 == pAtUart->lineBufIdx) && ('D' == pAtUart->lineBuf[pAtUart->lineBufIdx - 1]))
          {
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 6:
          if ((7 == pAtUart->lineBufIdx) && (',' == pAtUart->lineBuf[pAtUart->lineBufIdx - 1]))
          {
            ipdLenStartIdx = pAtUart->lineBufIdx;
            state += 1;
          }
          else
          {
            state = 0;
          }
          break;
        case 7:
        {
          const char ch = pAtUart->lineBuf[pAtUart->lineBufIdx - 1];
          if (':' == ch)
          {
            state += 1;
          }
          else
          {
            if ((ch < '0') || (ch > '9'))
            {
              state = 0;
            }
          }
          break;
        }
        default:
          state = 0;
          break;
      }
      if (8 == state)
      {
        char* end = &pAtUart->lineBuf[pAtUart->lineBufIdx - 1];
        *pLenIPD = (uint16_t)strtoul(&pAtUart->lineBuf[ipdLenStartIdx], &end, 10);
        pAtUart->lineBufIdx = 0;
        pAtUart->lineBuf[pAtUart->lineBufIdx] = '\0';
        *pIsTimeout = false;
        atUart_sendReceivedLine(pAtUart);
        pAtUart->sigNotify = osLayer_SignalNum_None;
        break;
      }
    }
  }
  pAtUart->flagWaitingLine = false;
  return true;
}

bool atUart_stopReadingResponse(void)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;
  return atUart_putReqStopReadingResponse(pAtUart);
}

/* Protected functions ************************************************************************************************/

/**
 * @brief Init instance of UartGsm class.
 * @param pAtUart                  - ptr to the AT_UART_t instance.
 * @return true if success.
 */
bool atUart_init(AT_UART_t* const pAtUart)
{
  pAtUart->sigNotify = osLayer_SignalNum_None;
  pAtUart->lineBufIdx = 0;
  pAtUart->flagLineReady = false;
  pAtUart->flagWaitingLine = false;
  pAtUart->flagReadingResponse = false;

  osLayer_semaInit(&pAtUart->sema);
  osLayer_semaSignal(&pAtUart->sema);

  return true;
}

/**
 * @brief Deinit instance of UartGsm class.
 * @param pAtUart                  - ptr to the AT_UART_t instance.
 * @return true if success.
 */
void atUart_deinit(AT_UART_t* const pAtUart)
{
  osLayer_semaDeinit(&pAtUart->sema);
}

static bool atUart_sendReqWaitNotification(
    AT_UART_t* const pAtUart)
{
  atUart_handleEventAT_Req(pAtUart);

  return true;
}

bool atUart_putReqSendCmdAT(
    AT_UART_t* const pAtUart,
    const char* const atCmd)
{
  const size_t len = strlen(atCmd);
  if (0 == len)
  {
    LOG_ATCMD_ERR("AT cmd has zero len");
    return false;
  }
  if (len + 1 >= sizeof(pAtUart->atCmdBuf))
  {
    LOG_ATCMD_ERR("AT cmd len=%u is greater than lim=%u", (len + 1), sizeof(pAtUart->atCmdBuf));
    return false;
  }

  osLayer_semaWaitInfinite(&pAtUart->sema);

  strcpy(pAtUart->atCmdBuf, atCmd);
  pAtUart->flagSendData = false;
  pAtUart->atCmdData = NULL;
  pAtUart->atCmdDataLen = 0;

  return atUart_sendReqWaitNotification(pAtUart);
}

bool atUart_putReqSendCmdWithData(
    AT_UART_t* const pAtUart,
    const char* const atCmd,
    const uint8_t* const buf,
    const uint16_t bufLen)
{
  const size_t len = strlen(atCmd);
  if (0 == len)
  {
    return false;
  }
  if (len + 1 >= sizeof(pAtUart->atCmdBuf))
  {
    return false;
  }

  osLayer_semaWaitInfinite(&pAtUart->sema);

  strcpy(pAtUart->atCmdBuf, atCmd);
  pAtUart->flagSendData = true;
  pAtUart->atCmdData = buf;
  pAtUart->atCmdDataLen = bufLen;

  return atUart_sendReqWaitNotification(pAtUart);
}

bool atUart_putReqStopReadingResponse(
    AT_UART_t* const pAtUart)
{
   atUart_handleEventStopReadingResponse(pAtUart);

  osLayer_semaSignal(&pAtUart->sema);

  return true;
}

static void atUart_sendLine(
    AT_UART_t* const pAtUart,
    const char* const cmd)
{
  (void)pAtUart;

  LOG_ATCMD_INFO("> %s", cmd);

  static const char crlf[] = "\r\n";
  const size_t len = strlen(cmd);

  osLayer_sleepTicks(20);
  halUART_waitTxReady(pAtUart->pSerialDrv);
  halUART_TransmitIT(pAtUart->pSerialDrv, (uint8_t*)cmd, (size_t)len);
  halUART_waitTxReady(pAtUart->pSerialDrv);
  halUART_TransmitIT(pAtUart->pSerialDrv, (uint8_t*)crlf, 2);
  halUART_waitTxReady(pAtUart->pSerialDrv);
}

/* Static functions ***************************************************************************************************/

static void atUart_sendReceivedLine(AT_UART_t* const pAtUart)
{
  strncpy(pAtUart->reqLineBuf, pAtUart->lineBuf, pAtUart->reqLineLen);
  pAtUart->reqLineBuf[pAtUart->reqLineLen - 1] = '\0';
  pAtUart->flagLineReady = false;
  pAtUart->flagWaitingLine = false;
  pAtUart->reqLineIsTimeout = false;
  pAtUart->sigNotify = AT_UART_SIGNAL__NOTIFY_READY;
}

static void atUart_handleEventCharReady(AT_UART_t* const pAtUart, const uint8_t ch)
{
  LOG_ATCMD_DBG("got char 0x%02x '%c' at idx=%u", ch, (ch >= 32) && (ch < 128) ? ch : '?', pAtUart->lineBufIdx);
  switch (ch)
  {
    case '\r':
      pAtUart->lineBuf[pAtUart->lineBufIdx] = '\0';
      break;
    case '\n':
      pAtUart->lineBuf[pAtUart->lineBufIdx] = '\0';
      pAtUart->lineBufIdx = 0;
      LOG_COMMON_INFO("[AT_UART]: %s", pAtUart->lineBuf);
      if (pAtUart->flagReadingResponse)
      {
        pAtUart->flagLineReady = true;
        if (pAtUart->flagWaitingLine)
        {
          atUart_sendReceivedLine(pAtUart);
        }
      }
      else
      {
        if ('+' == pAtUart->lineBuf[0])
        {
          if (NULL != pAtUart->cbUnsolicitedResponseHandler)
          {
            pAtUart->cbUnsolicitedResponseHandler(
                pAtUart->cbUnsolicitedResponseHandlerParam,
                pAtUart->lineBuf);
          }
        }
      }

      break;
    default:
      if (pAtUart->lineBufIdx < (sizeof(pAtUart->lineBuf) - 1))
      {
        pAtUart->lineBuf[pAtUart->lineBufIdx++] = ch;
      }
      else
      {
        pAtUart->lineBuf[pAtUart->lineBufIdx] = '\0';
        pAtUart->lineBufIdx = 0;
        if (pAtUart->flagReadingResponse)
        {
          pAtUart->flagLineReady = true;
          if (pAtUart->flagWaitingLine)
          {
            atUart_sendReceivedLine(pAtUart);
          }
        }
      }
      break;
  }
}

static void atUart_handleEventAT_Req(AT_UART_t* const pAtUart)
{
  pAtUart->flagReadingResponse = true;
  atUart_sendLine(pAtUart, pAtUart->atCmdBuf);
  if (pAtUart->flagSendData)
  {
    if ((NULL != pAtUart->atCmdData) && (0 != pAtUart->atCmdDataLen))
    {
      osLayer_sleepTicks(20);
      atUart_sendBuf(pAtUart, pAtUart->atCmdData, pAtUart->atCmdDataLen);
    }
  }
}

static void atUart_handleEventStopReadingResponse(AT_UART_t* const pAtUart)
{
  pAtUart->flagReadingResponse = false;
  pAtUart->flagLineReady = false;
  pAtUart->flagWaitingLine = false;
  if (0 != pAtUart->lineBufIdx)
  {
    pAtUart->lineBuf[pAtUart->lineBufIdx] = '\0';
    LOG_ATCMD_WARN("Stop: part of line ready: %s", pAtUart->lineBuf);
  }

}

bool atUart_doActions(bool* const pFlagReady)
{
  AT_UART_t* const pAtUart = &g_atUartInstance;

  *pFlagReady = false;

  size_t numBytes = 0;
  uint8_t ch = 0;
  const osLayer_DeltaMilliseconds_t timeout_ms = 100;
  if (cycleBuf_isEmpty(pAtUart->pCycleBufUART))
  {
    ulTaskNotifyTake(pdTRUE, timeout_ms);
  }
  if (!cycleBuf_isEmpty(pAtUart->pCycleBufUART))
  {
    ch = cycleBuf_read(pAtUart->pCycleBufUART);
    numBytes = 1;
  }
  if (0 != numBytes)
  {
    atUart_handleEventCharReady(pAtUart, ch);
    if (AT_UART_SIGNAL__NOTIFY_READY == pAtUart->sigNotify)
    {
      pAtUart->sigNotify = osLayer_SignalNum_None;
      *pFlagReady = true;
    }
    return true;
  }
  else
  {
    return false;
  }
}
