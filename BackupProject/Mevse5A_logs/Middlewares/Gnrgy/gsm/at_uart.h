#ifndef AT_UART_H
#define AT_UART_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "buffer/cycle_buf.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*atUart_CallbackUnsolicitedResponse)(void* const param, const char* const responseText);


/**
 * @brief Create instance of UartGsm class.
 * @return true if success.
 */
bool atUart_createInstance(CycleBuf_t* const pCycleBuf);

/**
 * @brief Destroy instance of UartGsm class.
 * @return true if success.
 */
bool atUart_destroyInstance(void);

/**
 * @brief Wait while thread task started and initialized.
 * @return none
 */
void atUart_waitInstanceThreadReady(void);

/**
 * @brief UART IRQ handler
 * @return none
 */
void atUart_irqHandler(void);

/**
 * @brief Subscribe to get unsolicited responses
 * @param cbUnsolicitedResponseHandler      - callback handler
 * @param param                             - ptr to param for callback
 */
void atUart_subscribeToUnsolicitedResponses(
    const atUart_CallbackUnsolicitedResponse cbUnsolicitedResponseHandler,
    void* const param);

/**
 * @brief Send AT-command request
 * @param atCmd     - ptr to string with AT-command
 * @return true if command was successfully sent
 */
bool atUart_sendCmdAT(const char* const atCmd);

/**
 * @brief Send AT-command request with data buffer
 * @param atCmd     - ptr to string with AT-command
 * @param buf       - ptr to buffer with data
 * @param bufLen    - length of the buf
 * @return true if command was successfully sent
 */
bool atUart_sendCmdWithData(
    const char* const atCmd,
    const uint8_t* const buf,
    const uint16_t bufLen);

/**
 * @brief Write buffer
 * @param buf       - ptr to buffer
 * @param bufLen    - length of buffer
 * @return true if command was successfully sent
 */
bool atUart_writeBuf(const uint8_t* const buf, const uint16_t bufLen);

/**
 * @brief Write string
 * @param pStr     - ptr to string
 * @return true if command was successfully sent
 */
bool atUart_writeString(const char* const pStr);

/**
 * @brief Wait line with response to AT-command
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @return true if success
 */
bool atUart_waitLineWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout);

/**
 * @brief Get line with response to AT-command
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @return true if success
 */
bool atUart_getLineWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout);

/**
 * @brief Read buffer
 * @param buf           - ptr to buffer
 * @param bufLen        - length of buffer
 * @param timeout_ms    -   timeout ms
 * @return true if command was successfully sent
 */
bool atUart_readBuf(uint8_t* const buf, const uint16_t bufLen, uint32_t timeout_ms);

/**
 * @brief Wait '>' in begining of line
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @return true if success
 */
bool atUart_waitAngleBracketWithTimeout(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout);

/**
 * @brief Wait '+IPD,<len>:'
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @param pLenIPD       - ptr to variable where length of folowing IPD is stored (if zero, IPD is not received yet)
 * @return true if success
 */
bool atUart_waitIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD);

/**
 * @brief Wait '+SDIPD,<len>:'
 * @param buf           - ptr to buf to store one line
 * @param bufLen        - size of buf
 * @param pIsTimeout    - ptr to variable where true will be stored if timeout occured
 * @param pLenIPD       - ptr to variable where length of folowing IPD is stored (if zero, IPD is not received yet)
 * @return true if success
 */
bool atUart_waitSDIPD(
    char* const buf,
    const size_t bufLen,
    const uint32_t timeout_ms,
    bool* const pIsTimeout,
    uint16_t* const pLenIPD);

/**
 * @brief Stop reading response of AT-command
 * @return true if success
 */
bool atUart_stopReadingResponse(void);

bool atUart_doActions(bool* const pFlagReady);

#ifdef __cplusplus
}
#endif

#endif //AT_UART_H
