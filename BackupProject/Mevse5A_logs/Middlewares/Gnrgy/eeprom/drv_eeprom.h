#ifndef DRV_EEPROM_H_
#define DRV_EEPROM_H_

#include <stdint.h>
#include <stdbool.h>
#include "os_driver/os_drv_i2c.h"

struct DrvEEPROM_TAG
{
  osDrvI2c_t* pDrvI2c;
  bool flagReady;
  halI2c_Addr_t slaveAddr;
  osLayer_DeltaTicks_t timeoutTicks;
};

typedef struct DrvEEPROM_TAG DrvEEPROM_t;

/**
 * @brief Create new DrvEEPROM_t object for EEPROM with specified address on the I2C bus.
 * @param pDrvI2c       - ptr to I2C-bus driver - @ref osDrvI2c_t
 * @param slaveAddr     - address of sensor on the I2C bus
 * @return ptr to created object @ref DrvEEPROM_t
 */
DrvEEPROM_t* drvEEPROM_create(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t slaveAddr
);

/**
 * @brief Initial check and configure EEPROM sensor.
 * @param pObj          - ptr to @ref DrvEEPROM_t
 * @return true if successful
 */
bool drvEEPROM_init(
    DrvEEPROM_t* const pObj
);

/**
 * @brief Check if sensor was successfully initialized.
 * @param pObj          - ptr to @ref DrvEEPROM_t
 * @return true if successful
 */
bool drvEEPROM_isReady(
    DrvEEPROM_t* const pObj
);

/**
 * @brief Delete DrvEEPROM_t object.
 * @param pObj          - ptr to @ref DrvEEPROM_t
 */
void drvEEPROM_delete(DrvEEPROM_t* const pObj);

/**
 * @brief Write One Byte.
 * @param pObj                       - ptr to @ref DrvEEPROM_t
 * @param usAddress  - Address will be written
 * @param ucValue    - Value will be written to address
 * @return true if successful
 */
bool drvEEPROM_writeOneByte(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t  ucValue
);

/**
 * @brief Write Array Bytes.
 * @param pObj                       - ptr to @ref DrvEEPROM_t
 * @param usAddress    - Start Address will be written
 * @param pValueArr    - ptr to array value will be written to address
 * @param ucLength     - Length of array value
 * @return true if successful
 */
bool drvEEPROM_writeArrayBytes(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t* pValueArr,
    uint8_t  ucLength
);

/**
 * @brief Read One Byte.
 * @param pObj          - ptr to @ref DrvEEPROM_t
 * @param usAddress     - Address will be read
 * @param pReadValue    - ptr to store value read
 * @return true if successful
 */
bool drvEEPROM_readOneByte(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t*  pReadValue
);

/**
 * @brief Read Array Bytes.
 * @param pObj         - ptr to @ref DrvEEPROM_t
 * @param usAddress    - Start Address will be read
 * @param pValueArr    - ptr to array value will be stored read data.
 * @param usLength     - Length of array value
 * @return true if successful
 */
bool drvEEPROM_readArrayBytes(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t*  pValueArr,
    uint16_t usLength
);

/**
  * @brief Read Current Address One Byte.
  * @param pObj          - ptr to @ref DrvEEPROM_t
  * @param pReadValue    - ptr to store value read
  * @return true if successful
  */
bool drvEEPROM_readCurrentOneByte(
    DrvEEPROM_t* const pObj,
    uint8_t*  pReadValue
);
    
/**
  * @brief Read Sequential Array Bytes.
  * @param pObj          - ptr to @ref DrvEEPROM_t
  * @param pReadValue    - ptr to store value read
  * @param ucLengthRead  - Length data to be read.
  * @return true if successful
  */
bool drvEEPROM_readSequential(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t*  pReadValue,
    uint8_t ucLengthRead
);
#endif
