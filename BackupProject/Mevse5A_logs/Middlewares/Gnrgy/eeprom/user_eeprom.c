#include "log/log.h"
#include "user_eeprom.h"
#include "drv_eeprom.h"
#include "app/gnrgy_config.h"

static DrvEEPROM_t* g_pDrvEEPROM;
extern osDrvI2c_t* g_pDrvI2c;

bool eeprom_init(void)
{
  if (NULL != g_pDrvEEPROM)
  {
    LOG_COMMON_ERR("EEPROM was already initialized");
    return false;
  }
  g_pDrvEEPROM = drvEEPROM_create(g_pDrvI2c, EEPROM_ADDRESS);
  if (NULL == g_pDrvEEPROM)
  {
    LOG_COMMON_ERR("drvEEPROM_create failed");
    return false;
  }
  return true;
}

bool eeprom_writebyte(uint16_t usAddress, uint8_t  ucValue)
{
  return drvEEPROM_writeOneByte(g_pDrvEEPROM, usAddress, ucValue);
}

bool eeprom_writebytes(uint16_t usAddress, uint8_t*  pValueArr, uint8_t  ucLength)
{
  return drvEEPROM_writeArrayBytes(g_pDrvEEPROM, usAddress, pValueArr, ucLength);
}

bool eeprom_readbyte(uint16_t usAddress, uint8_t*  pReadValue)
{
  return drvEEPROM_readOneByte(g_pDrvEEPROM, usAddress, pReadValue);
}

bool eeprom_readbytes(uint16_t usAddress, uint8_t* pArrayValue, uint8_t ucLength)
{
  return drvEEPROM_readSequential(g_pDrvEEPROM, usAddress, pArrayValue, ucLength);
}

bool eeprom_remove(void)
{
  if (NULL == g_pDrvEEPROM)
  {
    LOG_COMMON_ERR("EEPROM didn't initialize");
    return false;
  }
  drvEEPROM_delete(g_pDrvEEPROM);
  return true;
}
