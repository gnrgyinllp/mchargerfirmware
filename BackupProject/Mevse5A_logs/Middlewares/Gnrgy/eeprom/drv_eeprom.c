#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "os_driver/os_drv_i2c.h"
#include "log/log.h"
#include "app/gnrgy_config.h"
#include "drv_eeprom.h"

#define EEPROM_TIMEOUT_MS               (100)
/**
* @brief Create new DrvEEPROM_t object for EEPROM with specified address on the I2C bus.
* @param pDrvI2c       - ptr to I2C-bus driver - @ref osDrvI2c_t
* @param slaveAddr     - address of sensor on the I2C bus
* @return ptr to created object @ref DrvEEPROM_t
*/
DrvEEPROM_t* drvEEPROM_create(
  osDrvI2c_t* const pDrvI2c,
  const halI2c_Addr_t slaveAddr
)
{
  DrvEEPROM_t* const pObj = osLayer_calloc(1, sizeof(*pObj));
  if (NULL == pObj)
  {
    LOG_EEPROM_ERR("Can't allocate mem");
    return NULL;
  }
  pObj->pDrvI2c = pDrvI2c;
  pObj->flagReady = false;
  pObj->slaveAddr = slaveAddr;
  pObj->timeoutTicks = osLayer_convMillisecondsToTicks(EEPROM_TIMEOUT_MS);
  return pObj;
}

/**
* @brief Check if sensor was successfully initialized.
* @param pObj          - ptr to @ref DrvEEPROM_t
* @return true if successful
*/
bool drvEEPROM_isReady(
  DrvEEPROM_t* const pObj
)
{
  return pObj->flagReady;
}

/**
* @brief Delete DrvEEPROM_t object.
* @param pObj          - ptr to @ref DrvEEPROM_t
*/
void drvEEPROM_delete(DrvEEPROM_t* const pObj)
{
  osLayer_free(pObj);
}

/**
* @brief Write One Byte.
* @param pObj                       - ptr to @ref DrvEEPROM_t
* @param usAddress  - Address will be written
* @param ucValue    - Value will be written to address
* @return true if successful
*/
bool drvEEPROM_writeOneByte(
                            DrvEEPROM_t* const pObj,
                            uint16_t usAddress,
                            uint8_t  ucValue
)
{
  uint8_t ucLowAddr, ucHighAddr;
  ucLowAddr = usAddress & 0x00FF;
  ucHighAddr = (uint8_t)((usAddress & 0xFF00) >> 8);

  const uint8_t txbuf[3] = {
    ucHighAddr,
    ucLowAddr,
    ucValue,
  };

  LOG_EEPROM_INFO("slave=0x%02x, regAddr=0x%02x, val=0x%02x (%u)", pObj->slaveAddr, usAddress, ucValue, ucValue);
  if (!osDrvI2c_masterTransmit(pObj->pDrvI2c, pObj->slaveAddr, txbuf, sizeof(txbuf), pObj->timeoutTicks))
  {
    LOGFL_EEPROM_ERR("slave=0x%02x, regAddr=0x%02x", pObj->slaveAddr, usAddress);
    return false;
  }
  return true;
}

/**
 * @brief Write Array Bytes.   NOTE:  This API need to check. CANNOT WORK NOW
 * @param pObj         - ptr to @ref DrvEEPROM_t
 * @param usAddress    - Start Address will be written
 * @param pValueArr    - ptr to array value will be written to address
 * @param ucLength     - Length of array value
 * @return true if successful
 */
bool drvEEPROM_writeArrayBytes(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t*  pValueArr,
    uint8_t  ucLength
)
{
  uint8_t ucLowAddr, ucHighAddr;
  ucLowAddr = usAddress & 0x00FF;
  ucHighAddr = (uint8_t)((usAddress & 0xFF00) >> 8);
  uint8_t txbuf[34];

  memset(txbuf,0,34*sizeof(uint8_t));

  txbuf[0] = ucHighAddr;
  txbuf[1] = ucLowAddr;
  memcpy(&txbuf[2], pValueArr, ucLength);

  if (!osDrvI2c_masterTransmit(pObj->pDrvI2c, pObj->slaveAddr, txbuf, sizeof(txbuf), pObj->timeoutTicks))
  {
    LOGFL_EEPROM_ERR("slave=0x%02x, regAddr=0x%02x", pObj->slaveAddr, usAddress);
    return false;
  }

  return true;
}

/**
 * @brief Read One Byte.
 * @param pObj          - ptr to @ref DrvEEPROM_t
 * @param usAddress     - Address will be read
 * @param pReadValue    - ptr to store value read
 * @return true if successful
 */
bool drvEEPROM_readOneByte(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t*  pReadValue
)
{
  uint8_t ucLowAddr, ucHighAddr;
  ucLowAddr = (uint8_t)(usAddress & 0x00FF);
  ucHighAddr = (uint8_t)((usAddress & 0xFF00) >> 8);

  const uint8_t txbuf[2] = {
      ucHighAddr,
      ucLowAddr,
  };
  uint8_t rxbuf[1] = {0};

  *pReadValue = 0;

  if (!osDrvI2c_masterTransaction(pObj->pDrvI2c,
      pObj->slaveAddr,
      txbuf,
      sizeof(txbuf),
      NULL,
      0,
      pObj->timeoutTicks))
  {
    LOG_COMMON_ERR("slave=0x%02x, regAddr=0x%02x", pObj->slaveAddr, usAddress);
    return false;
  }

  if (!osDrvI2c_masterTransaction(pObj->pDrvI2c,
      pObj->slaveAddr,
      NULL,
      0,
      rxbuf,
      sizeof(rxbuf),
      pObj->timeoutTicks))
  {
    LOG_COMMON_ERR("slave=0x%02x, regAddr=0x%02x", pObj->slaveAddr, usAddress);
    return false;
  }

  *pReadValue = rxbuf[0];
  LOG_EEPROM_INFO("slave=0x%02x, regAddr=0x%02x, val=0x%02x (%u)", pObj->slaveAddr, usAddress, *pReadValue, *pReadValue);
  return true;
}

/**
  * @brief Read Current Address One Byte.
  * @param pObj          - ptr to @ref DrvEEPROM_t
  * @param pReadValue    - ptr to store value read
  * @return true if successful
  */
bool drvEEPROM_readCurrentOneByte(
    DrvEEPROM_t* const pObj,
    uint8_t*  pReadValue
)
{
  uint8_t rxbuf[1] = {0};

  *pReadValue = 0;

  if (!osDrvI2c_masterTransaction(pObj->pDrvI2c,
      pObj->slaveAddr,
      NULL,
      0,
      rxbuf,
      sizeof(rxbuf),
      pObj->timeoutTicks))
  {
    LOGFL_EEPROM_ERR("slave=0x%02x", pObj->slaveAddr);
    return false;
  }

  *pReadValue = rxbuf[0];
  LOG_EEPROM_INFO("slave=0x%02x, val=0x%02x (%u)", pObj->slaveAddr, *pReadValue, *pReadValue);
  return true;
}

/**
  * @brief Read Sequential Array Bytes.
  * @param pObj          - ptr to @ref DrvEEPROM_t
  * @param pReadValue    - ptr to store value read
  * @param ucLengthRead  - Length data to be read.
  * @return true if successful
  */
bool drvEEPROM_readSequential(
    DrvEEPROM_t* const pObj,
    uint16_t usAddress,
    uint8_t*  pReadValue,
    uint8_t ucLengthRead
)
{
  if (ucLengthRead < 1)
  {
     LOG_EEPROM_ERR("You want to read ZERO byte ?");
     return false;
  }

  uint8_t ucLowAddr, ucHighAddr;
  ucLowAddr = (uint8_t)(usAddress & 0x00FF);
  ucHighAddr = (uint8_t)((usAddress & 0xFF00) >> 8);

  const uint8_t txbuf[2] = {
      ucHighAddr,
      ucLowAddr,
  };
 
  uint8_t *rxbuf = osLayer_calloc(ucLengthRead + 1, sizeof(*rxbuf));
  if (NULL == rxbuf)
  {
    LOG_EEPROM_ERR("Can't allocate mem");
    return false;
  }

  if (!osDrvI2c_masterTransaction(pObj->pDrvI2c,
      pObj->slaveAddr,
      txbuf,
      sizeof(txbuf),
      rxbuf,
      sizeof(rxbuf),
      pObj->timeoutTicks))
  {
    LOGFL_EEPROM_ERR("slave=0x%02x", pObj->slaveAddr);
    return false;
  }

  memcpy(pReadValue, rxbuf, ucLengthRead*sizeof(uint8_t));
  LOG_EEPROM_INFO("slave=0x%02x, val=0x%02x (%u)", pObj->slaveAddr, pReadValue[0], pReadValue[0]);
  return true;
}