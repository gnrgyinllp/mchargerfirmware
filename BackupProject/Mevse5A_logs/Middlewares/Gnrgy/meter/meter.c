#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "os_driver/os_drv_spi.h"
#include "log/log.h"
#include "app/gnrgy_config.h"
#include "drv_meter.h"
#include "meter.h"

osDrvSpi_t* g_pDrvSpi;
static DrvMeter_t* g_pDrvMeter;

bool meter_Init(void)
{
  g_pDrvSpi = osDrvSpi_init(TARGET_SPI_METER);
  if (NULL == g_pDrvSpi)
  {
    LOG_COMMON_ERR("%s failed", "osDrvSpi_init");
    return false;
  }
  g_pDrvMeter = drvMeter_create(g_pDrvSpi);
  if (NULL == g_pDrvMeter)
  {
    LOG_COMMON_ERR("%s failed", "drvMeter_create");
    return false;
  }
  meter_Reset();
  return true;
}

bool meter_Reset(void)
{
  HAL_GPIO_WritePin(RST_METER_GPIO_Port, RST_METER_Pin, GPIO_PIN_RESET);
  HAL_Delay(20);
  HAL_GPIO_WritePin(RST_METER_GPIO_Port, RST_METER_Pin, GPIO_PIN_SET);
  HAL_Delay(50);
  //To use the SPI port, toggle the SS/HSA pin three times from high to low
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS High
  HAL_Delay(1);
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // HSS Low
  HAL_Delay(1);
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS High
  HAL_Delay(1);
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // HSS Low
  HAL_Delay(1);
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS High
  HAL_Delay(1);
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // HSS Low
  HAL_Delay(1);
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS High
  HAL_Delay(1000);
  return true;
}


bool meter_readconfig(uint16_t address)
{
  bool LenReturnValue = false;
  uint8_t rxbuf[1] = {0x00};

  LenReturnValue = drvMeter_readFromMeter(g_pDrvMeter,address, &rxbuf[0], 1);
  LOG_COMMON_INFO("Value of register is 0x%02x", rxbuf[0]);
  return LenReturnValue;
}


bool meter_write(void)
{
  bool LenReturnValue = false;
  uint16_t usAddressConfig2 = 0xEC01;
  uint8_t txbuf = 0x00;

  LenReturnValue = drvMeter_writeToMeter(g_pDrvMeter, usAddressConfig2, &txbuf, 1);
  return LenReturnValue;
}