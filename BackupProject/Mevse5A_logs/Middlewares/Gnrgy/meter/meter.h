#ifndef METER_H_
#define METER_H_

#include <stdint.h>
#include <stdbool.h>
#include "drv_meter.h"

bool meter_Init(void);

bool meter_Reset(void);

bool meter_readconfig(uint16_t address);

bool meter_write(void);


#endif /* METER_H_ */
