
#ifndef LED_PWM_H
#define LED_PWM_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void led_init(void);
void led_setPWM(const uint8_t ucPWM);
void ledRed_turnOn(void);
void ledRed_turnOff(void);
void ledBlue_turnOn(void);
void ledBlue_turnOff(void);

#ifdef __cplusplus
}
#endif

#endif
