#include "led_rgb.h"
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include "os_layer\os_layer.h"
#include "main.h"
#include "log/log.h"
#include "hal_driver/hal_gpio.h"

/**********************************************************************************************************************/
extern TIM_HandleTypeDef htim12;
#define LED_RGB_MAX_PERIOD      (100)

#define LED_WHITE_HTIM          (&htim12)
#define LED_WHITE_CHANNEL   TIM_CHANNEL_1

/**********************************************************************************************************************/

void led_init(void)
{
  HAL_TIM_PWM_Start(LED_WHITE_HTIM, LED_WHITE_CHANNEL);
  led_setPWM(0);
}

void led_setPWM(const uint8_t ucPWM)
{
  LED_WHITE_HTIM->Instance->CCR1 = ucPWM;
}

void ledRed_turnOn(void)
{
  halGPIO_setPort(RED_LED_GPIO_Port, RED_LED_Pin);
}

void ledRed_turnOff(void)
{
  halGPIO_clearPort(RED_LED_GPIO_Port, RED_LED_Pin);
}

void ledBlue_turnOn(void)
{
  halGPIO_setPort(BLUE_LED_GPIO_Port, BLUE_LED_Pin);
}

void ledBlue_turnOff(void)
{
  halGPIO_clearPort(BLUE_LED_GPIO_Port, BLUE_LED_Pin);
}


