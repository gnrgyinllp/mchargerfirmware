#ifndef TEMP_H_
#define TEMP_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum TEMP_REGISTER_TAG
{
  REMOTE_TEMPERATURE_REG = 0x01,
  CONFIGURATION_REG = 0x03,
  MANUFACTURE_IDENTIFICATION_REG = 0xFE,
  DEVICE_REVISION_REG = 0xFF
} TEMP_REGISTER_e;

typedef enum TEMP_CONFIG_TAG
{
  NORMAL_MODE = 0x00,
  SHUTDOWN = 0x40
} TEMP_CONFIG_e;


bool temp_init(void);
bool temp_readCurrentTemp(uint8_t *pValue);
bool temp_readManufacture(uint8_t *pValue);
bool temp_readDeviceRevision(uint8_t *pValue);
bool temp_setConfig(TEMP_CONFIG_e enConfig);
bool temp_remove(void);
#endif