#ifndef DRV_TEMP_H_
#define DRV_TEMP_H_

#include <stdint.h>
#include <stdbool.h>
#include "os_driver/os_drv_i2c.h"

struct DrvTemp_TAG
{
  osDrvI2c_t* pDrvI2c;
  bool flagReady;
  halI2c_Addr_t slaveAddr;
  osLayer_DeltaTicks_t timeoutTicks;
};

typedef struct DrvTemp_TAG DrvTemp_t;

/**
 * @brief Create new DrvTemp_t object for temperature with specified address on the I2C bus.
 * @param pDrvI2c       - ptr to I2C-bus driver - @ref osDrvI2c_t
 * @param slaveAddr     - address of sensor on the I2C bus
 * @return ptr to created object @ref DrvTemp_t
 */
DrvTemp_t* drvTemp_create(
    osDrvI2c_t* const pDrvI2c,
    const halI2c_Addr_t slaveAddr
);

/**
 * @brief Initial check and configure temperature sensor.
 * @param pObj          - ptr to @ref DrvTemp_t
 * @return true if successful
 */
bool drvTemp_init(
    DrvTemp_t* const pObj
);

/**
 * @brief Check if sensor was successfully initialized.
 * @param pObj          - ptr to @ref DrvTemp_t
 * @return true if successful
 */
bool drvTemp_isReady(
    DrvTemp_t* const pObj
);

/**
 * @brief Delete DrvTemp_t object.
 * @param pObj          - ptr to @ref DrvTemp_t
 */
void drvTemp_delete(DrvTemp_t* const pObj);

/**
 * @brief Write One Byte.
 * @param pObj       - ptr to @ref DrvTemp_t
 * @param usAddress  - Address will be written
 * @param ucValue    - Value will be written to address
 * @return true if successful
 */
bool drvTemp_read(
    DrvTemp_t* const pObj,
    uint8_t ucAddress,
    uint8_t* pValue
);
#endif
