#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "usart.h"
#include "app/app_glob.h"
#include "log/log.h"
#include "stm32f4xx_hal.h"
#include "sdcard/file.h"
#include "sdcard/sd_card.h"
#include "app/gnrgy_config.h"
#include "eeprom/drv_eeprom.h"
#include "led/led_rgb.h"
#include "temp/temp.h"
#include "meter/meter.h"
#include "temp/drv_temp.h"
#include "os_driver/os_drv_i2c.h"
#include "os_driver/os_drv_uart.h"
#include "log/log_ll.h"
#include "led/led_rgb.h"
#include "sdcard/file.h"
#include "hal_driver/hal_sdcard.h"
#include "eeprom/eeprom.h"
#include "bluetooth/drv_BLESerial.h"

extern osDrvI2c_t* g_pDrvI2c;
extern osDrvUart_t* g_dbgTask_pDrvUART;

void dbgConsole_printfLine(const char* const fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  log_vprintfLineToUartWithPrefix("!: ", NULL, fmt, args);
  va_end(args);
}

void dbgConsole_printfAck(void)
{
  log_printfLineToUart("ACK");
}

void dbgConsole_printfRespOK(void)
{
  log_printfLineToUart("OK");
}

void dbgConsole_printfRespERROR(void)
{
  log_printfLineToUart("ERROR");
}

static bool handleCmd_unknown(const char* const param)
{
  dbgConsole_printfLine("Error: Unknown command, enter 'help' to get list of supported commands");
  return true;
}

static bool strCmpPrefix(const char* const src, const char* const prefix, const char** pParam)
{
  *pParam = NULL;
  const size_t len = strlen(prefix);
  if (0 == strncmp(src, prefix, len))
  {
    *pParam = &src[len];
    return true;
  }
  return false;
}

static bool handleCmd_reset(const char* const param)
{
  dbgConsole_printfRespOK();
  osLayer_sleepTicks(500);
  HAL_NVIC_SystemReset();
  return true;
}

static bool handleCmd_ledred(const char* const param)
{
  const bool pinLevel = (bool)strtoul(param, NULL, 10);
  dbgConsole_printfLine("GPIO LED RED SET: %d", pinLevel);
  pinLevel == 0 ? ledRed_turnOff() : ledRed_turnOn();
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_ledblue(const char* const param)
{
  const bool pinLevel = (bool)strtoul(param, NULL, 10);
  dbgConsole_printfLine("GPIO LED BLUE SET: %d", pinLevel);
  pinLevel == 0 ? ledBlue_turnOff() : ledBlue_turnOn();
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_Contactor(const char* const param)
{
  const bool pinLevel = (bool)strtoul(param, NULL, 10);
  dbgConsole_printfLine("CONTACTOR SET: %d", pinLevel);
  HAL_GPIO_WritePin(CONTACTOR_OUT_GPIO_Port, CONTACTOR_OUT_Pin, pinLevel ? GPIO_PIN_RESET : GPIO_PIN_SET);
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_temp(void)
{
  bool LenStatus;
  uint8_t ucManufacture;
  uint8_t ucDeviceRevision;

  dbgConsole_printfLine("Test Sensor Temperature");
  LenStatus = temp_init();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("temp_init failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Read Manufacture Identification ");
  LenStatus = temp_readManufacture(&ucManufacture);
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("temp_readManufacture failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Read Device and Revision Identification ");
  LenStatus = temp_readDeviceRevision(&ucDeviceRevision);
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("temp_readDeviceRevision failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Delete object Temp ");
  LenStatus = temp_remove();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("temp_remove failed");
    dbgConsole_printfRespERROR();
    return false;
  }

  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_meter(void)
{
  bool LenStatus;

  dbgConsole_printfLine("Test Meter Sensor");
  LenStatus = meter_Init();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("meter_Init failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Read Register LCYCMODE ");
  LenStatus = meter_readconfig(0xE702);
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("meter_readconfig failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Read Register MMODE ");
  LenStatus = meter_readconfig(0xE700);
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("meter_readconfig failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_bluetooth(void)
{
  bool LenStatus;

  dbgConsole_printfLine("Test Bluetooth Module");
  dbgConsole_printfLine("Set BT to machine mode");
  LenStatus = drv_BLESetMachineMode();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("drv_BLESetMachineMode failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Disable header response for BT");
  LenStatus = drv_BLEDisableHeader();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("drv_BLEDisableHeader failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Set BT name");
  LenStatus = drv_BLESetNameDevice();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("drv_BLESetNameDevice failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Set BT IO feature status_led");
  LenStatus = drv_BLESetIOFeature();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("drv_BLESetIOFeature failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Set BT IO operation status_led");
  LenStatus = drv_BLESetIOOperation();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("drv_BLESetIOOperation failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Set BT Adversing Mode");
  LenStatus = drv_BLESetAdversingMode();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("drv_BLESetAdversingMode failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Set BT Power Mode");
  LenStatus = drv_BLESetPowerMode();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("drv_BLESetPowerMode failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_sdcardIsInserted(void)
{
  dbgConsole_printfLine("SD-Card: check is SD-Card inserted?");
  const uint8_t isInserted = BSP_PlatformIsDetected();
  log_printfLineToUart("+SD_DET:%d", (int)isInserted);
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_sdcardMount(void)
{
  dbgConsole_printfLine("SD-Card: mount");
  if (!halSDcard_init())
  {
    LOG_COMMON_ERR("halSDcard_init failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  sdCard_init();
  if (!sdCard_mount())
  {
    LOG_COMMON_ERR("sdCard_mount failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_sdcardUnmount(void)
{
  dbgConsole_printfLine("SD-Card: unmount");
  if (!sdCard_unmount())
  {
    LOG_COMMON_ERR("sdCard_mount failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfRespOK();
  return true;
}

FileInfo_t g_file;
char g_fileBuf[_MAX_SS];

static bool handleCmd_sdcardWriteFile(void)
{
  dbgConsole_printfLine("SD-Card: write file");
  const FileName_t fileName = fileNamePrintf("test.txt");
  if (!fileOpenForWrite(&g_file, &fileName))
  {
    LOG_COMMON_ERR("fileOpenForWrite failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  memset(g_fileBuf, 'a', sizeof(g_fileBuf));
  g_fileBuf[sizeof(g_fileBuf) - 2] = '\r';
  g_fileBuf[sizeof(g_fileBuf) - 1] = '\n';
  const size_t fileSize = 256 * 1024;
  const size_t numSectors = fileSize / sizeof(g_fileBuf);
  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  for (unsigned i = 0; i < numSectors; ++i)
  {
    if (!fileWrite(&g_file, g_fileBuf, sizeof(g_fileBuf)))
    {
      LOG_COMMON_ERR("fileWrite failed");
      fileClose(&g_file);
      dbgConsole_printfRespERROR();
      return false;
    }
  }
  const osLayer_DeltaTicks_t deltaTicks = osLayer_getElapsedTicks(t0);
  const osLayer_DelayUMilliseconds_t delta_ms = osLayer_convTicksToMilliseconds(deltaTicks);
  if (!fileClose(&g_file))
  {
    LOG_COMMON_ERR("fileClose failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  log_printfLineToUart("+SDCARD_WRITE_SPEED:%u kb/sec", (unsigned)(fileSize * 1000 / delta_ms / 1024));
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_sdcardReadFile(void)
{
  dbgConsole_printfLine("SD-Card: read file");
  const FileName_t fileName = fileNamePrintf("test.txt");
  if (!fileOpenForRead(&g_file, &fileName))
  {
    LOG_COMMON_ERR("fileOpenForRead failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  const size_t fileSize = fileInfoGetSize(&g_file);
  const size_t numSectors = fileSize / sizeof(g_fileBuf);
  const osLayer_Ticks_t t0 = osLayer_getSysTick();
  for (unsigned i = 0; i < numSectors; ++i)
  {
    if (!fileRead(&g_file, g_fileBuf, sizeof(g_fileBuf)))
    {
      LOG_COMMON_ERR("fileRead failed");
      fileClose(&g_file);
      dbgConsole_printfRespERROR();
      return false;
    }
  }
  const osLayer_DeltaTicks_t deltaTicks = osLayer_getElapsedTicks(t0);
  const osLayer_DelayUMilliseconds_t delta_ms = osLayer_convTicksToMilliseconds(deltaTicks);
  if (!fileClose(&g_file))
  {
    LOG_COMMON_ERR("fileClose failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  log_printfLineToUart("+SDCARD_READ_SPEED:%u kb/sec", (unsigned)(fileSize * 1000 / delta_ms / 1024));
  dbgConsole_printfRespOK();
  return true;
}

static bool handleCmd_eeprom(void)
{
  bool LenStatus = false;
  uint8_t writebuf[5] = {0xAA, 0xBB, 0xCC, 0xDD, 0xEE };
  uint8_t readbuf;
  dbgConsole_printfLine("Test EEPROM");
  LenStatus = eeprom_init();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("eeprom_init failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Write one byte EEPROM");
  LenStatus = eeprom_writebyte(0x01, writebuf[0]);
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("eeprom_writebyte failed");
    dbgConsole_printfRespERROR();
    return false;
  }

  dbgConsole_printfLine("Read one byte EEPROM");
  LenStatus = eeprom_readbyte(0x01, &readbuf);
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("eeprom_readbyte failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Validate EEPROM");
  if (readbuf != writebuf[0])
  {
    LOG_COMMON_ERR("Inconsistency between write and read");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Validate EEPROM Passed");
  LenStatus = eeprom_writebytes(0x02, writebuf, 5);
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("eeprom_writebytes failed");
    dbgConsole_printfRespERROR();
    return false;
  }
  dbgConsole_printfLine("Delete object Temp ");
  LenStatus = eeprom_remove();
  if (false == LenStatus)
  {
    LOG_COMMON_ERR("eeprom_remove failed");
    dbgConsole_printfRespERROR();
    return false;
  }

  dbgConsole_printfRespOK();
  return true;
}
static bool handleCmd_help(void)
{
  dbgConsole_printfLine("Supported commands:");
  dbgConsole_printfLine("  help             print list of supported commands");
  dbgConsole_printfLine("  ledred {0|1}");
  dbgConsole_printfLine("  ledblue {0|1}");
  dbgConsole_printfLine("  contactor {0|1}");
  dbgConsole_printfLine("  tempsensor ");
  dbgConsole_printfLine("  metersensor ");
  dbgConsole_printfLine("  sdcard_is_inserted ");
  dbgConsole_printfLine("  sdcard_mount ");
  dbgConsole_printfLine("  sdcard_unmount ");
  dbgConsole_printfLine("  sdcard_write_file ");
  dbgConsole_printfLine("  sdcard_read_file ");
  dbgConsole_printfLine("  eeprom ");
  dbgConsole_printfLine("  bluetooth ");
  dbgConsole_printfLine("  reset");
  return true;
}

static bool handleCmd(const char* const cmd)
{
  const char* param = NULL;
  if (0 == strcmp("?", cmd))
  {
    return handleCmd_help();
  }
  else if (0 == strcmp("help", cmd))
  {
    return handleCmd_help();
  }
  else if (strCmpPrefix(cmd, "reset", &param))
  {
    return handleCmd_reset(param);
  }
  else if (strCmpPrefix(cmd, "ledred", &param))
  {
    return handleCmd_ledred(param);
  }
  else if (strCmpPrefix(cmd, "ledblue", &param))
  {
    return handleCmd_ledblue(param);
  }
  else if (strCmpPrefix(cmd, "contactor", &param))
  {
    return handleCmd_Contactor(param);
  }
  else if (0 == strcmp("tempsensor", cmd))
  {
    return handleCmd_temp();
  }
  else if (0 == strcmp("metersensor", cmd))
  {
    return handleCmd_meter();
  }
  else if (0 == strcmp("sdcard_is_inserted", cmd))
  {
    return handleCmd_sdcardIsInserted();
  }
  else if (0 == strcmp("sdcard_mount", cmd))
  {
    return handleCmd_sdcardMount();
  }
  else if (0 == strcmp("sdcard_unmount", cmd))
  {
    return handleCmd_sdcardUnmount();
  }
  else if (0 == strcmp("sdcard_write_file", cmd))
  {
    return handleCmd_sdcardWriteFile();
  }
  else if (0 == strcmp("sdcard_read_file", cmd))
  {
    return handleCmd_sdcardReadFile();
  }
  else if (0 == strcmp("eeprom", cmd))
  {
    return handleCmd_eeprom();
  }
  else if (0 == strcmp("bluetooth", cmd))
  {
    return handleCmd_bluetooth();
  }
  else
  {
    return handleCmd_unknown(param);
  }
}

int mainTestFunc(void)
{
  log_init();
  fileInit();
  g_dbgTask_pDrvUART = logLL_uartGetDrv();
  g_pDrvI2c = osDrvI2c_init(1);
  if (NULL == g_pDrvI2c)
  {
    LOG_COMMON_CRIT("osDrvI2c_init failed");
    return 0;
  }

  log_printfCrLfToUart();
  LOG_COMMON_MSG("=== GNRGY hardware test console ===");
  LOG_COMMON_MSG("To view the list of supported commands, enter command 'help'");

  logLL_uartStartReceiving();
  for (;;)
  {
    static char lineBuf[256];
    log_printfToUart("> ");
    if (!logLL_uartReadLineWithEdit(lineBuf, sizeof(lineBuf)))
    {
      LOG_COMMON_ERR("logLL_uartReadLine failed");
      continue;
    }
    log_printfCrLfToUart();
    LOG_COMMON_INFO("Got cmd: %s", lineBuf);
    if ('$' == lineBuf[0])
    {
      const size_t lineLen = strlen(lineBuf);
      if (lineLen <= 4)
      {
        dbgConsole_printfLine("Error: bad command '%s'", lineBuf);
        continue;
      }
      char* const endPtr = &lineBuf[lineLen-3];
      if ('*' != *endPtr)
      {
        dbgConsole_printfLine("Error: bad command '%s'", lineBuf);
        continue;
      }
      char* end = NULL;
      uint8_t checksum = 0;
      {
        const unsigned long expChecksum = strtoul(endPtr + 1, &end, 16);
        if (expChecksum > 255)
        {
          dbgConsole_printfLine("Error: bad command '%s'", lineBuf);
          continue;
        }
        checksum = (uint8_t)expChecksum;
      }
      for (const uint8_t* p = (uint8_t*)&lineBuf[1]; p != (uint8_t*)endPtr; ++p)
      {
        checksum ^= *p;
      }
      if (0 != checksum)
      {
        dbgConsole_printfLine("Error: bad command checksum '%s'", lineBuf);
        continue;
      }
      *endPtr = '\0';
      dbgConsole_printfAck();
      handleCmd(&lineBuf[1]);
    }
    else
    {
      handleCmd(lineBuf);
    }
  }
  LOG_COMMON_MSG("All tests finished");
  return 0;
}