#ifndef CYCLE_BUF_H_
#define CYCLE_BUF_H_

#include <stdint.h>
#include <stdbool.h>
#include "os_layer/os_layer.h"

typedef struct CycleBuf_TAG
{
  uint16_t firstIdx;
  volatile uint16_t lastIdx;
  uint8_t* cycleBuf;
  uint16_t cycleBufSize;
  osLayer_ThreadId_t threadId;
} CycleBuf_t;

void cycleBuf_init(CycleBuf_t* const pCycleBuf, uint8_t* const buf, const uint16_t bufSize);
CycleBuf_t* cycleBuf_create(const uint16_t bufSize);
void cycleBuf_setCurThreadId(CycleBuf_t* const pCycleBuf);
bool cycleBuf_isEmpty(CycleBuf_t* const pCycleBuf);
uint8_t* cycleBuf_getPtrToLast(CycleBuf_t* const pCycleBuf);
bool cycleBuf_moveForwardLastIdx(CycleBuf_t* const pCycleBuf);
bool cycleBuf_append(CycleBuf_t* const pCycleBuf, const uint8_t val);
uint8_t cycleBuf_read(CycleBuf_t* const pCycleBuf);

#endif /* CYCLE_BUF_H_ */
