// this file must be include in "log.h"

/*** LOG for FACILITY_OS_DRV_I2C ******************************************************************************************/

/**
 * @brief Prints log with 'TRACE' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_TRACE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_TRACE, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'NOISE' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_NOISE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_NOISE, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOGV_OS_DRV_I2C_DBG(fmt_, args_)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, args_)

#define LOGF_OS_DRV_I2C_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_OS_DRV_I2C_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_OS_DRV_I2C_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_INFO(fmt_, ...)   \
    LOG(LOG_SEVERITY_INFO, LOG_FACILITY_OS_DRV_I2C, fmt_, ##__VA_ARGS__)

#define LOGF_OS_DRV_I2C_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_OS_DRV_I2C_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_OS_DRV_I2C_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGP_OS_DRV_I2C_INFO(prefix_, fmt_, ...)    \
    LOGP(LOG_SEVERITY_INFO, LOG_FACILITY_OS_DRV_I2C, prefix_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGV_OS_DRV_I2C_INFO(fmt_, args_)    \
    LOGV(LOG_SEVERITY_INFO, LOG_FACILITY_OS_DRV_I2C, fmt_, args_)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGVP_OS_DRV_I2C_INFO(prefix_, fmt_, args_)    \
    LOGVP(LOG_SEVERITY_INFO, LOG_FACILITY_OS_DRV_I2C, prefix_, fmt_, args_)

/**
 * @brief Prints log with 'WARN' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_WARN(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_OS_DRV_I2C, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_OS_DRV_I2C_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_OS_DRV_I2C_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_OS_DRV_I2C_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_OS_DRV_I2C, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_OS_DRV_I2C_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_OS_DRV_I2C_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_OS_DRV_I2C_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_ERR_FUNC(func_, fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_OS_DRV_I2C, NULL, 0, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'CRIT' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_CRIT(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

#define LOGF_OS_DRV_I2C_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_OS_DRV_I2C, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_OS_DRV_I2C_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_OS_DRV_I2C_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_OS_DRV_I2C, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_OS_DRV_I2C_MSG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_MSG, LOG_FACILITY_OS_DRV_I2C, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

