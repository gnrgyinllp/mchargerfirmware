#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include "os_layer/os_layer.h"
#include "app/gnrgy_config.h"
#include "log_ll.h"

#if defined(LOG_UART)
extern UART_HandleTypeDef huart1;
#define SERIAL_DEVICE(x_)       SERIAL_DEVICE_(x_)
#define SERIAL_DEVICE_(x_)      (&huart##x_)

UART_HandleTypeDef* const g_logUART = SERIAL_DEVICE(LOG_UART);

void logLL_uartWaitAsyncTransmitComplete(bool flagSleepAllowed)
{
    UART_HandleTypeDef* huart = g_logUART;
    while ((huart->gState == HAL_UART_STATE_BUSY_TX) || (huart->gState == HAL_UART_STATE_BUSY_TX_RX))
    {
        if (flagSleepAllowed)
        {
            osLayer_sleepTicks(1);
        }
    }
}

void logLL_uartTransmitAsync(const void* const buf, const uint16_t bufLen, const bool flagSleepAllowed)
{
    logLL_uartWaitAsyncTransmitComplete(flagSleepAllowed);
    if (flagSleepAllowed)
    {
        halUART_TransmitIT(g_logUART, (uint8_t*)buf, bufLen);
    }
    else
    {
        HAL_UART_Transmit(g_logUART, (uint8_t*)buf, bufLen, HAL_MAX_DELAY);
    }
}

void logLL_uartTransmit(const void* const buf, const uint16_t bufLen, const bool flagSleepAllowed)
{
    logLL_uartTransmitAsync(buf, bufLen, flagSleepAllowed);
    logLL_uartWaitAsyncTransmitComplete(flagSleepAllowed);
}

void logLL_setBaud(const unsigned baud)
{
    logLL_uartWaitAsyncTransmitComplete(true);
    g_logUART->Init.BaudRate = baud;
    HAL_UART_Init(g_logUART);
}

#endif

