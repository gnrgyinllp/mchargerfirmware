#if !defined(LOG_LL_H_)
#define LOG_LL_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

void logLL_init(void);
void logLL_write(const void* const buf, const uint16_t bufLen);
void logLL_putc(const char ch);

#if defined(LOG_UART)
void logLL_uartTransmit(const void* const buf, const uint16_t bufLen, const bool flagSleepAllowed);
void logLL_uartTransmitAsync(const void* const buf, const uint16_t bufLen, const bool flagSleepAllowed);
void logLL_uartWaitAsyncTransmitComplete(bool flagSleepAllowed);
void logLL_setBaud(const unsigned baud);
#endif

#if defined(LOG_USB)
void logLL_usbTransmitWaitAsyncComplete(void);
bool logLL_usbTransmitAsync(
        const void* const txBuf,
        const uint16_t txBufLen);
bool logLL_usbTransmitSync(
        const void* const txBuf,
        const uint16_t txBufLen);
#endif

#ifdef __cplusplus
}
#endif

#endif /* LOG_LL_H_ */
