// this BLUETOOTH must be include in "log.h"

/*** LOG for FACILITY_BLUETOOTH ******************************************************************************************/

/**
 * @brief Prints log with 'TRACE' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_TRACE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_TRACE, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'NOISE' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_NOISE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_NOISE, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOGV_BLUETOOTH_DBG(fmt_, args_)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, args_)

#define LOGF_BLUETOOTH_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_BLUETOOTH_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_BLUETOOTH_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_INFO(fmt_, ...)   \
    LOG(LOG_SEVERITY_INFO, LOG_FACILITY_BLUETOOTH, fmt_, ##__VA_ARGS__)

#define LOGF_BLUETOOTH_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_BLUETOOTH_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_BLUETOOTH_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGP_BLUETOOTH_INFO(prefix_, fmt_, ...)    \
    LOGP(LOG_SEVERITY_INFO, LOG_FACILITY_BLUETOOTH, prefix_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGV_BLUETOOTH_INFO(fmt_, args_)    \
    LOGV(LOG_SEVERITY_INFO, LOG_FACILITY_BLUETOOTH, fmt_, args_)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGVP_BLUETOOTH_INFO(prefix_, fmt_, args_)    \
    LOGVP(LOG_SEVERITY_INFO, LOG_FACILITY_BLUETOOTH, prefix_, fmt_, args_)

/**
 * @brief Prints log with 'WARN' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_WARN(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_BLUETOOTH, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_BLUETOOTH_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_BLUETOOTH_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_BLUETOOTH_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_BLUETOOTH, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_BLUETOOTH_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_BLUETOOTH_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_BLUETOOTH_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_ERR_FUNC(func_, fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_BLUETOOTH, NULL, 0, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'CRIT' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_CRIT(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

#define LOGF_BLUETOOTH_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_BLUETOOTH, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_BLUETOOTH_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_BLUETOOTH_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_BLUETOOTH, __BLUETOOTH__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_BLUETOOTH_MSG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_MSG, LOG_FACILITY_BLUETOOTH, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

