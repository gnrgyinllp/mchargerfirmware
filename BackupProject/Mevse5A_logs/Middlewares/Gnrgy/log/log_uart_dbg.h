#ifndef LOG_UART_DBG_H_
#define LOG_UART_DBG_H_

#include <stdarg.h>
#include "utils/zz.h"

#ifdef __cplusplus
extern "C" {
#endif

void logUartDbg_vprintf(const char* fmt, va_list args);

/**
 * @brief Disable IRQ, abort current tx and print log to UART.
 * @note The aim of this function is to print log to UART only
 *       in case of serious/fatal error (hardfault, stack overflow, system assert, etc.),
 *       therefore, interrupts remain disabled after exiting from this function.
 */
ZZ_ATTR_FORMAT_PRINTF(1, 2)
void logUartDbg_printf(const char* fmt, ...);

#ifdef __cplusplus
}
#endif

#endif /* LOG_UART_DBG_H_ */
