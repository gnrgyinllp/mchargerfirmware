// this file must be include in "log.h"
#include "../../Gnrgy/log/log.h"
/*** LOG for FACILITY_COMMON ******************************************************************************************/

/**
 * @brief Prints log with 'TRACE' severity for specified 'facility'
 */
#define LOG_COMMON_TRACE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_TRACE, LOG_FACILITY_COMMON, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'NOISE' severity for specified 'facility'
 */
#define LOG_COMMON_NOISE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_NOISE, LOG_FACILITY_COMMON, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOG_COMMON_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_COMMON, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOGV_COMMON_DBG(fmt_, args_)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_COMMON, NULL, 0, __func__, fmt_, args_)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOG_COMMON_INFO(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_COMMON, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'WARN' severity for specified 'facility'
 */
#define LOG_COMMON_WARN(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_COMMON, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_COMMON_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_COMMON, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_COMMON_ERR_FUNC(func_, fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_COMMON, NULL, 0, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'CRIT' severity for specified 'facility'
 */
#define LOG_COMMON_CRIT(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_COMMON, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_COMMON_MSG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_MSG, LOG_FACILITY_COMMON, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

