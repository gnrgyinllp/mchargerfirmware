#include "log.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "log_ll.h"
#include "sdcard/file.h"
#include "utils/sys_queue1.h"
#include "utils/t_snprintf.h"
#include "utils/zz.h"
#include "asserts/hw_assert.h"
#include "os_layer/os_layer.h"

bool g_logMutexInitialied;
bool g_logDisable;
osLayer_Mutex_t g_logMutex;
static uint32_t g_logDelayAfterPrint = 50;
static char g_logBuf[2048];

static const char* g_logSeverityStr[] = {
  "NONE",
  " MSG",
  "CRIT",
  " ERR",
  "WARN",
  "INFO",
  " DBG",
  "NOIS",
  "TRCE",
};

static const char* g_logFacilityStr[] = {
  "COMMON",
  "FILE",
  "SDCARD",
  "I2C",
  "HAL_I2C",
  "OS_DRV_I2C",
  "SPI",
  "HAL_SPI",
  "OS_DRV_SPI",
  "EEPROM",
  "METER",
  "CHARGER",
  "TEMP",
  "BLUETOOTH",
  "ATCOM",
};

HW_ASSERT_COMPILE_TIME(LOG_FACILITY_COUNT_ == ZZ_COUNT_OF(g_logFacilityStr));

static log_Severity g_logSeverity[LOG_FACILITY_COUNT_] = {
  [LOG_FACILITY_COMMON] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_FILE] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_SDCARD] = LOG_SEVERITY_MSG,
  [LOG_FACILITY_I2C] = LOG_SEVERITY_MSG,
  [LOG_FACILITY_HAL_I2C] = LOG_SEVERITY_MSG,
  [LOG_FACILITY_OS_DRV_I2C] = LOG_SEVERITY_MSG,
  [LOG_FACILITY_SPI] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_HAL_SPI] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_OS_DRV_SPI] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_EEPROM] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_METER] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_CHARGER] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_TEMP] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_BLUETOOTH] = LOG_SEVERITY_INFO,
  [LOG_FACILITY_ATCMD] = LOG_SEVERITY_INFO,
};
#if LOG_ENABLED
void log_init(void)
{
    osLayer_mutexInit(&g_logMutex);
    g_logMutexInitialied = true;
}
#endif

bool log_lock(void)
{
    if (g_logMutexInitialied)
    {
        osLayer_mutexEnter(&g_logMutex);
    }
    return g_logMutexInitialied;
}

void log_unlock(void)
{
    if (g_logMutexInitialied)
    {
        osLayer_mutexExit(&g_logMutex);
    }
}

void log_disable(void)
{
    log_lock();
    g_logDisable = true;
    log_unlock();
}

void log_enable(void)
{
    log_lock();
    g_logDisable = false;
    log_unlock();
}

void log_setLevel(const log_Facility facility, const log_Severity severity)
{
    if ((unsigned)facility >= LOG_FACILITY_COUNT_)
    {
        LOG(LOG_SEVERITY_MSG, LOG_FACILITY_COMMON, "Failed to set severity for facility=%u - out of range [0..%u]",
            (unsigned)facility,
            (unsigned)(LOG_FACILITY_COUNT_ - 1));
        return;
    }
    g_logSeverity[facility] = severity;
}

bool log_isEnabled(
        const log_Severity severity,
        const log_Facility facility
)
{
    if (g_logDisable)
    {
        return false;
    }
    if ((unsigned)facility < LOG_FACILITY_COUNT_)
    {
        if (severity > g_logSeverity[facility])
        {
            return false;
        }
    }
    return true;
}

void log_setDelayAfterPrint(uint32_t delay_ms)
{
  g_logDelayAfterPrint = delay_ms;
}

void log_writeMsgWithPrefixAndSuffixWithoutLock(
        const char* const prefix,
        const char* const suffix,
        const char* msg,
        const uint16_t msgLen,
        const bool flagSleepAllowed)
{
  if (NULL != prefix)
  {
      const size_t prefixLen = strlen(prefix);
#if LOG_UART
      logLL_uartTransmit(prefix, (uint16_t)prefixLen, flagSleepAllowed);
#endif
#if LOG_USB
    logLL_usbTransmitSync(prefix, prefixLen);
#endif
#if LOG_GWIN
    if (NULL != g_logLoggerGWin)
    {
      gwinPutCharArray(g_logLoggerGWin, prefix, prefixLen);
    }
#endif
  }
  {
#if LOG_UART
    logLL_uartTransmit(msg, msgLen, flagSleepAllowed);
#endif
#if LOG_USB
    logLL_usbTransmitSync(msg, msgLen);
#endif
  }
#if LOG_GWIN
  if (NULL != g_logLoggerGWin)
  {
    gwinPutCharArray(g_logLoggerGWin, msg, msgLen);
  }
#endif
  if (NULL != suffix)
  {
    const size_t suffixLen = strlen(suffix);
#if LOG_UART
    logLL_uartTransmit(suffix, (uint16_t)suffixLen, flagSleepAllowed);
#endif
#if LOG_USB
    logLL_usbTransmitSync(suffix, suffixLen);
#endif
#if LOG_GWIN
    if (NULL != g_logLoggerGWin)
    {
      gwinPutCharArray(g_logLoggerGWin, suffix, suffixLen);
      osLayer_sleepTicks(g_logDelayAfterPrint);
    }
#endif
  }
}

void logDbg_printf(const char* fmt, ...)
{
#if LOG_UART
  va_list args;
  va_start(args, fmt);
  t_vsnprintf(g_logBuf, sizeof(g_logBuf), fmt, args);
  va_end(args);
  logLL_uartTransmit(g_logBuf, (uint16_t)strlen(g_logBuf), false);
  logLL_uartTransmit("\r\n", 2, false);
#endif
}

static void log_vprintfWithPrefixAndSuffixWithoutLock(const char* fmt, va_list args, const char* const prefix, const char* const suffix)
{
    t_vsnprintf(g_logBuf, sizeof(g_logBuf), fmt, args);
    log_writeMsgWithPrefixAndSuffixWithoutLock(prefix, suffix, g_logBuf, (uint16_t)strlen(g_logBuf), true);
}

#if 0
static void log_printfWithPrefixAndSuffixWithoutLock(const char* const prefix, const char* const suffix, const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log_vprintfWithPrefixAndSuffixWithoutLock(fmt, args, NULL, "\r\n");
    va_end(args);
}
#endif

void log_vprintfWithPrefixAndSuffix(const char* fmt, va_list args, const char* const prefix, const char* const suffix)
{
    log_lock();
    log_vprintfWithPrefixAndSuffixWithoutLock(fmt, args, prefix, suffix);
    log_unlock();
}

void log_printfWithPrefixAndSuffix(const char* const prefix, const char* const suffix, const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  log_vprintfWithPrefixAndSuffix(fmt, args, prefix, suffix);
  va_end(args);
}


void log_vprintf(const char* fmt, va_list args)
{
    log_vprintfWithPrefixAndSuffix(fmt, args, NULL, NULL);
}

void log_setBaud(const unsigned baud)
{
    log_lock();
#if LOG_UART
    logLL_setBaud(baud);
#endif
    log_unlock();
}


void log_printf(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log_vprintfWithPrefixAndSuffix(fmt, args, NULL, NULL);
    va_end(args);
}

void log_vprintfWithBinaryBuffer(const uint8_t* const buf, uint16_t bufLen, const char* fmt, va_list args)
{
    log_lock();
    const int msgPrefixLen = t_vsnprintf(g_logBuf, sizeof(g_logBuf), fmt, args);
#if LOG_UART
    logLL_uartTransmit(g_logBuf, (uint16_t)msgPrefixLen, true);
    logLL_uartTransmit(buf, bufLen, true);
#endif
#if LOG_USB
    logLL_usbTransmitSync(g_logBuf, (uint16_t)msgPrefixLen);
    logLL_usbTransmitSync(buf, bufLen);
#endif
    log_unlock();
}

void log_printfWithBinaryBuffer(const uint8_t* const buf, const uint16_t bufLen, const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log_vprintfWithBinaryBuffer(buf, bufLen, fmt, args);
    va_end(args);
}

void log_printfLine(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log_vprintfWithPrefixAndSuffix(fmt, args, NULL, "\r\n");
    va_end(args);
}

void log_printfLineWithPrefix(const char* prefix, const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log_vprintfWithPrefixAndSuffix(fmt, args, prefix, "\r\n");
    va_end(args);
}


void log_printfEOL(void)
{
     log_printMsgWithPrefix("", "");
}


void log_printMsgWithPrefix(const char* const prefix, const char* const msg)
{
    log_lock();
    log_writeMsgWithPrefixAndSuffixWithoutLock(prefix, "\r\n", msg, (uint16_t)strlen(msg), true);
    log_unlock();
}

static int log_print_prefix(
        char* const buf,
        const size_t bufSize,
        const log_Severity severity,
        const log_Facility facility,
        const char* const file,
        const int line,
        const char* const func
)
{
    int idx = 0;
    {
        osLayer_Ticks_t sysTick = osLayer_getSysTick();
        idx += t_snprintf(&buf[idx], bufSize - idx, "[%6lu]", (unsigned long)sysTick);
    }

#if 0
    {
        const unsigned long procId = log_getCurProcId();
        const unsigned long threadId = log_getCurThreadId();
        idx += t_snprintf(&g_logBuf[idx], sizeof(g_logBuf) - idx, "%lu/%lu; ", procId, threadId);
    }
#endif
    {
      const TaskHandle_t curTaskHandle = xTaskGetCurrentTaskHandle();
      const char* curTaskName = pcTaskGetName(NULL);
      if (NULL == curTaskName)
      {
        curTaskName = "";
      }
      idx += t_snprintf(&buf[idx], bufSize - idx, "[0x%p/%s]", (void*)curTaskHandle, curTaskName);
    }
    {
        const char* severityStr = "";
        if (severity < ZZ_COUNT_OF(g_logSeverityStr))
        {
            severityStr = g_logSeverityStr[severity];
        }
        idx += t_snprintf(&buf[idx], bufSize - idx, "[%s]", severityStr);
    }

    {
        const char* facilityStr = "";
        if (facility < ZZ_COUNT_OF(g_logFacilityStr))
        {
            facilityStr = g_logFacilityStr[facility];
        }
        idx += t_snprintf(&buf[idx], bufSize - idx, "[%s] ", facilityStr);
    }

    if (NULL != func)
    {
        idx += t_snprintf(&buf[idx], bufSize - idx, "%s: ", func);
    }
    if (NULL != file)
    {
        const char* fileName =
#if defined(_WIN32)
                strrchr(file, '\\');
#else
        strrchr(file, '/');
#endif
        if (NULL != fileName)
        {
            fileName++;
        }
        else
        {
            fileName = file;
        }
        idx += t_snprintf(&buf[idx], bufSize - idx, "{%s, line %d}: ", fileName, line);
    }
    return idx;
}

void logDbg_printMsgWithPrefix(
        const log_Severity severity,
        const log_Facility facility,
        const char* const msg)
{
    if (!log_isEnabled(severity, facility))
    {
        return;
    }

    char prefixBuf[32];
    log_print_prefix(prefixBuf, sizeof(prefixBuf), severity, facility, NULL, 0, NULL);

    log_writeMsgWithPrefixAndSuffixWithoutLock(prefixBuf, "\r\n", msg, (uint16_t)strlen(msg), false);
}

void log_vprintfExtWithPrefix(
        const log_Severity severity,
        const log_Facility facility,
        const char* const file,
        const int line,
        const char* const func,
        const char* const prefix,
        const char* const fmt,
        va_list args
)
{
    if (!log_isEnabled(severity, facility))
    {
        return;
    }

    log_lock();

    int idx = log_print_prefix(g_logBuf, sizeof(g_logBuf), severity, facility, file, line, func);

    if (NULL != prefix)
    {
        idx += t_snprintf(&g_logBuf[idx], sizeof(g_logBuf) - idx, "%s", prefix);
    }
    idx += t_vsnprintf(&g_logBuf[idx], sizeof(g_logBuf) - idx, fmt, args);
    idx += t_snprintf(&g_logBuf[idx], sizeof(g_logBuf) - idx, "\r\n");
    log_writeMsgWithPrefixAndSuffixWithoutLock(NULL, NULL, g_logBuf, (uint16_t)idx, true);

    log_unlock();
}

void log_vprintfExt(
        const log_Severity severity,
        const log_Facility facility,
        const char* const file,
        const int line,
        const char* const func,
        const char* const fmt,
        va_list args
)
{
    log_vprintfExtWithPrefix(severity, facility, file, line, func, NULL, fmt, args);
}

ZZ_ATTR_FORMAT_PRINTF(6, 7)
void log_printfExt(
        const log_Severity severity,
        const log_Facility facility,
        const char* const file,
        const int line,
        const char* const func,
        const char* const fmt,
        ...)
{
    va_list args;
    va_start(args, fmt);
    log_vprintfExt(severity, facility, file, line, func, fmt, args);
    va_end(args);
}

void log_dump_vprintfWithPrefix(
    const log_Severity severity,
    const log_Facility facility,
    const char* const file,
    const int line,
    const char* const func,
    const unsigned char* buf,
    const size_t len,
    const char* const prefix,
    const char* const fmt,
    va_list args
)
{
    log_lock();

    if (!log_isEnabled(severity, facility))
    {
        log_unlock();
        return;
    }

    int idx = log_print_prefix(g_logBuf, sizeof(g_logBuf), severity, facility, file, line, func);

    if (NULL != prefix)
    {
        idx += t_snprintf(&g_logBuf[idx], sizeof(g_logBuf) - idx, "%s", prefix);
    }
    idx += t_vsnprintf(&g_logBuf[idx], sizeof(g_logBuf) - idx, fmt, args);
    idx += t_snprintf(&g_logBuf[idx], sizeof(g_logBuf) - idx, "\r\n");
    log_writeMsgWithPrefixAndSuffixWithoutLock(NULL, NULL, g_logBuf, (uint16_t)idx, true);

//    log_printDump(severity, buf, len);

    log_unlock();
}

ZZ_ATTR_FORMAT_PRINTF(8, 9)
void log_dump_printf(
    const log_Severity severity,
    const log_Facility facility,
    const char* const file,
    const int line,
    const char* const func,
    const unsigned char* buf,
    const size_t len,
    const char* const fmt,
    ...
)
{
    va_list args;
    va_start(args, fmt);
    log_dump_vprintfWithPrefix(severity, facility, file, line, func, buf, len, NULL, fmt, args);
    va_end(args);
}

/**********************************************************************************************************************/

/**
 * @brief To redirect printf output for Keil uVision we need to override fputc.
 */
int fputc(int ch, FILE* f)
{
    osLayer_mutexEnter(&g_logMutex);
    logLL_putc((char)ch);
    osLayer_mutexExit(&g_logMutex);
    return ch;
}

/**
 * @brief To redirect printf output for GCC we need to override _write_r.
 */
int _write_r(void* r, int file, const uint8_t* ptr, size_t len)
{
    osLayer_mutexEnter(&g_logMutex);
    while (len--)
    {
        logLL_putc(*ptr++);
    }
    osLayer_mutexExit(&g_logMutex);
    return len;
}



