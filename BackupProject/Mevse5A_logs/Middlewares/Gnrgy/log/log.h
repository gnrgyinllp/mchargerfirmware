#if !defined(LOG_H_)
#define LOG_H_

#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "utils/attribs.h"
#include "log/log_facility_common.h"
#include "log/log_facility_file.h"
#include "log/log_facility_sdcard.h"
#include "log/log_facility_i2c.h"
#include "log/log_facility_hal_i2c.h"
#include "log/log_facility_os_drv_i2c.h"
#include "log/log_facility_spi.h"
#include "log/log_facility_hal_spi.h"
#include "log/log_facility_os_drv_spi.h"
#include "log/log_facility_eeprom.h"
#include "log/log_facility_meter.h"
#include "log/log_facility_charger.h"
#include "log/log_facility_temp.h"
#include "log/log_facility_at_com.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum log_Severity_TAG
{
  LOG_SEVERITY_NONE = 0,
  LOG_SEVERITY_MSG = 1,
  LOG_SEVERITY_CRIT = 2,
  LOG_SEVERITY_ERR = 3,
  LOG_SEVERITY_WARN = 4,
  LOG_SEVERITY_INFO = 5,
  LOG_SEVERITY_DBG = 6,
  LOG_SEVERITY_NOISE = 7,
  LOG_SEVERITY_TRACE = 8,
} log_Severity;

typedef enum log_Facility_TAG
{
  LOG_FACILITY_COMMON = 0,
  LOG_FACILITY_FILE = 1,
  LOG_FACILITY_SDCARD = 2,
  LOG_FACILITY_I2C = 3,
  LOG_FACILITY_HAL_I2C = 4,
  LOG_FACILITY_OS_DRV_I2C = 5,
  LOG_FACILITY_SPI = 6,
  LOG_FACILITY_HAL_SPI = 7,
  LOG_FACILITY_OS_DRV_SPI = 8,
  LOG_FACILITY_EEPROM = 9,
  LOG_FACILITY_METER = 10,
  LOG_FACILITY_CHARGER = 11,
  LOG_FACILITY_TEMP = 12,
  LOG_FACILITY_BLUETOOTH = 13,
  LOG_FACILITY_ATCMD = 14,
  LOG_FACILITY_COUNT_,
} log_Facility;

/**********************************************************************************************************************/

#if LOG_ENABLED
void log_init(void);
#else
static inline void log_init(void) {}
#endif

bool log_lock(void);
void log_unlock(void);

void log_disable(void);
void log_enable(void);

void log_setBaud(const unsigned baud);

void log_setLevel(const log_Facility facility, const log_Severity severity);

bool log_isEnabled(
        const log_Severity severity,
        const log_Facility facility);

void log_addLoggerGWin(void* gh);
void log_setDelayAfterPrint(uint32_t delay_ms);

/**
 * @brief Prints logs on UART and console.
 */
ZZ_ATTR_FORMAT_PRINTF(1, 2)
void log_printf(const char* fmt, ...);

ZZ_ATTR_FORMAT_PRINTF(1, 2)
void log_printfLine(const char* fmt, ...);

ZZ_ATTR_FORMAT_PRINTF(2, 3)
void log_printfLineWithPrefix(const char* prefix, const char* fmt, ...);

void log_printfEOL(void);

void log_vprintfWithPrefixAndSuffix(const char* fmt, va_list args, const char* const prefix, const char* const suffix);
void log_printfWithPrefixAndSuffix(const char* const prefix, const char* const suffix, const char* fmt, ...);

void log_vprintfWithBinaryBuffer(const uint8_t* const buf, uint16_t bufLen, const char* fmt, va_list args);

ZZ_ATTR_FORMAT_PRINTF(3, 4)
void log_printfWithBinaryBuffer(const uint8_t* const buf, const uint16_t bufLen, const char* fmt, ...);

void log_printMsgWithPrefix(const char* const prefix, const char* const msg);

void logDbg_printf(const char* fmt, ...);

void logDbg_printMsgWithPrefix(
        const log_Severity severity,
        const log_Facility facility,
        const char* const msg);

/**
 * @brief Prints logs on UART and console.
 */
void log_vprintf(const char* fmt, va_list args);

void log_vprintfExtWithPrefix(
        const log_Severity severity,
        const log_Facility facility,
        const char* const file,
        const int line,
        const char* const func,
        const char* const prefix,
        const char* const fmt,
        va_list args
);

void log_vprintfExt(
        const log_Severity severity,
        const log_Facility facility,
        const char* const file,
        const int line,
        const char* const func,
        const char* const fmt,
        va_list args);

ZZ_ATTR_FORMAT_PRINTF(6, 7)
void log_printfExt(
        const log_Severity severity,
        const log_Facility facility,
        const char* const file,
        const int line,
        const char* const func,
        const char* const fmt,
        ...);
void log_printDump(
    const log_Severity severity,
    const unsigned char* const buf,
    const size_t bufSize
);

ZZ_ATTR_FORMAT_PRINTF(8, 9)
void log_dump_printf(
    const log_Severity severity,
    const log_Facility facility,
    const char* const file,
    const int line,
    const char* const func,
    const unsigned char* buf,
    const size_t len,
    const char* const fmt,
    ...
);

void log_writeMsgWithPrefixAndSuffixWithoutLock(
        const char* const prefix,
        const char* const suffix,
        const char* msg,
        const uint16_t msgLen,
        const bool flagSleepAllowed);


/**********************************************************************************************************************/

#if LOG_ENABLED

/**
 * @brief Prints log with specified 'severity' for 'facility'
 */
#define LOG_EX(severity_, facility_, file_, line_, func_, fmt_, ...) \
    log_printfExt(severity_, facility_, file_, line_, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with specified 'severity' for 'facility'
 */
#define LOG(severity_, facility_, fmt_, ...) \
    log_printfExt(severity_, facility_, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGV(severity_, facility_, fmt_, args_) \
    log_vprintfExt(severity_, facility_, NULL, 0, NULL, fmt_, args_)

/**
 * @brief Prints log with 'TRACE' severity for specified 'facility'
 */
#define LOG_TRACE(facility_, fmt_, ...)  \
    log_printfExt(LOG_SEVERITY_TRACE, facility_, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'NOISE' severity for specified 'facility'
 */
#define LOG_NOISE(facility_, fmt_, ...)  \
    log_printfExt(LOG_SEVERITY_NOISE, facility_, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOG_DBG(facility_, fmt_, ...)    \
    log_printfExt(LOG_SEVERITY_DBG, facility_, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOGV_DBG(facility_, fmt_, args_)    \
    log_vprintfExt(LOG_SEVERITY_DBG, facility_, NULL, 0, __func__, fmt_, args_)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOG_INFO(facility_, fmt_, ...)   \
    log_printfExt(LOG_SEVERITY_INFO, facility_, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility', also prints __func__
 */
#define LOGF_INFO(facility_, fmt_, ...)   \
    log_printfExt(LOG_SEVERITY_INFO, facility_, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'WARN' severity for specified 'facility'
 */
#define LOG_WARN(facility_, fmt_, ...)   \
    log_printfExt(LOG_SEVERITY_WARN, facility_, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_ERR(facility_, fmt_, ...)    \
    log_printfExt(LOG_SEVERITY_ERR, facility_, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_ERR_FUNC(facility_, func_, fmt_, ...)    \
    log_printfExt(LOG_SEVERITY_ERR, facility_, NULL, 0, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'CRIT' severity for specified 'facility'
 */
#define LOG_CRIT(facility_, fmt_, ...)   \
    log_printfExt(LOG_SEVERITY_CRIT, facility_, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_MSG(facility_, fmt_, ...)    \
    log_printfExt(LOG_SEVERITY_MSG, facility_, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_VMSG(facility_, fmt_, args_)    \
    log_vprintfExt(LOG_SEVERITY_MSG, facility_, NULL, 0, NULL, fmt_, args_)


#define LOG_DUMP(severity_, facility_, buf_, len_, fmt_, ...) \
    log_dump_printf(severity_, facility_, NULL, 0, NULL, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_MSG(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_MSG, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_CRIT(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_CRIT, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_ERR(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_ERR, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_WARN(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_WARN, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_INFO(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_INFO, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_DBG(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_DBG, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_NOISE(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_NOISE, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#define LOG_DUMP_TRACE(facility_, buf_, len_, fmt_, ...) \
    LOG_DUMP(LOG_SEVERITY_TRACE, facility_, buf_, len_, fmt_, ##__VA_ARGS__)

#else

#define LOG_EX(severity_, facility_, file_, line_, func_, fmt_, ...)
#define LOG(severity_, facility_, fmt_, ...)
#define LOGV(severity_, facility_, fmt_, args_)
#define LOG_TRACE(facility_, fmt_, ...)
#define LOG_NOISE(facility_, fmt_, ...)
#define LOG_DBG(facility_, fmt_, ...)
#define LOGV_DBG(facility_, fmt_, args_)
#define LOG_INFO(facility_, fmt_, ...)
#define LOGF_INFO(facility_, fmt_, ...)
#define LOG_WARN(facility_, fmt_, ...)
#define LOG_ERR(facility_, fmt_, ...)
#define LOG_ERR_FUNC(facility_, func_, fmt_, ...)
#define LOG_CRIT(facility_, fmt_, ...)
#define LOG_MSG(facility_, fmt_, ...)
#define LOG_VMSG(facility_, fmt_, args_)

#define LOG_DUMP(severity_, facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_MSG(facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_CRIT(facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_ERR(facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_WARN(facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_INFO(facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_DBG(facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_NOISE(facility_, buf_, len_, fmt_, ...)
#define LOG_DUMP_TRACE(facility_, buf_, len_, fmt_, ...)

#endif /* LOG_ENABLED */

#ifdef __cplusplus
}
#endif

#endif /* LOG_H_ */
