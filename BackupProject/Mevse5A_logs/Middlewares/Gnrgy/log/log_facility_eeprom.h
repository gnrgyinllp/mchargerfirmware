// this file must be include in "log.h"

/*** LOG for FACILITY_EEPROM ******************************************************************************************/

/**
 * @brief Prints log with 'TRACE' severity for specified 'facility'
 */
#define LOG_EEPROM_TRACE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_TRACE, LOG_FACILITY_EEPROM, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'NOISE' severity for specified 'facility'
 */
#define LOG_EEPROM_NOISE(fmt_, ...)  \
    LOG_EX(LOG_SEVERITY_NOISE, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOG_EEPROM_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'DBG' severity for specified 'facility'
 */
#define LOGV_EEPROM_DBG(fmt_, args_)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, args_)

#define LOGF_EEPROM_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_EEPROM_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_EEPROM, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_EEPROM_DBG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_DBG, LOG_FACILITY_EEPROM, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOG_EEPROM_INFO(fmt_, ...)   \
    LOG(LOG_SEVERITY_INFO, LOG_FACILITY_EEPROM, fmt_, ##__VA_ARGS__)

#define LOGF_EEPROM_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_EEPROM_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_EEPROM, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_EEPROM_INFO(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_INFO, LOG_FACILITY_EEPROM, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGP_EEPROM_INFO(prefix_, fmt_, ...)    \
    LOGP(LOG_SEVERITY_INFO, LOG_FACILITY_EEPROM, prefix_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGV_EEPROM_INFO(fmt_, args_)    \
    LOGV(LOG_SEVERITY_INFO, LOG_FACILITY_EEPROM, fmt_, args_)

/**
 * @brief Prints log with 'INFO' severity for specified 'facility'
 */
#define LOGVP_EEPROM_INFO(prefix_, fmt_, args_)    \
    LOGVP(LOG_SEVERITY_INFO, LOG_FACILITY_EEPROM, prefix_, fmt_, args_)

/**
 * @brief Prints log with 'WARN' severity for specified 'facility'
 */
#define LOG_EEPROM_WARN(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_EEPROM, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_EEPROM_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_EEPROM_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_EEPROM, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_EEPROM_WARN(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_WARN, LOG_FACILITY_EEPROM, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_EEPROM_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_EEPROM, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

#define LOGF_EEPROM_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_EEPROM_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_EEPROM, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_EEPROM_ERR(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_EEPROM, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'ERR' severity for specified 'facility'
 */
#define LOG_EEPROM_ERR_FUNC(func_, fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_ERR, LOG_FACILITY_EEPROM, NULL, 0, func_, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'CRIT' severity for specified 'facility'
 */
#define LOG_EEPROM_CRIT(fmt_, ...)   \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_EEPROM, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

#define LOGF_EEPROM_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_EEPROM, NULL, 0, __func__, fmt_, ##__VA_ARGS__)

#define LOGL_EEPROM_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_EEPROM, __FILE__, __LINE__, NULL, fmt_, ##__VA_ARGS__)

#define LOGFL_EEPROM_CRIT(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_CRIT, LOG_FACILITY_EEPROM, __FILE__, __LINE__, __func__, fmt_, ##__VA_ARGS__)

/**
 * @brief Prints log with 'MSG' severity for specified 'facility'
 */
#define LOG_EEPROM_MSG(fmt_, ...)    \
    LOG_EX(LOG_SEVERITY_MSG, LOG_FACILITY_EEPROM, NULL, 0, NULL, fmt_, ##__VA_ARGS__)

