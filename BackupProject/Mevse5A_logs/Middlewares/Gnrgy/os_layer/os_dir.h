#ifndef OS_DIR_H
#define OS_DIR_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "utils/sys_queue1.h"
#include "ff.h"
#include "sdcard/file.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct DirInfo_TAG DirInfo_t;
struct DirInfo_TAG
{
#if OS_FILE_SUPPORT_SUSPEND
  ZTAILQ_ENTRY(DirInfo_TAG) list;
#endif
  char* path;
  DIR* pDir;
  uint32_t curPos;
};

#if OS_FILE_SUPPORT_SUSPEND
void osDir_closeAllOpenedDirsOnSuspend(void);
void osDir_openAllOpenedDirsOnResume(FILINFO* const pFileInfo);
#endif

bool osDir_create(const char* const dirPath);

bool osDir_createIfNotExist(const char* dirPath);

DirInfo_t* osDir_open(const char* const dirPath);

bool osDir_read(
    DirInfo_t* const pDirInfo,
    FILINFO* const pFno
);

bool osDir_close(DirInfo_t* const pDirInfo);

bool osDir_getFirstFile(
    const char* const dirPath,
    FileName_t* const pFileName
);

bool osDir_removeAllFiles(const char* const dirPath);

bool osDir_countFilesRecursively(
    const char* const dirPath,
    uint32_t* const pCnt
);

bool osDir_forEachFileWithCondition(
    const char* const dirPath,
    void* paramInput,
    void (* cb)(
        const char* const fileName,
        const size_t fileSize,
        void* param,
        bool* blCondition
    ),
    bool* blStatus
);

bool osDir_forEachFile(
    const char* const dirPath,
    void (* cb)(
        const char* const fileName,
        const size_t fileSize
    )
);

#ifdef __cplusplus
}
#endif

#endif //OS_DIR_H
