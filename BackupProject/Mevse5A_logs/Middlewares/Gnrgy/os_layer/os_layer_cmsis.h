#ifndef OSLAYER_CMSIS_H
#define OSLAYER_CMSIS_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#if defined(STM32L433xx)
#include "stm32l4xx.h"
#endif
#if defined(STM32F407xx) || defined(STM32F429xx) || defined(STM32F469xx)
#include "stm32f4xx.h"
#endif
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "task.h"
#include "semphr.h"

#ifdef __cplusplus
extern "C" {
#endif

#define OS_LAYER_GPIO_PORT(port_)               port_

#define OS_LAYER_TIME_IMMEDIATE                 0
#define OS_LAYER_TIME_INFINITE                  0x7FFFFFFF

#define OS_LAYER_MS_TO_TICKS(ms_)               (ms_)
#define OS_LAYER_TICKS_TO_MS(ticks_)            (ticks_)

typedef uint32_t osLayer_Ticks_t;
typedef int32_t osLayer_DeltaTicks_t;
typedef int32_t osLayer_DeltaMilliseconds_t;
typedef int32_t osLayer_DeltaSeconds_t;

typedef uint32_t osLayer_DelayUTicks_t;
typedef uint32_t osLayer_DelayUMilliseconds_t;


typedef struct osLayer_Semaphore_TAG
{
    SemaphoreHandle_t handle;
} osLayer_Semaphore_t;

typedef struct osLayer_Mutex_TAG
{
    SemaphoreHandle_t handle;
} osLayer_Mutex_t;

typedef struct osLayer_RecursiveMutex_TAG
{
    SemaphoreHandle_t handle;
} osLayer_RecursiveMutex_t;

typedef struct osLayer_ThreadId_TAG
{
    osThreadId threadId;
} osLayer_ThreadId_t;

typedef void osLayer_ThreadReturn_t;

typedef osLayer_ThreadReturn_t (*osLayer_ThreadFunc_t)(void*);

#define OS_LAYER_DECLARE_THREAD_FUNCTION(fnName, param) \
    osLayer_ThreadReturn_t fnName(void const *param)

#define OS_LAYER_RETURN_FROM_THREAD_FUNCTION() \
    vTaskDelete(NULL); \
    return;

//typedef enum osLayer_ThreadPriority_TAG
//{
//    osLayer_ThreadPriority_Idle = osPriorityIdle,                   ///< priority: idle (lowest)
//    osLayer_ThreadPriority_Low = osPriorityLow,                     ///< priority: low
//    osLayer_ThreadPriority_BelowNormal = osPriorityBelowNormal,     ///< priority: below normal
//    osLayer_ThreadPriority_Normal = osPriorityNormal,               ///< priority: normal (default)
//    osLayer_ThreadPriority_AboveNormal = osPriorityAboveNormal,     ///< priority: above normal
//    osLayer_ThreadPriority_High = osPriorityHigh,                   ///< priority: high
//    osLayer_ThreadPriority_Realtime = osPriorityRealtime,           ///< priority: realtime (highest)
//} osLayer_ThreadPriority_e;

#define OS_LAYER_THREAD_PRIORITY_IDLE           osPriorityIdle
#define OS_LAYER_THREAD_PRIORITY_LOW            osPriorityLow
#define OS_LAYER_THREAD_PRIORITY_BELOWNORMAL    osPriorityBelowNormal
#define OS_LAYER_THREAD_PRIORITY_NORMAL         osPriorityNormal
#define OS_LAYER_THREAD_PRIORITY_ABOVENORMAL    osPriorityAboveNormal
#define OS_LAYER_THREAD_PRIORITY_HIGH           osPriorityHigh
#define OS_LAYER_THREAD_PRIORITY_REALTIME       osPriorityRealtime

typedef osThreadDef_t osLayer_ThreadDef_t;
typedef StackType_t osLayer_StackType_t;
typedef StaticTask_t osLayer_StaticTask_t;

#define OS_LAYER_THREAD_DEF(name_, threadFunc_, priority_, instances_, stacksz_) \
    osThreadDef(name_, threadFunc_, priority_, instances_, stacksz_)

#define OS_LAYER_THREAD_STATIC_DEF(name_, threadFunc_, priority_, instances_, stacksz_, stackBuf_, ctrlBlock_) \
    osThreadStaticDef(name_, threadFunc_, priority_, instances_, stacksz_, stackBuf_, ctrlBlock_)


#define OS_LAYER_THREAD(name_)  \
    osThread(name_)

/**********************************************************************************************************************/

static inline osLayer_DeltaTicks_t osLayer_getTicksPerSec(void)
{
    return 1000;
}

static inline osLayer_DelayUMilliseconds_t osLayer_convDeltaTicksToDelayUMilliseconds(
    const osLayer_DeltaTicks_t deltaTicks
)
{
  osLayer_DelayUMilliseconds_t delta_ms = 0;
  if (OS_LAYER_TIME_INFINITE == deltaTicks)
  {
    delta_ms = osWaitForever;
  }
  else if (deltaTicks < 0)
  {
    delta_ms = 0;
  }
  else
  {
    delta_ms = (osLayer_DelayUMilliseconds_t)OS_LAYER_TICKS_TO_MS(deltaTicks);
  }
  return delta_ms;
}

static inline osLayer_DelayUTicks_t osLayer_convDeltaTicksToDelayUTicks(
    const osLayer_DeltaTicks_t deltaTicks
)
{
  osLayer_DelayUTicks_t deltaUTicks = 0;
  if (OS_LAYER_TIME_INFINITE == deltaTicks)
  {
    deltaUTicks = HAL_MAX_DELAY;
  }
  else if (deltaTicks < 0)
  {
    deltaUTicks = 0;
  }
  else
  {
    deltaUTicks = (osLayer_DelayUTicks_t)deltaTicks;
  }
  return deltaUTicks;
}

/**********************************************************************************************************************/

static inline osLayer_ThreadId_t osLayer_getCurThreadId(void)
{
  const osLayer_ThreadId_t threadId = {
      .threadId = osThreadGetId()
  };
  return threadId;
}

static inline bool osLayer_semaInit(
        osLayer_Semaphore_t* const pSema
)
{
    pSema->handle = xSemaphoreCreateBinary();
    return (NULL != pSema->handle) ? true : false;
}

static inline void osLayer_semaDeinit(
    osLayer_Semaphore_t* const pSema)
{
    vSemaphoreDelete(pSema->handle);
}

static inline bool osLayer_semaWait(
        osLayer_Semaphore_t* const pSema,
        const osLayer_DeltaTicks_t deltaTicks
)
{
    return (bool)xSemaphoreTake(pSema->handle, osLayer_convDeltaTicksToDelayUTicks(deltaTicks));
}

static inline bool osLayer_semaWaitInfinite(
        osLayer_Semaphore_t* const pSema
)
{
    return osLayer_semaWait(pSema, OS_LAYER_TIME_INFINITE);
}

static inline bool osLayer_semaWaitImmediate(
    osLayer_Semaphore_t* const pSema)
{
    return osLayer_semaWait(pSema, OS_LAYER_TIME_IMMEDIATE);
}

static inline bool osLayer_semaSignal(
    osLayer_Semaphore_t* const pSema)
{
    return (bool)xSemaphoreGive(pSema->handle);
}

static inline bool osLayer_semaSignalFromISR(
    osLayer_Semaphore_t* const pSema)
{
    return (bool)xSemaphoreGiveFromISR(pSema->handle, pdFALSE);
}

static inline bool osLayer_mutexInit(
        osLayer_Mutex_t* const pMutex
)
{
    pMutex->handle = xSemaphoreCreateMutex();
    return (NULL != pMutex->handle) ? true : false;
}

static inline void osLayer_mutexDeinit(osLayer_Mutex_t* const pMutex)
{
    vSemaphoreDelete(pMutex->handle);
}

static inline bool osLayer_mutexWaitWithTimeout(
    osLayer_Mutex_t* const pMutex,
    const osLayer_DeltaTicks_t timeoutTicks)
{
    return xSemaphoreTake(pMutex->handle, osLayer_convDeltaTicksToDelayUTicks(timeoutTicks)) == pdTRUE;
}

static inline bool osLayer_mutexEnter(osLayer_Mutex_t* const pMutex)
{
    return osLayer_mutexWaitWithTimeout(pMutex, OS_LAYER_TIME_INFINITE);
}

static inline bool osLayer_mutexExit(osLayer_Mutex_t* const pMutex)
{
    return (xSemaphoreGive(pMutex->handle) == pdTRUE) ? true : false;
}

static inline bool osLayer_recursiveMutexInit(
        osLayer_RecursiveMutex_t* const pMutex
)
{
    pMutex->handle = xSemaphoreCreateRecursiveMutex();
    return (NULL != pMutex->handle) ? true : false;
}

static inline void osLayer_recursiveMutexDeinit(osLayer_RecursiveMutex_t* const pMutex)
{
    vSemaphoreDelete(pMutex->handle);
}

static inline bool osLayer_recursiveMutexWaitWithTimeout(
      osLayer_RecursiveMutex_t* const pMutex,
      const osLayer_DeltaTicks_t timeoutTicks)
{
    return xSemaphoreTakeRecursive(pMutex->handle, osLayer_convDeltaTicksToDelayUTicks(timeoutTicks)) == pdTRUE;
}

static inline bool osLayer_recursiveMutexEnter(osLayer_RecursiveMutex_t* const pMutex)
{
    return osLayer_recursiveMutexWaitWithTimeout(pMutex, OS_LAYER_TIME_INFINITE);
}

static inline bool osLayer_recursiveMutexExit(osLayer_RecursiveMutex_t* const pMutex)
{
    return (xSemaphoreGiveRecursive(pMutex->handle) == pdTRUE) ? true : false;
}

static inline osLayer_Ticks_t osLayer_getSysTick(void)
{
    return HAL_GetTick();
}

static inline void osLayer_sleepTicks(osLayer_DeltaTicks_t deltaTicks)
{
    vTaskDelay(osLayer_convDeltaTicksToDelayUTicks(deltaTicks));
}

#ifdef __cplusplus
}
#endif

#endif //OSLAYER_CMSIS_H
