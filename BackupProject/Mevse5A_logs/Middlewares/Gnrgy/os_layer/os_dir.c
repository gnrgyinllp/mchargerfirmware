#include "os_dir.h"
#include <string.h>
#include "ff.h"
#include "sdcard/file.h"
#include "log/log.h"
#include "utils/t_snprintf.h"
#include "utils/sys_queue1.h"

#if OS_FILE_SUPPORT_SUSPEND
static ZTAILQ_HEAD(FileHeadOfListOfOpenedDirs, DirInfo_TAG) g_fileListOfOpenedDirs;

void osDir_closeAllOpenedDirsOnSuspend(void)
{
  DirInfo_t* pDirInfo = NULL;
  ZTAILQ_FOREACH(pDirInfo, &g_fileListOfOpenedDirs, list)
  {
    LOG_FILE_INFO("SUSPEND: close dir %s at cnt %u", pDirInfo->path, (unsigned)pDirInfo->curPos);
    const FRESULT fres = f_closedir(pDirInfo->pDir);
    if (FR_OK != fres)
    {
      LOG_FILE_ERR("Can't close dir %s: %s", pDirInfo->path, fileErrInfo(fres));
    }
  }
}

void osDir_openAllOpenedDirsOnResume(FILINFO* const pFileInfo)
{
  DirInfo_t* pDirInfo = NULL;
  ZTAILQ_FOREACH(pDirInfo, &g_fileListOfOpenedDirs, list)
  {
    {
      LOG_FILE_INFO("SUSPEND: reopen dir %s at cnt %u", pDirInfo->path, (unsigned)pDirInfo->curPos);
#if defined(_WIN32)
      pDirInfo->pDir = win_opendir(pDirInfo->path);
        if (NULL == pDirInfo->pDir)
        {
          LOG_FILE_ERR("Can't open dir: %s: errno=%d", pDirInfo->path, errno);
          continue;
        }
#else
      const FRESULT fres = f_opendir(pDirInfo->pDir, pDirInfo->path);
      if (FR_OK != fres)
      {
        LOG_FILE_ERR("Can't reopen dir '%s': %s", pDirInfo->path, fileErrInfo(fres));
        continue;
      }
#endif
    }
    for (unsigned i = 0; i < pDirInfo->curPos; ++i)
    {
      f_readdir(pDirInfo->pDir, pFileInfo);
    }
  }
}
#endif

bool osDir_create(const char* const dirPath)
{
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_mkdir(dirPath);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOG_FILE_ERR("Can't create dir %s: %s", dirPath, fileErrInfo(fres));
    return false;
  }
  return true;
}

bool osDir_createIfNotExist(const char* dirPath)
{
  bool flagIsDir = false;
  if (!fileIsExistEx(dirPath, &flagIsDir))
  {
    LOG_SDCARD_INFO("directory doesn't exist: %s", dirPath);
    if (!osDir_create(dirPath))
    {
      return false;
    }
    LOG_SDCARD_INFO("directory %s was created successfully", dirPath);
  }
  else
  {
    if (flagIsDir)
    {
      LOG_SDCARD_INFO("directory already exists: %s", dirPath);
    }
    else
    {
      LOG_SDCARD_INFO("file instead of dir exists: %s", dirPath);
      fileRemoveEx(dirPath);
      if (!osDir_create(dirPath))
      {
        return false;
      }
      LOG_SDCARD_INFO("directory %s was created successfully", dirPath);
    }
  }
  return true;
}

DirInfo_t* osDir_open(const char* const dirPath)
{
  LOG_FILE_NOISE("Open dir %s", dirPath);
  DirInfo_t* const pDirInfo = osLayer_malloc(sizeof(*pDirInfo) + strlen(dirPath) + 1);
  if (NULL == pDirInfo)
  {
    LOG_FILE_ERR("Can't allocate memory for DirInfo: %s", dirPath);
    return NULL;
  }
  pDirInfo->path = (char*)(pDirInfo + 1);
  strcpy(pDirInfo->path, dirPath);
  pDirInfo->curPos = 0;
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
#if defined(_WIN32)
  pDirInfo->pDir = win_opendir(dirPath);
  if (NULL == pDirInfo->pDir)
  {
    osLayer_free(pDirInfo);
    fileUnlock();
    LOG_FILE_ERR("Can't open dir: %s: errno=%d", dirPath, errno);
    return NULL;
  }
#else
  pDirInfo->pDir = osLayer_malloc(sizeof(*pDirInfo->pDir));
  if (NULL == pDirInfo->pDir)
  {
    osLayer_free(pDirInfo);
    fileUnlock();
    LOG_FILE_ERR("Can't allocate memory for DIR: %s", dirPath);
    return NULL;
  }
  const FRESULT fres = f_opendir(pDirInfo->pDir, dirPath);
  if (FR_OK != fres)
  {
    osLayer_free(pDirInfo);
    fileUnlock();
    LOG_FILE_ERR("Can't open dir: %s: %s", dirPath, fileErrInfo(fres));
    return NULL;
  }
#endif
#if OS_FILE_SUPPORT_SUSPEND
  ZTAILQ_INSERT_TAIL(&g_fileListOfOpenedDirs, pDirInfo, list);
#endif
  fileUnlock();
  return pDirInfo;
}

bool osDir_read(
    DirInfo_t* const pDirInfo,
    FILINFO* const pFno
)
{
  LOG_FILE_NOISE("Close dir %s", pDirInfo->path);
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_readdir(pDirInfo->pDir, pFno);
  if (FR_OK != fres)
  {
    fileUnlock();
    LOG_FILE_ERR("Can't read dir: %s", fileErrInfo(fres));
    return false;
  }
  pDirInfo->curPos += 1;
  fileUnlock();
  return true;
}

bool osDir_close(DirInfo_t* const pDirInfo)
{
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
#if OS_FILE_SUPPORT_SUSPEND
  ZTAILQ_REMOVE(&g_fileListOfOpenedDirs, pDirInfo, list);
#endif
  const FRESULT fres = f_closedir(pDirInfo->pDir);
#if !defined(_WIN32)
  osLayer_free(pDirInfo->pDir);
#endif
  osLayer_free(pDirInfo);
  fileUnlock();
  if (FR_OK != fres)
  {
    LOG_FILE_ERR("Can't close dir: %s", fileErrInfo(fres));
    return false;
  }
  return true;
}

bool osDir_getFirstFile(
    const char* const dirPath,
    FileName_t* const pFileName
)
{
  DirInfo_t* const pDirInfo = osDir_open(dirPath);
  if (NULL == pDirInfo)
  {
    LOG_SDCARD_WARN("can't open directory %s", dirPath);
    return false;
  }
  FILINFO* const pFileInfo = osLayer_calloc(1, sizeof(pFileInfo));
  if (NULL == pFileInfo)
  {
    osDir_close(pDirInfo);
    LOG_SDCARD_ERR("can't allocate meme");
    return false;
  }
  bool ret = false;
  for (;;)
  {
    if (!osDir_read(pDirInfo, pFileInfo))
    {
      LOG_SDCARD_WARN("read dir %s failed", dirPath);
      break;
    }
    if (pFileInfo->fname[0] == 0) // last file
    {
      break;
    }
    if (pFileInfo->fname[0] == '.')
    {
      continue;
    }
    if (pFileInfo->fattrib & AM_DIR)
    {
      continue;
    }
    const char* const fname = pFileInfo->fname[0] ? pFileInfo->fname : pFileInfo->altname;
    if (NULL != pFileName)
    {
      t_snprintf(pFileName->buf, sizeof(pFileName->buf), "%s/%s", dirPath, fname);
    }
    ret = true;
    break;
  }
  osLayer_free(pFileInfo);
  if (!osDir_close(pDirInfo))
  {
    LOG_SDCARD_WARN("failed on closing dir %s", dirPath);
    return false;
  }
  return ret;
}

bool osDir_removeAllFiles(const char* const dirPath)
{
  FileName_t filePath = {{0}};
  while (osDir_getFirstFile(dirPath, &filePath))
  {
    if (!fileRemove(&filePath))
    {
      return false;
    }
  }
  return true;
}

typedef struct OsDirCountFilesRecursively_TAG
{
  FILINFO* pFileInfo;
  int level;
  uint32_t cnt;
  FileName_t path;
} OsDirCountFilesRecursively_t;

static bool osDir_countFilesRecursively_(
    OsDirCountFilesRecursively_t* const pCntInfo
)
{
  FILINFO* const pFileInfo = pCntInfo->pFileInfo;
  static const char* const spaces = "                                             ";
  bool ret = false;

  pCntInfo->level += 1;

  DirInfo_t* const pDirInfo = osDir_open(pCntInfo->path.buf);
  if (NULL == pDirInfo)
  {
    LOG_COMMON_INFO("can't open directory %s", pCntInfo->path.buf);
    pCntInfo->level -= 1;
    return false;
  }
  for (;;)
  {
    if (!osDir_read(pDirInfo, pFileInfo))
    {
      LOG_COMMON_INFO("read dir %s failed", pCntInfo->path.buf);
      break;
    }
    if (pFileInfo->fname[0] == 0) // last file
    {
      ret = true;
      break;
    }
    if (pFileInfo->fname[0] == '.')
    {
      continue;
    }
    const char* const fname = pFileInfo->fname[0] ? pFileInfo->fname : pFileInfo->altname;

    if (pFileInfo->fattrib & AM_DIR)
    {
      LOG_COMMON_INFO("%.*s%s/%s/", pCntInfo->level * 2, spaces, pCntInfo->path.buf, fname);
      if ((strlen(pCntInfo->path.buf) + 1 + strlen(fname) + 1) > sizeof(pCntInfo->path.buf))
      {
        LOG_COMMON_INFO("Path buffer too small");
        break;
      }
      strcat(pCntInfo->path.buf, "/");
      strcat(pCntInfo->path.buf, fname);

      const bool tmpRes = osDir_countFilesRecursively_(pCntInfo);
      char* p = strrchr(pCntInfo->path.buf, '/');
      if (NULL != p)
      {
        *p = '\0';
      }
      if (!tmpRes)
      {
        break;
      }
    }
    else
    {
      pCntInfo->cnt += 1;
      LOG_COMMON_INFO("%.*s%s/%s", (pCntInfo->level - 1) * 2, spaces, pCntInfo->path.buf, fname);
    }
  }
  pCntInfo->level -= 1;
  if (!osDir_close(pDirInfo))
  {
    LOG_SDCARD_INFO("failed on closing dir %s", pCntInfo->path.buf);
    return false;
  }
  return ret;
}

bool osDir_countFilesRecursively(
    const char* const dirPath,
    uint32_t* const pCnt
)
{
  FILINFO* const pFileInfo = osLayer_calloc(1, sizeof(pFileInfo));
  if (NULL == pFileInfo)
  {
    LOG_SDCARD_ERR("can't allocate meme");
    return false;
  }
  OsDirCountFilesRecursively_t cntFiles = {
      .pFileInfo = pFileInfo,
      .level = 0,
      .cnt = 0,
      .path.buf = {0},
  };
  strncpy(cntFiles.path.buf, dirPath, sizeof(cntFiles.path.buf));
  cntFiles.path.buf[sizeof(cntFiles.path.buf) - 1] = '\0';

  const bool res = osDir_countFilesRecursively_(&cntFiles);
  osLayer_free(pFileInfo);
  *pCnt = cntFiles.cnt;
  return res;
}

bool osDir_forEachFileWithCondition(
    const char* const dirPath,
    void* paramInput,
    void (* cb)(
        const char* const fileName,
        const size_t fileSize,
        void* param,
        bool* blCondition
    ),
    bool* blStatus
)
{
  bool ret = false;
  bool blValid = false;

  DirInfo_t* const pDirInfo = osDir_open(dirPath);
  if (NULL == pDirInfo)
  {
    LOG_SDCARD_WARN("can't open directory %s", dirPath);
    return false;
  }
  FILINFO* const pFileInfo = osLayer_calloc(1, sizeof(pFileInfo));
  if (NULL == pFileInfo)
  {
    osDir_close(pDirInfo);
    LOG_SDCARD_ERR("can't allocate meme");
    return false;
  }
  for (;;)
  {
    if (!osDir_read(pDirInfo, pFileInfo))
    {
      LOG_SDCARD_WARN("read dir %s failed", dirPath);
      break;
    }
    if (pFileInfo->fname[0] == 0) // last file
    {
      ret = true;
      break;
    }
    if (pFileInfo->fname[0] == '.')
    {
      continue;
    }
    const char* const fname = pFileInfo->fname[0] ? pFileInfo->fname : pFileInfo->altname;

    if (pFileInfo->fattrib & AM_DIR)
    {
      continue;
    }
    cb(fname, pFileInfo->fsize, paramInput, &blValid);
    if (blValid)
    {
      *blStatus = true;
      ret = true;
      break;
    }
  }
  osLayer_free(pFileInfo);
  if (!osDir_close(pDirInfo))
  {
    LOG_SDCARD_WARN("failed on closing dir %s", dirPath);
    return false;
  }
  return ret;
}

typedef struct OsDirForEachFileInfo_TAG
{
  void (*cb)( const char* const fileName, const size_t fileSize);
} OsDirForEachFileInfo_t;

static void osDir_forEachFileCallback(
    const char* const fileName,
    const size_t fileSize,
    void* param,
    bool* blCondition
)
{
  OsDirForEachFileInfo_t* const pInfo = param;
  pInfo->cb(fileName, fileSize);
  *blCondition = false;
}

bool osDir_forEachFile(
    const char* const dirPath,
    void (* cb)(
        const char* const fileName,
        const size_t fileSize
    )
)
{
  OsDirForEachFileInfo_t info = {
      .cb = cb,
  };
  bool status = false;
  return osDir_forEachFileWithCondition(dirPath, &info, &osDir_forEachFileCallback, &status);
}

bool osDir_CurrentPWD(char* path)
{
#define PATH_SIZE 128
  LOG_FILE_NOISE("Read current PWD %s", path);
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_getcwd(path, 128);
  if (FR_OK != fres)
  {
    fileUnlock();
    LOG_FILE_ERR("Can't get dir: %s", fileErrInfo(fres));
    return false;
  }
  fileUnlock();
  return true;
}

bool osDir_SetCD(char* path)
{
#define PATH_SIZE 128
  LOG_FILE_NOISE("Set path %s", path);
  if (!fileLock())
  {
    LOG_FILE_ERR("fileLock failed");
    return false;
  }
  const FRESULT fres = f_chdir(path);
  if (FR_OK != fres)
  {
    fileUnlock();
    LOG_FILE_ERR("Can't set path: %s", fileErrInfo(fres));
    return false;
  }
  fileUnlock();
  return true;
}

bool osDir_listCurrentDirectory(const char* const dirPath)
{
  bool ret = false;
  bool blValid = false;
  uint16_t len = 0;
  DirInfo_t* const pDirInfo = osDir_open(dirPath);
  if (NULL == pDirInfo)
  {
    LOG_SDCARD_WARN("can't open directory %s", dirPath);
    return false;
  }
  
  FILINFO* const pFileInfo = osLayer_calloc(1, sizeof(pFileInfo));
  if (NULL == pFileInfo)
  {
    osDir_close(pDirInfo);
    LOG_SDCARD_ERR("can't allocate meme");
    return false;
  }
  
  for (;;)
  {
    if (!osDir_read(pDirInfo, pFileInfo))
    {
      LOG_SDCARD_WARN("read dir %s failed", dirPath);
      break;
    }
    if (pFileInfo->fname[0] == 0) // last file
    {
      ret = true;
      break;
    }
    if (pFileInfo->fname[0] == '.')
    {
      continue;
    }
    const char* const fname = pFileInfo->fname[0] ? pFileInfo->fname : pFileInfo->altname;
    
    if (pFileInfo->fattrib & AM_DIR)
    {
      len = strlen(dirPath);
      LOG_COMMON_INFO(dirPath);
      continue;
    }
    else
    {
      LOG_COMMON_INFO("%s",pFileInfo->fname);
    }
  }
  osLayer_free(pFileInfo);
  if (!osDir_close(pDirInfo))
  {
    LOG_SDCARD_WARN("failed on closing dir %s", dirPath);
    return false;
  }
  return ret;
}