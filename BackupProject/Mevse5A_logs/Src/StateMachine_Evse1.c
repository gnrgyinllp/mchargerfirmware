
/**
******************************************************************************
* @file    StateMachine_Evse1.c
* @author  Josep Pinto
* @version V1.0.0
* @date    1-Jul-2017
* @brief   Main State Machine for the EVSE controller 
*          
******************************************************************************
* 
*
******************************************************************************
*/ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "EVSEtest1.h"
#include "stm32f4xx_hal.h"
#include "FreeRTOS.h"
#include "StateMachine.h"
#include <stdbool.h>
#include "log/log.h"


extern void Lock(void);
extern void UnLock(void);

extern uint8_t         CP_FinalStatus;    
extern RTC_HandleTypeDef hrtc;
extern char             RfidEE[2],AudiEE[2];
extern MainState_Tag g_MainState;
TickType_t      xTimeNowEvse,XtimeWhiteLED = 0;
TickType_t      g_ulSystemTime,g_ulSystemPreviousTimestamp,g_ulSystemPreviousTimestampBlinking=0;
TickType_t      g_ulSystemPreviousTimestampBuzzer=0;
//TickType_t      ChargerStateTimePassed;
uint8_t         WhiteLedState = 0;
uint8_t         WhiteLedDuty = 0;
uint8_t         IdAllowed = 1;
uint8_t         state_charger,state_vehicle, pre_charger;
uint8_t         state_vehicle_pending;
uint8_t         state_led_white;
uint8_t         state_led_blue;
uint8_t         state_led_red;
uint8_t         WakeUp = 0;
uint8_t         Buzzer_State=BEEP_OFF,Buzzer_ONOFF=0,Buzzer_Counter = 0, Buzzer_Alert = 1;
uint8_t         isBlinkLedStateOn = 1;
uint16_t        Current_Charge, Max_Current_Charge = MAX_CURRENT_ALLOWED,Init_Current_Charge = INITIAL_CURRENT_CHARGE;
uint8_t         Contactor_State = 0;
uint32_t        EnergyCounter = 0;
GPIO_PinState   Contactor_Mon;
SessionObject   currentSessionObject;
bool            error_status[MAX_NUM_ERROR];



/*
WhiteLedFlash:
funtion to make white led flashing like H1 and VW and VP 
REALTIME_PERIOD_200_MS is the time it takes each time to change the pwm to increase or decreasea brightness
WHITELEDDUTYADDITION is the amount of duty cycle that is changed the pwm for brightnes every REALTIME_PERIOD_200_MS
WHITELEDMAXDUTY is the Max duty cycle that reaches the brightness
*/
void WhiteLedFlash(void){    
  xTimeNowEvse = xTaskGetTickCount();
  WhiteLedTurnON();
  if((xTimeNowEvse-XtimeWhiteLED)>= REALTIME_PERIOD_200_MS){
    XtimeWhiteLED = xTimeNowEvse;
    if (WhiteLedState == 0){      
      WhiteLedSetDuty(WhiteLedDuty);
      WhiteLedDuty = WhiteLedDuty + WHITELEDDUTYADDITION;
      if (WhiteLedDuty > WHITELEDMAXDUTY){
        WhiteLedDuty = WhiteLedDuty - (2 * WHITELEDDUTYADDITION);
        WhiteLedState = 1;
      }
    }
    else if (WhiteLedState == 1){  
      WhiteLedSetDuty(WhiteLedDuty);
      WhiteLedDuty = WhiteLedDuty - WHITELEDDUTYADDITION;
      if (WhiteLedDuty == 0){         
        WhiteLedState = 0;
      }
    }
  }  
}

/*
CheckLedsState:
It is called in a low priority task in order to drive the leds behaviour
- WHITE (ON, OFF, CHARGING_FLASH)
- RED (ERROR ON, OFF)
- BLUE (CONNECTED ON, OFF)
*/
void CheckLedsState(void)
{
  // In case we need to use pwm in the white led 
  if(state_led_white == LED_CHARGE_FLASH) {
    WhiteLedFlash();
  }  
  g_ulSystemTime = xTaskGetTickCount();
  if ((g_ulSystemTime - g_ulSystemPreviousTimestampBlinking) >= LED_BLINK_PERIOD_MS)
  {
    g_ulSystemPreviousTimestampBlinking = g_ulSystemTime;
    isBlinkLedStateOn = !isBlinkLedStateOn;
    if (state_led_white == LED_ON || 
        (state_led_white == LED_BLINK && isBlinkLedStateOn))
    {
      WhiteLedTurnON();
    }
    else if (state_led_white == LED_OFF || 
             (state_led_white == LED_BLINK && !isBlinkLedStateOn))
    {
      WhiteLedTurnOFF();
    }
    
    if (state_led_blue == LED_ON || 
        (state_led_blue == LED_BLINK && isBlinkLedStateOn))
    {
      BlueLedTurnON();
    }
    else if (state_led_blue == LED_OFF || 
             (state_led_blue == LED_BLINK && !isBlinkLedStateOn))
    {
      BlueLedTurnOFF();
    }
    
    if (state_led_red == LED_ON || 
        (state_led_red == LED_BLINK && isBlinkLedStateOn))
    {
      RedLedTurnON();
    }
    else if (state_led_red == LED_OFF || 
             (state_led_red == LED_BLINK && !isBlinkLedStateOn))
    {
      RedLedTurnOFF();
    }
  }
}

/*
CheckBuzzerState:
It is called in a low priority task in order to drive the Buzzer signaling
- LONG BEEP
- SHORT BEEP (EV CONNECTED)
- REPEATED SHORT BEED (ERROR)
*/
void CheckBuzzerState(void)
{
  g_ulSystemTime = xTaskGetTickCount();
  
  if (Buzzer_State == BEEP_LONG){
    if (Buzzer_ONOFF == 0){
      g_ulSystemPreviousTimestampBuzzer = g_ulSystemTime;
      Buzzer_ONOFF = 1;
      Buzzer_ON();
    }else{
      if((g_ulSystemTime - g_ulSystemPreviousTimestampBuzzer)> BUZZER_LONG_PERIOD_MS){        
        Buzzer_ONOFF = 0;
        Buzzer_OFF();
        Buzzer_State = BEEP_OFF;
      }        
    }
  }
  else if (Buzzer_State == BEEP_SHORT){ 
    if (Buzzer_ONOFF == 0){
      g_ulSystemPreviousTimestampBuzzer = g_ulSystemTime;
      Buzzer_ONOFF = 1;
      Buzzer_ON();
    }else{
      if((g_ulSystemTime - g_ulSystemPreviousTimestampBuzzer)> BUZZER_SHORT_PERIOD_MS){        
        Buzzer_ONOFF = 0;
        Buzzer_OFF();
        Buzzer_State = BEEP_OFF;
      }        
    }     
  }
  else if (Buzzer_State == BEEP_ALERT){ 
    if (Buzzer_Counter <= BUZZER_BEEP_ALERT_MAX_COUNT){       
      if (Buzzer_ONOFF == 0){
        g_ulSystemPreviousTimestampBuzzer = g_ulSystemTime;
        Buzzer_ONOFF = 1;        
      }else{
        if((g_ulSystemTime - g_ulSystemPreviousTimestampBuzzer)> BUZZER_SHORT_PERIOD_MS){        
          Buzzer_ONOFF = 0;
          if (Buzzer_Alert == 0){
            Buzzer_OFF();
            Buzzer_Alert = 1;
          } else {
            Buzzer_ON();
            Buzzer_Alert = 0;
            Buzzer_Counter ++;
          }           
        }        
      }     
    } else {       
      Buzzer_OFF(); 
      Buzzer_ONOFF = 0;
      Buzzer_Counter = 0;
      Buzzer_Alert = 0;
      Buzzer_State = BEEP_OFF; 
    } 
  } else {
    Buzzer_State = BEEP_OFF;   
    Buzzer_ONOFF = 0;
    Buzzer_Counter = 0;
    Buzzer_OFF();
    Buzzer_Alert = 0;   
  }
}


/*
CurrentToDutyCycle:
To translate the Current asked to the duty cycle value
*/
uint16_t CurrentToDutyCycle(uint16_t i_Current)
{
  uint16_t result;  
  if (i_Current >= 6ul && i_Current <= 51ul)  {
    result = i_Current * 10ul / 6ul;  
  }
  else if (i_Current > 51ul && i_Current <= 80ul)
  {
    result = (i_Current * 2ul / 5ul) + 64ul;
  }
  return result;
}


/*
InitEvseStateMachine:
Initalization of the StateMachine of the EVSE
+ 12V steady
+ No Beeper
+ No Led 
+ state_charger = STATESCHARGER_READY
+ Current_Charge = to the initial charging current
*/
void InitEvseStateMachine(void)
{  
  Buzzer_State = BEEP_OFF;    
  state_led_white = LED_OFF; 
  state_led_blue = LED_OFF;
  PILOT_12V();
  WakeUp = 0;
  state_charger = STATESCHARGER_READY;
  //state_vehicle = STATESVEHICLE_NONE;
  //IdAllowed = 1;
  Current_Charge = Init_Current_Charge;
}

/*
CP_State_Machine_Handler:
Basic function to handle the charging protocol between the Car and the Charger

*/
void CP_State_Machine_Handler( uint8_t CP_new_State ){ 
  
  
  state_vehicle = CP_new_State;    
  // check the Contactor to know if it is open(0) or close(1)
  Contactor_Mon = HAL_GPIO_ReadPin(CONTACTOR_MON_GPIO_Port, CONTACTOR_MON_Pin);//ReadContactorMonitoring();
  
#ifdef POWER_FAILURE  
  //readADC2Value();
#endif  
  //Checking the if the signal is minus 12V to set an error
  // only in case we are in a wake up state, where we force the minus 12V during 3 seconds
  // State F would be allowed
  if ((state_vehicle == STATE_F) && (state_charger != STATESCHARGER_WAKEUP))
  {
    //SetError(ERROR_IN_VEHICLE);
  }
  // In oder case, we start the state machine rutine
  else
  {
    ClearError(ERROR_IN_VEHICLE);
    ClearError(ERROR_EVSE_NOT_AVAILABLE);
    
    //checking the Charger State
#if 0
    if (state_charger!= pre_charger)
    {
      LOG_COMMON_INFO("State Charger: %d %d", state_charger, state_vehicle);
      pre_charger = state_charger;
    }
#endif
    switch (state_charger)
    {
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_READY.
      State A
      12V Steady
      It is the iddle state when no car is connected to the charger
      and no error occurs. 
      So waiting for a car connection
      *//////////////////////////////////////////////////////////////
    case STATESCHARGER_READY:        
#ifdef MELT     
      
      while (Contactor_Mon == GPIO_PIN_SET){
        //state_led_red = LED_ON;
        PILOT_minus_12V();             
        SetError(ERROR_MELT_CONTACTOR);
        Contactor_Mon = HAL_GPIO_ReadPin(CONTACTOR_MON_GPIO_Port, CONTACTOR_MON_Pin);//ReadContactorMonitoring();
        osDelay(1);
      }
      //state_led_red = LED_OFF; 
      PILOT_12V(); 
      ClearError(ERROR_MELT_CONTACTOR);
      
#endif      
      //Only if it is allowed(when rfid or blueetoth mobile application access is OK
      //We can start the process
      if (IdAllowed == 1){
        // Checking the CP line we know if the car is connected in regular mode (9v Steady) or basic mode (6v Steady)
        if ((state_vehicle == STATE_B1)||(state_vehicle == STATE_C0))//STATESVEHICLE_CONNECTED)
        {          
          state_led_red = LED_OFF;
          state_led_white = LED_OFF;
          state_led_blue = LED_ON;
          Buzzer_State = BEEP_SHORT;//BEEP_ALERT;//BEEP_LONG;//BEEP_SHORT;
          SetStateCharger(STATESCHARGER_DELAY);
          ClearError(ERROR_CPTOGND);
        }
        if (state_vehicle == STATE_E){
          SetStateCharger(STATESCHARGER_CPTOGND);
          SetError(ERROR_CPTOGND);
          osDelay(1);
        } 
        
      } else if(IdAllowed == 0) {
        
        if ((state_vehicle == STATE_B1)||(state_vehicle == STATE_C0))//STATESVEHICLE_CONNECTED)
        {          
          state_led_red = LED_OFF;
          state_led_white = LED_OFF;
          state_led_blue = LED_BLINK;
        }
        if (state_vehicle == STATE_A){
          SetStateCharger(STATESCHARGER_POSTCHARGING);
          state_led_blue = LED_OFF;
          
        }
        
        
      }
      break;
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_DELAY.
      State B1 or State C0
      In this State we wait for les than 1sec (DELAY_STAGE_CS) with a steady signal 9v or 6V        
      *//////////////////////////////////////////////////////////////    
    case STATESCHARGER_DELAY:           
      if (ChargerStateTimePassed() >= DELAY_STAGE_CS)
      {      
        if ((state_vehicle == STATE_B1)||(state_vehicle == STATE_C0))//STATESVEHICLE_CONNECTED)
        {
          SetStateCharger(STATESCHARGER_AUTHORIZATION); 
          ///initializeSessionObject();
        }
#ifdef MITSUBISHI
        /* else if (state_vehicle == STATESVEHICLE_READYCHARGING ||
        state_vehicle == STATESVEHICLE_READYCHARGINGVENTILATION)
        {
        SetStateCharger(STATESCHARGER_AUTHORIZATION);
        ///initializeSessionObject();
      }*/
#endif
        else // unexpected bavior
        {
          InitEvseStateMachine();
          SetError(ERROR_UNKNOWN);
        }
      }
      
      if (state_vehicle != STATE_B1 &&
          state_vehicle != STATE_C0 &&
            state_vehicle != STATE_C &&
              state_vehicle != STATE_D)  
      {
        InitEvseStateMachine();  
        SetStateCharger(STATESCHARGER_READY);
      } else if (state_vehicle == STATE_A){
        SetStateCharger(STATESCHARGER_READY);
        InitEvseStateMachine();  
      }
      break;
      
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_AUTHORIZATION.
      In this state in case we are not authorized we wait to get authorization
      When is setted, normally we need to pass rfid car or send bluetooth instruction.
      By default no authorization is required
      *//////////////////////////////////////////////////////////////           
    case STATESCHARGER_AUTHORIZATION:
      //show payment method menu
      //get user input for PIN if required
      //get vehicle selection is available
      //send authorization request to server
      
      //if (currentSessionObject.isAuthorized){
      SetStateCharger(STATESCHARGER_AUTHORIZED);  
      //}        
      break;
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_AUTHORIZED.
      Once is Authorized we start the PWM signal. Whith the PWM sent, the car will know the
      max current allowed to charge. So we got to State B2      
      *//////////////////////////////////////////////////////////////           
    case STATESCHARGER_AUTHORIZED:
      //initializeSessionObject();
      //display unit pricing
      CheckCableStatus();      
      EnablePWM();
      SetStateCharger(STATESCHARGER_VEHICLECONNECTED);  
      break;
      
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_VEHICLECONNECTED.
      In this state we check the CP line in order to know if the car is 
      ready to charge or we wait for it (State B2)        
      *//////////////////////////////////////////////////////////////  
    case STATESCHARGER_VEHICLECONNECTED:
      if (state_vehicle == STATE_A)//STATESVEHICLE_NONE)
      {
        InitEvseStateMachine();
      }
      else if (state_vehicle == STATE_C)//STATESVEHICLE_READYCHARGING)
      {
        state_led_blue = LED_OFF;
        state_led_white = LED_ON;
        Lock();
        SetStateCharger(STATESCHARGER_PRECHARGING);
      }
      else if (state_vehicle == STATE_B2)
      {
        state_led_blue = LED_OFF;
        state_led_white = LED_CHARGE_FLASH;
      }
      else if (state_vehicle == STATE_D)//STATESVEHICLE_READYCHARGINGVENTILATION)
      {
        state_led_blue = LED_OFF;
        SetStateCharger(STATESCHARGER_DELAY_VENTILATION); 
      }
      else if (state_vehicle == STATE_A2)
      {
        SetStateCharger(STATESCHARGER_DELAY_DISCONNECTION);
        
      } else if (state_vehicle == STATE_E){
        SetStateCharger(STATESCHARGER_CPTOGND);
        SetError(ERROR_CPTOGND);
        osDelay(1);
      } else if (state_vehicle == STATE_G){
        SetStateCharger(STATESCHARGER_NODIODE);
        SetError(ERROR_NODIODE);
        osDelay(1);        
      }
      
      
      
      /* If it is the first time to get here in this charging session the charger
      needs to do a wakeup process if once we get to State B2 we last 30 seconds without 
      car accepting to start the charge.
      the WAKE UP Process is:
      State B2 = 30 seconds
      State E or F = 3 seconds
      State B2 = to infinite
      */
      if (WakeUp == 0){
        if (ChargerStateTimePassed() >= DELAY_WAKE_UP_STATE_B1){
          SetStateCharger(STATESCHARGER_WAKEUP);
          SetCPDuty(0); //Set to F during 3secs
          WakeUp = 1;
        }
      }    
      break;
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_WAKEUP.
      Waiting case in order to get to 3 seconds in State F       
      *//////////////////////////////////////////////////////////////         
    case  STATESCHARGER_WAKEUP:
      if (ChargerStateTimePassed() >= DELAY_WAKE_UP_STATE_F){
        SetStateCharger(STATESCHARGER_DELAY); 
        DisablePWM();  
      }
      
      break;   
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_DELAY_DISCONNECTION.
      This is to avoid microdisconection of the charger,
      For example when we use a simulator and the simulator jumps from 9v to 6V.
      there is a small frame of time that it is not connected to any resistor the the value is 12V  
      In case it takes more time than DELAY_DISCONNECTION, the disconnection is effective and we go to state A again
      *//////////////////////////////////////////////////////////////         
    case STATESCHARGER_DELAY_DISCONNECTION:
      if (ChargerStateTimePassed() >= DELAY_DISCONNECTION)  // T-ventilation
      {          
        InitEvseStateMachine();
      }
      else if (state_vehicle == STATE_A)//STATESVEHICLE_NONE)
      {
        InitEvseStateMachine();
      }
      else if (state_vehicle == STATE_C)//STATESVEHICLE_READYCHARGING)
      {
        state_led_blue = LED_OFF;
        state_led_white = LED_ON;
        Lock();
        SetStateCharger(STATESCHARGER_PRECHARGING);
      }
      else if (state_vehicle == STATE_D)//STATESVEHICLE_READYCHARGINGVENTILATION)
      {
        //state_led_blue = LED_BLINK;
        SetStateCharger(STATESCHARGER_DELAY_VENTILATION); 
      }
      break;
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_DELAY_VENTILATION.
      We get to this state in case we are asking a ventilation state. State D
      And we set a delay before we start the charging.
      *//////////////////////////////////////////////////////////////         
    case STATESCHARGER_DELAY_VENTILATION:
      if (ChargerStateTimePassed() >= DELAY_TIMER_VENTILATION)  // T-ventilation
      {          
        SetStateCharger(STATESCHARGER_PRECHARGING);
        /* if (isVentilationRequirementsMet())
        {
        Lock();
        SetStateCharger(STATESCHARGER_PRECHARGING);  
      }
          else          {
        SetError(ERROR_VENTILATION_REQUIREMENT);        
      }*/
      }
      else if (state_vehicle != STATE_D) //STATESVEHICLE_READYCHARGINGVENTILATION)
      {
        InitEvseStateMachine();
      }
      break;
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_PRECHARGING.
      We get to the State C in this state and after a delay (DELAY_TIMER_ACON):
      + Close contactor
      + Start White Led Flashing
      *//////////////////////////////////////////////////////////////             
    case STATESCHARGER_PRECHARGING:
      if (ChargerStateTimePassed() >= DELAY_TIMER_ACON) // T-ACon
      {
        if (state_vehicle == STATE_C || //STATESVEHICLE_READYCHARGING || 
            state_vehicle == STATE_D)//STATESVEHICLE_READYCHARGINGVENTILATION)
        {
          state_led_white = LED_CHARGE_FLASH;
          state_led_blue = LED_OFF;
          //currentSessionObject.meterStart = 0;//TBD            
          CloseContactor();
          Contactor_State = CLOSE_CONTACTOR_STATE;
          Lock();
          //leds_aux_all_ON();
          SetStateCharger(STATESCHARGER_CHARGING);
          ClearError(ERROR_TIMER_CONTACTOR_ON);
        }
        else
        {
          InitEvseStateMachine();
          SetError(ERROR_TIMER_CONTACTOR_ON);
        }
      }        
      break;
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_CHARGING.
      This State is the CHARGING State 
      it will keep in this state until the CP line(state_vehicle) is not changing
      *//////////////////////////////////////////////////////////////          
    case STATESCHARGER_CHARGING:        
#ifdef POWER_FAILURE     
      if (ChargerStateTimePassed() >= DELAY_POWER_FAILURE)
      {
        if (!ReadEnergyPulseState()){ 
          enableCharging(false); 
          UnLock();
          
          SetError(ERROR_POWERFAILURE);
        }
      }
#endif  
#ifdef MELT     
      
      
      Contactor_Mon = HAL_GPIO_ReadPin(CONTACTOR_MON_GPIO_Port, CONTACTOR_MON_Pin);
      if ((ChargerStateTimePassed() >= DELAY_NOAC_CHECK) && (Contactor_Mon == GPIO_PIN_RESET)){//ReadContactorMonitoring();
        SetError(ERROR_NOAC);
        state_led_white = LED_OFF;
        OpenContactor();
        Contactor_State = OPEN_CONTACTOR_STATE;
        UnLock();
        //PILOT_12V();
        PILOT_minus_12V();          
      }
      //state_led_red = LED_OFF; 
      
      //ClearError(ERROR_MELT_CONTACTOR);
      
#endif 
      
      //Authorization or charger finished
      if (IdAllowed == 0){ 
        state_led_white = LED_OFF;
        OpenContactor();
        Contactor_State = OPEN_CONTACTOR_STATE;
        //UnLock();
        PILOT_12V();
        SetStateCharger(STATESCHARGER_POSTCHARGING);
      }
      
      
      
      if (state_vehicle == STATE_B1 ||
          state_vehicle == STATE_B2 ||//STATESVEHICLE_CONNECTED ||
            state_vehicle == STATE_A ||
              state_vehicle == STATE_F ||  
                state_vehicle == STATE_A2)//STATESVEHICLE_NONE)        
      {
        state_led_red = LED_OFF;
        SetStateCharger(STATESCHARGER_POSTCHARGING);
        //IdAllowed = 0;
        state_led_white = LED_OFF;
      } else if (state_vehicle == STATE_E){
        SetError(ERROR_CPTOGND);
        state_led_white = LED_OFF;
        OpenContactor();            
        UnLock();
        SetStateCharger(STATESCHARGER_CPTOGND);
      } else if (state_vehicle == STATE_G){
        SetStateCharger(STATESCHARGER_NODIODE);
        SetError(ERROR_NODIODE);
        osDelay(1);        
      }/*else if (state_vehicle != STATE_C &&//STATESVEHICLE_READYCHARGING && 
      state_vehicle != STATE_D)//STATESVEHICLE_READYCHARGINGVENTILATION)
      {
      state_led_white = LED_OFF;
      SetStateCharger(STATESCHARGER_POSTCHARGING);
      //IdAllowed = 0;
      SetError(ERROR_WHILE_CHARGING);
    }        */
      break;
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_POSTCHARGING.
      To finish a Chargins Session we will apply a delay of DELAY_TIMER_COFF
      Before open contactor
      *//////////////////////////////////////////////////////////////              
    case STATESCHARGER_POSTCHARGING:        
      if ((ChargerStateTimePassed()) >= DELAY_TIMER_COFF) // T-ACoff1
      {
        state_led_blue = LED_OFF;
        OpenContactor();
        Contactor_State = OPEN_CONTACTOR_STATE;
        UnLock();
        //EndSessionObject();       
        
        
        if (state_vehicle == STATE_B1 || state_vehicle == STATE_B2 )        
        {
          SetStateCharger(STATESCHARGER_POSTCHARGING_1); 
          state_led_blue = LED_ON;
        }           
        else if (state_vehicle == STATE_A ||
                 state_vehicle == STATE_F ||  
                   state_vehicle == STATE_A2)       
        {          
          SetStateCharger(STATESCHARGER_ENDCHARGINGSESSION);
        } else if (state_vehicle == STATE_E){
          SetError(ERROR_CPTOGND);
          state_led_white = LED_OFF;
          OpenContactor();            
          UnLock();
          SetStateCharger(STATESCHARGER_CPTOGND);
        } else if (state_vehicle == STATE_G){
          SetStateCharger(STATESCHARGER_NODIODE);
          SetError(ERROR_NODIODE);
          osDelay(1);        
        }         
        
        
        
      }        
      break;
      
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_POSTCHARGING_1.
      Once the CAr bttery is full, S2 is opened from car side and we get in state
      B2 (9v PWM) during 2 minutes waiting to restart the charging.
      *//////////////////////////////////////////////////////////////              
    case STATESCHARGER_POSTCHARGING_1:        
      
      if ((( AudiEE[1] == '1' ) && ( AudiEE[0] == '0'))){    
        if ((ChargerStateTimePassed()) >= DELAY_POSTCHARGE_B2_B1) // T-ACoff1
        {
          SetStateCharger(STATESCHARGER_POSTCHARGING_2);   
          PILOT_12V();          
        }          
      }
      
      if (state_vehicle == STATE_A ||
          state_vehicle == STATE_F ||  
            state_vehicle == STATE_A2)//STATESVEHICLE_NONE)        
      {        
        
        state_led_red = LED_OFF;
        SetStateCharger(STATESCHARGER_ENDCHARGINGSESSION);
        //IdAllowed = 0;
        state_led_white = LED_OFF;
      } 
      else if (state_vehicle == STATE_C)//STATESVEHICLE_READYCHARGING)
      {
        state_led_blue = LED_OFF;
        state_led_white = LED_ON;
        Lock();
        SetStateCharger(STATESCHARGER_PRECHARGING);
      }        
      else if (state_vehicle == STATE_E){
        SetError(ERROR_CPTOGND);
        state_led_white = LED_OFF;
        OpenContactor();            
        UnLock();
        SetStateCharger(STATESCHARGER_CPTOGND);
      } else if (state_vehicle == STATE_G){
        SetStateCharger(STATESCHARGER_NODIODE);
        SetError(ERROR_NODIODE);
        osDelay(1);        
      }         
      
      
      break; 
      
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_POSTCHARGING_2.
      Once the CAr bttery is full, and after 2 minutes in B2 we put close pwm in order
      to not drain battery of Audi.
      *//////////////////////////////////////////////////////////////              
    case STATESCHARGER_POSTCHARGING_2:        
      
      
      if (state_vehicle == STATE_A ||
          state_vehicle == STATE_F ||  
            state_vehicle == STATE_A2)//STATESVEHICLE_NONE)        
      {        
        
        state_led_red = LED_OFF;
        SetStateCharger(STATESCHARGER_ENDCHARGINGSESSION);
        //IdAllowed = 0;
        state_led_white = LED_OFF;
      }        
      else if (state_vehicle == STATE_E){
        SetError(ERROR_CPTOGND);
        state_led_white = LED_OFF;
        OpenContactor();            
        UnLock();
        SetStateCharger(STATESCHARGER_CPTOGND);
      } else if (state_vehicle == STATE_G){
        SetStateCharger(STATESCHARGER_NODIODE);
        SetError(ERROR_NODIODE);
        osDelay(1);        
      }         
      
      
      break;        
      
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_ENDCHARGINGSESSION.
      Once the Charge is finished
      We get to the Steady signal
      And get back to the initial state 
      *//////////////////////////////////////////////////////////////            
    case STATESCHARGER_ENDCHARGINGSESSION:        
#ifdef MELT          
      
      while (Contactor_Mon == GPIO_PIN_SET){
        state_led_blue = LED_OFF;
        state_led_white = LED_OFF;
        PILOT_minus_12V();             
        SetError(ERROR_MELT_CONTACTOR);
        Contactor_Mon = HAL_GPIO_ReadPin(CONTACTOR_MON_GPIO_Port, CONTACTOR_MON_Pin);//ReadContactorMonitoring();
        osDelay(1);
      }               
      ClearError(ERROR_MELT_CONTACTOR);
      
#endif      
      //TBD end session
      //ulocaltime(g_LocalTimeSec, &(currentSessionObject.timeEnd));//TBD
      //send session information
      //recieve and display session outcome results
      //if (state_vehicle == STATESVEHICLE_CONNECTED)
      //{
      // setStateCharger(STATESCHARGER_VEHICLECONNECTED); 
      //}
      //else
      //{            
      InitEvseStateMachine();
      //}        
      break;   
      
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_CPTOGND.
      Deal with CT to GND failure
      *//////////////////////////////////////////////////////////////            
    case STATESCHARGER_CPTOGND:        
      
      if (state_vehicle != STATE_E){
        SetStateCharger(STATESCHARGER_ENDCHARGINGSESSION);
        ClearError(ERROR_CPTOGND);
        osDelay(1);
      } 
      
      break; 
      
      /*////////////////////////////////////////////////////////////
      STATESCHARGER_NODIODE.
      Deal with CT to GND failure
      *//////////////////////////////////////////////////////////////            
    case STATESCHARGER_NODIODE:        
      
      if (state_vehicle != STATE_G){
        SetStateCharger(STATESCHARGER_ENDCHARGINGSESSION);
        ClearError(ERROR_NODIODE);
        osDelay(1);
      } 
      
      break;                
      
      
    } 
  }   
}



void EnablePWM(void)
{  
  SetCPDuty(CurrentToDutyCycle(Current_Charge));
}

void DisablePWM(void)
{  
  SetCPDuty(101);
}

/*ChargerStateTimePassed
This is the diference between the actual(g_ulSystemTime) time 
and the time passed from the last State of charge is changed
*/
TickType_t ChargerStateTimePassed(void)
{
  return g_ulSystemTime - g_ulSystemPreviousTimestamp;
}

void SetError(uint8_t i_Error)
{
  error_status [i_Error] = true;
  state_led_red = LED_ON;
}

void ClearError(uint8_t i_Error)
{
  uint8_t er = 0;
  uint8_t leder = 0;
  error_status [i_Error] = false;
  leder=0;
  for (er=0;er<MAX_NUM_ERROR;er++){
    if (error_status[er] == true) leder++;
  }
  if (leder == 0) state_led_red = LED_OFF;
}

void SetStateCharger(char i_StateCharger)
{    
  g_ulSystemPreviousTimestamp = g_ulSystemTime;
  state_charger = i_StateCharger;    
  
#ifdef ZIGBEE
  EVSE_status = state_charger;
  SPIsendCmd(0,SEND_EVSE_STATUS, &EVSE_status ,1);
  //SPIsendCmd(0x0C80, &EVSE_status ,1);
#endif  
  
}

void CheckCableStatus(void)
{
  uint32_t   currentCableInput;
  uint16_t   current_rate;
  
#ifdef CURRENTCABLEREADING  
  currentCableInput = readCableCurrentCoding(); 
#else
  currentCableInput = ADC_63A_LEVEL_MAX; 
#endif  
  current_rate = currentCableInput;     
  Current_Charge = Init_Current_Charge;
  
  if (currentCableInput >= ADC_13A_LEVEL_MIN && 
      currentCableInput <= ADC_13A_LEVEL_MAX)
  {
    current_rate = 13;
    if (Init_Current_Charge >= 13){
      Current_Charge = 13;
    } else {
      Current_Charge = Init_Current_Charge;
    }
    
  }
  else if (currentCableInput >= ADC_20A_LEVEL_MIN && 
           currentCableInput <= ADC_20A_LEVEL_MAX)
  {    
    current_rate = 20;
    if (Init_Current_Charge >= 20){
      Current_Charge = 20;
    } else {
      Current_Charge = Init_Current_Charge;
    }
  }
  else if (currentCableInput >= ADC_32A_LEVEL_MIN && 
           currentCableInput <= ADC_32A_LEVEL_MAX)
  {
    current_rate = 32;
    if (Init_Current_Charge >= 32){
      Current_Charge = 32;
    } else {
      Current_Charge = Init_Current_Charge;
    } 
  }
  else if (currentCableInput >= ADC_63A_LEVEL_MIN && 
           currentCableInput <= ADC_63A_LEVEL_MAX)
  {
#ifdef THREE_PHASE
    current_rate = 70;
    if (Init_Current_Charge >= 70){
      Current_Charge = 70;
    } else {
      Current_Charge = Init_Current_Charge;
    } 
#else   
    current_rate = 63;
    if (Init_Current_Charge >= 63){
      Current_Charge = 63;
    } else {
      Current_Charge = Init_Current_Charge;
    } 
#endif
  }
  else if (currentCableInput > ADC_13A_LEVEL_MAX)
  {
    Current_Charge = 3; /// this means to set the Duty cycle to 5%
    reportError();
  }
}

uint32_t readCableCurrentCoding(void)
{
  uint32_t total=0;
  uint8_t i = 0;  
  
  //result[i] = ADC1_read();
  
  
  return total;
}


void resetSessionObject()
{
  currentSessionObject.isAuthorized = 0;
  currentSessionObject.ConsumptionWH = 0;
  currentSessionObject.Power = 0;
  currentSessionObject.priceUnit = 0.0;//TBD
}
void initializeSessionObject()
{
  //TBD *** SET SESSION INFORMATION ***
  currentSessionObject.meterStart = EnergyCounter;
  currentSessionObject.ConsumptionWH = 0;
  currentSessionObject.Power = 0;
  //currentSessionObject.userId = 0;//TBD
  currentSessionObject.userId = 0;//LastUserIDreading;
  currentSessionObject.vehicleId = 0;//TBD
  currentSessionObject.currency = Nis;//TBD
  currentSessionObject.priceUnit = 0.5;//TBD
  currentSessionObject.paymentMethod = Credit;//TBD
  // IMPORTANT!!! To read just the Time is also a must to read also DATE, if not it freezes RTC  
  HAL_RTC_GetTime(&hrtc, &currentSessionObject.timeStart, RTC_FORMAT_BCD);   
  HAL_RTC_GetDate(&hrtc, &currentSessionObject.dateStart, RTC_FORMAT_BCD); 
  
  //ulocaltime(g_LocalTimeSec, &(currentSessionObject.timeStart));//TBD check
  //we are going to send the realtime packet to start the session,
  //in the future, we need to use a startsession command to ask for autoritzation
  //SendRealTimePacket();
  SendInitSessionPacket();
  
  
}

void EndSessionObject()
{
  //TBD *** SET SESSION INFORMATION ***
  currentSessionObject.meterEnd = EnergyCounter;
  //currentSessionObject.ConsumptionWH = (currentSessionObject.meterEnd - currentSessionObject.meterStart)*WH_PER_PULSE_METER;
  //ulocaltime(g_LocalTimeSec, &(currentSessionObject.timeEnd));//TBD check
  // IMPORTANT!!! To read just the Time is also a must to read also DATE, if not it freezes RTC  
  HAL_RTC_GetTime(&hrtc, &currentSessionObject.timeEnd, RTC_FORMAT_BCD);   
  HAL_RTC_GetDate(&hrtc, &currentSessionObject.dateEnd, RTC_FORMAT_BCD);
  
  //we are going to send the realtime packet to end the session,
  //in the future, we need to use a startsession command to ask for autoritzation
  SendEndSessionPacket();
  // SendRealTimePacket();
}

void reportError()
{
  state_led_red = LED_ON;
  //enablePWM(true);
  //pwm_set_duty_cycle(DUTY_CYCLE_5);
}