/**
  ******************************************************************************
  * @file    drv_95HF.c 
  * @author  MMY Application Team
  * @version $Revision: 1348 $
  * @date    $Date: 2015-11-05 11:36:33 +0100 (Thu, 05 Nov 2015) $
  * @brief   This file provides set of driver functions to manage communication 
  *          between MCU and xx95HF chip (CR95HF) 
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under ST MYLIBERTY SOFTWARE LICENSE AGREEMENT (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/myliberty  
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
  * AND SPECIFICALLY DISCLAIMING THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------------------ */
#include "stdbool.h"
#include "drv_CR95HF.h"
#include "lib_PCD.h"


/* Standard includes. */
#include "string.h"
#include "stdio.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "rfid.h"

/* Example includes. */
#include "FreeRTOS_CLI.h"

#include "serial.h"
/** @addtogroup BSP
  * @{
  */

/** @addtogroup Components
  * @{
  */ 


/** @addtogroup XX95HF
  * @{
  */

/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Global variables ---------------------------------------------------------*/
/** @defgroup XX95HF_Global variables
  * @{
  */
/** 
* @brief  buffer to exchange data with the RF tranceiver.
*/
uint8_t	u95HFBuffer [RFTRANS_95HF_MAX_BUFFER_SIZE+3];

/** 
 *  @brief This uTimeOut variable is used as a timeout duting the communication with the RF tranceiver 
 */

extern volatile bool	        uDataReady; 
extern volatile bool	        uTimeOut;

bool EnableTimeOut              = true;

/* ConfigStructure */ 										 
drv95HF_ConfigStruct	        drv95HFConfig;
/**
  * @}
  */ 

/* Private functions ---------------------------------------------------------*/
/** @defgroup XX95HF_Private_Functions
  * @{
  */
#define RFTRANS_95HF_NSS_LOW() 			({})
#define RFTRANS_95HF_NSS_HIGH()  		({})
/* set state on IRQ_In pin */
#define RFTRANS_95HF_IRQIN_LOW() 		({})	
#define RFTRANS_95HF_IRQIN_HIGH()		({})
/* set state on SPI_CLK pin */
#define RFTRANS_95HF_SCK_LOW() 			({})
#define RFTRANS_95HF_SCK_HIGH()  		({})

void RFTRANS_SPI_Init(void){};
void RFTRANS_SPI_MSPInit(void){};
void RFTRANS_ConfigureInterfacePin(void){};
void SPI_SendReceiveBuffer(const uint8_t *pCommand, uint8_t length, uint8_t *pResponse)
{
  (void)pCommand;
  (void)length;
  (void)pResponse;
}
uint8_t SPI_SendReceiveByte(uint8_t data)
{
  (void)data;
  return 0x00;
}

/**
 *	@brief  this functions initializes the SPI in order to communicate with the 95HF device
 *  @param  None
 *  @retval void 
 */
static void drv95HF_InitializeSPI(void)
{
}

/**
 *	@brief  This function sends a reset command over SPI bus
 *  @param  None
 *  @retval None
 */
static void drv95HF_SendSPIResetByte(void)
{
}

/**
 *	@brief  This function polls 95HF chip until a response is ready or
 *				  the counter of the timeout overflows
 *  @param  None
 *  @retval PCD_POLLING_TIMEOUT : The time out was reached 
 *  @retval PCD_SUCCESS_CODE : A response is available
 */
static int8_t drv95HF_SPIPollingCommand( void )
{
  return RFTRANS_95HF_SUCCESS_CODE;	
}

/**												   
 *	@brief  This function send a command to 95HF device over UART bus
 *  @param  *pData : pointer on data to send to the 95HF device
 *  @retval None
 */
static void drv95HF_SendUARTCommand(const uint8_t *pData)
{
  UART_SendBuffer(pData);
}

/**
 *	@brief  This functions recovers a response from RFTRANS_95HF device over UART bus
 *  @param  *pData : pointer on data received from RFTRANS_95HF device
 *  @retval None
 */
static void drv95HF_ReceiveUARTResponse(uint8_t *pData)
{
  UART_ReceiveBuffer(pData);
}

/**
 * @}
 */

/** @defgroup drv_95HF_Public_Functions
 * 	@{
 */

/**
 *	@brief  this functions initializes UART in order to communicate with 95HF device
 *  @param  BaudRate : value of the Baudrate to use during UART communication
 *  @retval None 
 */
void drv95HF_InitializeUART(const uint32_t BaudRate)
{

}

/**
* @brief  	Initilize the 95HF device config structure
* @param  	None
* @retval 	None
*/
void drv95HF_InitConfigStructure (void)
{
  drv95HFConfig.uInterface 		= RFTRANS_95HF_INTERFACE_UART;
  drv95HFConfig.uSpiMode 		= RFTRANS_95HF_SPI_POLLING;
  drv95HFConfig.uState 			= RFTRANS_95HF_STATE_POWERUP;
  drv95HFConfig.uCurrentProtocol 	= RFTRANS_95HF_PROTOCOL_UNKNOWN;
  drv95HFConfig.uMode 			= RFTRANS_95HF_MODE_UNKNOWN;
}

/**
 *	@brief  Send a reset sequence over SPI bus (Reset command ,wait ,negative pulse on IRQin).
 *  @param  None
 *  @retval None
 */
void drv95HF_ResetSPI(void)
{	
}

/**
 *	@brief  returns the value of interface pin. 
 *				  Low level means UART bus was choose.
 *				  High level means SPI bus was choose.
 *  @param  None
 *  @retval Bit_RESET : 0
 *  @retval Bit_SET 	: 1
 */
int8_t drv95HF_GetInterfacePinState(void)
{
  return 0x00;
}

/**  
 *  @brief  This function returns the IRQout state
 *  @param  None
 *  @retval Pin set 	: 1
 *  @retval Pin reset	: 0
 */
int8_t drv95HF_GetIRQOutState(void)
{
  return 0x00;
}

/**
 *	@brief  This function initialize MCU serial interface peripheral (SPI or UART)
 *  @param  None
 *  @retval None
 */
void drv95HF_InitilizeSerialInterface(void)
{
   drv95HFConfig.uInterface = RFTRANS_95HF_INTERFACE_UART;
   /* -- Initialize UART Interface -- */ 
   drv95HF_InitializeUART(BAUDRATE_DATARATE_DEFAULT);

}

/**
 *	@brief  This function enable the interruption
 *  @param  None
 *  @retval None
 */
void drv95HF_EnableInterrupt(void)
{
}

/**
 *	@brief  This function disable the interruption
 *  @param  None
 *  @retval None
 */
void drv95HF_DisableInterrupt(void)
{
}

/**
 *	@brief  This function returns the Interface selected(UART or SPI)
 *  @param  None
 *  @retval RFTRANS_INTERFACE_UART : the UART interface is selected
 *  @retval RFTRANS_INTERFACE_SPI : the SPI interface is selected
 */
uint8_t drv95HF_GetSerialInterface(void)
{
  return drv95HFConfig.uInterface;
}

/**
 *	@brief  This function sends a command over SPI bus
 *  @param  *pData : pointer on data to send to the xx95HF
 *  @retval None
 */
void drv95HF_SendSPICommand(const uint8_t *pData)
{
  (void)pData;
}



/**
 *	@brief  This fucntion recovers a response from 95HF device
 *  @param  *pData : pointer on data received from 95HF device
 *  @retval None
 */
void drv95HF_ReceiveSPIResponse(uint8_t *pData)
{
  (void)pData;
}

/**
 *	@brief  This function send a command to 95HF device over SPI or UART bus and receive its response
 *  @param  *pCommand  : pointer on the buffer to send to the 95HF device ( Command | Length | Data)
 *  @param  *pResponse : pointer on the 95HF device response ( Command | Length | Data)
 *  @retval RFTRANS_95HF_SUCCESS_CODE : the function is succesful
 */
int8_t drv95HF_SendReceive(const uint8_t *pCommand, uint8_t *pResponse)
{		
  uint8_t command = *pCommand;

  /* First step  - Sending command	*/
  drv95HF_SendUARTCommand(pCommand);
 
  /* Second step - Receiving bytes */
  drv95HF_ReceiveUARTResponse(pResponse);

  return RFTRANS_95HF_SUCCESS_CODE; 
}

/**
 *	@brief  This function send a command to 95HF device over UART bus
 *  @param  *pCommand  : pointer on the buffer to send to the 95HF ( Command | Length | Data)
 *  @retval None
 */
void drv95HF_SendCmd(const uint8_t *pCommand)
{
  if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
    /* First step  - Sending command 	*/
    drv95HF_SendSPICommand(pCommand);
  
  else if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_UART)
    /* First step  - Sending command	*/
    drv95HF_SendUARTCommand(pCommand);
}

/**
 *	@brief  This function is a specific command. It's made polling and reading sequence. 
 *  @param  *pResponse : pointer on the 95HF device response ( Command | Length | Data)
 *  @retval RFTRANS_95HF_SUCCESS_CODE : the function is succesful
 *  @retval RFTRANS_95HF_POLLING_RFTRANS_95HF : the polling sequence returns an error
 */
int8_t  drv95HF_PoolingReading (uint8_t *pResponse)
{
  *pResponse =RFTRANS_95HF_ERRORCODE_DEFAULT;
  *(pResponse+1) = 0x00;
  
  if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
  {
    /* First step - Polling	*/
    if (drv95HF_SPIPollingCommand( ) != RFTRANS_95HF_SUCCESS_CODE)
    {	*pResponse = RFTRANS_95HF_ERRORCODE_TIMEOUT;
    return RFTRANS_95HF_ERRORCODE_TIMEOUT;	
    }
    /* Second step  - Receiving bytes */
    drv95HF_ReceiveSPIResponse(pResponse);
  }
  else if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_UART)
  {
    /* Second step - Receiving bytes 	*/
    drv95HF_ReceiveUARTResponse(pResponse);
  }
  return RFTRANS_95HF_SUCCESS_CODE; 
}

/**
 *	@brief  Send a negative pulse on IRQin pin
 *  @param  None
 *  @retval None
 */
void drv95HF_SendIRQINPulse(void)
{
  /* Send a pulse on IRQ_IN (UART_TX) */
  UART_SendByte(0x00);
  /* Need to wait 10ms after the pulse before to send the first command */
  HAL_Delay(10); 
}

/**
 *	@brief  this functions put the ST95HF in sleep/hibernate mode
 *  @param  WU_source : Source selected to wake up the device (WU_TIMEOUT,WU_TAG,WU_FIELD,WU_IRQ,WU_SPI)
 *  @param  mode : Can be IDLE_SLEEP_MODE or IDLE_HIBERNATE_MODE
 *  @retval None 
 */
void drv95HF_Idle(const uint8_t WU_source, const uint8_t mode)
{
  uint8_t pCommand[] = {RFTRANS_95HF_COMMAND_IDLE, IDLE_CMD_LENTH, 0, 0, 0, 0, 0 ,0x18 ,0x00 ,0x00 ,0x60 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00};
  
  if (mode == IDLE_SLEEP_MODE) /* SLEEP */
  {
    /* Select the wake up source*/
    pCommand[2] = WU_source;
    /* Select SLEEP mode */
    if (WU_source == WU_FIELD)
    {
      pCommand[3] = GETMSB(SLEEP_FIELD_ENTER_CTRL);
      pCommand[4] = GETLSB(SLEEP_FIELD_ENTER_CTRL);
    }
    else
    {
      pCommand[3] = GETMSB(SLEEP_ENTER_CTRL);
      pCommand[4] = GETLSB(SLEEP_ENTER_CTRL);
    }
    pCommand[5] = GETMSB(SLEEP_WU_CTRL);
    pCommand[6] = GETLSB(SLEEP_WU_CTRL);
  }
  else /* HIBERNATE */
  {
    /* Select the wake up source, only IRQ is available for HIBERNATE mode*/
    pCommand[2] = WU_IRQ;
    /* Select HIBERNATE mode */
    pCommand[3] = GETMSB(HIBERNATE_ENTER_CTRL);
    pCommand[4] = GETLSB(HIBERNATE_ENTER_CTRL);
    pCommand[5] = GETMSB(HIBERNATE_WU_CTRL);
    pCommand[6] = GETLSB(HIBERNATE_WU_CTRL);
    pCommand[10] = 0x00;
  }
  
  /* Send the command */
  if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
    /* First step  - Sending command 	*/
    drv95HF_SendSPICommand(pCommand);
  else if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_UART)
    /* First step  - Sending command	*/
    drv95HF_SendUARTCommand(pCommand);
  
}
/**
 * @}
 */ 

/**
 * @}
 */

/**
 * @}
 */ 

/**
 * @}
 */

/******************* (C) COPYRIGHT 2016 STMicroelectronics *****END OF FILE****/
