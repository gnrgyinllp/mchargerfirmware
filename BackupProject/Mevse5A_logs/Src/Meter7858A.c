/**
  ******************************************************************************
  * File Name          : EVSEtest1.c
  * Description        : test of basic EV charger operation
  ******************************************************************************
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "EVSEtest1.h"
#include "stm32f4xx_hal.h"
#include "FreeRTOS.h"
#include "StateMachine.h"
#include "main.h"
#include "Meter7858A.h"

extern SPI_HandleTypeDef hspi1;


uint8_t dataout1, dataout2,mi;
uint8_t datatosend[4],datatosend2[4],Reg_Address[2],Data_to_Write[4];
uint8_t datatoread[4],voltageA[4],voltageB[4],voltageC[4];
uint32_t volA,volB,volC;
uint16_t period;
float freq0;
//uint8_t SPITxData[10];
//uint8_t SPIRxData[10];



/*
Meter Init
It is the function for the initial configuration of the Meter IC AD7858A


*/
void MeterInit(void){

  Meter_Reset();

 /* Reg_Address [0] = 0xEC;
  Reg_Address [1] = 0x01;
  Data_to_Write [0] = 0x55;
  ADE7858A_SPI_SendData(Reg_Address,Data_to_Write,1,50);
   */

   datatosend[0] = 0x53;
//   Write_ADE7858A_SPI(CONFIG2_METER_REG, datatosend, 1, SPI_TIMEOUT);
//   Write_ADE7858A_SPI(CONFIG2_METER_REG, datatosend, 1, SPI_TIMEOUT);

   Read_ADE7858A_SPI(0xE702, datatoread , 1, SPI_TIMEOUT);


   // set CONFIG with regular sorted channels and not Rowosky coils
   datatosend[0] = 0x00;
   datatosend[1] = 0x00;
//   Write_ADE7858A_SPI(CONFIG_METER_REG, datatosend, 2, SPI_TIMEOUT);

    // set Vgain
   datatosend[0] = 0x0C;
   datatosend[1] = 0x00;
   datatosend[2] = 0x00;
   datatosend[3] = 0x00;
//   WriteValue_ADE7858A_32u(AVGAIN_METER_REG, 0x00000000 );
//   WriteValue_ADE7858A_32u(BVGAIN_METER_REG, 0x00000000 );
//   WriteValue_ADE7858A_32u(CVGAIN_METER_REG, 0x00000000 );
  //Write_ADE7858A_SPI(AVGAIN_METER_REG, datatosend, 4, SPI_TIMEOUT);
  // Write_ADE7858A_SPI(BVGAIN_METER_REG, datatosend, 4, SPI_TIMEOUT);
  // Write_ADE7858A_SPI(CVGAIN_METER_REG, datatosend, 4, SPI_TIMEOUT);


   // ENABLE RAM WRITE PROTECT
//   datatosend[0] = 0xAD;
//   Write_ADE7858A_SPI(RAMPROTEC1_METER_REG, datatosend, 1, SPI_TIMEOUT);
//   datatosend[0] = 0x80;
//   Write_ADE7858A_SPI(RAMPROTEC2_METER_REG, datatosend, 1, SPI_TIMEOUT);



    ///Start DSP functionality
   datatosend[0] = 0x00;
   datatosend[1] = 0x01;
   Write_ADE7858A_SPI(RUN_METER_REG, datatosend, 2, SPI_TIMEOUT);
   Write_ADE7858A_SPI(RUN_METER_REG, datatosend, 2, SPI_TIMEOUT);
   Read_ADE7858A_SPI(RUN_METER_REG, datatoread , 2, SPI_TIMEOUT);


   HAL_Delay(50);

   volA = ReadValue_ADE7858A_32u(AVRMS_METER_REG);
   volB = ReadValue_ADE7858A_32u(BVRMS_METER_REG);
   volC = ReadValue_ADE7858A_32u(CVRMS_METER_REG);


  dataout1 = 2;

}

void MeterReading(void){

  mi++;

  if (mi=100){

    mi=0;
    volA = ReadValue_ADE7858A_32u(AVRMS_METER_REG);
    volB = ReadValue_ADE7858A_32u(BVRMS_METER_REG);
    volC = ReadValue_ADE7858A_32u(CVRMS_METER_REG);

    period = ReadValue_ADE7858A_16u(PERIOD_LINE_NETWORK_REG);
    freq0 = 256000 / period;

    dataout1 = 2;

  }


}


/*
Meter Reset
Once we do reset we need to togle MOSI pin 3 times
*/
void Meter_Reset(void){

  HAL_GPIO_WritePin(GPIOB, RST_METER_Pin, GPIO_PIN_RESET);
  HAL_Delay(20);
  HAL_GPIO_WritePin(GPIOB, RST_METER_Pin, GPIO_PIN_SET);
  HAL_Delay(50);
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low
  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high
  HAL_Delay(1);

}

//https://stackoverflow.com/questions/35780290/how-to-use-hardware-nss-spi-on-stm32f4
//https://ez.analog.com/thread/87375-stm32f4-discovery-spi-adau1701-readback
//void Write_MeterSPI(void){
void Read_Config2_Meter(void){

  uint8_t ChipaddrRw,celladdress1,celladdress2;
  uint8_t datatosend[10];
  dataout1 = 0;


  datatosend[0] = 0x71;
  datatosend[1] = 0xEC;
  datatosend[2] = 0x01;


  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low

  HAL_SPI_Transmit(&hspi1, datatosend, 3,SPI_TIMEOUT);

  HAL_SPI_Receive(&hspi1, datatoread, 1,SPI_TIMEOUT);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high

  HAL_Delay(1);
  dataout1 = 0;

}


void Write_Config2_Meter(void){

  uint8_t ChipaddrRw,celladdress1,celladdress2;

  datatosend[0] = 0x70;
  datatosend[1] = 0xEC;
  datatosend[2] = 0x01;
  datatosend[3] = 0x54;

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low

  HAL_SPI_Transmit(&hspi1, datatosend, 4,50);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high

  HAL_Delay(1);
  dataout2 = 0;

}





void Write_ADE7858A_SPI(uint16_t address, uint8_t* data, uint16_t size, uint32_t timeout){


  uint8_t SPITxData[10];
  uint8_t ix;

  SPITxData[0] = 0x70;
  SPITxData[1] = address >> 8;
  SPITxData[2] = address & 0xFF;
  for (ix=0;ix<size;ix++){
    SPITxData[3+ix] = *data++;
  }

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low

  HAL_SPI_Transmit(&hspi1, SPITxData, (size + 3) ,timeout);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high

  HAL_Delay(1);
  dataout2 = 0;

}

void Read_ADE7858A_SPI(uint16_t address, uint8_t* SPIRxData, uint16_t size, uint32_t timeout){


  uint8_t SPITxData[10];
  uint8_t ix;

  SPITxData[0] = 0x71;
  SPITxData[1] = address >> 8;
  SPITxData[2] = address & 0xFF;

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low

  HAL_SPI_Transmit(&hspi1, SPITxData,  3 , timeout);

  HAL_SPI_Receive(&hspi1, SPIRxData, size , timeout);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high

  HAL_Delay(1);
  dataout2 = 0;

}

uint32_t ReadValue_ADE7858A_32u(uint16_t address){

  uint8_t SPITxData[4],SPIRxData[4];
  uint8_t ix;
  uint32_t total;

  SPITxData[0] = 0x71;
  SPITxData[1] = address >> 8;
  SPITxData[2] = address & 0xFF;

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low

  HAL_SPI_Transmit(&hspi1, SPITxData,  3 , SPI_TIMEOUT);

  HAL_SPI_Receive(&hspi1, SPIRxData, 4 , SPI_TIMEOUT);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high

  HAL_Delay(1);
  total |= SPIRxData[0] << 24;
  total |= SPIRxData[1] << 16;
  total |= SPIRxData[2] << 8;
  total |= SPIRxData[3];


  return total;

}


uint16_t ReadValue_ADE7858A_16u(uint16_t address){

  uint8_t SPITxData[4],SPIRxData[4];
  uint8_t ix;
  uint16_t total;

  SPITxData[0] = 0x71;
  SPITxData[1] = address >> 8;
  SPITxData[2] = address & 0xFF;

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low

  HAL_SPI_Transmit(&hspi1, SPITxData,  3 , SPI_TIMEOUT);

  HAL_SPI_Receive(&hspi1, SPIRxData, 2 , SPI_TIMEOUT);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high

  HAL_Delay(1);

  total |= SPIRxData[0] << 8;
  total |= SPIRxData[1];


  return total;

}

uint32_t WriteValue_ADE7858A_32u(uint16_t address, uint32_t data){

  uint8_t SPITxData[4];

  SPITxData[0] = 0x70;
  SPITxData[1] = address >> 8;
  SPITxData[2] = address & 0xFF;
  SPITxData[3] = (data & 0xff000000) >> 24;
  SPITxData[4] = (data & 0x00ff0000) >> 16;
  SPITxData[5] = (data & 0x0000ff00) >> 8;
  SPITxData[6] = (data & 0x000000ff);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_RESET ); // NSS1 low

  HAL_SPI_Transmit(&hspi1, SPITxData,  7 , SPI_TIMEOUT);

  HAL_GPIO_WritePin( METER_CS_Port, METER_CS_Pin, GPIO_PIN_SET ); // NSS1 high


}