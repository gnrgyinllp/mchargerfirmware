/**
******************************************************************************
* File Name          : EVSEtest1.c
* Description        : test of basic EV charger operation
******************************************************************************
*
******************************************************************************
*/
/* Includes ------------------------------------------------------------------*/
#include "EVSEtest1.h"
#include "stm32f4xx_hal.h"
#include "FreeRTOS.h"
#include "StateMachine.h"
#include "main.h"
#include "Meter7858A.h"
#ifdef DEVELOPMENT
#include "eeprom/user_eeprom.h"
#endif
#include "os_layer/os_layer.h"
#include "log/log.h"
int ContactorCh1Status;
GPIO_PinState UserButton,UserButton0;
extern TIM_HandleTypeDef htim1,htim3,htim12;
extern UART_HandleTypeDef huart1;
extern ADC_HandleTypeDef hadc1;
extern uint8_t   rxbuffer[100];
/* Virtual address defined by the user: 0xFFFF value is prohibited */
extern uint16_t         VirtAddVarTab[NB_OF_VAR];
extern uint16_t         VarDataTab[NB_OF_VAR];
extern uint16_t         VarDataTmp, VarValue;
extern uint16_t         Init_Current_Charge,Max_Current_Charge;

uint16_t countdma0 = 0;
uint16_t countdma1 = 0;
uint16_t countdma2 = 0;  

int             dutyCycle = 1000;
uint8_t          LedPWM=0;
int             freq = 4000;
uint8_t         buf[ADC_ARRAY_LENGHT];
int8_t          adc_buf[ADC_ARRAY_LENGHT];
char            *buffer;
uint16_t        len, delay;
uint32_t        adcvalue;
char            CP_Status,CP_Status1,CP_FinalStatus,CP_FinalStatus1;
char            FirstEE[2],BackBoneSNEE[12],PasswordEE[12],LoginEE[2],MaxCurrentEE[2],CurrentEE[2],CSTypeEE[2],MeterEE[2],BleEE[2],RS485EE[2],RS485AddEE[2],RfidEE[2],
AuthEE[2],OutputTypeEE[2],FirmwareEE[6],DateFlashEE[8],AudiEE[2];

static          xQueueHandle    xCP_Status;
uint16_t        high_c,low_c;
uint16_t        Count12V,Count9V,Count6V,Count3V,Count0V,Count_12V;
uint16_t        VarAdd,VarData,VarLen;
volatile osLayer_ThreadId_t g_adcThreadId;
/* 
User_Init
It is the function for the initial configuration of the Evse control
*/
void User_Init(void){
  
  //ContactorCh1Status = CloseContactorCH1();
  xCP_Status = xQueueCreate( CP_QUEUE_LENGHT, ( unsigned portBASE_TYPE ) sizeof( signed portCHAR ) );	
  vQueueAddToRegistry(xCP_Status, "CP_STATUS_QUEUE");
  //Start PWM for cp 
  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2);  //tested and working
   
  //HAL_ADC_Start_DMA(&hadc1,(uint32_t*)buf,ADC_ARRAY_LENGHT);
    
  //Max current readed from EEPROM
  ReadMaxCurrent();
  
  //Initial current readed from EEPROM
  ReadInitialCurrent();
  
  MeterInit();  
  //Init EVSE State Machine
  InitEvseStateMachine();
  
}


/* ReadInitialCurrent
*  Read from EEPROM or Flash memory program the value of the current to charger
*  
*/
void ReadInitialCurrent(void){
  uint16_t      Decimal;
  uint16_t      Units;
  
  //Initial current readed from prggram memory
  //Init_Current_Charge = INITIAL_CURRENT_CHARGE; 
  
  //Read from EEPROM
  Decimal = (int)CurrentEE[0];   
  Decimal = Decimal - 0x30;
  Units = (int)CurrentEE[1];
  Units = Units - 0x30;
  Init_Current_Charge = Decimal * 10 + Units;
  if (Init_Current_Charge > Max_Current_Charge) Init_Current_Charge = Max_Current_Charge;
  
}


/* ReadMaxCurrent
*  Read from EEPROM or Flash memory program the value of the current to charger
*  
*/
void ReadMaxCurrent(void){
  uint16_t      Decimal;
  uint16_t      Units;
  
  //Initial current readed from prggram memory
  //Init_Current_Charge = INITIAL_CURRENT_CHARGE; 
  
  //Read from EEPROM
  Decimal = (int)CurrentEE[0];   
  Decimal = Decimal - 0x30;
  Units = (int)CurrentEE[1];
  Units = Units - 0x30;
  Max_Current_Charge = Decimal * 10 + Units;
  
}
void BlueButton_Led(void){
  
  UserButton = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
  if ((UserButton == 1) && (UserButton != UserButton0)){
    
    HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_12);
    
  }
  UserButton0 = UserButton;
  
}

void DutyLoop(void){
  
  htim3.Instance->CCR1 = dutyCycle;
  dutyCycle += 100; 
  if (dutyCycle >= PERIOD_INIT_PWM1) dutyCycle = 0;
  HAL_Delay(100);
  
}

void DutyLoop4(void){
  
  if (delay > 65000){
    //htim4.Instance->CCR3 = dutyCycle;
    dutyCycle += 100; 
    if (dutyCycle >= 20000) dutyCycle = 0;
    delay = 0;
  }    
  delay++;
}

void FreqLoop(void){
  
  htim3.Instance->ARR = freq;
  freq += 1000; 
  if (freq >= PERIOD_INIT_PWM1) freq = 4000;
  HAL_Delay(1000);
  
}



void GetAdc(void){  
  uint16_t i;
  for (i=0;i<10000;i++) buf[i] = (uint8_t) HAL_ADC_GetValue(&hadc1); 
  my_printf("adc sample: /n");  
  for (i=0;i<10000;i++) {
    my_printf("%u ", buf[i]);  
  }
  /* Put string to USART */
  HAL_Delay(10000);   
}  

/*Buzzer_On
this function is giving frequency to buzzer to be turned ON
*/
void Buzzer_ON(void){
  
  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1);    
  
}

/*Buzzer_OFF
this function turn off the Buzzer
*/
void Buzzer_OFF(void){
  
  HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_1);  
  
}
/* CloseContactorCH1
*  Enables the power to the EV. Close the contactor in order to drive the current to the Car
*  returs an integuer with the status of the last instruction 0 = open contactor, 1 = close contactor
*/
int CloseContactor(void){
  
  int a = 1;  
  
  HAL_GPIO_WritePin(GPIOB, CONTACTOR_OUT_Pin, GPIO_PIN_SET); 
  
  return a;
}
int OpenContactor(void){
  
  int a = 0;  
  
  HAL_GPIO_WritePin(GPIOB, CONTACTOR_OUT_Pin, GPIO_PIN_RESET); 
  
  return a;
}

void Lock(void){
  
  
  
}

void UnLock(void){
  
  
  
}

int BlueLedTurnON(void){
  
  int a = 1;  
  HAL_GPIO_WritePin(GPIOB, BLUE_LED_Pin, GPIO_PIN_SET);
  
  return a;
}
int BlueLedTurnOFF(void){
  
  int a = 0;  
  HAL_GPIO_WritePin(GPIOB, BLUE_LED_Pin, GPIO_PIN_RESET);
  
  return a;
}

/*RedLedTurnON
and
RedLedTurnOFF
Drives the Red Led to indicates Errors
*/

int RedLedTurnON(void){
  
  int a = 1;  
  HAL_GPIO_WritePin(GPIOB, RED_LED_Pin, GPIO_PIN_SET);
  
  return a;
}
int RedLedTurnOFF(void){
  
  int a = 0;  
  HAL_GPIO_WritePin(GPIOB, RED_LED_Pin, GPIO_PIN_RESET);
  
  return a;
}

/*
WhiteLedTurnON
WhiteLedTurnOFF
and
WhiteLedSetDuty
Turns on and off the White led and it configures the DutyCycle to set the brightness of the White Led
*/

int WhiteLedTurnON(void){
  
  int a = 1;  
  HAL_TIM_PWM_Start(&htim12,TIM_CHANNEL_1);
  
  return a;
}
int WhiteLedTurnOFF(void){
  
  int a = 0;  
  HAL_TIM_PWM_Stop(&htim12,TIM_CHANNEL_1);
  
  return a;
}
void WhiteLedSetDuty(uint8_t DC){
  
  htim12.Instance->CCR1 = DC;
  
  
}

/*
CPON, CPOFF, SetCPDuty, PILOT_12V and PILOT_minus_12V
These function just start and stop the CP PWM
PWM is set all time ON mainly and we just set the duty cycle between 100%(12V) and 0%(-12V)
12V = 101
-12V = 0
*/
int CPON(void){
  
  int a = 1;  
  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2);
  
  return a;
}
int CPOFF(void){
  
  int a = 0;  
  HAL_TIM_PWM_Stop(&htim3,TIM_CHANNEL_2);
  
  return a;
}
void SetCPDuty(uint16_t DC){
  
  htim3.Instance->CCR2 = DC;
  
}  
void PILOT_12V(void){
  
  htim3.Instance->CCR2 = 101;
  
}
void PILOT_minus_12V(void){
  
  htim3.Instance->CCR2 = 0;
  
}


/*  
ReadContactorMonitoring 
This function is checking the status of the Contactor 1 (CLOSE) and 0 (OPEN)
*/
GPIO_PinState ReadContactorMonitoring(void){
  
  GPIO_PinState Read_gpio;
  
  Read_gpio = HAL_GPIO_ReadPin(CONTACTOR_MON_GPIO_Port, CONTACTOR_MON_Pin);
  
  return Read_gpio;  
}

static bool adcConvert(void)
{
  ADC_HandleTypeDef* hadc = &hadc1;
  g_adcThreadId = osLayer_getCurThreadId();
  {
    const HAL_StatusTypeDef res = HAL_ADC_Start_DMA(hadc, (uint32_t*)buf, ADC_ARRAY_LENGHT);
    if (HAL_OK != res)
    {
      LOG_COMMON_INFO("Failed to start DMA");
      return false;
    }
  }
  ulTaskNotifyTake(pdTRUE, 10);
  if (0 != g_adcThreadId.threadId)
  {
    return false;
  }
  HAL_ADC_Stop_DMA(hadc);
  return true;
}

void ADC_Process(void)
{
  adcConvert();
  
  high_c = 0;
  low_c = 0;
  Count12V=0;
  Count9V=0;
  Count6V=0;
  Count3V=0;
  Count0V=0;
  Count_12V=0;
  
  for (int i=0;i<ADC_ARRAY_LENGHT;i++){        
    if ((buf[i]>LOWER_LIMIT_12V)&&(buf[i]<UPPER_LIMIT_12V)){        
      Count12V++;
      adc_buf[i] = PWM_A_12V;        
      
    } else if ((buf[i]>LOWER_LIMIT_9V)&&(buf[i]<UPPER_LIMIT_9V)){  
      Count9V++;
      adc_buf[i] = PWM_B_9V;
      
    } else if ((buf[i]>LOWER_LIMIT_6V)&&(buf[i]<UPPER_LIMIT_6V)){
      Count6V++;        
      adc_buf[i] = PWM_C_6V;
      
    } else if ((buf[i]>LOWER_LIMIT_3V)&&(buf[i]<UPPER_LIMIT_3V)){
      Count3V++;        
      adc_buf[i] = PWM_D_3V;
      
    } else if ((buf[i]>LOWER_LIMIT_0V)&&(buf[i]<UPPER_LIMIT_0V)){        
      Count0V++;      
      adc_buf[i] = PWM_E_0V;        
      
    } else if ((buf[i]>LOWER_LIMIT_MINUS12V)&&(buf[i]<UPPER_LIMIT_MINUS12V)){       
      Count_12V++;                  
      adc_buf[i] = PWM_F_MINUS12V;
      
    } else {        
      adc_buf[i] = PWM_NOVALID;
      
    }             
  }
  
  
  // State A. No car presence. 12V steady 
  if (Count12V > CP_STEADY_COUNTER_TRIGGER){
    CP_Status = STATE_A;
    
    // State A2. Error state 12V pwm  
  }else if((Count12V > CP_PWM_ACTIVITY_MIN)&&(Count_12V > CP_PWM_ACTIVITY_MIN)){            
    CP_Status = STATE_A2;   
    
    // State B1. Car connected. 9V steady value  
  }else if (Count9V > CP_STEADY_COUNTER_TRIGGER){           
    CP_Status = STATE_B1;
    
    // State B2. Precharging state 9V pwm  
  }else if((Count9V > CP_PWM_ACTIVITY_MIN)&&(Count_12V > CP_PWM_ACTIVITY_MIN)){            
    CP_Status = STATE_B2; 
    
    // State C0. Car connected. 6V steady value  
  }else if (Count6V > CP_STEADY_COUNTER_TRIGGER){           
    CP_Status = STATE_C0;        
    
    // State C. Charging state 6V pwm          
  }else if((Count6V > CP_PWM_ACTIVITY_MIN)&&(Count_12V > CP_PWM_ACTIVITY_MIN)){            
    CP_Status = STATE_C;     
    
    // State D. 3V pwm ventilation state
  }else if((Count3V > CP_PWM_ACTIVITY_MIN)&&(Count_12V > CP_PWM_ACTIVITY_MIN)){    
    CP_Status = STATE_D;
    
    // CP to GND State to detect GND cp failure
  }else if(Count0V > CP_STEADY_COUNTER_TRIGGER){
    CP_Status = STATE_E;
    
    // State F --> -12V
  }else if(Count_12V > CP_STEADY_COUNTER_TRIGGER){
    CP_Status = STATE_F; 
    
    // No diode detection in PWM mode 
  }else if((Count_12V < CP_PWM_ACTIVITY_MIN)&&
           (((Count9V > CP_PWM_ACTIVITY_MIN)&&(Count9V < CP_STEADY_COUNTER_TRIGGER))||
            ((Count6V > CP_PWM_ACTIVITY_MIN)&&(Count6V < CP_STEADY_COUNTER_TRIGGER))||
              ((Count3V > CP_PWM_ACTIVITY_MIN)&&(Count3V < CP_STEADY_COUNTER_TRIGGER))))   
  {
    CP_Status = STATE_G; 
    
    // any other case
  } else {
    CP_Status = STATE_H; 
    
  }
  
  // check if the status is stable comparing with the last status detection,
  //    if it is the same then CP_FinalStatus is updated
  if (CP_Status == CP_Status1){         
    CP_FinalStatus =  CP_Status;   
    //xQueueSendFromISR( xCP_Status, &CP_FinalStatus, &xHigherPriorityTaskWoken );
    
  }        
  CP_Status1 = CP_Status;
  
}

/* CP ADC Conversion complete callback 
HAL_ADC_ConvCpltCallback
Every time DMA is full of data, this interrupt call back is called and processes all the 
ADC reading received from the CP input signaling in order to determine the status of the Control Pilot Line
*/
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
  if (0 != g_adcThreadId.threadId)
  {
    const osThreadId threadId = g_adcThreadId.threadId;
    g_adcThreadId.threadId = 0;
    
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    vTaskNotifyGiveFromISR(threadId, &xHigherPriorityTaskWoken);
    
    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
    should be performed to ensure the interrupt returns directly to the highest
    priority task.  The macro used for this purpose is dependent on the port in
    use and may be called portEND_SWITCHING_ISR(). */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

signed portBASE_TYPE xCP_GetStatus(  signed portCHAR *CPStatusChar, portTickType xBlockTime )
{
  
	/* Get the next character from the buffer.  Return false if no characters
	are available, or arrive before xBlockTime expires. */
	if( xQueueReceive( xCP_Status, CPStatusChar, xBlockTime ) )
	{
		return pdTRUE;
	}
	else
	{
		return pdFALSE;
	}
}

void ReadEepromEVSEValues(void){
  int i;  
#ifdef DEVELOPMENT
  uint8_t HighByte;
  uint8_t LowByte;
#endif
  for (i=0;i<EEPROM_NUM_VAR;i++){  
#ifdef DEVELOPMENT
    /* Start address from 1 */
    eeprom_readbyte(2*VirtAddVarTab[i],&LowByte);
    eeprom_readbyte(2*VirtAddVarTab[i] + 1,&HighByte);
    VarDataTab[i] = (uint16_t)((HighByte << 8) | (LowByte));
#else
    if((EE_ReadVariable(VirtAddVarTab[i],  &VarDataTab[i])) != HAL_OK)
    {
      //Error_Handler(); // dissable error in order firts time we flash the code
      // and there is no data saved in the emulated eeprom then we need to  
    }
#endif
    
  }
  
  FirstEE[0] =  VarDataTab[EEPROM_FIRST_TIME] & 0x00ff;
  FirstEE[1] =  VarDataTab[EEPROM_FIRST_TIME] >> 8; 
  
  PasswordEE[0] =  VarDataTab[PASS_NUM0_ADDRESS] & 0x00ff;
  PasswordEE[1] =  VarDataTab[PASS_NUM0_ADDRESS] >> 8;
  PasswordEE[2] =  VarDataTab[PASS_NUM1_ADDRESS] & 0x00ff;
  PasswordEE[3] =  VarDataTab[PASS_NUM1_ADDRESS] >> 8;
  PasswordEE[4] =  VarDataTab[PASS_NUM2_ADDRESS] & 0x00ff;
  PasswordEE[5] =  VarDataTab[PASS_NUM2_ADDRESS] >> 8;
  PasswordEE[6] =  VarDataTab[PASS_NUM3_ADDRESS] & 0x00ff;
  PasswordEE[7] =  VarDataTab[PASS_NUM3_ADDRESS] >> 8;
  PasswordEE[8] =  VarDataTab[PASS_NUM4_ADDRESS] & 0x00ff;
  PasswordEE[9] =  VarDataTab[PASS_NUM4_ADDRESS] >> 8;
  PasswordEE[10] =  VarDataTab[PASS_NUM5_ADDRESS] & 0x00ff;
  PasswordEE[11] =  VarDataTab[PASS_NUM5_ADDRESS] >> 8;  
  
  BackBoneSNEE[0] =  VarDataTab[SERIAL_NUM0_ADDRESS] & 0x00ff;
  BackBoneSNEE[1] =  VarDataTab[SERIAL_NUM0_ADDRESS] >> 8;
  BackBoneSNEE[2] =  VarDataTab[SERIAL_NUM1_ADDRESS] & 0x00ff;
  BackBoneSNEE[3] =  VarDataTab[SERIAL_NUM1_ADDRESS] >> 8;
  BackBoneSNEE[4] =  VarDataTab[SERIAL_NUM2_ADDRESS] & 0x00ff;
  BackBoneSNEE[5] =  VarDataTab[SERIAL_NUM2_ADDRESS] >> 8;
  BackBoneSNEE[6] =  VarDataTab[SERIAL_NUM3_ADDRESS] & 0x00ff;
  BackBoneSNEE[7] =  VarDataTab[SERIAL_NUM3_ADDRESS] >> 8;
  BackBoneSNEE[8] =  VarDataTab[SERIAL_NUM4_ADDRESS] & 0x00ff;
  BackBoneSNEE[9] =  VarDataTab[SERIAL_NUM4_ADDRESS] >> 8;
  BackBoneSNEE[10] =  VarDataTab[SERIAL_NUM5_ADDRESS] & 0x00ff;
  BackBoneSNEE[11] =  VarDataTab[SERIAL_NUM5_ADDRESS] >> 8;  

  
  LoginEE[0] =  VarDataTab[LOGIN_ENABLE] & 0x00ff;
  LoginEE[1] =  VarDataTab[LOGIN_ENABLE] >> 8; 
  
  MaxCurrentEE[0] =  VarDataTab[MAX_CURRENT_EEPROM_ADDRESS] & 0x00ff;
  MaxCurrentEE[1] =  VarDataTab[MAX_CURRENT_EEPROM_ADDRESS] >> 8; 
  
  CurrentEE[0] =  VarDataTab[CURRENT_EEPROM_ADDRESS] & 0x00ff;
  CurrentEE[1] =  VarDataTab[CURRENT_EEPROM_ADDRESS] >> 8;
  
  CSTypeEE[0] =  VarDataTab[CHARGER_TYPE_EEPROM_ADDRESS] & 0x00ff;
  CSTypeEE[1] =  VarDataTab[CHARGER_TYPE_EEPROM_ADDRESS] >> 8; 
  
  OutputTypeEE[0] =  VarDataTab[OUTPUT_TYPE_ADDRESS] & 0x00ff;
  OutputTypeEE[1] =  VarDataTab[OUTPUT_TYPE_ADDRESS] >> 8; 
  
  MeterEE[0] =  VarDataTab[METER_EEPROM_ADDRESS] & 0x00ff;
  MeterEE[1] =  VarDataTab[METER_EEPROM_ADDRESS] >> 8; 
  
  BleEE[0] =  VarDataTab[BLE_EEPROM_ENABLE] & 0x00ff;
  BleEE[1] =  VarDataTab[BLE_EEPROM_ENABLE] >> 8;  
  
  RS485EE[0] =  VarDataTab[RS485_EEPROM_ENABLE] & 0x00ff;
  RS485EE[1] =  VarDataTab[RS485_EEPROM_ENABLE] >> 8;  
  
  RS485AddEE[0] =  VarDataTab[RS485ID_EEPROM_ENABLE] & 0x00ff;
  RS485AddEE[1] =  VarDataTab[RS485ID_EEPROM_ENABLE] >> 8;    
  
  RfidEE[0] =  VarDataTab[RFID_EEPROM_ENABLE] & 0x00ff;
  RfidEE[1] =  VarDataTab[RFID_EEPROM_ENABLE] >> 8;  
  
  AuthEE[0] =  VarDataTab[AUTHORIZATION_ENABLE] & 0x00ff;
  AuthEE[1] =  VarDataTab[AUTHORIZATION_ENABLE] >> 8;  
  
  FirmwareEE[0] =  VarDataTab[FIRMWARE_VERSION_ADDRESS0] & 0x00ff;
  FirmwareEE[1] =  VarDataTab[FIRMWARE_VERSION_ADDRESS0] >> 8;
  FirmwareEE[2] =  VarDataTab[FIRMWARE_VERSION_ADDRESS1] & 0x00ff;
  FirmwareEE[3] =  VarDataTab[FIRMWARE_VERSION_ADDRESS1] >> 8;
  FirmwareEE[4] =  VarDataTab[FIRMWARE_VERSION_ADDRESS2] & 0x00ff;
  FirmwareEE[5] =  VarDataTab[FIRMWARE_VERSION_ADDRESS2] >> 8;
  
  DateFlashEE[0] =  VarDataTab[DATE_PROGRAM_ADDRESS0] & 0x00ff;
  DateFlashEE[1] =  VarDataTab[DATE_PROGRAM_ADDRESS0] >> 8;
  DateFlashEE[2] =  VarDataTab[DATE_PROGRAM_ADDRESS1] & 0x00ff;
  DateFlashEE[3] =  VarDataTab[DATE_PROGRAM_ADDRESS1] >> 8;
  DateFlashEE[4] =  VarDataTab[DATE_PROGRAM_ADDRESS2] & 0x00ff;
  DateFlashEE[5] =  VarDataTab[DATE_PROGRAM_ADDRESS2] >> 8;
  DateFlashEE[6] =  VarDataTab[DATE_PROGRAM_ADDRESS3] & 0x00ff;
  DateFlashEE[7] =  VarDataTab[DATE_PROGRAM_ADDRESS3] >> 8;
  
  AudiEE[0] =  VarDataTab[AUDI_CHARGING_ADDRESS0] & 0x00ff;
  AudiEE[1] =  VarDataTab[AUDI_CHARGING_ADDRESS0] >> 8; 

}

  
void InitWriteEepromEVSEValues(void){
  int i;  
  uint16_t EEadd;
  char     EEdat[EEPROM_NUM_VAR*2];
  
  if (( FirstEE[1] != '1' ) || ( FirstEE[0] != '0'))
  {
    //if (FirstEE != "01"){
    
    strcpy(EEdat, EEPROM_FIRST_TIME_INIT);
    strncat( EEdat, PASS_EEPROM_INIT, 12 );
    strncat( EEdat, SERIAL_NUM_INIT, 12 );
    strncat( EEdat, LOGIN_ENABLE_INIT, 2 );
    strncat( EEdat, MAX_CURRENT_INIT, 2 );
    strncat( EEdat, CURRENT_INIT, 2 );
    strncat( EEdat, CHARGER_TYPE_INIT, 2 );
    strncat( EEdat, OUTPUT_TYPE_INIT, 2 );
    strncat( EEdat, METER_INIT, 2 );
    strncat( EEdat, BLE_ENABLE_INIT, 2 );
    strncat( EEdat, RS485_ENABLE_INIT, 2 );
    strncat( EEdat, RS485ID_INIT, 2 );    
    strncat( EEdat, RFID_ENABLE_INIT, 2 );    
    strncat( EEdat, AUTHORIZATION_ENABLE_INIT, 2 );
    strncat( EEdat, FIRMWARE_VERSION_INIT, 6 );
    strncat( EEdat, DATE_PROGRAM_INIT, 8 );
	strncat( EEdat, AUDI_CHARGE_INIT, 2 );
#ifdef DEVELOPMENT
    for (int i = 0; i < EEPROM_NUM_VAR*2; i ++)
    {
      eeprom_writebyte(i,EEdat[i]); 
    }
#else 
    WriteEepromEVSEValuesInit(0,EEdat,EEPROM_NUM_VAR*2); 
#endif
  }   
}


void WriteEepromEVSEValues(char WAdd[], char WDat[], size_t WLen){
  uint16_t i,i16 = 0;    
  VarLen = WLen;
  VarAdd = atoi(WAdd); 
  
  for (i=0;i<VarLen;i=i+2){  
    memcpy(&VarValue, &WDat[i], 2);
#ifdef DEVELOPMENT
    eeprom_writebyte(2*VarAdd, (uint8_t)(VarValue&0xFF));
    eeprom_writebyte(2*VarAdd + 1, (uint8_t)((VarValue >> 8)&0xFF));    
#else
    if(EE_WriteVariable(VirtAddVarTab[VarAdd+i16], VarValue) != HAL_OK)
    {
      Error_Handler();
    }      
    if(EE_ReadVariable(VirtAddVarTab[VarAdd+i16], &VarDataTab[VarAdd+i16]) != HAL_OK)
    {
      Error_Handler();
    }    
    if(VarValue != VarDataTab[VarAdd+i16])
    {
      Error_Handler();
    }
#endif
    i16++;
    
  }
}


void WriteEepromEVSEValuesInit(uint16_t WAddInt, char WDat[], size_t WLen){
  uint16_t i,i16 = 0;    
  VarLen = WLen;
  //VarAdd = atoi(WAdd); 
  
  for (i=0;i<VarLen;i=i+2){  
    memcpy(&VarValue, &WDat[i], 2);
    if(EE_WriteVariable(VirtAddVarTab[WAddInt+i16], VarValue) != HAL_OK)
    {
      Error_Handler();
    }      
    if(EE_ReadVariable(VirtAddVarTab[WAddInt+i16], &VarDataTab[WAddInt+i16]) != HAL_OK)
    {
      Error_Handler();
    }    
    if(VarValue != VarDataTab[WAddInt+i16])
    {
      Error_Handler();
    }  
    i16++;
    
  }
}

