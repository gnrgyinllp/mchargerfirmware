#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "stm32f4xx_hal.h"
#include "EVSEtest1.h"
#include "FreeRTOS.h"


#define MAX_INPUT_LENGTH    50
#define MAX_OUTPUT_LENGTH   100

static const int8_t * const pcWelcomeMessage =
  "FreeRTOS command server.\r\nType Help to view a list of registered commands.\r\n";

extern UART_HandleTypeDef huart1;
extern uint8_t LedPWM;

uint8_t         rxbuffer[100];
char            rxbuf[100];
uint8_t         instr_rx_flag, instr_rx_len;
uint8_t         lenrx;

void vprint(const char *fmt, va_list argp)
{
    char string[200];
    if(0 < vsprintf(string,fmt,argp)) // build string
    {
        HAL_UART_Transmit(&huart1, (uint8_t*)string, strlen(string), 0xffffff); // send message via UART
    }
}

void my_printf(const char *fmt, ...) // custom printf() function
{
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}

void Serial_Invalid_Instruction(void){

    my_printf("\r\nInvalid instruction");

} 

void Serial_Console(void){

  uint8_t i;
  char  *s;
  
  if (instr_rx_flag == 1){        
    if (instr_rx_len>0 ){
    
    for (i=0;i<instr_rx_len;i++){
      rxbuf[i] = (char)rxbuffer[i];
    }
      
   // s = rxbuf[0];
            
    if(strcmp(rxbuf,"12")==0){
      my_printf("\r\n 12 received");
    }else if(strcmp(rxbuf,"13")==0){        
      my_printf("\r\n 13 received");
    }else if(strcmp(rxbuf,OPEN_CH1_INS)==0){     
      OpenContactorCH1();
    }else if(strcmp(rxbuf,CLOSE_CH1_INS)==0){     
      CloseContactorCH1();
    }else if(strcmp(rxbuf,OPEN_CH2_INS)==0){     
      OpenContactorCH2();
    }else if(strcmp(rxbuf,CLOSE_CH2_INS)==0){     
      CloseContactorCH2();  
    }else if(strcmp(rxbuf,"duton")==0){ 
      LedPWM = 1;
    }else if(strcmp(rxbuf,"dutoff")==0){ 
      LedPWM = 0;
    }else {
      Serial_Invalid_Instruction();
    }
    
    for (i=0;i<instr_rx_len;i++){
      rxbuf[i] = 0x00;
    }
    
    }     
  
    my_printf("\r\nWallBox-->");

    instr_rx_flag = 0;
    instr_rx_len = 0;
  
  }

}
        
void Serial_Console_Init(void){
  

  my_printf("\r\n GNRGY LTD.       ");
  my_printf("\r\n ------------      ");
  my_printf("\r\n M EVSE code ver_1.0     ");
  my_printf("\r\n"); 
  my_printf("\r\nM Evse-->");

}    
