void checkLedsState()
{
  if ((g_ulSystemTimeMS - g_ulSystemPreviousTimestampBlinkingMS) >= LED_BLINK_PERIOD_MS)
  {
    g_ulSystemPreviousTimestampBlinkingMS = g_ulSystemTimeMS;
    isBlinkLedStateOn = !isBlinkLedStateOn;
    if (state_led_green == LED_ON || 
        (state_led_green == LED_BLINK && isBlinkLedStateOn))
    {
        led_green_ON();
    }
    else if (state_led_green == LED_OFF || 
        (state_led_green == LED_BLINK && !isBlinkLedStateOn))
    {
        led_green_OFF();
    }
    
    if (state_led_blue == LED_ON || 
        (state_led_blue == LED_BLINK && isBlinkLedStateOn))
    {
        led_blue_ON();
    }
    else if (state_led_blue == LED_OFF || 
        (state_led_blue == LED_BLINK && !isBlinkLedStateOn))
    {
        led_blue_OFF();
    }
    
    if (state_led_red == LED_ON || 
        (state_led_red == LED_BLINK && isBlinkLedStateOn))
    {
        led_red_ON();
    }
    else if (state_led_red == LED_OFF || 
        (state_led_red == LED_BLINK && !isBlinkLedStateOn))
    {
        led_red_OFF();
    }
  }
}

void initializeStateMachine()
{
    g_ulSystemPendingStateTimestampMS = g_ulSystemTimeMS;
    g_ulSystemPreviousTimestampBlinkingMS = g_ulSystemTimeMS;
    state_led_green = LED_ON;
    pwm_disable();
    PILOT_12V();
    state_charger = STATESCHARGER_READY;
    state_vehicle = STATESVEHICLE_NONE;
    current_charge = INITIAL_CURRENT_CHARGE;
}
            
void enableDisconnectionOfCable(tBoolean i_Enable)
{
  if (i_Enable)
  {
    UnLock();
  }
  else
  {
    Lock();
  }
}

void enablePWM(tBoolean i_Enable)
{
  pwm_init(); 
  if (i_Enable)
  {

    pwm_enable();
    isPWMEnabled = true;
  }
  else
  {
    pwm_disable();
    isPWMEnabled = false;
  }
}

unsigned long readADCValue()
{
  unsigned long result = 0;
  
  if (isPWMEnabled)
  {
    result = ADC0_read();
  }
  else
  {
    result = ADC0_normal_read();
  }
  
  return result;
}

void readADC2Value(void)
{
  unsigned long result = 0;
  
    result = ADC2_read();

    if (result < ADC_POWER_FAILURE_VALUE){
      contactor_OFF();
    }    
 
}

unsigned long readPilotLevel()
{
  unsigned long result[50];
  unsigned long total=0;
  unsigned int i,s = 0;
  
  for (i=0;i<50;i++){
  
    result[i] = ADC0_read();
    if (result[i]<400){
     total = total + result[i];
     s++;      
    }    
    SysCtlDelay(333);
  }  
  total = total/s;
  return total;
}

unsigned long readCableCurrentCoding()
{
  unsigned long result[10];
  unsigned long total=0;
  unsigned int i = 0;
  
  for (i=0;i<10;i++){  
    result[i] = ADC1_read();
    total = total + result[i];
    SysCtlDelay(333);
  }  
  total = total/10;
  return total;
}

tBoolean isVentilationRequirementsMet()
{
  return false;
}

tBoolean isCharging()
{
  return isCurrentlyCharging;
}

void enableCharging(tBoolean i_Enable)
{
  isCurrentlyCharging = i_Enable;
  if (i_Enable)
  {
    contactor_ON();
  }
  else
  {
    contactor_OFF();
  }
}

tBoolean isOscillatorOn()
{
  return true;
}

unsigned long chargerStateTimePassedMS()
{
  return g_ulSystemTimeMS - g_ulSystemPreviousTimestampMS;
}

void setStateCharger(char i_StateCharger)
{    
    g_ulSystemPreviousTimestampMS = g_ulSystemTimeMS;
    state_charger = i_StateCharger;    
    
#ifdef ZIGBEE
    EVSE_status = state_charger;
    SPIsendCmd(0,SEND_EVSE_STATUS, &EVSE_status ,1);
    //SPIsendCmd(0x0C80, &EVSE_status ,1);
#endif  
  
}

void checkCableStatus()
{
  
#ifdef CURRENTCABLEERADING  
  currentCableInput = readCableCurrentCoding(); 
#else
  currentCableInput = ADC_63A_LEVEL_MAX; 
#endif  
  current_rate = (unsigned short)currentCableInput;
  
    pwm_set_duty_cycle(currentToDutyCycle(current_charge));
  if (currentCableInput >= ADC_13A_LEVEL_MIN && 
      currentCableInput <= ADC_13A_LEVEL_MAX)
  {
    current_rate = 13;
    if (current_charge >= 13ul){
        pwm_set_duty_cycle(currentToDutyCycle(13ul));
    }else{
       pwm_set_duty_cycle(currentToDutyCycle(current_charge));
    }
    
  }
  else if (currentCableInput >= ADC_20A_LEVEL_MIN && 
           currentCableInput <= ADC_20A_LEVEL_MAX)
  {    
    current_rate = 20;
    if (current_charge >= 20ul){
        pwm_set_duty_cycle(currentToDutyCycle(20ul));
    }else{
       pwm_set_duty_cycle(currentToDutyCycle(current_charge));
    }
  }
  else if (currentCableInput >= ADC_32A_LEVEL_MIN && 
           currentCableInput <= ADC_32A_LEVEL_MAX)
  {
    current_rate = 32;
    if (current_charge >= 32ul){
        pwm_set_duty_cycle(currentToDutyCycle(32ul));
    }else{
       pwm_set_duty_cycle(currentToDutyCycle(current_charge));
    } 
  }
  else if (currentCableInput >= ADC_63A_LEVEL_MIN && 
           currentCableInput <= ADC_63A_LEVEL_MAX)
  {
#ifdef THREE_PHASE
        current_rate = 70;
    if (current_charge >= 70ul){
        pwm_set_duty_cycle(currentToDutyCycle(70ul));
    }else{
       pwm_set_duty_cycle(currentToDutyCycle(current_charge));
    } 
#else   
        current_rate = 63;
    if (current_charge >= 63ul){
        pwm_set_duty_cycle(currentToDutyCycle(63ul));
    }else{
       pwm_set_duty_cycle(currentToDutyCycle(current_charge));
    }
#endif
  }
  else if (currentCableInput > ADC_13A_LEVEL_MAX)
  {
    pwm_set_duty_cycle(DUTY_CYCLE_5);
    reportError();
  }
}

void checkVehicleState()
{
  char tempState;
  
  currentVehicleInput = readPilotLevel();  
  if (currentVehicleInput >= ADC_12V_LEVEL_MIN && currentVehicleInput <= ADC_12V_LEVEL_MAX)
  {
    tempState = STATESVEHICLE_NONE;
  }
  else if (currentVehicleInput >= ADC_9V_LEVEL_MIN && currentVehicleInput <= ADC_9V_LEVEL_MAX)
  {
    tempState = STATESVEHICLE_CONNECTED;
  }
  else if (currentVehicleInput >= ADC_6V_LEVEL_MIN && currentVehicleInput <= ADC_6V_LEVEL_MAX)
  {
    tempState = STATESVEHICLE_READYCHARGING;
  }
  else if (currentVehicleInput >= ADC_3V_LEVEL_MIN && currentVehicleInput <= ADC_3V_LEVEL_MAX)
  {
    tempState = STATESVEHICLE_READYCHARGINGVENTILATION;
  }
  else if (currentVehicleInput > ADC_3V_LEVEL_MAX)
  {
    tempState = STATESVEHICLE_ERROR;
  }
  
  if (state_vehicle_pending != tempState)
  {
    g_ulSystemPendingStateTimestampMS = g_ulSystemTimeMS;
  }
  else if ((g_ulSystemTimeMS - g_ulSystemPendingStateTimestampMS) >= 
           DELAY_READ_STABLE_VEHICLE_STATE_MS)
  {
    state_vehicle = state_vehicle_pending;
  }
  
  state_vehicle_pending = tempState;
}

unsigned long currentToDutyCycle(unsigned long i_Current)
{
  unsigned long result;
  
  if (i_Current >= 6ul && i_Current <= 51ul)
  {
    result = i_Current * 10ul / 6ul;  
  }
  else if (i_Current > 51ul && i_Current <= 80ul)
  {
    result = (i_Current * 2ul / 5ul) + 64ul;
  }
#ifdef INVERSED_SIGNAL  
  return 100ul - result;
#else
  return result;
#endif
}
         
void reportError()
{
  state_led_red = LED_ON;
  //enablePWM(true);
  //pwm_set_duty_cycle(DUTY_CYCLE_5);
}

void setInitialState()
{
  state_led_green = LED_ON;
  state_led_blue = LED_OFF;
  state_led_red = LED_OFF;
  
  enableCharging(false);
  enableDisconnectionOfCable(true);
  enablePWM(false);
  PILOT_12V();
  setStateCharger(STATESCHARGER_READY);
  resetSessionObject();
}

void checkMeterPulse()
{
  //if (ReadEnergyPulseState())
  if(ReadLockingState())   
  {
    if (isSecondPulseRead == 0)
    {
      currentSessionObject.ConsumptionWH++;    
      EnergyCounter++;
      isSecondPulseRead = 1;
    }
  }
  else
  {
    isSecondPulseRead = 0;
  }
}




void resetSessionObject()
{
  currentSessionObject.isAuthorized = false;
  currentSessionObject.ConsumptionWH = 0;
  currentSessionObject.Power = 0;
  currentSessionObject.priceUnit = 0.0;//TBD
}
void initializeSessionObject()
{
    //TBD *** SET SESSION INFORMATION ***
    currentSessionObject.meterStart = EnergyCounter;
    currentSessionObject.ConsumptionWH = 0;
    currentSessionObject.Power = 0;
    //currentSessionObject.userId = 0;//TBD
    currentSessionObject.userId = LastUserIDreading;
    currentSessionObject.vehicleId = 0;//TBD
    currentSessionObject.currency = Nis;//TBD
    currentSessionObject.priceUnit = 0.5;//TBD
    currentSessionObject.paymentMethod = Credit;//TBD
    ulocaltime(g_LocalTimeSec, &(currentSessionObject.timeStart));//TBD check
    //we are going to send the realtime packet to start the session,
    //in the future, we need to use a startsession command to ask for autoritzation
    //SendRealTimePacket();
    SendInitSessionPacket();
    
    
}

void EndSessionObject()
{
    //TBD *** SET SESSION INFORMATION ***
    currentSessionObject.meterEnd = EnergyCounter;
    //currentSessionObject.ConsumptionWH = (currentSessionObject.meterEnd - currentSessionObject.meterStart)*WH_PER_PULSE_METER;
    ulocaltime(g_LocalTimeSec, &(currentSessionObject.timeEnd));//TBD check
    //we are going to send the realtime packet to end the session,
    //in the future, we need to use a startsession command to ask for autoritzation
    SendEndSessionPacket();
   // SendRealTimePacket();
}


void setError(char i_Error)
{
  error_status = error_status | i_Error;
  state_led_red = LED_ON;
}

void clearError(char i_Error)
{
  error_status = error_status ^ i_Error;
  state_led_red = LED_OFF;
}


void checkStateMachine()
{
  
  //checkMeterPulse();
  checkLedsState();
  checkVehicleState();
#ifdef POWER_FAILURE  
  //readADC2Value();
#endif  
  if (state_vehicle == STATESVEHICLE_ERROR)
  {
      setError(ERROR_IN_VEHICLE);
  }
  else if (state_vehicle == STATESVEHICLE_EVSENOTAVAILABLE)
  {
      setError(ERROR_EVSE_NOT_AVAILABLE);
  }
  else
  {
    clearError(ERROR_IN_VEHICLE | ERROR_EVSE_NOT_AVAILABLE);
    switch (state_charger)
    {
      case STATESCHARGER_READY:
        
 #ifdef MELT          
      /*  if (ReadEnergyPulseState()){
          while (ReadEnergyPulseState()){
            led_red_ON(); 
            led_green_OFF();
            
            PILOT_minus_12V();
          } 
          
          
         }*/
        
     if (ReadEnergyPulseState()){
          while (ReadEnergyPulseState()){
            led_red_ON(); 
            led_green_OFF();
            PILOT_minus_12V();
            setError(ERROR_MELT_CONTACTOR);
          }
            led_red_OFF(); 
            led_green_ON();
            PILOT_12V(); 
            clearError(ERROR_MELT_CONTACTOR);
        }   
        
#endif      
     
     if (IdAllowed == 1){
         if (state_vehicle == STATESVEHICLE_CONNECTED)
         {          
            state_led_red = LED_OFF;
            state_led_green = LED_BLINK;
            setStateCharger(STATESCHARGER_DELAY);        
            
          }
#ifdef MITSUBISHI
          else if (state_vehicle == STATESVEHICLE_READYCHARGING ||
                 state_vehicle == STATESVEHICLE_READYCHARGINGVENTILATION)
          {
            state_led_red = LED_OFF;
            state_led_green = LED_BLINK;
            setStateCharger(STATESCHARGER_DELAY);   
          }
#endif    
     }
     
        break;
        
      case STATESCHARGER_DELAY:
        
        
        
        
        if (chargerStateTimePassedMS() >= DELAY_STAGE_MS)
        {      
          if (state_vehicle == STATESVEHICLE_CONNECTED)
          {
            setStateCharger(STATESCHARGER_AUTHORIZATION); 
            initializeSessionObject();
          }
#ifdef MITSUBISHI
          else if (state_vehicle == STATESVEHICLE_READYCHARGING ||
                   state_vehicle == STATESVEHICLE_READYCHARGINGVENTILATION)
          {
              setStateCharger(STATESCHARGER_AUTHORIZATION);
              initializeSessionObject();
          }
#endif
          else // unexpected bavior
          {
            setInitialState();
            setError(ERROR_UNKNOWN);
          }
        }
        else if (state_vehicle != STATESVEHICLE_CONNECTED &&
                 state_vehicle != STATESVEHICLE_READYCHARGING &&
                 state_vehicle != STATESVEHICLE_READYCHARGINGVENTILATION)
        {
          setInitialState();  
        }
        break;
        
      case STATESCHARGER_AUTHORIZATION:
        //show payment method menu
        //get user input for PIN if required
        //get vehicle selection is available
        //send authorization request to server
        
        //if (currentSessionObject.isAuthorized)
        
        {
          setStateCharger(STATESCHARGER_AUTHORIZED);  
        }
        
        break;
      case STATESCHARGER_AUTHORIZED:
          //initializeSessionObject();
          //display unit pricing
          enablePWM(true);
          checkCableStatus();
          setStateCharger(STATESCHARGER_VEHICLECONNECTED);  
        break;
      case STATESCHARGER_VEHICLECONNECTED:
        if (state_vehicle == STATESVEHICLE_NONE)
        {
          setInitialState();
        }
        else if (state_vehicle == STATESVEHICLE_READYCHARGING)
        {
          state_led_blue = LED_BLINK;
          enableDisconnectionOfCable(false);
          setStateCharger(STATESCHARGER_PRECHARGING);
        }
        else if (state_vehicle == STATESVEHICLE_READYCHARGINGVENTILATION)
        {
            state_led_blue = LED_BLINK;
            setStateCharger(STATESCHARGER_DELAY_VENTILATION); 
        }
        break;
        
      case STATESCHARGER_DELAY_VENTILATION:
        if (chargerStateTimePassedMS() >= DELAY_TIMER_VENTILATION)  // T-ventilation
        {
          if (isVentilationRequirementsMet())
          {
            enableDisconnectionOfCable(false);
            setStateCharger(STATESCHARGER_PRECHARGING);  
          }
          else
          {
            setError(ERROR_VENTILATION_REQUIREMENT);        
          }
        }
        else if (state_vehicle != STATESVEHICLE_READYCHARGINGVENTILATION)
        {
          setInitialState();
        }
      break;
      
      case STATESCHARGER_PRECHARGING: 
       
        
        if (chargerStateTimePassedMS() >= DELAY_TIMER_ACON) // T-ACon
        {
          if (state_vehicle == STATESVEHICLE_READYCHARGING || 
            state_vehicle == STATESVEHICLE_READYCHARGINGVENTILATION)
          {
            state_led_green = LED_OFF;
            state_led_blue = LED_ON;
            //currentSessionObject.meterStart = 0;//TBD
            enableCharging(true);            
            Lock();
            leds_aux_all_ON();
            setStateCharger(STATESCHARGER_CHARGING); 
          }
          else
          {
            setInitialState();
            setError(ERROR_TIMER_CONTACTOR_ON);
          }
        }
        
        break;
        
      case STATESCHARGER_CHARGING:
        
#ifdef POWER_FAILURE     
        if (chargerStateTimePassedMS() >= DELAY_POWER_FAILURE)
        {
          if (!ReadEnergyPulseState()){ 
              enableCharging(false); 
              UnLock();
              
              setError(ERROR_POWERFAILURE);
          }
        }
#endif        
        //Blinking_Leds();
        Leds_charging_effect();
        
        if (state_vehicle == STATESVEHICLE_CONNECTED ||
            state_vehicle == STATESVEHICLE_NONE)        
        {
          state_led_red = LED_OFF;
          setStateCharger(STATESCHARGER_POSTCHARGING);
          IdAllowed = 0;
          leds_aux_all_OFF();
        }
        else if (state_vehicle != STATESVEHICLE_READYCHARGING && 
            state_vehicle != STATESVEHICLE_READYCHARGINGVENTILATION)
        {
          leds_aux_all_OFF();
          setStateCharger(STATESCHARGER_POSTCHARGING);
          IdAllowed = 0;
          setError(ERROR_WHILE_CHARGING);
        }
        
        break;
        
      case STATESCHARGER_POSTCHARGING:        
        if (chargerStateTimePassedMS() >= DELAY_TIMER_COFF) // T-ACoff1
        {
          state_led_blue = LED_OFF;
          enableCharging(false);
          UnLock();
          //currentSessionObject.meterStop = 1;//TBD
          //enablePWM(false);
          EndSessionObject();
          enableDisconnectionOfCable(true);          
          setStateCharger(STATESCHARGER_ENDCHARGINGSESSION);
        }
        
        break;
        
      case STATESCHARGER_ENDCHARGINGSESSION:        
        //if (chargerStateTimePassedMS() >= 3000ul) // T-OscillatorOff
#ifdef MELT          
        if (ReadEnergyPulseState()){
          while (ReadEnergyPulseState()){
            led_red_ON(); 
            led_green_OFF();
            setError(ERROR_MELT_CONTACTOR);
            PILOT_minus_12V();
          } 
          clearError(ERROR_MELT_CONTACTOR);          
        }
#endif  
        
        
        //TBD end session
        ulocaltime(g_LocalTimeSec, &(currentSessionObject.timeEnd));//TBD
        //send session information
        //recieve and display session outcome results
        //if (state_vehicle == STATESVEHICLE_CONNECTED)
        {
         // setStateCharger(STATESCHARGER_VEHICLECONNECTED); 
        }
        //else
        {            
          setInitialState();
        }        
        break;
    }
  }
}

//*****************************************************************************
//
//! Handles the main application loop.
//!
//! This function initializes and configures the device and the software, then
//! performs the main loop.
//!
//! \return Never returns.
//