
//////   RFID.H ////////////////////////////

/* RFID includes. */

#include "EVSEtest1.h"
#include "StateMachine.h"
#include "stm32f4xx_hal.h"

/* Standard includes. */
#include "string.h"
#include "stdio.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"



#define MAX_RX_RFID_DATA   10
#define TYPE_RFID_PACKET_SIZE   (MAX_RX_RFID_DATA + 2)


typedef struct RFIDPACKET
{
  char Command;      

  uint8_t Length;     

  char   Data[MAX_RX_RFID_DATA];    

}RxDataPacketTypeDef;




void InitRFIDModule(void);
void RFID_TXOperation(void);
uint8_t gCR95HF_PORsequence( void );
void gCR95HF_Send_IRQIN_NegativePulse(void);
uint8_t gCR95HF_Echo(uint8_t *pResponse);
uint8_t gRFIDSendReceive(uint8_t *pCommand, uint8_t *pResponse);
static void gCR95HF_Send_UART_Command(uint8_t *pData);
static void gCR95HF_Receive_UART_Response(uint8_t *pData);
uint8_t gCR95HF_IDN(uint8_t *pResponse);
void gPCD_FieldOff( void );
int8_t gPCD_ProtocolSelect(const uint8_t Length,const uint8_t Protocol,const uint8_t *Parameters,uint8_t *pResponse);
static uint8_t gIsAnAvailableProtocol (uint8_t Protocol);
int8_t gISO15693_GetUID (uint8_t *UIDout);
int8_t gISO15693_Init	( void );
int8_t gPCD_WriteRegister(const uint8_t Length,const uint8_t Address,const uint8_t Flags,const uint8_t *pData,uint8_t *pResponse);
static int8_t gISO15693_Inventory(const uint8_t Flags , const uint8_t AFI, const uint8_t MaskLength, const uint8_t *MaskValue, uint8_t *pResponse);
int8_t gPCD_SendRecv(const uint8_t Length,const uint8_t *Parameters,uint8_t *pResponse);
static int8_t gISO15693_CreateRequestFlag(const uint8_t SubCarrierFlag,const uint8_t DataRateFlag,const uint8_t InventoryFlag,const uint8_t ProtExtFlag,const uint8_t SelectOrAFIFlag,const uint8_t AddrOrNbSlotFlag,const uint8_t OptionFlag,const uint8_t RFUFlag);
static int8_t gISO15693_IsInventoryFlag(const uint8_t FlagsByte);
static int8_t gISO15693_GetSelectOrAFIFlag(const uint8_t FlagsByte);
int8_t gPCD_IsReaderResultCodeOk(uint8_t CmdCode, const uint8_t *ReaderReply);
