/**
  ******************************************************************************
  * @file    Meter7858A.h
  * @author  Josep Pinto
  * @version V1.0.0
  * @date    4-Jan-2018
  * @brief   This files contains all functions declarations and definitions 
  *          for the EV charging functionality test
  ******************************************************************************
  * 
  *
  ******************************************************************************
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __Meter7858A_H
#define __Meter7858A_H
  /* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "Meter7858A.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "main.h"
#include "eeprom.h"

    
#define Meter_ADDRESS   0x70    
#define METER_CS_Port  GPIOA
#define METER_CS_Pin   GPIO_PIN_4
#define RST_METER_Pin  GPIO_PIN_1 
#define TOGLE_INIT_DELAY 1
#define SPI_TIMEOUT      50       

    
// ------------------------------
//   ADE7858A Registers ADDRESSES
#define  RUN_METER_REG          0xE228    

#define  CONFIG_METER_REG       0xE618    
#define  CONFIG2_METER_REG      0xEC01
    
#define  RAMPROTEC1_METER_REG   0xE7FE
#define  RAMPROTEC2_METER_REG   0xE7E3    
    
#define  AIGAIN_METER_REG       0x4380
#define  AVGAIN_METER_REG       0x4381    
#define  BIGAIN_METER_REG       0x4382
#define  BVGAIN_METER_REG       0x4383
#define  CIGAIN_METER_REG       0x4384
#define  CVGAIN_METER_REG       0x4385
    
#define  AIRMS_METER_REG        0x43C0 
#define  AVRMS_METER_REG        0x43C1 
#define  BIRMS_METER_REG        0x43C2 
#define  BVRMS_METER_REG        0x43C3 
#define  CIRMS_METER_REG        0x43C4 
#define  CVRMS_METER_REG        0x43C5     
    
   
#define  VAWV_METER_REG         0xE510 
#define  VBWV_METER_REG         0xE511 
#define  VCWV_METER_REG         0xE512

#define  PERIOD_LINE_NETWORK_REG 0xE607
    

    
    
    
/* USER CODE BEGIN Includes */
void MeterInit(void); 
void MeterReading(void);
void Meter_Reset(void);
void Read_Config2_Meter(void);
void Write_Config2_Meter(void);
void ADE7858A_SPI_SendData(uint8_t* adress, uint8_t* data, uint16_t size, uint32_t timeout);
void Write_ADE7858A_SPI(uint16_t address, uint8_t* data, uint16_t size, uint32_t timeout);
void Read_ADE7858A_SPI(uint16_t address, uint8_t* data, uint16_t size, uint32_t timeout);
uint32_t ReadValue_ADE7858A_32u(uint16_t adress);
uint16_t ReadValue_ADE7858A_16u(uint16_t address);
uint32_t WriteValue_ADE7858A_32u(uint16_t address, uint32_t data);


#endif /* __EVSEtest1_H */